sap.ui.define([
    "sap/ui/core/mvc/Controller",
    "sap/ui/core/routing/History"
], function (Controller, History) {
    "use strict";
    return Controller.extend("sap.ui.demo.nav.controller.BaseController", {
        onInit: function () {
            var oRouter = this.getRouter();
            oRouter.attachBypassed(function (oEvent) {
                var sHash = oEvent.getParameter("hash");
                // do something here, i.e. send logging data to the back end for analysis
                // telling what resource the user tried to access...
                // ne raboti vaka ??
                jQuery.sap.log.info("Sorry, but the hash '" + sHash + "' is invalid.", "The resource was not found.");
            });
            oRouter.attachRouteMatched(function (oEvent) {
                var sRouteName = oEvent.getParameter("name");
                // do something, i.e. send usage statistics to back end
                // in order to improve our app and the user experience (Build-Measure-Learn cycle)
                // ne raboti vaka ??
                jQuery.sap.log.info("User accessed route " + sRouteName + ", timestamp = " + new Date().getTime());
            });
        },
        getRouter: function () {
            return sap.ui.core.UIComponent.getRouterFor(this);
        },
        onNavBack: function (oEvent) {
            var oHistory, sPreviousHash;
            oHistory = History.getInstance();
            sPreviousHash = oHistory.getPreviousHash();
            if (sPreviousHash !== undefined) {
                window.history.go(-1);
            } else {
                this.getRouter().navTo("appHome", {}, true /*no history*/);
            }
        }
    });
});