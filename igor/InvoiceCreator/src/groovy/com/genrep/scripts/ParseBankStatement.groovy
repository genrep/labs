package com.genrep.scripts

import com.genrep.statements.BankStatement
import com.genrep.statements.BankStatementItem

import java.text.DecimalFormat

/**
 * Created by igor on 9/17/14.
 */
def dataList = []
def theInfoName = '/home/igor/Documents/GNRP/izvod.txt'

File theInfoFile = new File( theInfoName )

if( !theInfoFile.exists() ) {
    println "File does not exist"
} else {
    def driveInfo = [:]
    BankStatement izvod = new BankStatement()
    DecimalFormat decimalFormat = new DecimalFormat()
    decimalFormat.setParseBigDecimal(true);
    // Step through each line in the file
    theInfoFile.eachLine { line, index->
        // If the line isn't blank
        println index
        if( line.trim()){
            switch (index){
                case 1:
                    izvod.bankStatementId = line
                    break
                case 2:
                    izvod.issueDate = new Date().parse('dd.MM.yyyy',line)
                    break
                case 3:
                    izvod.bankAccountNumber = line
                    break
                case 4:
                    izvod.balanceBefore = (BigDecimal)decimalFormat.parse(line)
                    break
                case 5:
                    izvod.balanceAfter = (BigDecimal)decimalFormat.parse(line)
                    break
                default:
                    BankStatementItem stavka = new BankStatementItem()
//                println "70-18-19-6-74-24-24"
                    stavka.publicEntity = line[0..69].toString().trim()
                    stavka.publicEntityBankAccountNumber = line[70..87].toString().trim()
                    stavka.transferSum = (BigDecimal)decimalFormat.parse(line[88..106].toString().trim())
                    stavka.transactionCode = Integer.parseInt(line[107..112].toString().trim())
                    stavka.shortDescription = line[113..184].toString().trim()
                    stavka.borrowingCode = line[185..208].toString().trim()
                    stavka.approvalCode = line[209..232].toString().trim()
                    println stavka.properties.each {
                        println it
                    }

            }
//        if( line.trim() ) {
//            // Split into a key and value
//            def (key,value) = line.split( '\t: ' ).collect { it.trim() }
//            // and store them in the driveInfo Map
//            driveInfo."$key" = value
//        }
//        else {
//            // If the line is blank, and we have some info
//            if( driveInfo ) {
//                // store it in the list
//                dataList << driveInfo
//                // and clear it
//                driveInfo = [:]
//            }
        }
    }
    println izvod.properties
}
//    // when we've finished the file, store any remaining data
//    if( driveInfo ) {
//        dataList << driveInfo
//    }
//}
//
//dataList.eachWithIndex { it, index ->
//    println "Drive $index"
//    it.each { k, v ->
//        println "\t$k = $v"
//    }
//}