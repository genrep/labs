package com.genrep.scripts

import com.genrep.account.BankAccount
import com.genrep.account.PersonalBankAccount

/**
 * Created by igor on 10/7/14.
 */
//BankAccount ba = new BankAccount()
//ba.balance = 20000
//ba.bankAccountNumber = '123456'
//ba.bankName = "NLB"
//ba.id = 0
//ba.version = 0
//ba.publicEntity = "GNRP"
//
//PersonalBankAccount pbaO = new PersonalBankAccount()
//pbaO.id = 0
//pbaO.version = 0
//pbaO.bankName = "KOMERCIJALNA"
//pbaO.bankAccountNumber = '654321'
//pbaO.firstName = 'Igor'
//pbaO.lastName = 'Ivanovski'
//
//BankAccount baO = (BankAccount) pbaO
//println baO.properties
//println baO.class
//baO.save()
//PersonalBankAccount pba = (PersonalBankAccount) ba

List.metaClass.each = {closure->
    println owner
    println this
    println delegate
    for(value in delegate){
        if(value.equals("KUR")){
            closure(value)
        }
    }
}

List.getMetaClass(eachW = {closure,index->
        println owner
        println this
        println delegate
        int i = 0
        for(value in delegate){
            if(value.equals("KUR")){
                closure(value,i)
            }
            i++
        }

})

def list = ['PICKA','KUR','JAJCA']
list.each{
    println "PUSI "+it
    println it
}

list.eachW{
    println it
}