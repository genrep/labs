package com.genrep.scripts

/**
 * Created by igor on 9/30/14.
 */

import com.genrep.statements.BankStatement
import com.genrep.statements.BankStatementItem
import org.apache.commons.collections.MultiHashMap
import org.apache.commons.collections.MultiMap
import org.apache.commons.collections.keyvalue.MultiKey
import org.apache.commons.collections.map.MultiKeyMap

import java.text.DecimalFormat


def theInfoName = '/home/igor/Documents/GNRP/MT940.300'

File theInfoFile = new File( theInfoName )

if( !theInfoFile.exists() ) {
    println "File does not exist"
} else {

    BankStatement izvod = new BankStatement()
    DecimalFormat decimalFormat = new DecimalFormat()
    decimalFormat.setParseBigDecimal(true);
    // Step through each line in the file
    def fileMap = [:]

    theInfoFile.eachLine { line, index ->
        println index+" : "+line
        if(line.startsWith(":")){
            def cl = line.tokenize(":").collect()
            if(!fileMap.containsKey(cl[0])){
                fileMap.put(cl[0],cl[1])
            }else{
                def item = fileMap.get(cl[0])
                if(item instanceof List){
                    item.add(cl[1])
                    fileMap.put(cl[0],item)
                }else{
                    def list = []
                    list << item
                    list << cl[1]
                    fileMap.put(cl[0],list)
                }

            }
        }

    }

    if(fileMap){
        fileMap.each { k, v ->
            switch (k){
                case '25':
                    izvod.bankAccountNumber = v
                    println izvod.bankAccountNumber
                    break
                case '28C':
                    izvod.bankStatementId = v
                    println izvod.bankStatementId
                    break
                case '60F':
                    def l = v.length()
                    izvod.balanceBefore = (BigDecimal)decimalFormat.parse(v[10..l-1].toString())
                    println izvod.balanceBefore
                    def date = v[1..7].toString()
                    izvod.issueDate = Date.parse("yyMMdd",date)
                    println izvod.issueDate
                    break
                case '61':
                    println v
                    v = v[0]
                    println v
                    BankStatementItem stavka = new BankStatementItem()
                    int factor = 1
                    int start = 0
                    StringBuilder sum = new StringBuilder()
                    String code = null
                    String ref = null

                    for(int i = 0; i < v.length(); i++)
                    {
                        char c = v.charAt(i)
                        if(c == 'C'){
                            start = i
                            break
                        }
                        if(c == 'D'){
                            factor=-1
                            start = i
                            break
                        }
                    }
                    for(int i = start ; i < v.length(); i++){
                        char c = v.charAt(i)
                        if(c.isDigit(c)){
                            while(v.charAt(i).isDigit(v.charAt(i)) || v.charAt(i) == ','){
                                sum.append(v.charAt(i))
                                i++
                            }
                            start = i
                            break
                        }
                    }
                    println sum
                    code = v[start..start+3].toString()
                    ref = v[start+4..v.length()-1].toString()
                    println code
                    println ref



                    break
//                case 5:
//                    izvod.balanceAfter = (BigDecimal)decimalFormat.parse(line)
//                    break
//                default:
//                    BankStatementItem stavka = new BankStatementItem()
////                println "70-18-19-6-74-24-24"
//                    stavka.publicEntity = line[0..69].toString().trim()
//                    stavka.publicEntityBankAccountNumber = line[70..87].toString().trim()
//                    stavka.transferSum = (BigDecimal)decimalFormat.parse(line[88..106].toString().trim())
//                    stavka.transactionCode = Integer.parseInt(line[107..112].toString().trim())
//                    stavka.shortDescription = line[113..184].toString().trim()
//                    stavka.borrowingCode = line[185..208].toString().trim()
//                    stavka.approvalCode = line[209..232].toString().trim()
//                    println stavka.properties.each {
//                        println it
//                    }

            }
        }
    }
//        if( line.trim()){
//            switch (index){
//                case 1:
//                    izvod.bankStatementId = line
//                    break
//                case 2:
//                    izvod.issueDate = new Date().parse('dd.MM.yyyy',line)
//                    break
//                case 3:
//                    izvod.bankAccountNumber = line
//                    break
//                case 4:
//                    izvod.balanceBefore = (BigDecimal)decimalFormat.parse(line)
//                    break
//                case 5:
//                    izvod.balanceAfter = (BigDecimal)decimalFormat.parse(line)
//                    break
//                default:
//                    BankStatementItem stavka = new BankStatementItem()
////                println "70-18-19-6-74-24-24"
//                    stavka.publicEntity = line[0..69].toString().trim()
//                    stavka.publicEntityBankAccountNumber = line[70..87].toString().trim()
//                    stavka.transferSum = (BigDecimal)decimalFormat.parse(line[88..106].toString().trim())
//                    stavka.transactionCode = Integer.parseInt(line[107..112].toString().trim())
//                    stavka.shortDescription = line[113..184].toString().trim()
//                    stavka.borrowingCode = line[185..208].toString().trim()
//                    stavka.approvalCode = line[209..232].toString().trim()
//                    println stavka.properties.each {
//                        println it
//                    }
//
//            }
//        if( line.trim() ) {
//            // Split into a key and value
//            def (key,value) = line.split( '\t: ' ).collect { it.trim() }
//            // and store them in the driveInfo Map
//            driveInfo."$key" = value
//        }
//        else {
//            // If the line is blank, and we have some info
//            if( driveInfo ) {
//                // store it in the list
//                dataList << driveInfo
//                // and clear it
//                driveInfo = [:]
//            }
}
//    }
//    println izvod.properties
//}
//    // when we've finished the file, store any remaining data
//    if( driveInfo ) {
//        dataList << driveInfo
//    }
//}
//
//dataList.eachWithIndex { it, index ->
//    println "Drive $index"
//    it.each { k, v ->
//        println "\t$k = $v"
//    }
//}