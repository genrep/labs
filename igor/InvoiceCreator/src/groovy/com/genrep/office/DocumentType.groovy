package com.genrep.office

/**
 * Created by igor on 17.4.15.
 */
class DocumentType {
    Long id
    String name
    String workspace
    String morphClass
}
