package com.genrep.filterPane

import org.apache.commons.logging.Log
import org.apache.commons.logging.LogFactory
import org.codehaus.groovy.grails.commons.GrailsDomainClass

/**
 * Created by igor on 7/15/14.
 */
class FilterPaneUtils {

    private static final Log log = LogFactory.getLog(this)

    static resolveDomainClass(grailsApplication, bean) {
        String beanName
        def result

        log.debug("resolveDomainClass: bean is ${bean?.class}")
        if(bean instanceof GrailsDomainClass) {
            return bean
        }

        if(bean instanceof Class) {
            beanName = bean.name
        } else if(bean instanceof String) {
            beanName = bean
        }

        if(beanName) {
            result = grailsApplication.getDomainClass(beanName)
            if(!result) {
                result = grailsApplication.domainClasses.find { it.clazz.simpleName == beanName }
            }
        }
        result
    }
}
