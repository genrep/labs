package com.genrep.auth

import com.genrep.authentication.User
import com.genrepsoft.toolkit.acl.IRole
import com.genrepsoft.toolkit.acl.IUserProfile
import grails.plugin.springsecurity.userdetails.GrailsUserDetailsService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.dao.DataAccessException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import com.genrepsoft.toolkit.authentication.RegistrationClient

/**
 * Created by igor on 8/27/14.
 */
class GenrepUserDetailsService implements GrailsUserDetailsService {

    private Logger _log = LoggerFactory.getLogger(getClass())

    protected Logger getLog() { _log }

    @Override
    UserDetails loadUserByUsername(String username, boolean loadRoles) throws UsernameNotFoundException, DataAccessException {

       User genrepUser = null

            try{

                RegistrationClient rc = new RegistrationClient()
                genrepUser = (User) rc.getUser(username,username)
            }
            catch (Exception e){
                log.error(e.fillInStackTrace())
                log.error("Registration service is not available. Please check configuration.")
            }


            if (genrepUser != null) {


                try {
                    Collection<GrantedAuthority> auths = genrepUser.getAuthorities()
                    IUserProfile<IRole> profile = genrepUser.getUserProfile()
                    log.debug("User from registration login in.")
                    log.debug("User $genrepUser.fullName incoming.")
                    log.debug("User $genrepUser.username has the following roles: $profile.profileRoles")
                    createUserDetails genrepUser, auths, profile
                }

                catch (Exception ex) {
                    log.debug("Can't load authorities in GenrepUserDetailsService!!!")
                    log.error(ex.fillInStackTrace())
                }
            }
            else {
                log.warn "User not found: $username"
                throw new UsernameNotFoundException('User not found', username)
            }
        }


    @Override
    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        loadUserByUsername username, true
    }

    protected UserDetails createUserDetails(user, Collection<GrantedAuthority> auths,IUserProfile<IRole> profile ) {

        String firstname = user.firstname
        String lastname = user.lastname
        String username = user.username
        String password = user.password
        Boolean EULAAccepted = user.EULAAccepted
        IUserProfile<IRole> userProfile = profile
        GrantedAuthority[] authorities = auths
        Boolean accountNonExpired = user.accountNonExpired
        Boolean accountNonLocked = user.accountNonLocked
        Boolean credentialsNonExpired = user.credentialsNonExpired
        Boolean enabled = user.enabled

        try {
//         def us = new GenrepJUser()
         def authUser =  new GenrepUser(firstname, lastname, username,
                    password, EULAAccepted,
                    userProfile, authorities,
                    accountNonExpired, accountNonLocked,
                    credentialsNonExpired, enabled)
         println authUser.properties
         return authUser

        }
        catch(Exception e){
            log.error(e.fillInStackTrace())
            return null
        }
    }

}
