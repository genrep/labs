package com.genrep.auth

import com.genrep.authentication.User
import com.genrepsoft.toolkit.acl.IRole
import com.genrepsoft.toolkit.acl.IUserProfile
import org.springframework.security.core.GrantedAuthority

/**
 * Created by igor on 8/27/14.
 */

// Nasleduva od com.genrep.authentication.User
public class GenrepUser extends User {



    public GenrepUser(){
        super()
    }
    public GenrepUser(String firstname, String lastname, String username,
                      String password, Boolean EULAAccepted,
                      IUserProfile<IRole> userProfile, GrantedAuthority[] authorities,
                      Boolean accountNonExpired, Boolean accountNonLocked,
                      Boolean credentialsNonExpired, Boolean enabled) {
        super(firstname,lastname,username, password, EULAAccepted, userProfile, authorities,
                accountNonExpired, accountNonLocked, credentialsNonExpired, enabled)
    }

}

