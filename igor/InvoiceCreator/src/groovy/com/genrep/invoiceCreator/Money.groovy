package com.genrep.invoiceCreator

import mk.nbrm.klservice.Kurs
import mk.nbrm.klservice.KursSoap
import org.apache.commons.logging.LogFactory

import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat


/**
 * Created by igor on 6/10/14.
 */
class Money{


    private static final log = LogFactory.getLog(this)
    static def messageSource = SpringUtil.getBean('messageSource')

    BigDecimal amount = 0
    Currency currency = Currency.getInstance('MKD')

    private static char decimalSeparator = ','
    private static String inPattern = messageSource.getMessage('decimal.pattern.in',null,new Locale('en','US'))
    private static String outPattern = messageSource.getMessage('decimal.pattern.out',null,new Locale('en','US'))

    static constraints = {
        amount(scale:2)
    }

    static Money getInstance(String value, String currency) {
        BigDecimal newAmount = formatStringToBigDecimal(value)
        def newCurrency = Currency.getInstance(currency)
        Money money = new Money(amount:newAmount, currency:newCurrency)
        return money
    }

    static Money getInstance(BigDecimal value, Currency currency) {
        Money money = new Money(amount:value, currency:currency)
        return money
    }

    int hashCode() {
        return amount.hashCode() + currency.hashCode()
    }
    String toString() {
        String formatted = formatBigDecimalToString(amount)
        return "${formatted} ${currency?.currencyCode}"
    }

    boolean equals(Object other) {
        if (!other)                     return false;
        if (!(other instanceof Money))  return false;
        if (currency != other.currency) return false;
        if (amount != other.amount)     return false;
        return true;
    }



    static String formatBigDecimalToString(BigDecimal amount) {
        // Format the output
        Locale loc = new Locale('mk', 'MK')
        NumberFormat nf = NumberFormat.getNumberInstance(loc);
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern(outPattern);
        return df.format(amount);
    }


    static BigDecimal formatStringToBigDecimal(String str){

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(decimalSeparator)

        DecimalFormat decimalFormat = new DecimalFormat(inPattern, symbols);
        decimalFormat.setParseBigDecimal(true);

        try {
            def result = (BigDecimal) decimalFormat.parse(str)
            return result
        }catch (Exception e){
            log.error e.message,e.fillInStackTrace()
            return null
        }
    }

    static BigDecimal formatStringToBigDecimalNbrm(String str){

        char decimalSeparator = '.'
        String inPattern = '###0.0000'
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(decimalSeparator)

        DecimalFormat decimalFormat = new DecimalFormat(inPattern, symbols);
        decimalFormat.setParseBigDecimal(true);

        try {
            def result = (BigDecimal) decimalFormat.parse(str)
            return result
        }catch (Exception e){
            log.error e.message,e.fillInStackTrace()
            return null
        }
    }

    Money convertTo(Currency newCurrency, Date toDate = null) {
        if (newCurrency == currency) return this;
        if (!toDate) toDate = new Date().clearTime()
        def multiplier = 0
        def rate = ExchangeRate.findByBaseCurrencyAndToCurrencyAndDate(currency,newCurrency,toDate)
        if(rate==null){
            rate = updateExchangeRates(currency,newCurrency,toDate)
        }

        if(rate==null){
            def c = ExchangeRate.createCriteria()
            rate = c.get {
                and {
                    eq('baseCurrency', this.currency)
                    eq('toCurrency', newCurrency)
                }
                le('date', toDate)
                order('date', 'desc')
                maxResults(1)
            }
        }

        if (!rate) {
            throw new IllegalArgumentException("No exchange rate found")
        }

        multiplier = rate.rate

        return new Money(amount:amount * multiplier, currency:newCurrency)
    }

    Money convertTo(Currency newCurrency, Date toDate = null, ExchangeRate rate) {
        if (newCurrency == currency) return this;
        if (!toDate) toDate = new Date().clearTime()
        def multiplier = 0

        if(rate==null){
            def c = ExchangeRate.createCriteria()
            rate = c.get {
                and {
                    eq('baseCurrency', this.currency)
                    eq('toCurrency', newCurrency)
                }
                le('date', toDate)
                order('date', 'desc')
                maxResults(1)
            }
        }

        if (!rate) {
            throw new IllegalArgumentException("No exchange rate found")
        }

        multiplier = rate.rate

        return new Money(amount:amount * multiplier, currency:newCurrency)
    }

    static ExchangeRate updateExchangeRates(Currency currency, Currency newCurrency, Date toDate) {
        // Implement exchange rate caching as a service
        String currencyCode = newCurrency.currencyCode

        if(currencyCode.equals('MKD')) {
            currencyCode = currency.currencyCode
        }

        Kurs kurs = new Kurs()
        KursSoap kursSoapClient = kurs.getKursSoap()
        ExchangeRate exchangeRate

        try {

            def rates = kursSoapClient.getExchangeRate(toDate.format('dd.MM.yyyy'), toDate.format('dd.MM.yyyy'))
            def records = new XmlSlurper().parseText(rates)
            def allCountries = records.KursZbir
            def valuta = allCountries.findAll { it.Oznaka =~ currencyCode }
            def nominalRate = valuta.collect { it.Sreden }[0].toString()
            nominalRate = formatStringToBigDecimalNbrm(nominalRate)

            if (!newCurrency.currencyCode.equals('MKD')) {
                BigDecimal dividend = new BigDecimal("1")
                BigDecimal rate = null
                try {
                    rate = dividend.divide(nominalRate,4,RoundingMode.HALF_EVEN)
                }
                catch (Exception e){
                    println "Exception: "+e.message
                    log.error e.fillInStackTrace()
                }
                exchangeRate = new ExchangeRate(
                        baseCurrency: currency.currencyCode,
                        toCurrency: newCurrency.currencyCode,
                        baseCurrencyMeanValue: 1,
                        toCurrencyMeanValue: nominalRate,
                        date: toDate,
                        rate: rate)
            } else {
                exchangeRate = new ExchangeRate(
                        baseCurrency: currency.currencyCode,
                        toCurrency: newCurrency.currencyCode,
                        baseCurrencyMeanValue: nominalRate,
                        toCurrencyMeanValue: 1 ,
                        date: toDate,
                        rate: nominalRate)
            }
            exchangeRate.save(flush: true)
        }
        catch (Exception e) {
            //finish silent
            exchangeRate = null
        }
        return exchangeRate
    }

    Money clone() {
        return new Money(amount:this.amount, currency:currency)
    }

    Money plus(Money other) {
        assert other
        if (other.currency != this.currency) {
            other = other.convertTo(this.currency)
        }
        return new Money(amount:this.amount + other.amount, currency:currency)
    }

    Money minus(Money other) {
        return plus(other * -1)
    }

    Money plus(Number n) {
        if (!n) n = 0
        return new Money(amount:this.amount + n, currency:currency)
    }

    Money minus(Number n) {
        if (!n) n = 0
        return new Money(amount:this.amount - n, currency:currency)
    }

    Money multiply(Number n) {
        if (!n) n = 0
        return new Money(amount:this.amount * n, currency:currency)
    }

    Money div(Number n) {
        return new Money(amount:this.amount / n, currency:currency)
    }

}
