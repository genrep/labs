package com.genrep.invoiceCreator

/**
 * Created by igor on 1.4.15.
 */
public class Converter {

    public enum ConvertType{
        LAT2CYR, CYR2LAT
    }

    private String[] listLat = null;
    private String[] listCyr = null;

    public Converter() {
        initList();
    }

    static getInstance(){
        new Converter()
    }

    private void initList() {
        String lat = "Dzh Lj Nj A B C Ch Gj D Kj E F G H I J K L M N O P R S Sh T U V Z Zh Dz";
        lat += " " + lat.toLowerCase();
        lat += " Dzh Lj Nj";


        String cyr = "Џ Љ Њ А Б Ц Ч Ѓ Д Ќ Е Ф Г Х И Ј К Л М Н О П Р С Ш Т У В З Ж Ѕ";
        cyr += " " + cyr.toLowerCase();
        cyr += " Џ Љ Њ";

        listLat = lat.split(" ");
        listCyr = cyr.split(" ");
    }

    public String convertText(String line, ConvertType type){

        int i = 0;

        if(type == ConvertType.LAT2CYR)
            for(String item : listLat)
            {
                line = line.replaceAll(item, listCyr[i]);
                i++;
            }

        else if(type == ConvertType.CYR2LAT)
            for(String item : listCyr)
            {
                line = line.replaceAll(item, listLat[i]);
                i++;
            }

        return line;
    }

}

