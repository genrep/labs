package com.genrep.invoiceCreator

/**
 * Created by igor on 8/22/14.
 */
class TaxHelper {

    long id
    BigDecimal tax
    BigDecimal taxPriceSum
    String taxLabel
    int itemsCount

    static TaxHelper[] generateListOfTaxes(List<InvoiceItem> invoiceItems) {
        List<TaxHelper> list = []
        boolean flag
        for(int i=0;i<invoiceItems.size();i++){
            BigDecimal tax = invoiceItems[i].tax.value
            String taxLabel = tax.toString().concat("%")
            flag = true
            if(i==0){
                list << new TaxHelper(id: invoiceItems[i].id,
                        tax:tax,
                        taxPriceSum:invoiceItems[i].taxPrice,
                        taxLabel:taxLabel,
                        itemsCount: 1)
                continue
            }
            for(item in list){
                if(item.tax.equals(tax)){
                    item.addTax(invoiceItems[i].taxPrice)
                    flag=false
                    break
                }
            }
            if(flag){
                list << new TaxHelper(id: invoiceItems[i].id,
                        tax:tax,
                        taxPriceSum:invoiceItems[i].taxPrice,
                        taxLabel:taxLabel,
                        itemsCount: 1)
            }
        }
        return list
    }

    void addTax(BigDecimal tax){
        this.taxPriceSum+=tax
        this.itemsCount++
    }
}
