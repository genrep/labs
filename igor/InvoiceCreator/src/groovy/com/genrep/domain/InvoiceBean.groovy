package com.genrep.domain

import com.genrep.client.Client

/**
 * Created by igor on 9/4/14.
 */
class InvoiceBean {

    /**
     * Type of Invoice Class
     */
    Class aClass
    /**
     * Unique database id of the Invoice
     */
    String uid

    /**
     * Regular properties
     */
    String invoiceNumber
    Client client
    BigDecimal tax
    BigDecimal totalSum
    BigDecimal taxPriceSum
    BigDecimal taxFreeSum
    Currency currency
    Date issueDate
}
