package com.genrep.domain

import com.genrep.client.Client


/**
 * Created by igor on 9/2/14.
 */
abstract class AInvoice {

    // This two fields form the invoiceNumber
    Long invoiceCounter
    Long invoiceYear
    String invoiceNumber
    //BankAccount owner
    String swiftCode
    Client client
    Date issueDate

}
