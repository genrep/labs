package com.genrep.domain

import com.genrep.account.BankAccount

/**
 * Created by igor on 10/7/14.
 */
abstract class Transaction {

    BankAccount creditor
    BankAccount debtor
    BigDecimal amount
    Date date
    String transactionCode

}
