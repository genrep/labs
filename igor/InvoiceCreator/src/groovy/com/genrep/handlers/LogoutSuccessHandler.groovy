package com.genrep.handlers

import com.genrep.concurrency.locking.LockingService
import com.genrep.utils.ApplicationContextHolder
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler

import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * @since 11.01.2016
 * @author Igor Ivanovski
 */
class LogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {

    LockingService lockingService

    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        if (lockingService == null) {
            try {
                lockingService = (LockingService) ApplicationContextHolder.getBean('lockingService')
            } catch (Exception e) {
                logger.error(e.message)
            }
        }
        def username = authentication.principal?.username
        // release asset locks fucker
        lockingService.releaseLocks("lockedBy", username as String)
        super.handle(request, response, authentication)
    }
}
