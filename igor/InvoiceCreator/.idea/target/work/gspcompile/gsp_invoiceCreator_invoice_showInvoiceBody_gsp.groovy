import java.math.RoundingMode
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoice_showInvoiceBody_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoice/_showInvoiceBody.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
if(true && (invoiceInstance?.invoiceBodyOut)) {
printHtmlPart(1)
expressionOut.print(invoiceInstance.inCurrency)
printHtmlPart(2)
expressionOut.print(invoiceInstance.outCurrency)
printHtmlPart(3)
if(true && (invoiceInstance?.invoiceBodyIn && invoiceInstance?.invoiceBodyIn.invoiceItems.size()>0)) {
printHtmlPart(4)
invokeTag('message','g',12,['code':("invoice.inCurrency.desc"),'default':("Currency: ")],-1)
expressionOut.print(invoiceInstance.inCurrency)
printHtmlPart(5)
loop:{
int i = 0
for( invoiceItem in (invoiceInstance.invoiceBodyIn?.invoiceItems) ) {
printHtmlPart(6)
expressionOut.print(raw(invoiceItem?.description))
printHtmlPart(7)
expressionOut.print(invoiceItem?.quantity)
printHtmlPart(8)
expressionOut.print(invoiceItem?.unitPrice.decodeDecimalNumber())
printHtmlPart(8)
expressionOut.print(invoiceItem?.tax.value)
printHtmlPart(8)
expressionOut.print(invoiceItem?.taxPricePerUnit.decodeDecimalNumber())
printHtmlPart(8)
expressionOut.print(invoiceItem?.taxFreePrice.decodeDecimalNumber())
printHtmlPart(8)
expressionOut.print(invoiceItem?.taxPrice.decodeDecimalNumber())
printHtmlPart(8)
expressionOut.print(invoiceItem?.totalPrice.decodeDecimalNumber())
printHtmlPart(9)
i++
}
}
printHtmlPart(10)
expressionOut.print(invoiceInstance.invoiceBodyIn?.taxFreeSum.decodeDecimalNumber())
printHtmlPart(11)
expressionOut.print(invoiceInstance.invoiceBodyIn?.taxFreeSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber())
printHtmlPart(12)
expressionOut.print(invoiceInstance.invoiceBodyIn?.taxPriceSum.decodeDecimalNumber())
printHtmlPart(11)
expressionOut.print(invoiceInstance.invoiceBodyIn?.taxPriceSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber())
printHtmlPart(13)
expressionOut.print(invoiceInstance.invoiceBodyIn?.totalSum.setScale(2,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber())
printHtmlPart(11)
expressionOut.print(invoiceInstance.invoiceBodyIn?.getTotalSumRounded().decodeDecimalNumber())
printHtmlPart(14)
createTagBody(3, {->
printHtmlPart(15)
expressionOut.print(invoiceInstance.client.id)
printHtmlPart(16)
expressionOut.print(invoiceInstance.id)
printHtmlPart(17)
expressionOut.print(invoiceInstance.inCurrency.currencyCode)
printHtmlPart(18)
})
invokeTag('jasperReport','g',75,['jasper':("InvoiceReport"),'format':("PDF"),'name':(invoiceInstance?.name.replace(' ','_')),'controller':("report"),'action':("invoiceReport")],3)
printHtmlPart(19)
createTagBody(3, {->
printHtmlPart(15)
expressionOut.print(invoiceInstance.client.id)
printHtmlPart(16)
expressionOut.print(invoiceInstance.id)
printHtmlPart(17)
expressionOut.print(invoiceInstance.inCurrency.currencyCode)
printHtmlPart(18)
})
invokeTag('jasperReport','g',85,['jasper':("InvoiceReport"),'format':("PDF"),'name':("Applet Signing (MK)"),'controller':("invoiceDocument"),'action':("pushFileToSigningApplet")],3)
printHtmlPart(19)
createTagBody(3, {->
printHtmlPart(15)
expressionOut.print(invoiceInstance.client.id)
printHtmlPart(16)
expressionOut.print(invoiceInstance.id)
printHtmlPart(17)
expressionOut.print(invoiceInstance.inCurrency.currencyCode)
printHtmlPart(20)
invokeTag('actionSubmit','g',96,['class':("save"),'action':("transferInvoiceToMongo"),'value':(message(code: 'invoice.transferInvoice.button', default: 'Save as PDF'))],-1)
printHtmlPart(21)
})
invokeTag('form','g',98,['controller':("invoiceDocument"),'action':("transferInvoiceToMongo"),'method':("post")],3)
printHtmlPart(22)
}
printHtmlPart(23)
if(true && (invoiceInstance?.invoiceBodyOut && invoiceInstance?.invoiceBodyOut.invoiceItems.size()>0)) {
printHtmlPart(24)
invokeTag('message','g',106,['code':("invoice.outCurrency.desc"),'default':("Currency: ")],-1)
expressionOut.print(invoiceInstance.outCurrency)
printHtmlPart(25)
loop:{
int i = 0
for( invoiceItem in (invoiceInstance.invoiceBodyOut?.invoiceItems) ) {
printHtmlPart(6)
expressionOut.print(raw(invoiceItem?.description))
printHtmlPart(7)
expressionOut.print(invoiceItem?.quantity)
printHtmlPart(8)
expressionOut.print(invoiceItem?.unitPrice.decodeDecimalNumber())
printHtmlPart(8)
expressionOut.print(invoiceItem?.tax.value)
printHtmlPart(8)
expressionOut.print(invoiceItem?.taxPricePerUnit.decodeDecimalNumber())
printHtmlPart(8)
expressionOut.print(invoiceItem?.taxFreePrice.decodeDecimalNumber())
printHtmlPart(8)
expressionOut.print(invoiceItem?.taxPrice.decodeDecimalNumber())
printHtmlPart(8)
expressionOut.print(invoiceItem?.totalPrice.decodeDecimalNumber())
printHtmlPart(9)
i++
}
}
printHtmlPart(26)
expressionOut.print(invoiceInstance.invoiceBodyOut?.taxFreeSum.decodeDecimalNumber())
printHtmlPart(11)
expressionOut.print(invoiceInstance.invoiceBodyOut?.taxFreeSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber())
printHtmlPart(12)
expressionOut.print(invoiceInstance.invoiceBodyOut?.taxPriceSum.decodeDecimalNumber())
printHtmlPart(11)
expressionOut.print(invoiceInstance.invoiceBodyOut?.taxPriceSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber())
printHtmlPart(13)
expressionOut.print(invoiceInstance.invoiceBodyOut?.totalSum.setScale(2,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber())
printHtmlPart(11)
expressionOut.print(invoiceInstance.invoiceBodyOut?.getTotalSumRounded().decodeDecimalNumber())
printHtmlPart(14)
createTagBody(3, {->
printHtmlPart(15)
expressionOut.print(invoiceInstance.client.id)
printHtmlPart(16)
expressionOut.print(invoiceInstance.id)
printHtmlPart(17)
expressionOut.print(invoiceInstance.outCurrency.currencyCode)
printHtmlPart(27)
})
invokeTag('jasperReport','g',169,['jasper':("InvoiceReport"),'format':("PDF"),'name':(invoiceInstance?.name.replace(' ','_')),'controller':("report"),'action':("invoiceReportOut")],3)
printHtmlPart(19)
createTagBody(3, {->
printHtmlPart(15)
expressionOut.print(invoiceInstance.client.id)
printHtmlPart(16)
expressionOut.print(invoiceInstance.id)
printHtmlPart(17)
expressionOut.print(invoiceInstance.outCurrency.currencyCode)
printHtmlPart(27)
})
invokeTag('jasperReport','g',179,['jasper':("InvoiceReport"),'format':("PDF"),'name':("Applet Signing (MK)"),'controller':("invoiceDocument"),'action':("pushFileToSigningApplet")],3)
printHtmlPart(19)
createTagBody(3, {->
printHtmlPart(15)
expressionOut.print(invoiceInstance.client.id)
printHtmlPart(16)
expressionOut.print(invoiceInstance.id)
printHtmlPart(17)
expressionOut.print(invoiceInstance.outCurrency.currencyCode)
printHtmlPart(28)
invokeTag('actionSubmit','g',190,['class':("save"),'action':("transferInvoiceToMongo"),'value':(message(code: 'invoice.transferInvoice.button', default: 'Save as PDF'))],-1)
printHtmlPart(21)
})
invokeTag('form','g',192,['controller':("invoiceDocument"),'action':("transferInvoiceToMongo"),'method':("post")],3)
printHtmlPart(22)
}
printHtmlPart(29)
}
else {
printHtmlPart(30)
if(true && (invoiceInstance?.invoiceBodyIn && invoiceInstance?.invoiceBodyIn.invoiceItems.size()>0)) {
printHtmlPart(31)
invokeTag('message','g',203,['code':("invoice.inCurrency.desc"),'default':("Currency: ")],-1)
expressionOut.print(invoiceInstance.inCurrency)
printHtmlPart(32)
loop:{
int i = 0
for( invoiceItem in (invoiceInstance.invoiceBodyIn?.invoiceItems) ) {
printHtmlPart(33)
expressionOut.print(raw(invoiceItem?.description))
printHtmlPart(34)
expressionOut.print(invoiceItem?.quantity)
printHtmlPart(11)
expressionOut.print(invoiceItem?.unitPrice.decodeDecimalNumber())
printHtmlPart(11)
expressionOut.print(invoiceItem?.tax.value)
printHtmlPart(11)
expressionOut.print(invoiceItem?.taxPricePerUnit.decodeDecimalNumber())
printHtmlPart(11)
expressionOut.print(invoiceItem?.taxFreePrice.decodeDecimalNumber())
printHtmlPart(11)
expressionOut.print(invoiceItem?.taxPrice.decodeDecimalNumber())
printHtmlPart(11)
expressionOut.print(invoiceItem?.totalPrice.decodeDecimalNumber())
printHtmlPart(35)
i++
}
}
printHtmlPart(36)
expressionOut.print(invoiceInstance.invoiceBodyIn?.taxFreeSum.decodeDecimalNumber())
printHtmlPart(37)
expressionOut.print(invoiceInstance.invoiceBodyIn?.taxFreeSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber())
printHtmlPart(38)
expressionOut.print(invoiceInstance.invoiceBodyIn?.taxPriceSum.decodeDecimalNumber())
printHtmlPart(37)
expressionOut.print(invoiceInstance.invoiceBodyIn?.taxPriceSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber())
printHtmlPart(39)
expressionOut.print(invoiceInstance.invoiceBodyIn?.totalSum.setScale(2,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber())
printHtmlPart(37)
expressionOut.print(invoiceInstance.invoiceBodyIn?.getTotalSumRounded().decodeDecimalNumber())
printHtmlPart(40)
createTagBody(3, {->
printHtmlPart(41)
expressionOut.print(invoiceInstance.client.id)
printHtmlPart(42)
expressionOut.print(invoiceInstance.id)
printHtmlPart(43)
expressionOut.print(invoiceInstance.inCurrency.currencyCode)
printHtmlPart(44)
})
invokeTag('jasperReport','g',266,['jasper':("InvoiceReport"),'format':("PDF"),'name':(invoiceInstance?.name.replace(' ','_')),'controller':("report"),'action':("invoiceReport")],3)
printHtmlPart(45)
createTagBody(3, {->
printHtmlPart(41)
expressionOut.print(invoiceInstance.client.id)
printHtmlPart(42)
expressionOut.print(invoiceInstance.id)
printHtmlPart(43)
expressionOut.print(invoiceInstance.inCurrency.currencyCode)
printHtmlPart(44)
})
invokeTag('jasperReport','g',276,['jasper':("InvoiceReport"),'format':("PDF"),'name':("Applet Signing (MK)"),'controller':("invoiceDocument"),'action':("pushFileToSigningApplet")],3)
printHtmlPart(45)
createTagBody(3, {->
printHtmlPart(41)
expressionOut.print(invoiceInstance.client.id)
printHtmlPart(42)
expressionOut.print(invoiceInstance.id)
printHtmlPart(43)
expressionOut.print(invoiceInstance.inCurrency.currencyCode)
printHtmlPart(46)
invokeTag('actionSubmit','g',287,['class':("save"),'action':("transferInvoiceToMongo"),'value':(message(code: 'invoice.transferInvoice.button', default: 'Save as PDF'))],-1)
printHtmlPart(47)
})
invokeTag('form','g',289,['controller':("invoiceDocument"),'action':("transferInvoiceToMongo"),'method':("post")],3)
printHtmlPart(48)
}
printHtmlPart(0)
}
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427904339000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
