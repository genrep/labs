import com.genrep.invoiceCreator.ExchangeRate
import  com.genrep.invoiceCreator.Invoice
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoiceshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoice/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',6,['var':("entityName"),'value':(message(code: 'invoice.label', default: 'Invoice'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',7,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',7,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',7,[:],2)
printHtmlPart(1)
invokeTag('javascript','asset',8,['src':("invoice.js")],-1)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
invokeTag('message','g',13,['code':("default.link.skip.label"),'default':("Skip to content&hellip;")],-1)
printHtmlPart(5)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(6)
invokeTag('message','g',17,['code':("default.home.label")],-1)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',18,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('link','g',18,['class':("menuButton"),'action':("index")],2)
printHtmlPart(8)
createTagBody(2, {->
invokeTag('message','g',20,['code':("default.new.label"),'args':([entityName])],-1)
})
invokeTag('link','g',20,['class':("menuButton"),'action':("create")],2)
printHtmlPart(9)
invokeTag('message','g',25,['code':("default.show.label"),'args':([entityName])],-1)
printHtmlPart(10)
if(true && (flash.message)) {
printHtmlPart(11)
expressionOut.print(flash.message)
printHtmlPart(12)
}
printHtmlPart(13)
if(true && (invoiceInstance?.name)) {
printHtmlPart(14)
invokeTag('message','g',34,['code':("invoice.name.label"),'default':("Descriptive Name")],-1)
printHtmlPart(15)
invokeTag('fieldValue','g',37,['bean':(invoiceInstance),'field':("name")],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (invoiceInstance?.client)) {
printHtmlPart(18)
invokeTag('message','g',45,['code':("invoice.client.label"),'default':("Client")],-1)
printHtmlPart(19)
expressionOut.print(invoiceInstance?.client.name)
printHtmlPart(20)
}
printHtmlPart(17)
if(true && (invoiceInstance?.invoiceDescription)) {
printHtmlPart(21)
invokeTag('message','g',56,['code':("invoice.invoiceDescription.label"),'default':("Description")],-1)
printHtmlPart(22)
expressionOut.print(raw(invoiceInstance.invoiceDescription))
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (invoiceInstance?.invoiceNumber)) {
printHtmlPart(23)
invokeTag('message','g',66,['code':("invoice.invoiceNumber.label"),'default':("Invoice Number")],-1)
printHtmlPart(24)
invokeTag('fieldValue','g',69,['bean':(invoiceInstance),'field':("invoiceNumber")],-1)
printHtmlPart(20)
}
printHtmlPart(17)
if(true && (invoiceInstance?.invoiceNumberSort)) {
printHtmlPart(25)
invokeTag('message','g',77,['code':("invoice.invoiceNumberSort.label"),'default':("Invoice Number Sort")],-1)
printHtmlPart(26)
invokeTag('fieldValue','g',80,['bean':(invoiceInstance),'field':("invoiceNumberSort")],-1)
printHtmlPart(20)
}
printHtmlPart(17)
if(true && (invoiceInstance?.swiftCode)) {
printHtmlPart(27)
invokeTag('message','g',88,['code':("invoice.swiftCode.label"),'default':("Swift Code")],-1)
printHtmlPart(28)
invokeTag('fieldValue','g',91,['bean':(invoiceInstance),'field':("swiftCode")],-1)
printHtmlPart(20)
}
printHtmlPart(17)
if(true && (invoiceInstance?.paymentPercentage)) {
printHtmlPart(29)
invokeTag('message','g',99,['code':("invoice.paymentPercentage.label"),'default':("Payment Percentage (%)")],-1)
printHtmlPart(30)
invokeTag('fieldValue','g',102,['bean':(invoiceInstance),'field':("paymentPercentage")],-1)
printHtmlPart(20)
}
printHtmlPart(17)
if(true && (invoiceInstance?.status)) {
printHtmlPart(31)
invokeTag('message','g',110,['code':("invoice.status.label"),'default':("Invoice Status")],-1)
printHtmlPart(32)
invokeTag('fieldValue','g',113,['bean':(invoiceInstance),'field':("status")],-1)
printHtmlPart(20)
}
printHtmlPart(17)
if(true && (invoiceInstance?.exchangeRate)) {
printHtmlPart(33)
invokeTag('message','g',121,['code':("invoice.exchangeRate.label"),'default':("Exchange Rate")],-1)
printHtmlPart(34)
invokeTag('fieldValue','g',125,['bean':(invoiceInstance),'field':("exchangeRate")],-1)
printHtmlPart(35)
}
printHtmlPart(36)
invokeTag('message','g',134,['code':("invoice.documents.label"),'default':("Show Documents")],-1)
printHtmlPart(37)
createClosureForHtmlPart(38, 2)
invokeTag('link','g',137,['action':("showDocuments"),'id':(invoiceInstance.id)],2)
printHtmlPart(39)
invokeTag('render','g',144,['template':("/invoice/showInvoiceBody")],-1)
printHtmlPart(40)
if(true && (project)) {
printHtmlPart(41)
invokeTag('message','g',150,['code':("invoice.project.label"),'default':("Project")],-1)
printHtmlPart(42)
createTagBody(3, {->
printHtmlPart(43)
expressionOut.print(project?.title)
printHtmlPart(44)
})
invokeTag('link','g',155,['controller':("project"),'action':("show"),'id':(project?.id)],3)
printHtmlPart(45)
expressionOut.print(project?.contractNumberBearer)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (invoiceInstance?.issueDate)) {
printHtmlPart(46)
invokeTag('message','g',166,['code':("invoice.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(47)
invokeTag('formatDate','g',169,['date':(invoiceInstance?.issueDate),'format':("dd.MM.yyyy")],-1)
printHtmlPart(20)
}
printHtmlPart(17)
if(true && (invoiceInstance?.paymentDate)) {
printHtmlPart(48)
invokeTag('message','g',177,['code':("invoice.paymentDate.label"),'default':("Payment Date")],-1)
printHtmlPart(49)
invokeTag('formatDate','g',180,['date':(invoiceInstance?.paymentDate),'format':("dd.MM.yyyy")],-1)
printHtmlPart(20)
}
printHtmlPart(50)
createTagBody(2, {->
printHtmlPart(51)
createTagBody(3, {->
invokeTag('message','g',189,['code':("default.button.edit.label"),'default':("Edit")],-1)
})
invokeTag('link','g',189,['class':("edit"),'action':("edit"),'resource':(invoiceInstance)],3)
printHtmlPart(52)
invokeTag('actionSubmit','g',192,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(53)
createTagBody(3, {->
printHtmlPart(54)
invokeTag('message','g',195,['code':("default.button.copy.label"),'default':("Make a copy")],-1)
printHtmlPart(52)
})
invokeTag('link','g',196,['class':("create"),'action':("copy"),'resource':(invoiceInstance)],3)
printHtmlPart(55)
})
invokeTag('form','g',198,['url':([resource: invoiceInstance, action: 'delete']),'method':("DELETE")],2)
printHtmlPart(56)
})
invokeTag('captureBody','sitemesh',202,[:],1)
printHtmlPart(57)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427880782000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
