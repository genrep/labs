import com.genrep.statements.BankStatement
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatement/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',6,['var':("entityName"),'value':(message(code: 'bankStatement.label', default: 'BankStatement'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',7,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',7,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',7,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',8,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',14,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',15,['code':("bankStatement.list"),'default':("List Bank Statement")],-1)
})
invokeTag('link','g',15,['class':("menuButton"),'action':("index")],2)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',16,['code':("bankStatment.create"),'default':("New Bank Statement")],-1)
})
invokeTag('link','g',16,['class':("menuButton"),'action':("create")],2)
printHtmlPart(8)
invokeTag('message','g',21,['code':("bankStatement.show.label"),'default':("Bank Statement Basic Information")],-1)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
if(true && (bankStatementInstance?.ownerOfStatement)) {
printHtmlPart(13)
invokeTag('message','g',30,['code':("bankStatement.ownerOfStatement.label"),'default':("Bank Account")],-1)
printHtmlPart(14)
invokeTag('fieldValue','g',33,['bean':(bankStatementInstance),'field':("ownerOfStatement")],-1)
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (bankStatementInstance?.bankStatementId)) {
printHtmlPart(17)
invokeTag('message','g',41,['code':("bankStatement.bankStatementId.label"),'default':("Bank Statement Id")],-1)
printHtmlPart(18)
invokeTag('fieldValue','g',44,['bean':(bankStatementInstance),'field':("bankStatementId")],-1)
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (bankStatementInstance?.issueDate)) {
printHtmlPart(19)
invokeTag('message','g',52,['code':("bankStatement.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(20)
invokeTag('formatDate','g',55,['date':(bankStatementInstance?.issueDate),'format':("dd.MM.yyyy")],-1)
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (bankStatementInstance?.bankAccountNumber)) {
printHtmlPart(21)
invokeTag('message','g',63,['code':("bankStatement.bankAccountNumber.label"),'default':("Bank Account Number")],-1)
printHtmlPart(22)
invokeTag('fieldValue','g',66,['bean':(bankStatementInstance),'field':("bankAccountNumber")],-1)
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (bankStatementInstance?.balanceBefore)) {
printHtmlPart(23)
invokeTag('message','g',74,['code':("bankStatement.balanceBefore.label"),'default':("Balance Before")],-1)
printHtmlPart(24)
invokeTag('fieldValue','g',77,['bean':(bankStatementInstance),'field':("balanceBefore")],-1)
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (bankStatementInstance?.balanceAfter)) {
printHtmlPart(25)
invokeTag('message','g',85,['code':("bankStatement.balanceAfter.label"),'default':("Balance After")],-1)
printHtmlPart(26)
invokeTag('fieldValue','g',88,['bean':(bankStatementInstance),'field':("balanceAfter")],-1)
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (bankStatementInstance?.bankStatementDocId)) {
printHtmlPart(27)
invokeTag('message','g',96,['code':("bankStatement.bankStatementDocId.label"),'default':("Bank Statement Doc Id")],-1)
printHtmlPart(28)
invokeTag('fieldValue','g',99,['bean':(bankStatementInstance),'field':("bankStatementDocId")],-1)
printHtmlPart(15)
}
printHtmlPart(29)
createTagBody(2, {->
printHtmlPart(30)
createTagBody(3, {->
invokeTag('message','g',107,['code':("statements.action.parse"),'default':("Show Details")],-1)
})
invokeTag('link','g',107,['action':("showBankStatementDetails"),'resource':(bankStatementInstance)],3)
printHtmlPart(31)
createTagBody(3, {->
invokeTag('message','g',109,['code':("default.button.edit.label"),'default':("Edit")],-1)
})
invokeTag('link','g',109,['class':("edit"),'action':("edit"),'resource':(bankStatementInstance)],3)
printHtmlPart(31)
createTagBody(3, {->
invokeTag('message','g',111,['code':("bankStatements.editItems"),'default':("Edit Statement Items")],-1)
})
invokeTag('link','g',111,['class':("edit"),'action':("editBankStatementItems"),'resource':(bankStatementInstance)],3)
printHtmlPart(31)
invokeTag('actionSubmit','g',114,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(32)
})
invokeTag('form','g',116,['url':([resource: bankStatementInstance, action: 'delete']),'method':("DELETE")],2)
printHtmlPart(33)
})
invokeTag('captureBody','sitemesh',118,[:],1)
printHtmlPart(34)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
