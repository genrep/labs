import com.genrep.account.BankAccount
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankAccount_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankAccount/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(2)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(3)
expressionOut.print(error.field)
printHtmlPart(4)
}
printHtmlPart(5)
invokeTag('message','g',9,['error':(error)],-1)
printHtmlPart(6)
})
invokeTag('eachError','g',10,['bean':(bankAccountInstance),'var':("error")],2)
printHtmlPart(7)
})
invokeTag('hasErrors','g',12,['bean':(bankAccountInstance)],1)
printHtmlPart(8)
expressionOut.print(hasErrors(bean: bankAccountInstance, field: 'entityName', 'error'))
printHtmlPart(9)
invokeTag('message','g',17,['code':("bankAccount.entityName.label"),'default':("Entity Name")],-1)
printHtmlPart(10)
invokeTag('textField','g',21,['name':("entityName"),'required':(""),'value':(bankAccountInstance?.entityName)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: bankAccountInstance, field: 'bankName', 'error'))
printHtmlPart(12)
invokeTag('message','g',29,['code':("bankAccount.bankName.label"),'default':("Bank Name")],-1)
printHtmlPart(10)
invokeTag('textField','g',33,['name':("bankName"),'value':(bankAccountInstance?.bankName)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: bankAccountInstance, field: 'bankAccountNumber', 'error'))
printHtmlPart(13)
invokeTag('message','g',41,['code':("bankAccount.bankAccountNumber.label"),'default':("Bank Account Number")],-1)
printHtmlPart(10)
invokeTag('textField','g',45,['name':("bankAccountNumber"),'required':(""),'value':(bankAccountInstance?.bankAccountNumber)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: bankAccountInstance, field: 'balance', 'error'))
printHtmlPart(14)
invokeTag('message','g',53,['code':("bankAccount.balance.label"),'default':("Balance")],-1)
printHtmlPart(10)
invokeTag('priceAndTaxField','g',57,['id':("balance"),'name':("balance"),'value':(bankAccountInstance?.balance)],-1)
printHtmlPart(15)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
