import com.genrep.project.Project
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_projectshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/project/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'project.label', default: 'Project'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(0)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(3)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(4)
invokeTag('message','g',13,['code':("default.home.label")],-1)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('message','g',14,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('link','g',14,['class':("menuButton"),'action':("index")],2)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',15,['code':("default.new.label"),'args':([entityName])],-1)
})
invokeTag('link','g',15,['class':("menuButton"),'action':("create")],2)
printHtmlPart(7)
invokeTag('message','g',19,['code':("default.show.label"),'args':([entityName])],-1)
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(11)
if(true && (projectInstance?.technicalNumber)) {
printHtmlPart(12)
invokeTag('message','g',27,['code':("project.technicalNumber.label"),'default':("Technical Number")],-1)
printHtmlPart(13)
invokeTag('fieldValue','g',29,['bean':(projectInstance),'field':("technicalNumber")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (projectInstance?.title)) {
printHtmlPart(16)
invokeTag('message','g',36,['code':("project.title.label"),'default':("Title")],-1)
printHtmlPart(17)
invokeTag('fieldValue','g',38,['bean':(projectInstance),'field':("title")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (projectInstance?.description)) {
printHtmlPart(18)
invokeTag('message','g',46,['code':("project.description.label"),'default':("Description")],-1)
printHtmlPart(19)
invokeTag('fieldValue','g',49,['bean':(projectInstance),'field':("description")],-1)
printHtmlPart(20)
}
printHtmlPart(15)
if(true && (projectInstance?.client)) {
printHtmlPart(18)
invokeTag('message','g',57,['code':("project.client.label"),'default':("Client")],-1)
printHtmlPart(19)
invokeTag('fieldValue','g',60,['bean':(projectInstance),'field':("client")],-1)
printHtmlPart(20)
}
printHtmlPart(15)
if(true && (projectInstance?.contractNumber)) {
printHtmlPart(21)
invokeTag('message','g',67,['code':("project.contractNumber.label"),'default':("Contract Number")],-1)
printHtmlPart(22)
invokeTag('fieldValue','g',69,['bean':(projectInstance),'field':("contractNumber")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (projectInstance?.contractNumberBearer)) {
printHtmlPart(23)
invokeTag('message','g',76,['code':("project.contractNumberBearer.label"),'default':("Contract Number Bearer")],-1)
printHtmlPart(24)
invokeTag('fieldValue','g',78,['bean':(projectInstance),'field':("contractNumberBearer")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (projectInstance?.price)) {
printHtmlPart(25)
invokeTag('message','g',85,['code':("project.price.label"),'default':("Price")],-1)
printHtmlPart(26)
invokeTag('fieldValue','g',88,['bean':(projectInstance),'field':("price")],-1)
printHtmlPart(27)
invokeTag('fieldValue','g',89,['bean':(projectInstance),'field':("currency")],-1)
printHtmlPart(28)
}
printHtmlPart(15)
if(true && (projectInstance?.tax)) {
printHtmlPart(29)
invokeTag('message','g',97,['code':("project.tax.label"),'default':("Tax (%)")],-1)
printHtmlPart(30)
expressionOut.print(projectInstance?.tax?.value)
printHtmlPart(28)
}
printHtmlPart(15)
if(true && (projectInstance?.totalPrice)) {
printHtmlPart(31)
invokeTag('message','g',108,['code':("project.totalPrice.label"),'default':("Total Price")],-1)
printHtmlPart(32)
invokeTag('fieldValue','g',111,['bean':(projectInstance),'field':("totalPrice")],-1)
printHtmlPart(27)
invokeTag('fieldValue','g',112,['bean':(projectInstance),'field':("currency")],-1)
printHtmlPart(28)
}
printHtmlPart(15)
if(true && (projectInstance?.startDate)) {
printHtmlPart(33)
invokeTag('message','g',120,['code':("project.startDate.label"),'default':("Start Date")],-1)
printHtmlPart(34)
invokeTag('formatDate','g',123,['date':(projectInstance?.startDate),'format':("dd.MM.yyyy")],-1)
printHtmlPart(28)
}
printHtmlPart(15)
if(true && (projectInstance?.endDate)) {
printHtmlPart(35)
invokeTag('message','g',131,['code':("project.endDate.label"),'default':("End Date")],-1)
printHtmlPart(36)
invokeTag('formatDate','g',134,['format':("dd.MM.yyyy"),'date':(projectInstance?.endDate)],-1)
printHtmlPart(28)
}
printHtmlPart(15)
if(true && (projectInstance?.projectState)) {
printHtmlPart(18)
invokeTag('message','g',143,['code':("project.projectState.label"),'default':("Project State")],-1)
printHtmlPart(19)
invokeTag('fieldValue','g',146,['bean':(projectInstance),'field':("projectState")],-1)
printHtmlPart(20)
}
printHtmlPart(15)
if(true && (projectDocumentInstance?.documentFile)) {
printHtmlPart(37)
invokeTag('message','g',154,['code':("project.documentFile.label"),'default':("Document File")],-1)
printHtmlPart(38)
createTagBody(3, {->
printHtmlPart(39)
expressionOut.print(projectDocumentInstance?.documentName?:'Project Document')
printHtmlPart(27)
})
invokeTag('link','g',160,['controller':("project"),'action':("showProjectDocumentFile"),'target':("_blank"),'id':(projectDocumentInstance?.id)],3)
printHtmlPart(20)
}
printHtmlPart(15)
if(true && (projectDocumentInstance?.documentName)) {
printHtmlPart(40)
invokeTag('message','g',168,['code':("project.documentName.label"),'default':("Document Name")],-1)
printHtmlPart(41)
invokeTag('fieldValue','g',171,['bean':(projectDocumentInstance),'field':("documentName")],-1)
printHtmlPart(20)
}
printHtmlPart(15)
if(true && (projectInstance?.invoices || importedInvoicesList!=null)) {
printHtmlPart(42)
invokeTag('message','g',178,['code':("project.invoices.label"),'default':("Invoices")],-1)
printHtmlPart(43)
for( i in (projectInstance.invoices) ) {
printHtmlPart(44)
createTagBody(4, {->
expressionOut.print(i?.invoiceNumber)
})
invokeTag('link','g',182,['controller':("invoice"),'action':("show"),'id':(i.id)],4)
printHtmlPart(45)
}
printHtmlPart(46)
}
printHtmlPart(15)
if(true && (importedInvoicesList)) {
printHtmlPart(42)
invokeTag('message','g',191,['code':("project.importedInvoices.label"),'default':("Imported Invoices")],-1)
printHtmlPart(43)
for( i in (importedInvoicesList) ) {
printHtmlPart(47)
createTagBody(4, {->
expressionOut.print(i?.invoiceNumber)
})
invokeTag('link','g',195,['controller':("invoiceArchive"),'action':("show"),'id':(i.id)],4)
printHtmlPart(45)
}
printHtmlPart(48)
}
printHtmlPart(15)
if(true && (projectInstance?.paymentTerms)) {
printHtmlPart(49)
invokeTag('message','g',203,['code':("project.paymentTerms.label"),'default':("Payment Terms")],-1)
printHtmlPart(43)
for( p in (projectInstance.paymentTerms) ) {
printHtmlPart(50)
createTagBody(4, {->
expressionOut.print(p?.encodeAsHTML())
})
invokeTag('link','g',206,['controller':("paymentTerm"),'action':("show"),'id':(p.id)],4)
printHtmlPart(51)
}
printHtmlPart(46)
}
printHtmlPart(52)
createTagBody(2, {->
printHtmlPart(53)
createTagBody(3, {->
invokeTag('message','g',215,['code':("default.button.edit.label"),'default':("Edit")],-1)
})
invokeTag('link','g',215,['class':("edit"),'action':("edit"),'resource':(projectInstance)],3)
printHtmlPart(54)
invokeTag('actionSubmit','g',216,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(55)
})
invokeTag('form','g',218,['url':([resource:projectInstance, action:'delete']),'method':("DELETE")],2)
printHtmlPart(56)
})
invokeTag('captureBody','sitemesh',220,[:],1)
printHtmlPart(57)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427817119000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
