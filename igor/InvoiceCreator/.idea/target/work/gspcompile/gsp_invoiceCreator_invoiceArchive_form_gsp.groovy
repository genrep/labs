import com.genrep.invoiceRepository.Taxes
import  com.genrep.invoiceCreator.Currencies
import  com.genrep.invoiceRepository.InvoiceArchive
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoiceArchive_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoiceArchive/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'invoiceCounter', 'error'))
printHtmlPart(1)
invokeTag('message','g',6,['code':("invoice.invoiceCounter.label"),'default':("Invoice Counter")],-1)
printHtmlPart(2)
invokeTag('field','g',11,['type':("number"),'name':("invoiceCounter"),'id':("invoiceCounter"),'onchange':("generateInvoiceNumber()"),'value':(invoiceArchiveInstance?.invoiceCounter),'required':("")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'invoiceYear', 'error'))
printHtmlPart(4)
invokeTag('message','g',19,['code':("invoice.invoiceYear.label"),'default':("Invoice Year")],-1)
printHtmlPart(2)
invokeTag('field','g',24,['type':("number"),'name':("invoiceYear"),'id':("invoiceYear"),'onchange':("generateInvoiceNumber()"),'value':(invoiceArchiveInstance?.invoiceYear),'required':("")],-1)
printHtmlPart(5)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'invoiceNumber', 'error'))
printHtmlPart(6)
invokeTag('message','g',31,['code':("invoiceArchive.invoiceNumber.label"),'default':("Invoice Number")],-1)
printHtmlPart(2)
invokeTag('textField','g',35,['name':("invoiceNumber"),'required':(""),'readonly':(""),'value':(invoiceArchiveInstance?.invoiceNumber)],-1)
printHtmlPart(5)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'swiftCode', 'error'))
printHtmlPart(7)
invokeTag('message','g',42,['code':("invoiceArchive.swiftCode.label"),'default':("Swift Code")],-1)
printHtmlPart(2)
invokeTag('textField','g',46,['name':("swiftCode"),'value':(invoiceArchiveInstance?.swiftCode)],-1)
printHtmlPart(5)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'client', 'error'))
printHtmlPart(8)
invokeTag('message','g',53,['code':("invoiceArchive.client.label"),'default':("Client")],-1)
printHtmlPart(2)
invokeTag('select','g',59,['id':("client"),'name':("client.id"),'from':(com.genrep.client.Client.list()),'optionKey':("id"),'required':(""),'value':(invoiceArchiveInstance?.client?.id),'class':("many-to-one"),'noSelection':(['':'Please select client'])],-1)
printHtmlPart(9)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'invoiceDescription', 'error'))
printHtmlPart(10)
invokeTag('message','g',68,['code':("invoice.invoiceDescription.label"),'default':("Description")],-1)
printHtmlPart(2)
invokeTag('textArea','g',73,['name':("invoiceDescription"),'id':("invoiceDescription"),'cols':("40"),'rows':("5"),'maxlength':("3000"),'value':(invoiceArchiveInstance?.invoiceDescription)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'inCurrency', 'error'))
printHtmlPart(12)
invokeTag('message','g',81,['code':("invoiceArchive.currency.label"),'default':("Currency")],-1)
printHtmlPart(2)
invokeTag('currencySelect','g',86,['name':("inCurrency"),'value':(invoiceArchiveInstance?.inCurrency?:Currencies.MKD.currency),'from':(Currencies.values())],-1)
printHtmlPart(5)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'totalSum', 'error'))
printHtmlPart(13)
invokeTag('message','g',93,['code':("invoiceArchive.totalSum.label"),'default':("Total Sum")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',97,['id':("totalSum"),'name':("totalSum"),'required':(""),'value':(invoiceArchiveInstance?.totalSum)],-1)
printHtmlPart(5)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'taxFreeSum', 'error'))
printHtmlPart(14)
invokeTag('message','g',104,['code':("invoiceArchive.taxFreeSum.label"),'default':("Tax Free Sum")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',108,['id':("taxFreeSum"),'name':("taxFreeSum"),'required':(""),'value':(invoiceArchiveInstance?.taxFreeSum)],-1)
printHtmlPart(5)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'taxPriceSum', 'error'))
printHtmlPart(15)
invokeTag('message','g',115,['code':("invoiceArchive.taxPriceSum.label"),'default':("Tax Price Sum")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',119,['id':("taxPriceSum"),'name':("taxPriceSum"),'required':(""),'value':(invoiceArchiveInstance?.taxPriceSum)],-1)
printHtmlPart(16)
invokeTag('message','g',126,['code':("invoiceArchive.taxes.zero"),'default':("Tax (0%)")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',130,['id':("tax0"),'name':("tax0"),'value':(invoiceArchiveInstance?.taxes?.get(Taxes.ZERO))],-1)
printHtmlPart(16)
invokeTag('message','g',137,['code':("invoiceArchive.taxes.five"),'default':("Tax (5%)")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',141,['id':("tax5"),'name':("tax5"),'value':(invoiceArchiveInstance?.taxes?.get(Taxes.FIVE))],-1)
printHtmlPart(16)
invokeTag('message','g',148,['code':("invoiceArchive.taxes.eighteen"),'default':("Tax (18%)")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',152,['id':("tax18"),'name':("tax18"),'value':(invoiceArchiveInstance?.taxes?.get(Taxes.EIGHTEEN))],-1)
printHtmlPart(17)
invokeTag('message','g',160,['code':("invoiceArchive.project.label"),'default':("Add to Project")],-1)
printHtmlPart(2)
invokeTag('select','g',168,['id':("projectId"),'name':("projectId"),'from':(projectList),'noSelection':(['':'']),'optionValue':("title"),'optionKey':("technicalNumber"),'value':(invoiceArchiveInstance?.projectId)],-1)
printHtmlPart(18)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'issueDate', 'error'))
printHtmlPart(19)
invokeTag('message','g',176,['code':("invoiceArchive.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(20)
expressionOut.print(formatDate(date:invoiceArchiveInstance?.issueDate?:new Date(),format: 'dd.MM.yyyy'))
printHtmlPart(21)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427368974000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
