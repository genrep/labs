import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementDoc_fileForm_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatementDoc/_fileForm.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
invokeTag('message','g',5,['code':("bankStatementDoc.documentFile.label"),'default':("Statement Doc")],-1)
printHtmlPart(1)
invokeTag('message','g',17,['code':("bankStatementDocInstance.documentName.label"),'default':("Document Name")],-1)
printHtmlPart(2)
invokeTag('textField','g',21,['class':("wider-fields"),'name':("documentName"),'id':("documentName")],-1)
printHtmlPart(3)
invokeTag('message','g',28,['code':("bankStatementDoc.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(4)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
