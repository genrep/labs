import com.genrep.invoiceCreator.ExchangeRate
import com.genrep.invoiceCreator.Currencies
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_exchangeRate_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/exchangeRate/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
expressionOut.print(hasErrors(bean: exchangeRateInstance, field: 'date', 'error'))
printHtmlPart(2)
invokeTag('message','g',8,['code':("exchangeRate.date.label"),'default':("Date")],-1)
printHtmlPart(3)
invokeTag('datePicker','g',11,['name':("date"),'precision':("day"),'value':(exchangeRateInstance?.date)],-1)
printHtmlPart(4)
expressionOut.print(hasErrors(bean: exchangeRateInstance, field: 'baseCurrency', 'error'))
printHtmlPart(5)
invokeTag('message','g',17,['code':("exchangeRate.baseCurrency.label"),'default':("Base Currency")],-1)
printHtmlPart(3)
invokeTag('currencySelect','g',20,['name':("baseCurrency"),'value':(exchangeRateInstance?.baseCurrency),'from':(Currencies.values())],-1)
printHtmlPart(4)
expressionOut.print(hasErrors(bean: exchangeRateInstance, field: 'rate', 'error'))
printHtmlPart(6)
invokeTag('message','g',26,['code':("exchangeRate.rate.label"),'default':("Rate")],-1)
printHtmlPart(3)
invokeTag('field','g',29,['name':("rate"),'value':(fieldValue(bean: exchangeRateInstance, field: 'rate')),'required':("")],-1)
printHtmlPart(4)
expressionOut.print(hasErrors(bean: exchangeRateInstance, field: 'toCurrency', 'error'))
printHtmlPart(7)
invokeTag('message','g',35,['code':("exchangeRate.toCurrency.label"),'default':("To Currency")],-1)
printHtmlPart(3)
invokeTag('currencySelect','g',38,['name':("toCurrency"),'value':(exchangeRateInstance?.toCurrency),'from':(Currencies.values())],-1)
printHtmlPart(8)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
