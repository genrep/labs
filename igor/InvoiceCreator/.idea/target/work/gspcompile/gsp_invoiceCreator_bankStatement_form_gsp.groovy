import com.genrep.account.BankAccount
import  com.genrep.statements.BankStatement
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatement_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatement/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: bankStatementInstance, field: 'bankStatementId', 'error'))
printHtmlPart(1)
invokeTag('message','g',8,['code':("bankStatement.bankStatementId.label"),'default':("Bank Statement Id")],-1)
printHtmlPart(2)
invokeTag('textField','g',12,['name':("bankStatementId"),'required':(""),'value':(bankStatementInstance?.bankStatementId)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: bankStatementInstance, field: 'issueDate', 'error'))
printHtmlPart(4)
invokeTag('message','g',20,['code':("bankStatement.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(5)
expressionOut.print(formatDate(date:bankStatementInstance?.issueDate?:new Date(),format: 'dd.MM.yyyy'))
printHtmlPart(6)
expressionOut.print(hasErrors(bean: bankStatementInstance, field: 'ownerOfStatement', 'error'))
printHtmlPart(7)
invokeTag('message','g',33,['code':("bankStatement.ownerOfStatement.label"),'default':("Bank Account")],-1)
printHtmlPart(8)
invokeTag('select','g',40,['id':("ownerOfStatement"),'name':("ownerOfStatement.id"),'from':(BankAccount.findAllByOwner(true)),'optionKey':("id"),'optionValue':("entityName"),'required':(""),'value':(bankStatementInstance?.ownerOfStatement?.id),'class':("comboBox")],-1)
printHtmlPart(9)
expressionOut.print(hasErrors(bean: bankStatementInstance, field: 'balanceBefore', 'error'))
printHtmlPart(10)
invokeTag('message','g',49,['code':("bankStatement.balanceBefore.label"),'default':("Balance Before")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',54,['id':("balanceBefore"),'name':("balanceBefore"),'required':(""),'value':(bankStatementInstance?.balanceBefore)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: bankStatementInstance, field: 'balanceAfter', 'error'))
printHtmlPart(11)
invokeTag('message','g',61,['code':("bankStatement.balanceAfter.label"),'default':("Balance After")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',66,['id':("balanceAfter"),'name':("balanceAfter"),'required':(""),'value':(bankStatementInstance?.balanceAfter)],-1)
printHtmlPart(12)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
