import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementDoc_tableOfBankStatementItems_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatementDoc/_tableOfBankStatementItems.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
invokeTag('message','g',6,['code':("statements.item.publicEntity"),'default':("Public Entity")],-1)
printHtmlPart(1)
invokeTag('message','g',7,['code':("statements.item.publicEntityBankAccountNumber"),'default':("Bank Acc. No.")],-1)
printHtmlPart(1)
invokeTag('message','g',8,['code':("statements.item.transferSum"),'default':("Transfer Sum")],-1)
printHtmlPart(1)
invokeTag('message','g',9,['code':("statements.item.transcationCode"),'default':("Trans. Code")],-1)
printHtmlPart(1)
invokeTag('message','g',10,['code':("statements.item.shortDescription"),'default':("Description")],-1)
printHtmlPart(1)
invokeTag('message','g',11,['code':("statements.item.borrowingCode"),'default':("Borrowal")],-1)
printHtmlPart(2)
invokeTag('message','g',12,['code':("statements.item.approvalCode"),'default':("Approval")],-1)
printHtmlPart(3)
loop:{
int i = 0
for( bankStatementItem in (bankStatementInstance?.items?.sort{it.orderNumber}) ) {
printHtmlPart(4)
expressionOut.print((i % 2) == 0 ? 'even' : 'odd')
printHtmlPart(5)
expressionOut.print(bankStatementItem.publicEntity)
printHtmlPart(6)
expressionOut.print(bankStatementItem.publicEntityBankAccountNumber)
printHtmlPart(6)
expressionOut.print(bankStatementItem.transferSum.decodeDecimalNumber())
printHtmlPart(6)
expressionOut.print(bankStatementItem.transactionCode)
printHtmlPart(6)
expressionOut.print(bankStatementItem.shortDescription)
printHtmlPart(6)
expressionOut.print(bankStatementItem.borrowingCode)
printHtmlPart(6)
expressionOut.print(bankStatementItem.approvalCode)
printHtmlPart(6)
if(true && (form=='editBankStatementItems')) {
printHtmlPart(7)
expressionOut.print(bankStatementItem.id)
printHtmlPart(8)
expressionOut.print(form)
printHtmlPart(9)
}
printHtmlPart(10)
i++
}
}
printHtmlPart(11)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
