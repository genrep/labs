import com.genrep.invoiceRepository.Taxes
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoice_dialog_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoice/_dialog.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
invokeTag('textArea','g',9,['name':("descriptionD"),'id':("descriptionD"),'cols':("40"),'rows':("5"),'maxlength':("500"),'value':(""),'data-message':(message(code:'invoiceItem.invoiceDescription.error',default:'Description is obligatory'))],-1)
printHtmlPart(1)
invokeTag('field','g',16,['name':("quantityD"),'id':("quantityD"),'type':("number"),'min':("0"),'value':(""),'data-message':(message(code:'invoiceItem.quantity.error',default:'Fill in the quantity number'))],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',22,['name':("unitPriceD"),'id':("unitPriceD"),'value':(""),'data-message':(message(code:'invoiceItem.unitPrice.error',default:'Fill in the unit price'))],-1)
printHtmlPart(3)
invokeTag('select','g',30,['id':("taxD"),'name':("taxD"),'from':(Taxes.values()),'required':(""),'optionValue':("value"),'data-message':(message(code:'invoiceItem.tax.error',default:'Fill in the tax value')),'class':("many-to-one")],-1)
printHtmlPart(4)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423234587000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
