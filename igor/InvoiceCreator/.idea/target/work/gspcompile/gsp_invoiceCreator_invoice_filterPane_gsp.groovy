import com.genrep.client.Client
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoice_filterPane_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoice/_filterPane.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(fp.containerId)
printHtmlPart(1)
invokeTag('set','g',6,['var':("renderForm"),'value':("true")],-1)
printHtmlPart(2)
if(true && (renderForm)) {
printHtmlPart(3)
expressionOut.print(fp.formName)
printHtmlPart(4)
expressionOut.print(fp.formName)
printHtmlPart(5)
expressionOut.print(fp.formMethod)
printHtmlPart(6)
expressionOut.print(createLink(action: fp.formAction))
printHtmlPart(7)
expressionOut.print(fp.domain)
printHtmlPart(8)
expressionOut.print(fp.domain)
printHtmlPart(9)
}
printHtmlPart(2)
for( propMap in (fp.properties) ) {
printHtmlPart(10)
if(true && (propMap.key.toString().equals("issueDate"))) {
printHtmlPart(11)
invokeTag('set','g',18,['var':("paramF"),'value':(params."${propMap.key}From")],-1)
printHtmlPart(11)
invokeTag('set','g',19,['var':("paramT"),'value':(params."${propMap.key}To")],-1)
printHtmlPart(11)
invokeTag('set','g',20,['var':("param"),'value':(params."${propMap.key}")],-1)
printHtmlPart(12)
}
else if(true && (propMap.key.toString().equals("client"))) {
printHtmlPart(11)
invokeTag('set','g',26,['var':("param"),'value':(params."${propMap.key}")],-1)
printHtmlPart(13)
}
else if(true && (propMap.key.toString().equals("invoiceNumber"))) {
printHtmlPart(11)
invokeTag('set','g',32,['var':("param"),'value':(params."${propMap.key}")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (propMap.value.toString().equals("class java.lang.String"))) {
printHtmlPart(16)
expressionOut.print(propMap.key)
printHtmlPart(4)
expressionOut.print(propMap.key)
printHtmlPart(8)
expressionOut.print(param)
printHtmlPart(17)
}
else if(true && (propMap.value.toString().equals("class com.genrep.client.Client"))) {
printHtmlPart(18)
invokeTag('select','g',48,['id':(propMap.key),'name':(propMap.key),'from':(Client.list()),'optionKey':("id"),'optionValue':("name"),'noSelection':(['':'']),'value':(param),'class':("many-to-one")],-1)
printHtmlPart(11)
}
else if(true && (propMap.value.toString().equals("class java.util.Date"))) {
printHtmlPart(19)
expressionOut.print(propMap.key)
printHtmlPart(4)
expressionOut.print(propMap.key)
printHtmlPart(8)
expressionOut.print(param)
printHtmlPart(20)
expressionOut.print(propMap.key)
printHtmlPart(21)
expressionOut.print(propMap.key)
printHtmlPart(22)
expressionOut.print(paramF)
printHtmlPart(23)
expressionOut.print(propMap.key.toString())
printHtmlPart(24)
expressionOut.print(propMap.key)
printHtmlPart(25)
expressionOut.print(propMap.key)
printHtmlPart(26)
expressionOut.print(paramT)
printHtmlPart(23)
expressionOut.print(propMap.key.toString())
printHtmlPart(27)
}
printHtmlPart(28)
}
printHtmlPart(29)
invokeTag('actionSubmit','g',63,['value':("Apply"),'action':(fp?.action)],-1)
printHtmlPart(30)
expressionOut.print(fp.containerId.toString())
printHtmlPart(31)
if(true && (renderForm)) {
printHtmlPart(32)
}
printHtmlPart(33)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1425660983000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
