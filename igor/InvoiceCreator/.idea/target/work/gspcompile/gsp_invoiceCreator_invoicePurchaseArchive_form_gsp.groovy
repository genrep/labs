import com.genrep.invoiceRepository.Taxes
import  com.genrep.invoiceCreator.Currencies
import  com.genrep.invoiceRepository.InvoicePurchaseArchive
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoicePurchaseArchive_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoicePurchaseArchive/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'invoiceNumber', 'error'))
printHtmlPart(1)
invokeTag('message','g',7,['code':("invoicePurchaseArchive.invoiceNumber.label"),'default':("(Pro)Invoice Number")],-1)
printHtmlPart(2)
invokeTag('textField','g',11,['name':("invoiceNumber"),'required':(""),'class':("wider-fields"),'value':(invoicePurchaseArchiveInstance?.invoiceNumber)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'isProInvoice', 'error'))
printHtmlPart(4)
invokeTag('message','g',18,['code':("invoicePurchaseArchive.isProInvoice.label"),'default':("Is Pro Invoice")],-1)
printHtmlPart(5)
invokeTag('checkBox','g',24,['name':("isProInvoice"),'onchange':("toggleProInvoiceSelect(this)"),'value':(invoicePurchaseArchiveInstance?.isProInvoice)],-1)
printHtmlPart(6)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'refInvoice', 'error'))
printHtmlPart(7)
invokeTag('message','g',32,['code':("invoicePurchaseArchive.refInvoice.label"),'default':("Has Pro Invoice")],-1)
printHtmlPart(2)
invokeTag('select','g',39,['name':("refInvoice.id"),'id':("refInvoice"),'from':(proInvoices),'onchange':("setProInvoiceParams(this)"),'value':(InvoicePurchaseArchive.findWhere(invoiceNumber:  invoicePurchaseArchiveInstance?.refInvoice)?.id),'optionValue':("invoiceNumber"),'optionKey':("id"),'noSelection':(['':''])],-1)
printHtmlPart(8)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'admissionCounterNumber', 'error'))
printHtmlPart(9)
invokeTag('message','g',46,['code':("invoicePurchaseArchive.admissionCounterNumber.label"),'default':("Admission Number")],-1)
printHtmlPart(10)
expressionOut.print(invoicePurchaseArchiveInstance?.admissionCounterNumber)
printHtmlPart(11)
invokeTag('datePicker','g',53,['name':("admissionYear"),'id':("admissionYear"),'precision':("year"),'value':(new Date()),'relativeYears':([-2..0])],-1)
printHtmlPart(12)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'swiftCode', 'error'))
printHtmlPart(13)
invokeTag('message','g',63,['code':("invoicePurchaseArchive.swiftCode.label"),'default':("Swift Code")],-1)
printHtmlPart(2)
invokeTag('textField','g',67,['name':("swiftCode"),'value':(invoicePurchaseArchiveInstance?.swiftCode)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'vendor', 'error'))
printHtmlPart(14)
invokeTag('message','g',74,['code':("invoicePurchaseArchive.vendor.label"),'default':("Vendor")],-1)
printHtmlPart(2)
invokeTag('select','g',80,['id':("vendor"),'name':("vendor.id"),'from':(com.genrep.vendor.Vendor.list()),'optionKey':("id"),'required':(""),'value':(invoicePurchaseArchiveInstance?.vendor?.id),'class':("many-to-one"),'noSelection':(['':'Please select vendor'])],-1)
printHtmlPart(15)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'invoiceDescription', 'error'))
printHtmlPart(16)
invokeTag('message','g',89,['code':("invoicePurchaseArchive.invoiceDescription.label"),'default':("Invoice Description")],-1)
printHtmlPart(17)
invokeTag('textArea','g',93,['name':("invoiceDescription"),'id':("invoiceDescription"),'value':(invoicePurchaseArchiveInstance?.invoiceDescription)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'inCurrency', 'error'))
printHtmlPart(18)
invokeTag('message','g',99,['code':("invoicePurchaseArchive.inCurrency.label"),'default':("Currency")],-1)
printHtmlPart(2)
invokeTag('currencySelect','g',104,['name':("inCurrency"),'value':(invoicePurchaseArchiveInstance?.inCurrency?:Currencies.MKD.currency),'from':(com.genrep.invoiceCreator.Currencies.values())],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'totalSum', 'error'))
printHtmlPart(19)
invokeTag('message','g',111,['code':("invoicePurchaseArchive.totalSum.label"),'default':("Total Sum")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',116,['id':("totalSum"),'name':("totalSum"),'required':(""),'value':(invoicePurchaseArchiveInstance?.totalSum)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'taxFreeSum', 'error'))
printHtmlPart(20)
invokeTag('message','g',123,['code':("invoicePurchaseArchive.taxFreeSum.label"),'default':("Tax Free Sum")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',127,['id':("taxFreeSum"),'name':("taxFreeSum"),'required':(""),'value':(invoicePurchaseArchiveInstance?.taxFreeSum)],-1)
printHtmlPart(21)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'taxPriceSum', 'error'))
printHtmlPart(22)
invokeTag('message','g',135,['code':("invoicePurchaseArchive.taxPriceSum.label"),'default':("Tax Price Sum")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',139,['id':("taxPriceSum"),'name':("taxPriceSum"),'required':(""),'value':(invoicePurchaseArchiveInstance?.taxPriceSum)],-1)
printHtmlPart(23)
invokeTag('message','g',146,['code':("invoiceArchive.taxes.zero"),'default':("Tax (0%)")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',151,['id':("tax0"),'name':("tax0"),'value':(invoicePurchaseArchiveInstance?.taxes?.get(com.genrep.invoiceRepository.Taxes.ZERO))],-1)
printHtmlPart(24)
invokeTag('message','g',157,['code':("invoiceArchive.taxes.five"),'default':("Tax (5%)")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',162,['id':("tax5"),'name':("tax5"),'value':(invoicePurchaseArchiveInstance?.taxes?.get(Taxes.FIVE))],-1)
printHtmlPart(24)
invokeTag('message','g',168,['code':("invoiceArchive.taxes.eighteen"),'default':("Tax (18%)")],-1)
printHtmlPart(2)
invokeTag('priceAndTaxField','g',173,['id':("tax18"),'name':("tax18"),'value':(invoicePurchaseArchiveInstance?.taxes?.get(Taxes.EIGHTEEN))],-1)
printHtmlPart(21)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'issueDate', 'error'))
printHtmlPart(25)
invokeTag('message','g',180,['code':("invoicePurchaseArchive.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(26)
expressionOut.print(formatDate(date:invoicePurchaseArchiveInstance?.issueDate?:new Date(),format: 'dd.MM.yyyy'))
printHtmlPart(27)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'paymentDate', 'error'))
printHtmlPart(28)
invokeTag('message','g',194,['code':("invoicePurchaseArchive.paymentDate.label"),'default':("Payment Date")],-1)
printHtmlPart(29)
expressionOut.print(formatDate(date:invoicePurchaseArchiveInstance?.paymentDate,format: 'dd.MM.yyyy'))
printHtmlPart(30)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1429091183000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
