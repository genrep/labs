import com.genrep.client.Client
import  com.genrep.invoiceRepository.Taxes
import  com.genrep.invoiceCreator.Currencies
import  com.genrep.project.Project
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_project_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/project/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: projectInstance, field: 'technicalNumber', 'error'))
printHtmlPart(1)
invokeTag('message','g',8,['code':("project.technicalNumber.label"),'default':("Technical Number")],-1)
printHtmlPart(2)
invokeTag('textField','g',12,['name':("technicalNumber"),'required':(""),'value':(projectInstance?.technicalNumber)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: projectInstance, field: 'title', 'error'))
printHtmlPart(4)
invokeTag('message','g',19,['code':("project.title.label"),'default':("Title")],-1)
printHtmlPart(2)
invokeTag('textField','g',23,['name':("title"),'class':("wider-fields"),'required':(""),'value':(projectInstance?.title)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: projectInstance, field: 'description', 'error'))
printHtmlPart(5)
invokeTag('message','g',30,['code':("project.description.label"),'default':("Description")],-1)
printHtmlPart(2)
invokeTag('textArea','g',34,['name':("description"),'id':("description"),'value':(projectInstance?.description)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: projectInstance, field: 'client', 'error'))
printHtmlPart(6)
invokeTag('message','g',41,['code':("project.client.label"),'default':("Client")],-1)
printHtmlPart(7)
invokeTag('select','g',48,['id':("client"),'name':("client.id"),'from':(com.genrep.client.Client.list()),'optionKey':("id"),'optionValue':("name"),'required':(""),'noSelection':(['':'Please select client']),'value':(projectInstance?.client?.id),'class':("comboBox")],-1)
printHtmlPart(8)
expressionOut.print(hasErrors(bean: projectInstance, field: 'contractNumber', 'error'))
printHtmlPart(9)
invokeTag('message','g',57,['code':("project.contractNumber.label"),'default':("Contract Number")],-1)
printHtmlPart(2)
invokeTag('textField','g',62,['name':("contractNumber"),'required':(""),'value':(projectInstance?.contractNumber)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: projectInstance, field: 'contractNumberBearer', 'error'))
printHtmlPart(10)
invokeTag('message','g',69,['code':("project.contractNumberBearer.label"),'default':("Contract Number Bearer")],-1)
printHtmlPart(2)
invokeTag('textField','g',73,['name':("contractNumberBearer"),'required':(""),'value':(projectInstance?.contractNumberBearer)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: projectInstance, field: 'price', 'error'))
printHtmlPart(11)
invokeTag('message','g',79,['code':("project.price.label"),'default':("Price")],-1)
printHtmlPart(2)
invokeTag('textField','g',84,['name':("price"),'id':("price"),'onchange':("calculateTotalPrice()"),'value':(projectInstance?.price),'required':("")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: projectInstance, field: 'currency', 'error'))
printHtmlPart(12)
invokeTag('message','g',91,['code':("project.currency.label"),'default':("Currency")],-1)
printHtmlPart(2)
invokeTag('currencySelect','g',96,['name':("currency"),'value':(projectInstance.currency?:com.genrep.invoiceCreator.Currencies.MKD.currency),'from':(Currencies.values())],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: projectInstance, field: 'tax', 'error'))
printHtmlPart(13)
invokeTag('message','g',103,['code':("project.tax.label"),'default':("Tax (%)")],-1)
printHtmlPart(2)
invokeTag('select','g',109,['id':("tax"),'name':("tax"),'from':(com.genrep.invoiceRepository.Taxes.values()),'required':(""),'optionValue':("value"),'onchange':("calculateTotalPrice()"),'value':(projectInstance?.tax),'class':("many-to-one")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: projectInstance, field: 'totalPrice', 'error'))
printHtmlPart(14)
invokeTag('message','g',117,['code':("project.totalPrice.label"),'default':("Total Price")],-1)
printHtmlPart(2)
invokeTag('textField','g',122,['id':("totalPrice"),'name':("totalPrice"),'value':(projectInstance?.totalPrice),'required':("")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: projectInstance, field: 'startDate', 'error'))
printHtmlPart(15)
invokeTag('message','g',129,['code':("project.startDate.label"),'default':("Start Date")],-1)
printHtmlPart(16)
expressionOut.print(formatDate(date:projectInstance?.startDate?:new Date(),format: 'dd.MM.yyyy'))
printHtmlPart(17)
expressionOut.print(hasErrors(bean: projectInstance, field: 'endDate', 'error'))
printHtmlPart(18)
invokeTag('message','g',141,['code':("project.endDate.label"),'default':("End Date")],-1)
printHtmlPart(19)
expressionOut.print(formatDate(date:projectInstance?.endDate?:new Date(),format: 'dd.MM.yyyy'))
printHtmlPart(17)
expressionOut.print(hasErrors(bean: projectInstance, field: 'projectState', 'error'))
printHtmlPart(20)
invokeTag('message','g',153,['code':("project.projectState.label"),'default':("State")],-1)
printHtmlPart(2)
invokeTag('select','g',160,['id':("projectState"),'name':("projectState"),'from':(com.genrep.project.ProjectState.values()),'optionValue':("name"),'value':(projectInstance?.projectState),'noSelection':(['':'']),'class':("many-to-one")],-1)
printHtmlPart(21)
if(true && (params.action.equals("edit"))) {
printHtmlPart(22)
invokeTag('render','g',168,['template':("editFileForm")],-1)
printHtmlPart(23)
}
else {
printHtmlPart(24)
invokeTag('render','g',170,['template':("fileForm")],-1)
printHtmlPart(25)
}
printHtmlPart(26)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427900449000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
