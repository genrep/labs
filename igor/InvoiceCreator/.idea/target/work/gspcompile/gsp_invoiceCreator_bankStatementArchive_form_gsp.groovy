import com.genrep.statements.BankStatementArchive
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementArchive_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatementArchive/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: bankStatementArchiveInstance, field: 'docMongoId', 'error'))
printHtmlPart(1)
invokeTag('message','g',7,['code':("bankStatementArchive.docMongoId.label"),'default':("Doc Mongo Id")],-1)
printHtmlPart(2)
invokeTag('textField','g',10,['name':("docMongoId"),'required':(""),'value':(bankStatementArchiveInstance?.docMongoId)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: bankStatementArchiveInstance, field: 'bankStatementId', 'error'))
printHtmlPart(4)
invokeTag('message','g',16,['code':("bankStatementArchive.bankStatementId.label"),'default':("Bank Statement Id")],-1)
printHtmlPart(2)
invokeTag('textField','g',19,['name':("bankStatementId"),'required':(""),'value':(bankStatementArchiveInstance?.bankStatementId)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: bankStatementArchiveInstance, field: 'ownerOfStatement', 'error'))
printHtmlPart(5)
invokeTag('message','g',25,['code':("bankStatementArchive.ownerOfStatement.label"),'default':("Owner Of Statement")],-1)
printHtmlPart(6)
invokeTag('select','g',28,['id':("ownerOfStatement"),'name':("ownerOfStatement.id"),'from':(com.genrep.account.BankAccount.list()),'optionKey':("id"),'value':(bankStatementArchiveInstance?.ownerOfStatement?.id),'class':("many-to-one"),'noSelection':(['null': ''])],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: bankStatementArchiveInstance, field: 'documentFile', 'error'))
printHtmlPart(7)
invokeTag('message','g',34,['code':("bankStatementArchive.documentFile.label"),'default':("Document File")],-1)
printHtmlPart(8)
expressionOut.print(hasErrors(bean: bankStatementArchiveInstance, field: 'documentName', 'error'))
printHtmlPart(9)
invokeTag('message','g',43,['code':("bankStatementArchive.documentName.label"),'default':("Document Name")],-1)
printHtmlPart(6)
invokeTag('textField','g',46,['name':("documentName"),'value':(bankStatementArchiveInstance?.documentName)],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: bankStatementArchiveInstance, field: 'issueDate', 'error'))
printHtmlPart(10)
invokeTag('message','g',52,['code':("bankStatementArchive.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(6)
invokeTag('datePicker','g',55,['name':("issueDate"),'precision':("day"),'value':(bankStatementArchiveInstance?.issueDate),'default':("none"),'noSelection':(['': ''])],-1)
printHtmlPart(11)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
