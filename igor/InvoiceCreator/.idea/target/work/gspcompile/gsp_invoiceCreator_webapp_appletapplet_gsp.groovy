import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_webapp_appletapplet_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/applet/applet.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('applyLayout','g',8,['name':("mainApplet.gsp")],-1)
printHtmlPart(3)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',13,['code':("invoice.applet"),'default':("Invoice Signing Applet")],-1)
})
invokeTag('captureTitle','sitemesh',13,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',13,[:],2)
printHtmlPart(4)
expressionOut.print(params.downloadUrl)
printHtmlPart(5)
expressionOut.print(params.sessionId)
printHtmlPart(6)
expressionOut.print(params.docMongoId)
printHtmlPart(7)
expressionOut.print(params.documentSaveUrl)
printHtmlPart(8)
})
invokeTag('captureHead','sitemesh',35,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(9)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(10)
invokeTag('message','g',44,['code':("default.home.label")],-1)
printHtmlPart(11)
createTagBody(2, {->
printHtmlPart(12)
invokeTag('message','g',47,['code':("invoice.applet.backButton"),'default':("Back to Invoice")],-1)
printHtmlPart(13)
})
invokeTag('link','g',47,['class':("menuButton"),'controller':("invoice"),'action':("show"),'params':([id:session.invoiceId])],2)
printHtmlPart(14)
})
invokeTag('captureBody','sitemesh',49,[:],1)
printHtmlPart(15)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222879000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
