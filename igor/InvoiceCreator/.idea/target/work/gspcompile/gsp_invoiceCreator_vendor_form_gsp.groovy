import com.genrep.vendor.Vendor
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_vendor_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/vendor/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(2)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(3)
expressionOut.print(error.field)
printHtmlPart(4)
}
printHtmlPart(5)
invokeTag('message','g',9,['error':(error)],-1)
printHtmlPart(6)
})
invokeTag('eachError','g',10,['bean':(vendntInstance),'var':("error")],2)
printHtmlPart(7)
})
invokeTag('hasErrors','g',12,['bean':(vendorInstance)],1)
printHtmlPart(8)
expressionOut.print(hasErrors(bean: vendorInstance, field: 'name', 'error'))
printHtmlPart(9)
invokeTag('message','g',17,['code':("vendor.name.label"),'default':("Name")],-1)
printHtmlPart(10)
invokeTag('textField','g',22,['name':("name"),'class':("wider-fields"),'value':(vendorInstance?.name)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: vendorInstance, field: 'department', 'error'))
printHtmlPart(12)
invokeTag('message','g',30,['code':("vendor.department.label"),'default':("Department")],-1)
printHtmlPart(10)
invokeTag('textField','g',35,['name':("department"),'class':("wider-fields"),'value':(vendorInstance?.department)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: vendorInstance, field: 'address', 'error'))
printHtmlPart(13)
invokeTag('message','g',43,['code':("vendor.address.label"),'default':("Address")],-1)
printHtmlPart(10)
invokeTag('textField','g',48,['name':("address"),'class':("wider-fields"),'value':(vendorInstance?.address)],-1)
printHtmlPart(14)
expressionOut.print(hasErrors(bean: vendorInstance, field: 'bankAccountNumber', 'error'))
printHtmlPart(15)
invokeTag('message','g',55,['code':("vendor.bankAccountNumber.label"),'default':("Bank Account Number")],-1)
printHtmlPart(10)
invokeTag('textField','g',60,['name':("bankAccountNumber"),'class':("wider-fields"),'value':(vendorInstance?.bankAccountNumber)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: vendorInstance, field: 'viewInfo', 'error'))
printHtmlPart(16)
invokeTag('message','g',68,['code':("vendor.viewInfo.label"),'default':("Invoice Print")],-1)
printHtmlPart(17)
invokeTag('textArea','g',75,['name':("viewInfo"),'id':("viewInfo"),'cols':("40"),'rows':("5"),'maxlength':("500"),'value':(vendorInstance?.viewInfo)],-1)
printHtmlPart(18)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1429023074000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
