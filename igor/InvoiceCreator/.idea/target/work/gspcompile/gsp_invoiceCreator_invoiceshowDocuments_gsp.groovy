import com.genrep.invoiceCreator.ExchangeRate
import  com.genrep.invoiceCreator.Invoice
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoiceshowDocuments_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoice/showDocuments.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',12,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',13,['var':("entityName"),'value':(message(code: 'invoice.label', default: 'Invoice'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',14,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',14,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',14,[:],2)
printHtmlPart(2)
invokeTag('javascript','asset',14,['src':("invoice.js")],-1)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',14,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',22,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',23,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('link','g',23,['class':("menuButton"),'action':("index")],2)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',25,['code':("default.new.label"),'args':([entityName])],-1)
})
invokeTag('link','g',25,['class':("menuButton"),'action':("create")],2)
printHtmlPart(8)
createTagBody(2, {->
invokeTag('message','g',30,['code':("default.showDocuments.label"),'default':("Show Invoice Documents")],-1)
})
invokeTag('link','g',30,['action':("show"),'id':(invoiceId)],2)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
if(true && (invoiceDocuments)) {
printHtmlPart(13)
loop:{
int i = 0
for( invoiceDocument in (invoiceDocuments) ) {
printHtmlPart(14)
createTagBody(4, {->
printHtmlPart(15)
invokeTag('hiddenField','g',39,['name':("invoiceId"),'value':(invoiceId)],-1)
printHtmlPart(16)
createTagBody(5, {->
printHtmlPart(17)
expressionOut.print(invoiceDocument.documentName)
printHtmlPart(16)
})
invokeTag('link','g',42,['class':("doc-${invoiceDocument.mimeType.split('/')[1]}"),'controller':("worker"),'action':("readFileFromMongoDb"),'target':("_blank"),'params':([docMongoId: invoiceDocument.docMongoId, domain:'InvoiceDocument'])],5)
printHtmlPart(18)
expressionOut.print(invoiceDocument.dateCreated.format('dd.MM.yyyy'))
printHtmlPart(19)
invokeTag('actionSubmit','g',48,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(14)
})
invokeTag('form','g',48,['id':(invoiceDocument.id),'controller':("invoiceDocument"),'action':("delete"),'method':("post")],4)
printHtmlPart(20)
i++
}
}
printHtmlPart(21)
}
else {
printHtmlPart(22)
invokeTag('message','g',55,['code':("invoice.documents.notFound"),'default':("No documents found for this Invoice")],-1)
printHtmlPart(23)
}
printHtmlPart(24)
})
invokeTag('captureBody','sitemesh',55,[:],1)
printHtmlPart(25)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427888916000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
