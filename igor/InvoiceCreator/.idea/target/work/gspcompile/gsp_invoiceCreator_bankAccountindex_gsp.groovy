import com.genrep.account.BankAccount
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankAccountindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankAccount/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'bankAccount.label', default: 'BankAccount'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',13,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',14,['code':("bankAccount.create"),'default':("New Bank Account")],-1)
})
invokeTag('link','g',14,['class':("menuButton"),'action':("create")],2)
printHtmlPart(7)
invokeTag('message','g',18,['code':("default.list.label"),'args':([entityName])],-1)
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(11)
invokeTag('sortableColumn','g',26,['property':("entityName"),'title':(message(code: 'bankAccount.entityName.label', default: 'Entity Name'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',28,['property':("bankName"),'title':(message(code: 'bankAccount.bankName.label', default: 'Bank Name'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',30,['property':("bankAccountNumber"),'title':(message(code: 'bankAccount.bankAccountNumber.label', default: 'Bank Account Number'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',32,['property':("balance"),'title':(message(code: 'bankAccount.balance.label', default: 'Balance'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',34,['property':("owner"),'title':(message(code: 'bankAccount.owner.label', default: 'Is Owner'))],-1)
printHtmlPart(14)
loop:{
int i = 0
for( bankAccountInstance in (bankAccountInstanceList) ) {
printHtmlPart(15)
expressionOut.print((i % 2) == 0 ? 'even' : 'odd')
printHtmlPart(16)
createTagBody(3, {->
expressionOut.print(fieldValue(bean: bankAccountInstance, field: "entityName"))
})
invokeTag('link','g',41,['action':("show"),'id':(bankAccountInstance.id)],3)
printHtmlPart(17)
expressionOut.print(fieldValue(bean: bankAccountInstance, field: "bankName"))
printHtmlPart(17)
expressionOut.print(fieldValue(bean: bankAccountInstance, field: "bankAccountNumber"))
printHtmlPart(17)
expressionOut.print(fieldValue(bean: bankAccountInstance, field: "balance"))
printHtmlPart(18)
expressionOut.print(fieldValue(bean: bankAccountInstance, field: "owner"))
printHtmlPart(19)
i++
}
}
printHtmlPart(20)
invokeTag('paginate','g',56,['total':(bankAccountInstanceCount ?: 0)],-1)
printHtmlPart(21)
})
invokeTag('captureBody','sitemesh',59,[:],1)
printHtmlPart(22)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
