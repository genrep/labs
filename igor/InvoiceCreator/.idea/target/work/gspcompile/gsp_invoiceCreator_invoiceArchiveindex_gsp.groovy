import com.genrep.client.Client
import  com.genrep.invoiceRepository.InvoiceArchive
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoiceArchiveindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoiceArchive/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'invoiceArchive.label', default: 'InvoiceArchive'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',13,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',14,['code':("invoice.home.label"),'default':("Invoices")],-1)
})
invokeTag('link','g',14,['class':("menuButton"),'controller':("invoice"),'action':("index")],2)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',16,['code':("invoiceArchive.button.newImport"),'default':("New Import")],-1)
})
invokeTag('link','g',16,['class':("menuButton"),'action':("newImport")],2)
printHtmlPart(8)
invokeTag('message','g',20,['code':("default.list.label"),'args':([entityName])],-1)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
invokeTag('sortableColumn','g',28,['property':("documentName"),'title':(message(code: 'invoiceArchive.documentName.label', default: 'Name'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',30,['property':("invoiceNumber"),'title':(message(code: 'invoiceArchive.invoiceNumber.label', default: 'Invoice Number'))],-1)
printHtmlPart(14)
invokeTag('sortableColumn','g',32,['property':("client"),'title':(message(code: 'invoiceArchive.client.label', default: 'Client'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',34,['property':("totalSum"),'title':(message(code: 'invoiceArchive.totalSum.label', default: 'Total Sum'))],-1)
printHtmlPart(15)
invokeTag('sortableColumn','g',36,['property':("currency"),'title':(message(code: 'invoiceArchive.inCurrency.label', default: 'Currency'))],-1)
printHtmlPart(14)
invokeTag('sortableColumn','g',38,['property':("issueDate"),'title':(message(code: 'invoiceArchive.issueDate.label', default: 'Date '))],-1)
printHtmlPart(16)
loop:{
int i = 0
for( invoiceArchiveInstance in (invoiceArchiveInstanceList) ) {
printHtmlPart(17)
expressionOut.print((i % 2) == 0 ? 'even' : 'odd')
printHtmlPart(18)
createTagBody(3, {->
expressionOut.print(fieldValue(bean: invoiceArchiveInstance, field: "documentName"))
})
invokeTag('link','g',46,['action':("show"),'id':(invoiceArchiveInstance.id)],3)
printHtmlPart(19)
expressionOut.print(fieldValue(bean: invoiceArchiveInstance, field: "invoiceNumber"))
printHtmlPart(19)
expressionOut.print(Client.get(invoiceArchiveInstance?.client.id))
printHtmlPart(19)
expressionOut.print(invoiceArchiveInstance?.totalSum?.decodeDecimalNumber())
printHtmlPart(19)
expressionOut.print(fieldValue(bean: invoiceArchiveInstance, field: "inCurrency"))
printHtmlPart(20)
expressionOut.print(formatDate(date: invoiceArchiveInstance?.issueDate, format: 'dd/MM/yyyy'))
printHtmlPart(21)
i++
}
}
printHtmlPart(22)
invokeTag('paginate','g',63,['total':(invoiceArchiveInstanceCount ?: 0)],-1)
printHtmlPart(23)
})
invokeTag('captureBody','sitemesh',66,[:],1)
printHtmlPart(24)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1426501176000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
