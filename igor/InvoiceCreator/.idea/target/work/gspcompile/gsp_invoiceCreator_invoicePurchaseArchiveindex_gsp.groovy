import com.genrep.client.Client
import  com.genrep.invoiceRepository.InvoicePurchaseArchive
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoicePurchaseArchiveindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoicePurchaseArchive/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'invoicePurchaseArchive.label', default: 'InvoicePurchaseArchive'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(0)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(3)
invokeTag('message','g',11,['code':("default.link.skip.label"),'default':("Skip to content&hellip;")],-1)
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',14,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',16,['code':("invoiceArchive.button.newImport"),'default':("New Import")],-1)
})
invokeTag('link','g',16,['class':("menuButton"),'action':("newImport")],2)
printHtmlPart(7)
invokeTag('message','g',20,['code':("default.list.label"),'args':([entityName])],-1)
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(11)
invokeTag('sortableColumn','g',28,['property':("documentName"),'title':(message(code: 'invoicePurchaseArchive.documentName.label', default: 'Name'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',30,['property':("admissionCounterNumberView"),'title':(message(code: 'invoicePurchaseArchive.admissionCounterNumberView.label', default: 'Admission Number'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',32,['property':("invoiceNumber"),'title':(message(code: 'invoicePurchaseArchive.invoiceNumber.label', default: 'Invoice Number'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',34,['property':("vendor"),'title':(message(code: 'invoicePurchaseArchive.vendor.label', default: 'Vendor'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',36,['property':("totalSum"),'title':(message(code: 'invoicePurchaseArchive.totalSum.label', default: 'Total Sum'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',38,['property':("currency"),'title':(message(code: 'invoicePurchaseArchive.inCurrency.label', default: 'Currency'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',40,['property':("issueDate"),'title':(message(code: 'invoicePurchaseArchive.issueDate.label', default: 'Date '))],-1)
printHtmlPart(13)
loop:{
int i = 0
for( invoicePurchaseArchiveInstance in (invoicePurchaseArchiveInstanceList) ) {
printHtmlPart(14)
expressionOut.print((i % 2) == 0 ? 'even' : 'odd')
printHtmlPart(15)
createTagBody(3, {->
printHtmlPart(16)
expressionOut.print(fieldValue(bean: invoicePurchaseArchiveInstance, field: "documentName"))
printHtmlPart(17)
})
invokeTag('link','g',49,['action':("show"),'id':(invoicePurchaseArchiveInstance.id)],3)
printHtmlPart(18)
expressionOut.print(fieldValue(bean: invoicePurchaseArchiveInstance, field: "admissionCounterNumberView"))
printHtmlPart(18)
expressionOut.print(fieldValue(bean: invoicePurchaseArchiveInstance, field: "invoiceNumber"))
printHtmlPart(18)
expressionOut.print(com.genrep.vendor.Vendor.get(invoicePurchaseArchiveInstance?.vendor?.id))
printHtmlPart(18)
expressionOut.print(invoicePurchaseArchiveInstance?.totalSum?.decodeDecimalNumber())
printHtmlPart(18)
expressionOut.print(fieldValue(bean: invoicePurchaseArchiveInstance, field: "inCurrency"))
printHtmlPart(18)
expressionOut.print(formatDate(date: invoicePurchaseArchiveInstance?.issueDate, format: 'dd/MM/yyyy'))
printHtmlPart(19)
i++
}
}
printHtmlPart(20)
invokeTag('paginate','g',67,['total':(invoicePurchaseArchiveInstanceCount ?: 0)],-1)
printHtmlPart(21)
})
invokeTag('captureBody','sitemesh',70,[:],1)
printHtmlPart(22)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1428590762000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
