import com.genrep.invoiceCreator.Invoice
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoiceedit_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoice/edit.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',6,['var':("entityName"),'value':(message(code: 'invoice.label', default: 'Invoice'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',7,['code':("default.edit.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',7,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',7,[:],2)
printHtmlPart(2)
invokeTag('javascript','asset',9,['src':("invoice.js")],-1)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',11,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',17,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',18,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('link','g',18,['class':("menuButton"),'action':("index")],2)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',20,['code':("default.new.label"),'args':([entityName])],-1)
})
invokeTag('link','g',20,['class':("menuButton"),'action':("create")],2)
printHtmlPart(8)
invokeTag('message','g',25,['message':(invoiceInstance?.name)],-1)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(12)
createTagBody(3, {->
printHtmlPart(13)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(14)
expressionOut.print(error.field)
printHtmlPart(15)
}
printHtmlPart(16)
invokeTag('message','g',33,['error':(error)],-1)
printHtmlPart(17)
})
invokeTag('eachError','g',34,['bean':(invoiceInstance),'var':("error")],3)
printHtmlPart(18)
})
invokeTag('hasErrors','g',36,['bean':(invoiceInstance)],2)
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(19)
invokeTag('hiddenField','g',38,['name':("version"),'value':(invoiceInstance?.version)],-1)
printHtmlPart(20)
invokeTag('render','g',40,['template':("editForm")],-1)
printHtmlPart(21)
invokeTag('actionSubmit','g',44,['class':("save"),'action':("update"),'value':(message(code: 'default.button.update.label', default: 'Update'))],-1)
printHtmlPart(22)
})
invokeTag('form','g',46,['url':([resource: invoiceInstance, action: 'update']),'method':("PUT")],2)
printHtmlPart(23)
invokeTag('include','g',49,['view':("client/_form.gsp")],-1)
printHtmlPart(24)
})
invokeTag('captureBody','sitemesh',52,[:],1)
printHtmlPart(25)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427896597000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
