import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementcreate_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatement/create.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'bankStatement.label', default: 'BankStatement'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.create.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(1)
invokeTag('javascript','asset',7,['src':("statement.js")],-1)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',8,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',14,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',15,['code':("bankStatement.list"),'default':("List Bank Statement")],-1)
})
invokeTag('link','g',15,['class':("menuButton"),'action':("index")],2)
printHtmlPart(7)
invokeTag('message','g',20,['code':("bankStatement.create.label"),'default':("Add New Bank Statement")],-1)
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(11)
createTagBody(3, {->
printHtmlPart(12)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(13)
expressionOut.print(error.field)
printHtmlPart(14)
}
printHtmlPart(15)
invokeTag('message','g',28,['error':(error)],-1)
printHtmlPart(16)
})
invokeTag('eachError','g',29,['bean':(bankStatementInstance),'var':("error")],3)
printHtmlPart(17)
})
invokeTag('hasErrors','g',31,['bean':(bankStatementInstance)],2)
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(18)
invokeTag('render','g',34,['template':("form")],-1)
printHtmlPart(19)
})
invokeTag('form','g',36,['id':("formBankStatement"),'url':([resource: bankStatementInstance, action: 'save'])],2)
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(18)
invokeTag('render','g',39,['template':("formItems")],-1)
printHtmlPart(19)
})
invokeTag('form','g',41,['id':("formItems"),'url':([resource: bankStatementItemInstance, action: 'addBankStatementItem'])],2)
printHtmlPart(20)
invokeTag('submitButton','g',44,['name':("create"),'class':("save"),'form':("formBankStatement"),'value':(message(code: 'default.button.create.label', default: 'Create'))],-1)
printHtmlPart(21)
invokeTag('include','g',48,['view':("bankAccount/_form.gsp")],-1)
printHtmlPart(22)
})
invokeTag('captureBody','sitemesh',51,[:],1)
printHtmlPart(23)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
