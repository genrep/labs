import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_project_editFileForm_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/project/_editFileForm.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
if(true && (projectDocumentInstance?.documentFile)) {
printHtmlPart(0)
invokeTag('message','g',4,['code':("invoiceArchive.documentFile.label"),'default':("Document File")],-1)
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(2)
expressionOut.print(projectDocumentInstance?.documentName?:'Project Document')
printHtmlPart(3)
})
invokeTag('link','g',9,['controller':("project"),'action':("showProjectDocumentFile"),'target':("_blank"),'id':(projectDocumentInstance?.id)],2)
printHtmlPart(4)
}
else {
printHtmlPart(5)
expressionOut.print(hasErrors(bean: projectDocumentInstance, field: 'documentFile', 'error'))
printHtmlPart(6)
invokeTag('message','g',17,['code':("project.documentFile.label"),'default':("Document File")],-1)
printHtmlPart(7)
}
printHtmlPart(8)
if(true && (projectDocumentInstance?.documentName)) {
printHtmlPart(9)
invokeTag('message','g',29,['code':("project.documentName.label"),'default':("Document Name")],-1)
printHtmlPart(10)
invokeTag('fieldValue','g',32,['bean':(projectDocumentInstance),'field':("documentName")],-1)
printHtmlPart(11)
}
else {
printHtmlPart(5)
expressionOut.print(hasErrors(bean: projectDocumentInstance, field: 'documentName', 'error'))
printHtmlPart(12)
invokeTag('message','g',41,['code':("project.documentName.label"),'default':("Document Name")],-1)
printHtmlPart(13)
invokeTag('textField','g',45,['class':("wider-fields"),'name':("documentName"),'id':("documentName"),'value':(projectDocumentInstance?.documentName)],-1)
printHtmlPart(4)
}
printHtmlPart(8)
if(true && (projectDocumentInstance?.documentFile)) {
printHtmlPart(14)
expressionOut.print(projectDocumentInstance?.id)
printHtmlPart(15)
}
printHtmlPart(8)
if(true && (undoDocumentId)) {
printHtmlPart(16)
expressionOut.print(undoDocumentId)
printHtmlPart(17)
}
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427727896000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
