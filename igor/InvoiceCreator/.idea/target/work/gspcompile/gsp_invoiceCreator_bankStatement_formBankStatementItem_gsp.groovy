import com.genrep.statements.BankStatementItem
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatement_formBankStatementItem_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatement/_formBankStatementItem.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(2)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(3)
expressionOut.print(error.field)
printHtmlPart(4)
}
printHtmlPart(5)
invokeTag('message','g',8,['error':(error)],-1)
printHtmlPart(6)
})
invokeTag('eachError','g',9,['bean':(bankStatementItemInstance),'var':("error")],2)
printHtmlPart(7)
})
invokeTag('hasErrors','g',11,['bean':(bankStatementItemInstance)],1)
printHtmlPart(8)
expressionOut.print(hasErrors(bean: bankStatementItemInstance, field: 'publicEntity', 'error'))
printHtmlPart(9)
invokeTag('message','g',16,['code':("bankStatementItem.publicEntity.label"),'default':("Public Entity")],-1)
printHtmlPart(10)
invokeTag('textField','g',20,['name':("publicEntity"),'id':("publicEntity"),'required':(""),'value':(bankStatementItemInstance?.publicEntity)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: bankStatementItemInstance, field: 'publicEntityBankAccountNumber', 'error'))
printHtmlPart(12)
invokeTag('message','g',27,['code':("bankStatementItem.publicEntityBankAccountNumber.label"),'default':("Public Entity Bank Acco. No.")],-1)
printHtmlPart(10)
invokeTag('textField','g',32,['name':("publicEntityBankAccountNumber"),'id':("publicEntityBankAccountNumber"),'required':(""),'value':(bankStatementItemInstance?.publicEntityBankAccountNumber)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: bankStatementItemInstance, field: 'transferSum', 'error'))
printHtmlPart(13)
invokeTag('message','g',39,['code':("bankStatementItem.transferSum.label"),'default':("Transfer Sum")],-1)
printHtmlPart(10)
invokeTag('priceAndTaxField','g',43,['id':("transferSum"),'name':("transferSum"),'required':(""),'value':(bankStatementItemInstance?.transferSum)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: bankStatementItemInstance, field: 'shortDescription', 'error'))
printHtmlPart(14)
invokeTag('message','g',50,['code':("bankStatementItem.shortDescription.label"),'default':("Short Desc.")],-1)
printHtmlPart(10)
invokeTag('textArea','g',54,['name':("shortDescription"),'id':("shortDescription"),'required':(""),'value':(bankStatementItemInstance?.shortDescription)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: bankStatementItemInstance, field: 'transactionCode', 'error'))
printHtmlPart(15)
invokeTag('message','g',61,['code':("bankStatementItem.transactionCode.label"),'default':("Trans. Code")],-1)
printHtmlPart(10)
invokeTag('textField','g',65,['name':("transactionCode"),'id':("transactionCode"),'required':(""),'value':(bankStatementItemInstance?.transactionCode)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: bankStatementItemInstance, field: 'borrowingCode', 'error'))
printHtmlPart(16)
invokeTag('message','g',72,['code':("bankStatementItem.borrowingCode.label"),'default':("Borrowing Code")],-1)
printHtmlPart(10)
invokeTag('textField','g',76,['id':("borrowingCode"),'name':("borrowingCode"),'required':(""),'value':(bankStatementItemInstance?.borrowingCode)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: bankStatementItemInstance, field: 'approvalCode', 'error'))
printHtmlPart(16)
invokeTag('message','g',83,['code':("bankStatementItem.approvalCode.label"),'default':("Approval Code")],-1)
printHtmlPart(10)
invokeTag('textField','g',87,['id':("approvalCode"),'name':("approvalCode"),'required':(""),'value':(bankStatementItemInstance?.approvalCode)],-1)
printHtmlPart(17)
expressionOut.print(form)
printHtmlPart(18)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
