import com.genrep.statements.BankStatementArchive
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementArchiveshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatementArchive/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'bankStatementArchive.label', default: 'BankStatementArchive'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
invokeTag('message','g',11,['code':("default.link.skip.label"),'default':("Skip to content&hellip;")],-1)
printHtmlPart(5)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(6)
invokeTag('message','g',14,['code':("default.home.label")],-1)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',15,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('link','g',15,['class':("list"),'action':("index")],2)
printHtmlPart(8)
createTagBody(2, {->
invokeTag('message','g',16,['code':("default.new.label"),'args':([entityName])],-1)
})
invokeTag('link','g',16,['class':("create"),'action':("create")],2)
printHtmlPart(9)
invokeTag('message','g',20,['code':("default.show.label"),'args':([entityName])],-1)
printHtmlPart(10)
if(true && (flash.message)) {
printHtmlPart(11)
expressionOut.print(flash.message)
printHtmlPart(12)
}
printHtmlPart(13)
if(true && (bankStatementArchiveInstance?.docMongoId)) {
printHtmlPart(14)
invokeTag('message','g',28,['code':("bankStatementArchive.docMongoId.label"),'default':("Doc Mongo Id")],-1)
printHtmlPart(15)
invokeTag('fieldValue','g',30,['bean':(bankStatementArchiveInstance),'field':("docMongoId")],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (bankStatementArchiveInstance?.bankStatementId)) {
printHtmlPart(18)
invokeTag('message','g',37,['code':("bankStatementArchive.bankStatementId.label"),'default':("Bank Statement Id")],-1)
printHtmlPart(19)
invokeTag('fieldValue','g',39,['bean':(bankStatementArchiveInstance),'field':("bankStatementId")],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (bankStatementArchiveInstance?.ownerOfStatement)) {
printHtmlPart(20)
invokeTag('message','g',46,['code':("bankStatementArchive.ownerOfStatement.label"),'default':("Owner Of Statement")],-1)
printHtmlPart(21)
createTagBody(3, {->
expressionOut.print(bankStatementArchiveInstance?.ownerOfStatement?.encodeAsHTML())
})
invokeTag('link','g',48,['controller':("bankAccount"),'action':("show"),'id':(bankStatementArchiveInstance?.ownerOfStatement?.id)],3)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (bankStatementArchiveInstance?.documentFile)) {
printHtmlPart(22)
invokeTag('message','g',55,['code':("bankStatementArchive.documentFile.label"),'default':("Document File")],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (bankStatementArchiveInstance?.documentName)) {
printHtmlPart(23)
invokeTag('message','g',62,['code':("bankStatementArchive.documentName.label"),'default':("Document Name")],-1)
printHtmlPart(24)
invokeTag('fieldValue','g',64,['bean':(bankStatementArchiveInstance),'field':("documentName")],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (bankStatementArchiveInstance?.dateCreated)) {
printHtmlPart(25)
invokeTag('message','g',71,['code':("bankStatementArchive.dateCreated.label"),'default':("Date Created")],-1)
printHtmlPart(26)
invokeTag('formatDate','g',73,['date':(bankStatementArchiveInstance?.dateCreated)],-1)
printHtmlPart(16)
}
printHtmlPart(17)
if(true && (bankStatementArchiveInstance?.issueDate)) {
printHtmlPart(27)
invokeTag('message','g',80,['code':("bankStatementArchive.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(28)
invokeTag('formatDate','g',82,['date':(bankStatementArchiveInstance?.issueDate)],-1)
printHtmlPart(16)
}
printHtmlPart(29)
createTagBody(2, {->
printHtmlPart(30)
createTagBody(3, {->
invokeTag('message','g',90,['code':("default.button.edit.label"),'default':("Edit")],-1)
})
invokeTag('link','g',90,['class':("edit"),'action':("edit"),'resource':(bankStatementArchiveInstance)],3)
printHtmlPart(31)
invokeTag('actionSubmit','g',91,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(32)
})
invokeTag('form','g',93,['url':([resource:bankStatementArchiveInstance, action:'delete']),'method':("DELETE")],2)
printHtmlPart(33)
})
invokeTag('captureBody','sitemesh',95,[:],1)
printHtmlPart(34)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
