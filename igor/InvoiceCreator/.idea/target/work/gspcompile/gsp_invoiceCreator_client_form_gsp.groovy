import com.genrep.client.Client
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_client_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/client/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(2)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(3)
expressionOut.print(error.field)
printHtmlPart(4)
}
printHtmlPart(5)
invokeTag('message','g',9,['error':(error)],-1)
printHtmlPart(6)
})
invokeTag('eachError','g',10,['bean':(clientInstance),'var':("error")],2)
printHtmlPart(7)
})
invokeTag('hasErrors','g',12,['bean':(clientInstance)],1)
printHtmlPart(8)
expressionOut.print(hasErrors(bean: clientInstance, field: 'name', 'error'))
printHtmlPart(9)
invokeTag('message','g',17,['code':("client.name.label"),'default':("Name")],-1)
printHtmlPart(10)
invokeTag('textField','g',22,['name':("name"),'value':(clientInstance?.name)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: clientInstance, field: 'department', 'error'))
printHtmlPart(12)
invokeTag('message','g',30,['code':("client.department.label"),'default':("Department")],-1)
printHtmlPart(10)
invokeTag('textField','g',35,['name':("department"),'value':(clientInstance?.department)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: clientInstance, field: 'address', 'error'))
printHtmlPart(13)
invokeTag('message','g',43,['code':("client.address.label"),'default':("Address")],-1)
printHtmlPart(10)
invokeTag('textField','g',48,['name':("address"),'value':(clientInstance?.address)],-1)
printHtmlPart(14)
expressionOut.print(hasErrors(bean: clientInstance, field: 'bankAccountNumber', 'error'))
printHtmlPart(15)
invokeTag('message','g',55,['code':("client.bankAccountNumber.label"),'default':("Bank Account Number")],-1)
printHtmlPart(10)
invokeTag('textField','g',60,['name':("bankAccountNumber"),'value':(clientInstance?.bankAccountNumber)],-1)
printHtmlPart(11)
expressionOut.print(hasErrors(bean: clientInstance, field: 'viewInfo', 'error'))
printHtmlPart(16)
invokeTag('message','g',68,['code':("client.viewInfo.label"),'default':("Invoice Print")],-1)
printHtmlPart(17)
invokeTag('textArea','g',76,['name':("viewInfo"),'id':("viewInfo"),'cols':("40"),'rows':("5"),'maxlength':("500"),'value':(clientInstance?.viewInfo)],-1)
printHtmlPart(18)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423232944000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
