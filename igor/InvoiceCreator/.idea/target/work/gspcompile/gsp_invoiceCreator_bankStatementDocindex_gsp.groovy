import com.genrep.statements.BankStatementDoc
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementDocindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatementDoc/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'bankStatementDoc.label', default: 'BankStatementDoc'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(3)
invokeTag('javascript','asset',9,['src':("statement.js")],-1)
printHtmlPart(4)
})
invokeTag('captureHead','sitemesh',10,[:],1)
printHtmlPart(4)
createTagBody(1, {->
printHtmlPart(5)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(6)
invokeTag('message','g',15,['code':("default.home.label")],-1)
printHtmlPart(7)
expressionOut.print(createLink(uri: '/statements'))
printHtmlPart(6)
invokeTag('message','g',16,['code':("bankStatement.home.label"),'default':("Statements")],-1)
printHtmlPart(8)
createTagBody(2, {->
invokeTag('message','g',17,['code':("bankStatmentDoc.new"),'default':("Import Bank Statement")],-1)
})
invokeTag('link','g',17,['class':("menuButton"),'action':("create")],2)
printHtmlPart(9)
createTagBody(2, {->
invokeTag('message','g',18,['code':("bankStatmentDoc.multiImport"),'default':("Import Multi Statements")],-1)
})
invokeTag('link','g',18,['class':("menuButton"),'action':("multiImport")],2)
printHtmlPart(10)
invokeTag('message','g',22,['code':("bankStatementDoc.list.label"),'default':("Imported Documents Of Bank Statements")],-1)
printHtmlPart(11)
if(true && (flash.message)) {
printHtmlPart(12)
expressionOut.print(flash.message)
printHtmlPart(13)
}
printHtmlPart(14)
invokeTag('sortableColumn','g',30,['property':("statementDoc"),'title':(message(code: 'bankStatementDoc.documentFile.label', default: 'Statement Doc'))],-1)
printHtmlPart(15)
invokeTag('sortableColumn','g',32,['property':("isTransferred"),'title':(message(code: 'bankStatementDoc.isTransferred.label', default: 'Is Transferred'))],-1)
printHtmlPart(15)
invokeTag('sortableColumn','g',34,['property':("issueDate"),'title':(message(code: 'bankStatementDoc.issueDate.label', default: 'Issue Date'))],-1)
printHtmlPart(15)
invokeTag('sortableColumn','g',36,['property':("dateCreated"),'title':(message(code: 'bankStatementDoc.dateCreated.label', default: 'Date Created'))],-1)
printHtmlPart(16)
invokeTag('message','g',38,['code':("statements.action"),'default':("Action")],-1)
printHtmlPart(17)
loop:{
int i = 0
for( bankStatementDocInstance in (bankStatementDocInstanceList) ) {
printHtmlPart(18)
expressionOut.print((i % 2) == 0 ? 'even' : 'odd')
printHtmlPart(19)
if(true && (bankStatementDocInstance.mimeType!=null)) {
printHtmlPart(20)
createTagBody(4, {->
printHtmlPart(21)
expressionOut.print(bankStatementDocInstance?.documentName)
printHtmlPart(22)
})
invokeTag('link','g',51,['class':("doc-${bankStatementDocInstance.mimeType.split('/')[1]}"),'controller':("worker"),'action':("readFileFromMongoDb"),'target':("_blank"),'params':([docMongoId: bankStatementDocInstance.uuid, domain: 'BankStatementDoc'])],4)
printHtmlPart(23)
}
else {
printHtmlPart(20)
createTagBody(4, {->
printHtmlPart(21)
expressionOut.print(bankStatementDocInstance?.documentName)
printHtmlPart(22)
})
invokeTag('link','g',57,['controller':("worker"),'action':("readFileFromMongoDb"),'target':("_blank"),'params':([docMongoId: bankStatementDocInstance.uuid, domain: 'BankStatementDoc'])],4)
printHtmlPart(23)
}
printHtmlPart(24)
invokeTag('formatBoolean','g',60,['boolean':(bankStatementDocInstance.isTransferred)],-1)
printHtmlPart(25)
invokeTag('formatDate','g',62,['date':(bankStatementDocInstance.issueDate),'format':("dd.MM.yyyy")],-1)
printHtmlPart(25)
invokeTag('formatDate','g',64,['date':(bankStatementDocInstance.dateCreated),'format':("dd.MM.yyyy")],-1)
printHtmlPart(26)
createTagBody(3, {->
invokeTag('message','g',66,['code':("statements.action.parse"),'default':("Show Details")],-1)
printHtmlPart(22)
})
invokeTag('link','g',67,['action':("showBankStatementDetails"),'id':(bankStatementDocInstance.id)],3)
printHtmlPart(27)
createTagBody(3, {->
printHtmlPart(28)
invokeTag('actionSubmit','g',71,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(29)
})
invokeTag('form','g',73,['url':([resource:bankStatementDocInstance, action:'delete']),'method':("DELETE")],3)
printHtmlPart(30)
i++
}
}
printHtmlPart(31)
invokeTag('paginate','g',80,['total':(bankStatementDocInstanceCount ?: 0)],-1)
printHtmlPart(32)
})
invokeTag('captureBody','sitemesh',83,[:],1)
printHtmlPart(33)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
