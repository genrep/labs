import com.genrep.client.Client
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_clientshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/client/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'client.label', default: 'Client'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(0)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(3)
invokeTag('message','g',11,['code':("default.link.skip.label"),'default':("Skip to content&hellip;")],-1)
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',14,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',15,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('link','g',15,['class':("menuButton"),'action':("index")],2)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',16,['code':("default.new.label"),'args':([entityName])],-1)
})
invokeTag('link','g',16,['class':("menuButton"),'action':("create")],2)
printHtmlPart(8)
invokeTag('message','g',20,['code':("default.show.label"),'args':([entityName])],-1)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
if(true && (clientInstance?.name)) {
printHtmlPart(13)
invokeTag('message','g',29,['code':("client.name.label"),'default':("Name")],-1)
printHtmlPart(14)
invokeTag('fieldValue','g',31,['bean':(clientInstance),'field':("name")],-1)
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (clientInstance?.department)) {
printHtmlPart(17)
invokeTag('message','g',38,['code':("client.department.label"),'default':("Department")],-1)
printHtmlPart(18)
invokeTag('fieldValue','g',40,['bean':(clientInstance),'field':("department")],-1)
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (clientInstance?.address)) {
printHtmlPart(19)
invokeTag('message','g',47,['code':("client.address.label"),'default':("Address")],-1)
printHtmlPart(20)
invokeTag('fieldValue','g',49,['bean':(clientInstance),'field':("address")],-1)
printHtmlPart(15)
}
printHtmlPart(21)
if(true && (clientInstance?.bankAccount)) {
printHtmlPart(22)
invokeTag('message','g',57,['code':("client.bankAccountNumber.label"),'default':("Bank Account No.")],-1)
printHtmlPart(23)
expressionOut.print(clientInstance.bankAccount.bankAccountNumber)
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (clientInstance?.viewInfo)) {
printHtmlPart(24)
invokeTag('message','g',66,['code':("client.viewInfo.label"),'default':("Invoice Print")],-1)
printHtmlPart(25)
expressionOut.print(raw(clientInstance.viewInfo))
printHtmlPart(15)
}
printHtmlPart(26)
createTagBody(2, {->
printHtmlPart(27)
createTagBody(3, {->
invokeTag('message','g',76,['code':("default.button.edit.label"),'default':("Edit")],-1)
})
invokeTag('link','g',76,['class':("edit"),'action':("edit"),'resource':(clientInstance)],3)
printHtmlPart(28)
invokeTag('actionSubmit','g',77,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(29)
})
invokeTag('form','g',79,['url':([resource:clientInstance, action:'delete']),'method':("DELETE")],2)
printHtmlPart(30)
})
invokeTag('captureBody','sitemesh',81,[:],1)
printHtmlPart(31)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423233969000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
