import com.genrep.invoiceRepository.Taxes
import  com.genrep.client.Client
import  com.genrep.invoiceRepository.InvoicePurchaseArchive
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoicePurchaseArchiveshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoicePurchaseArchive/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'invoicePurchaseArchive.label', default: 'InvoicePurchaseArchive'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(0)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(3)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(4)
invokeTag('message','g',13,['code':("default.home.label")],-1)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('message','g',14,['code':("invoiceArchive.button.index"),'default':("List Of Imported Invoices")],-1)
})
invokeTag('link','g',14,['class':("menuButton"),'action':("index")],2)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',16,['code':("invoiceArchive.button.newImport"),'default':("New Import")],-1)
})
invokeTag('link','g',16,['class':("menuButton"),'action':("newImport")],2)
printHtmlPart(7)
invokeTag('message','g',20,['code':("default.show.label"),'args':([entityName])],-1)
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(11)
if(true && (invoicePurchaseArchiveInstance?.docMongoId)) {
printHtmlPart(12)
invokeTag('message','g',28,['code':("invoicePurchaseArchive.docMongoId.label"),'default':("Doc Mongo Id")],-1)
printHtmlPart(13)
invokeTag('fieldValue','g',30,['bean':(invoicePurchaseArchiveInstance),'field':("docMongoId")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.invoiceNumber)) {
printHtmlPart(16)
if(true && (invoicePurchaseArchiveInstance?.isProInvoice)) {
printHtmlPart(17)
invokeTag('message','g',38,['code':("invoicePurchaseArchive.proInvoiceNumber.label"),'default':("Pro Invoice Number")],-1)
printHtmlPart(18)
}
else {
printHtmlPart(17)
invokeTag('message','g',41,['code':("invoicePurchaseArchive.invoiceNumber.label"),'default':("Invoice Number")],-1)
printHtmlPart(18)
}
printHtmlPart(19)
invokeTag('fieldValue','g',44,['bean':(invoicePurchaseArchiveInstance),'field':("invoiceNumber")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.admissionCounterNumberView)) {
printHtmlPart(20)
invokeTag('message','g',51,['code':("invoicePurchaseArchive.admissionCounterNumberView.label"),'default':("Admission Number")],-1)
printHtmlPart(21)
invokeTag('fieldValue','g',53,['bean':(invoicePurchaseArchiveInstance),'field':("admissionCounterNumberView")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.swiftCode)) {
printHtmlPart(22)
invokeTag('message','g',60,['code':("invoicePurchaseArchive.swiftCode.label"),'default':("Swift Code")],-1)
printHtmlPart(23)
invokeTag('fieldValue','g',62,['bean':(invoicePurchaseArchiveInstance),'field':("swiftCode")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.vendor?.id)) {
printHtmlPart(24)
invokeTag('message','g',69,['code':("invoicePurchaseArchive.vendor.label"),'default':("Vendor")],-1)
printHtmlPart(25)
createTagBody(3, {->
printHtmlPart(26)
expressionOut.print(com.genrep.vendor.Vendor.get(invoicePurchaseArchiveInstance?.vendor.id).toString())
printHtmlPart(27)
})
invokeTag('link','g',74,['controller':("vendor"),'action':("show"),'id':(invoicePurchaseArchiveInstance?.vendor?.id)],3)
printHtmlPart(28)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.refInvoice)) {
printHtmlPart(16)
if(true && (invoicePurchaseArchiveInstance?.isProInvoice)) {
printHtmlPart(29)
invokeTag('message','g',83,['code':("invoicePurchaseArchive.ProRefInvoice.label"),'default':("Reference to Invoice")],-1)
printHtmlPart(18)
}
else {
printHtmlPart(29)
invokeTag('message','g',86,['code':("invoicePurchaseArchive.refInvoice.label"),'default':("Reference to Pro Invoice")],-1)
printHtmlPart(18)
}
printHtmlPart(30)
createTagBody(3, {->
printHtmlPart(26)
expressionOut.print(invoicePurchaseArchiveInstance?.refInvoice?.invoiceNumber)
printHtmlPart(27)
})
invokeTag('link','g',92,['controller':("invoicePurchaseArchive"),'action':("show"),'id':(invoicePurchaseArchiveInstance?.refInvoice?.id)],3)
printHtmlPart(31)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.invoiceDescription)) {
printHtmlPart(32)
invokeTag('message','g',99,['code':("invoicePurchaseArchive.invoiceDescription.label"),'default':("Invoice Description")],-1)
printHtmlPart(33)
invokeTag('fieldValue','g',103,['bean':(invoicePurchaseArchiveInstance),'field':("invoiceDescription")],-1)
printHtmlPart(34)
}
printHtmlPart(35)
if(true && (invoicePurchaseArchiveInstance?.totalSum)) {
printHtmlPart(36)
invokeTag('message','g',110,['code':("invoicePurchaseArchive.totalSum.label"),'default':("Total Sum")],-1)
printHtmlPart(37)
expressionOut.print(invoicePurchaseArchiveInstance?.totalSum?.decodeDecimalNumber())
printHtmlPart(31)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.taxFreeSum)) {
printHtmlPart(38)
invokeTag('message','g',120,['code':("invoicePurchaseArchive.taxFreeSum.label"),'default':("Tax Free Sum")],-1)
printHtmlPart(39)
expressionOut.print(invoicePurchaseArchiveInstance?.taxFreeSum?.decodeDecimalNumber())
printHtmlPart(31)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.taxPriceSum)) {
printHtmlPart(40)
invokeTag('message','g',130,['code':("invoicePurchaseArchive.taxPriceSum.label"),'default':("Tax Price Sum")],-1)
printHtmlPart(41)
expressionOut.print(invoicePurchaseArchiveInstance?.taxPriceSum?.decodeDecimalNumber())
printHtmlPart(31)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.taxes)) {
printHtmlPart(42)
if(true && (invoicePurchaseArchiveInstance?.taxes?.containsKey(com.genrep.invoiceRepository.Taxes.ZERO))) {
printHtmlPart(43)
invokeTag('message','g',141,['code':("invoicePurchaseArchive.taxes.zero"),'default':("Tax (0%)")],-1)
printHtmlPart(44)
expressionOut.print(invoicePurchaseArchiveInstance?.taxes?.get(Taxes.ZERO).decodeDecimalNumber())
printHtmlPart(45)
}
printHtmlPart(46)
if(true && (invoicePurchaseArchiveInstance?.taxes.containsKey(Taxes.FIVE))) {
printHtmlPart(43)
invokeTag('message','g',150,['code':("invoicePurchaseArchive.taxes.five"),'default':("Tax (5%)")],-1)
printHtmlPart(44)
expressionOut.print(invoicePurchaseArchiveInstance?.taxes?.get(Taxes.FIVE).decodeDecimalNumber())
printHtmlPart(45)
}
printHtmlPart(46)
if(true && (invoicePurchaseArchiveInstance?.taxes.containsKey(Taxes.EIGHTEEN))) {
printHtmlPart(43)
invokeTag('message','g',159,['code':("invoicePurchaseArchive.taxes.eigtheen"),'default':("Tax (18%)")],-1)
printHtmlPart(44)
expressionOut.print(invoicePurchaseArchiveInstance?.taxes?.get(Taxes.EIGHTEEN).decodeDecimalNumber())
printHtmlPart(45)
}
printHtmlPart(47)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.inCurrency)) {
printHtmlPart(48)
invokeTag('message','g',169,['code':("invoicePurchaseArchive.inCurrency.label"),'default':("Currency")],-1)
printHtmlPart(49)
invokeTag('fieldValue','g',171,['bean':(invoicePurchaseArchiveInstance),'field':("inCurrency")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.exchangeRate)) {
printHtmlPart(50)
invokeTag('message','g',178,['code':("invoicePurchaseArchive.exchangeRate.label"),'default':("Exchange Rate")],-1)
printHtmlPart(51)
createTagBody(3, {->
expressionOut.print(invoicePurchaseArchiveInstance?.exchangeRate?.encodeAsHTML())
})
invokeTag('link','g',180,['controller':("exchangeRate"),'action':("show"),'id':(invoicePurchaseArchiveInstance?.exchangeRate?.id)],3)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.documentFile)) {
printHtmlPart(52)
invokeTag('message','g',187,['code':("invoicePurchaseArchive.documentFile.label"),'default':("Document File")],-1)
printHtmlPart(53)
createTagBody(3, {->
printHtmlPart(27)
expressionOut.print(invoicePurchaseArchiveInstance?.documentName?:'Invoice Document')
printHtmlPart(54)
})
invokeTag('link','g',191,['controller':("invoicePurchaseArchive"),'action':("showInvoicePurchaseArchiveFile"),'target':("_blank"),'id':(invoicePurchaseArchiveInstance?.id)],3)
printHtmlPart(31)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.documentName)) {
printHtmlPart(55)
invokeTag('message','g',198,['code':("invoicePurchaseArchive.documentName.label"),'default':("Document Name")],-1)
printHtmlPart(56)
invokeTag('fieldValue','g',200,['bean':(invoicePurchaseArchiveInstance),'field':("documentName")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.issueDate)) {
printHtmlPart(57)
invokeTag('message','g',207,['code':("invoicePurchaseArchive.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(58)
invokeTag('formatDate','g',210,['format':("dd.MM.yyyy"),'date':(invoicePurchaseArchiveInstance?.issueDate)],-1)
printHtmlPart(31)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.paymentDate)) {
printHtmlPart(59)
invokeTag('message','g',217,['code':("invoicePurchaseArchive.paymentDate.label"),'default':("Payment Date")],-1)
printHtmlPart(60)
invokeTag('formatDate','g',220,['format':("dd.MM.yyyy"),'date':(invoicePurchaseArchiveInstance?.paymentDate)],-1)
printHtmlPart(31)
}
printHtmlPart(15)
if(true && (invoicePurchaseArchiveInstance?.dateCreated)) {
printHtmlPart(61)
invokeTag('message','g',227,['code':("invoicePurchaseArchive.dateCreated.label"),'default':("Date Created")],-1)
printHtmlPart(62)
invokeTag('formatDate','g',230,['format':("dd.MM.yyyy"),'date':(invoicePurchaseArchiveInstance?.dateCreated)],-1)
printHtmlPart(31)
}
printHtmlPart(63)
createTagBody(2, {->
printHtmlPart(64)
createTagBody(3, {->
invokeTag('message','g',238,['code':("default.button.edit.label"),'default':("Edit")],-1)
})
invokeTag('link','g',238,['class':("edit"),'action':("edit"),'resource':(invoicePurchaseArchiveInstance)],3)
printHtmlPart(42)
invokeTag('actionSubmit','g',239,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(65)
})
invokeTag('form','g',241,['url':([resource:invoicePurchaseArchiveInstance, action:'delete']),'method':("DELETE")],2)
printHtmlPart(66)
})
invokeTag('captureBody','sitemesh',243,[:],1)
printHtmlPart(67)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1428590710000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
