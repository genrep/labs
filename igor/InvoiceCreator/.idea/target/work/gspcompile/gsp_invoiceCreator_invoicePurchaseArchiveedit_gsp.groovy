import com.genrep.invoiceRepository.InvoicePurchaseArchive
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoicePurchaseArchiveedit_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoicePurchaseArchive/edit.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',6,['var':("entityName"),'value':(message(code: 'invoicePurchaseArchive.label', default: 'InvoicePurchaseArchive'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',7,['code':("default.edit.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',7,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',7,[:],2)
printHtmlPart(2)
invokeTag('javascript','asset',8,['src':("invoice.js")],-1)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',13,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',14,['code':("invoiceArchive.button.index"),'default':("List Of Imported Invoices")],-1)
})
invokeTag('link','g',14,['class':("menuButton"),'action':("index")],2)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',16,['code':("invoiceArchive.button.newImport"),'default':("New Import")],-1)
})
invokeTag('link','g',16,['class':("menuButton"),'action':("newImport")],2)
printHtmlPart(8)
invokeTag('message','g',20,['code':("default.edit.label"),'args':([entityName])],-1)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
createTagBody(2, {->
printHtmlPart(13)
createTagBody(3, {->
printHtmlPart(14)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(15)
expressionOut.print(error.field)
printHtmlPart(16)
}
printHtmlPart(17)
invokeTag('message','g',27,['error':(error)],-1)
printHtmlPart(18)
})
invokeTag('eachError','g',28,['bean':(invoicePurchaseArchiveInstance),'var':("error")],3)
printHtmlPart(19)
})
invokeTag('hasErrors','g',30,['bean':(invoicePurchaseArchiveInstance)],2)
printHtmlPart(20)
createTagBody(2, {->
printHtmlPart(21)
invokeTag('hiddenField','g',32,['name':("version"),'value':(invoicePurchaseArchiveInstance?.version)],-1)
printHtmlPart(22)
invokeTag('render','g',34,['template':("editForm")],-1)
printHtmlPart(23)
invokeTag('render','g',38,['template':("editFileForm")],-1)
printHtmlPart(24)
invokeTag('submitButton','g',42,['name':("update"),'class':("save"),'value':(message(code: 'default.button.update.label', default: 'Update'))],-1)
printHtmlPart(25)
})
invokeTag('uploadForm','g',44,['url':([resource:invoicePurchaseArchiveInstance, action:'update']),'method':("post")],2)
printHtmlPart(26)
})
invokeTag('captureBody','sitemesh',46,[:],1)
printHtmlPart(27)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1426266195000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
