import com.genrep.invoiceCreator.Invoice
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_reportquartal_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/report/quartal.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',6,['var':("entityName"),'value':(message(code: 'invoice.label', default: 'Invoice'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',7,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',7,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',7,[:],2)
printHtmlPart(1)
invokeTag('javascript','asset',8,['src':("invoice.js")],-1)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',15,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
printHtmlPart(7)
invokeTag('message','g',17,['code':("invoice.report.backButton"),'default':("Back to Reports")],-1)
printHtmlPart(8)
})
invokeTag('link','g',18,['class':("menuButton"),'controller':("report"),'action':("index")],2)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
invokeTag('multiDomainFilterPane','gnrp',29,['domains':("com.genrep.invoiceCreator.Invoice,com.genrep.invoiceRepository.InvoiceArchive"),'filterParams':("issueDate,client,inCurrency")],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',36,['property':("invoiceNumber"),'title':(message(code: 'invoice.invoiceNumber.label', default: 'Invoice Number'))],-1)
printHtmlPart(14)
invokeTag('sortableColumn','g',39,['property':("aClass"),'title':(message(code: 'invoice.invoiceNumber.label', default: 'Type'))],-1)
printHtmlPart(15)
invokeTag('message','g',41,['code':("invoice.client.label"),'default':("Client")],-1)
printHtmlPart(16)
invokeTag('sortableColumn','g',44,['property':("issueDate"),'title':(message(code: 'invoice.issueDate.label', default: 'Issue Date'))],-1)
printHtmlPart(17)
invokeTag('sortableColumn','g',48,['property':("totalSum"),'title':(message(code: 'invoice.totalSum.label', default: 'Total Sum'))],-1)
printHtmlPart(14)
invokeTag('sortableColumn','g',51,['property':("currency"),'title':(message(code: 'invoice.totalSum.label', default: 'Currency'))],-1)
printHtmlPart(18)
loop:{
int i = 0
for( invoiceInstance in (reportList) ) {
printHtmlPart(19)
expressionOut.print((i % 2) == 0 ? 'even' : 'odd')
printHtmlPart(20)
expressionOut.print(fieldValue(bean: invoiceInstance, field: "invoiceNumber"))
printHtmlPart(21)
expressionOut.print(fieldValue(bean: invoiceInstance, field: "aClass"))
printHtmlPart(21)
expressionOut.print(fieldValue(bean: invoiceInstance, field: "client"))
printHtmlPart(22)
invokeTag('formatDate','g',67,['date':(invoiceInstance.issueDate),'format':("dd.MM.yyyy")],-1)
printHtmlPart(22)
expressionOut.print(fieldValue(bean: invoiceInstance, field: "totalSum"))
printHtmlPart(21)
expressionOut.print(fieldValue(bean: invoiceInstance, field: "currency"))
printHtmlPart(23)
i++
}
}
printHtmlPart(24)
invokeTag('paginate','g',81,['total':(invoiceInstanceCount ?: 0),'params':(params)],-1)
printHtmlPart(25)
if(true && (showStats==true)) {
printHtmlPart(26)
invokeTag('render','g',86,['template':("reportStatistics")],-1)
printHtmlPart(27)
}
printHtmlPart(28)
})
invokeTag('captureBody','sitemesh',90,[:],1)
printHtmlPart(29)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
