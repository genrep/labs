import com.genrep.statements.BankStatement
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementeditBankStatementItems_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatement/editBankStatementItems.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',6,['var':("entityName"),'value':(message(code: 'bankStatement.label', default: 'BankStatement'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',7,['code':("default.edit.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',7,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',7,[:],2)
printHtmlPart(1)
invokeTag('javascript','asset',8,['src':("statement.js")],-1)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
invokeTag('message','g',13,['code':("default.link.skip.label"),'default':("Skip to content&hellip;")],-1)
printHtmlPart(5)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(6)
invokeTag('message','g',17,['code':("default.home.label")],-1)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',18,['code':("bankStatement.list"),'default':("List Bank Statement")],-1)
})
invokeTag('link','g',18,['class':("menuButton"),'action':("index")],2)
printHtmlPart(8)
invokeTag('message','g',24,['code':("bankStatement.editItems.label"),'default':("Edit Bank Statement Items")],-1)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(12)
createTagBody(3, {->
printHtmlPart(13)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(14)
expressionOut.print(error.field)
printHtmlPart(15)
}
printHtmlPart(16)
invokeTag('message','g',32,['error':(error)],-1)
printHtmlPart(17)
})
invokeTag('eachError','g',33,['bean':(bankStatementInstance),'var':("error")],3)
printHtmlPart(18)
})
invokeTag('hasErrors','g',35,['bean':(bankStatementInstance)],2)
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(19)
invokeTag('render','g',38,['template':("formItems")],-1)
printHtmlPart(20)
})
invokeTag('form','g',39,['id':("formItems"),'url':([resource: bankStatementItemInstance, action: 'addBankStatementItem'])],2)
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(21)
invokeTag('actionSubmit','g',43,['class':("save"),'action':("updateBankStatementItems"),'value':(message(code: 'default.button.update.label', default: 'Update'))],-1)
printHtmlPart(22)
})
invokeTag('form','g',45,['id':("formItems"),'url':([resource: bankStatementInstance, action: 'updateBankStatementItems'])],2)
printHtmlPart(23)
})
invokeTag('captureBody','sitemesh',47,[:],1)
printHtmlPart(24)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
