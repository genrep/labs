import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementDocshowBankStatementDetails_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatementDoc/showBankStatementDetails.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',14,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',15,['var':("entityName"),'value':(message(code: 'bankStatementDoc.label', default: 'BankStatementDoc'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',16,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',16,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',16,[:],2)
printHtmlPart(2)
invokeTag('javascript','asset',16,['src':("statement.js")],-1)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',16,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',22,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',23,['code':("bankStatmentDoc.list"),'default':("List Bank Statements")],-1)
})
invokeTag('link','g',23,['class':("menuButton"),'action':("index")],2)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',24,['code':("bankStatmentDoc.new"),'default':("New Bank Statement")],-1)
})
invokeTag('link','g',24,['class':("menuButton"),'action':("create")],2)
printHtmlPart(8)
invokeTag('message','g',29,['code':("bankStatement.showDetails.label"),'default':("Bank Statement Detailed Information")],-1)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
createTagBody(2, {->
printHtmlPart(13)
createTagBody(3, {->
printHtmlPart(14)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(15)
expressionOut.print(error.field)
printHtmlPart(16)
}
printHtmlPart(17)
invokeTag('message','g',37,['error':(error)],-1)
printHtmlPart(18)
})
invokeTag('eachError','g',37,['bean':(bankStatementInstance),'var':("error")],3)
printHtmlPart(19)
})
invokeTag('hasErrors','g',37,['bean':(bankStatementInstance)],2)
printHtmlPart(20)
if(true && (bankStatementInstance?.bankStatementId)) {
printHtmlPart(21)
invokeTag('message','g',46,['code':("bankStatement.bankStatementId.label"),'default':("Bank Statement ID")],-1)
printHtmlPart(22)
invokeTag('fieldValue','g',48,['bean':(bankStatementInstance),'field':("bankStatementId")],-1)
printHtmlPart(23)
}
printHtmlPart(24)
if(true && (bankStatementInstance?.issueDate)) {
printHtmlPart(25)
invokeTag('message','g',55,['code':("bankStatement.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(26)
invokeTag('formatDate','g',57,['date':(bankStatementInstance?.issueDate),'format':("dd.MM.yyyy")],-1)
printHtmlPart(23)
}
printHtmlPart(24)
if(true && (bankStatementInstance?.bankAccountNumber)) {
printHtmlPart(27)
invokeTag('message','g',64,['code':("bankStatement.bankAccountNumber.label"),'default':("Account Number")],-1)
printHtmlPart(22)
invokeTag('fieldValue','g',66,['bean':(bankStatementInstance),'field':("bankAccountNumber")],-1)
printHtmlPart(23)
}
printHtmlPart(24)
if(true && (bankStatementInstance?.balanceBefore)) {
printHtmlPart(28)
invokeTag('message','g',73,['code':("bankStatement.balanceBefore.label"),'default':("Balance Before")],-1)
printHtmlPart(22)
expressionOut.print(bankStatementInstance.balanceBefore.decodeDecimalNumber())
printHtmlPart(23)
}
printHtmlPart(24)
if(true && (bankStatementInstance?.balanceAfter)) {
printHtmlPart(29)
invokeTag('message','g',82,['code':("bankStatement.balanceAfter.label"),'default':("Balance After")],-1)
printHtmlPart(22)
expressionOut.print(bankStatementInstance.balanceAfter.decodeDecimalNumber())
printHtmlPart(23)
}
printHtmlPart(24)
invokeTag('render','g',86,['template':("tableOfBankStatementItems")],-1)
printHtmlPart(30)
invokeTag('message','g',92,['code':("bankStatement.total.label"),'default':("Total")],-1)
printHtmlPart(31)
expressionOut.print(total?.decodeDecimalNumber())
printHtmlPart(32)
if(true && (saved)) {
printHtmlPart(33)
createTagBody(3, {->
printHtmlPart(34)
invokeTag('actionSubmit','g',101,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(35)
})
invokeTag('form','g',101,['url':([resource:bankStatementDocInstance, action:'delete']),'method':("DELETE")],3)
printHtmlPart(2)
}
else {
printHtmlPart(33)
createTagBody(3, {->
printHtmlPart(34)
createTagBody(4, {->
printHtmlPart(36)
invokeTag('message','g',108,['code':("default.button.save.label"),'default':("Save")],-1)
printHtmlPart(37)
})
invokeTag('submitButton','g',109,['name':("save"),'class':("save")],4)
printHtmlPart(35)
})
invokeTag('form','g',109,['method':("post"),'action':("saveBankStatement"),'onsubmit':("return checkIfBankAccountExists('${bankStatementInstance.bankAccountNumber}')")],3)
printHtmlPart(2)
}
printHtmlPart(38)
invokeTag('include','g',115,['view':("bankAccount/_form.gsp")],-1)
printHtmlPart(39)
})
invokeTag('captureBody','sitemesh',116,[:],1)
printHtmlPart(40)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
