import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoice_formInvoiceBody_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoice/_formInvoiceBody.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
if(true && (invoiceInstance?.invoiceBodyIn)) {
printHtmlPart(1)
if(true && (invoiceInstance?.invoiceBodyIn?.invoiceItems)) {
printHtmlPart(2)
invokeTag('set','g',22,['var':("size"),'value':(invoiceInstance.invoiceBodyIn?.invoiceItems.size())],-1)
printHtmlPart(3)
loop:{
int i = 0
for( invoiceItem in (invoiceInstance.invoiceBodyIn?.invoiceItems) ) {
printHtmlPart(4)
expressionOut.print((invoiceItem.getEditableFields() as grails.converters.JSON).toString())
printHtmlPart(5)
if(true && (i>0 && i < size-1)) {
printHtmlPart(6)
createTagBody(5, {->
invokeTag('img','g',27,['dir':("images"),'file':("arrow-up.png"),'alter':("Up")],-1)
})
invokeTag('link','g',27,['url':("#"),'onclick':("updateInvoiceItemPosition(${i},'up')")],5)
printHtmlPart(6)
createTagBody(5, {->
invokeTag('img','g',28,['dir':("images"),'file':("arrow-down.png"),'alter':("Down")],-1)
})
invokeTag('link','g',28,['url':("#"),'onclick':("updateInvoiceItemPosition(${i},'down','edit')")],5)
printHtmlPart(7)
}
else if(true && (i == 0 && size > 1)) {
printHtmlPart(6)
createTagBody(5, {->
invokeTag('img','g',31,['dir':("images"),'file':("arrow-down.png"),'alter':("Down")],-1)
})
invokeTag('link','g',31,['url':("#"),'onclick':("updateInvoiceItemPosition(${i},'down')")],5)
printHtmlPart(7)
}
else if(true && (i == size-1 && size > 1)) {
printHtmlPart(6)
createTagBody(5, {->
invokeTag('img','g',34,['dir':("images"),'file':("arrow-up.png"),'alter':("Up")],-1)
})
invokeTag('link','g',34,['url':("#"),'onclick':("updateInvoiceItemPosition(${i},'up')")],5)
printHtmlPart(7)
}
else {
printHtmlPart(8)
}
printHtmlPart(9)
expressionOut.print(raw(invoiceItem?.description))
printHtmlPart(10)
expressionOut.print(invoiceItem?.quantity)
printHtmlPart(10)
expressionOut.print(invoiceItem?.unitPrice.decodeDecimalNumber())
printHtmlPart(10)
expressionOut.print(invoiceItem?.tax.value)
printHtmlPart(10)
expressionOut.print(invoiceItem?.taxPricePerUnit.decodeDecimalNumber())
printHtmlPart(10)
expressionOut.print(invoiceItem?.taxFreePrice.decodeDecimalNumber())
printHtmlPart(10)
expressionOut.print(invoiceItem?.taxPrice.decodeDecimalNumber())
printHtmlPart(10)
expressionOut.print(invoiceItem?.totalPrice.decodeDecimalNumber())
printHtmlPart(11)
createTagBody(4, {->
invokeTag('img','g',51,['dir':("images"),'file':("delete.png"),'alter':("Delete")],-1)
})
invokeTag('remoteLink','g',51,['controller':("invoiceItem"),'action':("deleteInvoiceItem"),'id':(invoiceItem?.id),'params':([sessionId:invoiceItem.sessionId.toString()]),'update':("invoiceBody")],4)
printHtmlPart(12)
i++
}
}
printHtmlPart(13)
expressionOut.print(invoiceInstance.invoiceBodyIn?.taxFreeSum.decodeDecimalNumber())
printHtmlPart(14)
expressionOut.print(invoiceInstance.invoiceBodyIn?.taxFreeSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber())
printHtmlPart(15)
expressionOut.print(invoiceInstance.invoiceBodyIn?.taxPriceSum.decodeDecimalNumber())
printHtmlPart(16)
expressionOut.print(invoiceInstance.invoiceBodyIn?.taxPriceSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber())
printHtmlPart(17)
expressionOut.print(invoiceInstance.invoiceBodyIn?.totalSum.setScale(2,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber())
printHtmlPart(16)
expressionOut.print(invoiceInstance.invoiceBodyIn?.getTotalSumRounded().decodeDecimalNumber())
printHtmlPart(18)
}
printHtmlPart(19)
}
printHtmlPart(20)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427815145000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
