import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_report_reportStatistics_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/report/_reportStatistics.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
invokeTag('message','g',4,['code':("reports.quartal.totalInvoices"),'default':("Number of Invoices : ")],-1)
printHtmlPart(1)
expressionOut.print(statMap.get('size'))
printHtmlPart(2)
invokeTag('message','g',9,['code':("reports.quartal.currency"),'default':("Currency : ")],-1)
printHtmlPart(1)
expressionOut.print(params.inCurrency)
printHtmlPart(2)
invokeTag('message','g',14,['code':("reports.quartal.totalSum"),'default':("Total Sum (with tax) : ")],-1)
printHtmlPart(1)
expressionOut.print(statMap.get('totalSum').decodeDecimalNumber())
printHtmlPart(2)
invokeTag('message','g',19,['code':("reports.quartal.taxFreeSum"),'default':("Total Sum (without tax) : ")],-1)
printHtmlPart(1)
expressionOut.print(statMap.get('taxFreeSum').decodeDecimalNumber())
printHtmlPart(2)
invokeTag('message','g',24,['code':("reports.quartal.taxPriceSum"),'default':("Total Sum of Tax : ")],-1)
printHtmlPart(1)
expressionOut.print(statMap.get('taxPriceSum').decodeDecimalNumber())
printHtmlPart(3)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
