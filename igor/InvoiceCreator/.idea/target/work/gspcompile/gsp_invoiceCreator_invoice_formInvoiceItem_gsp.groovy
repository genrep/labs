import com.genrep.invoiceRepository.Taxes
import  com.genrep.invoiceCreator.InvoiceItem
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoice_formInvoiceItem_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoice/_formInvoiceItem.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: invoiceItemInstance, field: 'description', 'error'))
printHtmlPart(1)
invokeTag('message','g',10,['code':("invoiceItem.description.label"),'default':("Description")],-1)
printHtmlPart(2)
invokeTag('textArea','g',18,['name':("description"),'id':("description"),'cols':("40"),'rows':("5"),'maxlength':("500"),'value':(""),'data-message':(message(code:'invoiceItem.invoiceDescription.error',default:'Description is obligatory'))],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean: invoiceItemInstance, field: 'quantity', 'error'))
printHtmlPart(4)
invokeTag('message','g',26,['code':("invoiceItem.quantity.label"),'default':("Quantity")],-1)
printHtmlPart(5)
invokeTag('field','g',31,['name':("quantity"),'id':("quantity"),'type':("number"),'min':("0"),'value':(""),'data-message':(message(code:'invoiceItem.quantity.error',default:'Fill in the quantity number'))],-1)
printHtmlPart(6)
expressionOut.print(hasErrors(bean: invoiceItemInstance, field: 'unitPrice', 'error'))
printHtmlPart(7)
invokeTag('message','g',39,['code':("invoiceItem.unitPrice.label"),'default':("Unit Price")],-1)
printHtmlPart(5)
invokeTag('priceAndTaxField','g',43,['id':("unitPrice"),'data-message':(message(code:'invoiceItem.unitPrice.error',default:'Fill in the unit price'))],-1)
printHtmlPart(8)
expressionOut.print(hasErrors(bean: invoiceItemInstance, field: 'tax', 'error'))
printHtmlPart(9)
invokeTag('message','g',51,['code':("invoiceItem.tax.label"),'default':("Tax (%)")],-1)
printHtmlPart(5)
invokeTag('select','g',59,['id':("tax"),'name':("tax"),'from':(Taxes.values()),'required':(""),'optionValue':("value"),'value':(invoiceItemInstance?.tax),'data-message':(message(code:'invoiceItem.tax.error',default:'Fill in the tax value')),'class':("many-to-one")],-1)
printHtmlPart(10)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1429097434000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
