import com.genrep.statements.BankStatementDoc
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementDoc_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatementDoc/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: bankStatementDocInstance, field: 'documentFile', 'error'))
printHtmlPart(1)
invokeTag('message','g',7,['code':("bankStatementDoc.statementDoc.label"),'default':("Statement Doc")],-1)
printHtmlPart(2)
expressionOut.print(hasErrors(bean: bankStatementDocInstance, field: 'documentName', 'error'))
printHtmlPart(3)
invokeTag('message','g',19,['code':("bankStatementDocInstance.documentName.label"),'default':("Document Name")],-1)
printHtmlPart(4)
invokeTag('textField','g',23,['class':("wider-fields"),'name':("documentName"),'id':("documentName"),'value':(bankStatementDocInstance?.documentName)],-1)
printHtmlPart(5)
expressionOut.print(hasErrors(bean: bankStatementDocInstance, field: 'issueDate', 'error'))
printHtmlPart(6)
invokeTag('message','g',30,['code':("bankStatementDoc.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(7)
expressionOut.print(formatDate(date:bankStatementDocInstance.issueDate,format: 'dd.MM.yyyy'))
printHtmlPart(8)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
