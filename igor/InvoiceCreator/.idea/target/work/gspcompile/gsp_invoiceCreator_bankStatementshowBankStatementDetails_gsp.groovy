import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementshowBankStatementDetails_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatement/showBankStatementDetails.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',13,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',14,['var':("entityName"),'value':(message(code: 'bankStatementDoc.label', default: 'BankStatementDoc'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',15,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',15,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',15,[:],2)
printHtmlPart(2)
invokeTag('javascript','asset',15,['src':("statement.js")],-1)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',15,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',21,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',22,['code':("bankStatmentDoc.list"),'default':("List Bank Statements")],-1)
})
invokeTag('link','g',22,['class':("menuButton"),'action':("index")],2)
printHtmlPart(7)
invokeTag('message','g',26,['code':("bankStatement.showDetails.label"),'default':("Bank Statement Detailed Information")],-1)
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(11)
if(true && (bankStatementInstance?.bankStatementId)) {
printHtmlPart(12)
invokeTag('message','g',35,['code':("bankStatement.bankStatementId.label"),'default':("Bank Statement ID")],-1)
printHtmlPart(13)
invokeTag('fieldValue','g',37,['bean':(bankStatementInstance),'field':("bankStatementId")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (bankStatementInstance?.issueDate)) {
printHtmlPart(16)
invokeTag('message','g',44,['code':("bankStatement.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(17)
invokeTag('formatDate','g',46,['date':(bankStatementInstance?.issueDate),'format':("dd.MM.yyyy")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (bankStatementInstance?.bankAccountNumber)) {
printHtmlPart(18)
invokeTag('message','g',53,['code':("bankStatement.bankAccountNumber.label"),'default':("Account Number")],-1)
printHtmlPart(13)
invokeTag('fieldValue','g',55,['bean':(bankStatementInstance),'field':("bankAccountNumber")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (bankStatementInstance?.balanceBefore)) {
printHtmlPart(19)
invokeTag('message','g',62,['code':("bankStatement.balanceBefore.label"),'default':("Balance Before")],-1)
printHtmlPart(13)
expressionOut.print(bankStatementInstance.balanceBefore.decodeDecimalNumber())
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (bankStatementInstance?.balanceAfter)) {
printHtmlPart(20)
invokeTag('message','g',71,['code':("bankStatement.balanceAfter.label"),'default':("Balance After")],-1)
printHtmlPart(13)
expressionOut.print(bankStatementInstance.balanceAfter.decodeDecimalNumber())
printHtmlPart(14)
}
printHtmlPart(15)
invokeTag('render','g',76,['template':("/bankStatementDoc/tableOfBankStatementItems")],-1)
printHtmlPart(21)
invokeTag('message','g',81,['code':("bankStatement.total.label"),'default':("Total")],-1)
printHtmlPart(22)
expressionOut.print(total.decodeDecimalNumber())
printHtmlPart(23)
})
invokeTag('captureBody','sitemesh',82,[:],1)
printHtmlPart(24)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
