import com.genrep.project.Project
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_projectindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/project/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'project.label', default: 'Project'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',13,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',14,['code':("default.new.label"),'args':([entityName])],-1)
})
invokeTag('link','g',14,['controller':("project"),'class':("menuButton"),'action':("create")],2)
printHtmlPart(7)
invokeTag('message','g',18,['code':("default.list.label"),'args':([entityName])],-1)
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(11)
invokeTag('sortableColumn','g',26,['property':("technicalNumber"),'title':(message(code: 'project.technicalNumber.label', default: 'Technical Number'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',28,['property':("title"),'title':(message(code: 'project.title.label', default: 'Title'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',30,['property':("contractNumber"),'title':(message(code: 'project.contractNumber.label', default: 'Contract Number'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',32,['property':("contractNumberBearer"),'title':(message(code: 'project.contractNumberBearer.label', default: 'Contract Number Bearer'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',34,['property':("price"),'title':(message(code: 'project.price.label', default: 'Price'))],-1)
printHtmlPart(12)
invokeTag('sortableColumn','g',36,['property':("tax"),'title':(message(code: 'project.tax.label', default: 'Tax (%)'))],-1)
printHtmlPart(13)
loop:{
int i = 0
for( projectInstance in (projectInstanceList) ) {
printHtmlPart(14)
expressionOut.print((i % 2) == 0 ? 'even' : 'odd')
printHtmlPart(15)
createTagBody(3, {->
expressionOut.print(fieldValue(bean: projectInstance, field: "technicalNumber"))
})
invokeTag('link','g',44,['controller':("project"),'action':("show"),'id':(projectInstance.id)],3)
printHtmlPart(16)
expressionOut.print(fieldValue(bean: projectInstance, field: "title"))
printHtmlPart(16)
expressionOut.print(fieldValue(bean: projectInstance, field: "contractNumber"))
printHtmlPart(16)
expressionOut.print(fieldValue(bean: projectInstance, field: "contractNumberBearer"))
printHtmlPart(16)
expressionOut.print(fieldValue(bean: projectInstance, field: "price"))
printHtmlPart(16)
expressionOut.print(projectInstance?.tax?.value)
printHtmlPart(17)
i++
}
}
printHtmlPart(18)
invokeTag('paginate','g',61,['total':(projectInstanceCount ?: 0)],-1)
printHtmlPart(19)
})
invokeTag('captureBody','sitemesh',64,[:],1)
printHtmlPart(20)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1429097603000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
