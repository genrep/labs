import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoicePurchaseArchive_editFileForm_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoicePurchaseArchive/_editFileForm.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
if(true && (invoicePurchaseArchiveInstance?.documentFile)) {
printHtmlPart(0)
invokeTag('message','g',3,['code':("invoicePurchaseArchive.documentFile.label"),'default':("Document File")],-1)
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(2)
expressionOut.print(invoicePurchaseArchiveInstance?.documentName?:'Invoice Document')
printHtmlPart(3)
})
invokeTag('link','g',7,['controller':("invoicePurchaseArchive"),'action':("showInvoicePurchaseArchiveFile"),'target':("_blank"),'id':(invoicePurchaseArchiveInstance?.id)],2)
printHtmlPart(4)
}
else {
printHtmlPart(5)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'documentFile', 'error'))
printHtmlPart(6)
invokeTag('message','g',15,['code':("invoicePurchaseArchive.documentFile.label"),'default':("Document File")],-1)
printHtmlPart(7)
}
printHtmlPart(8)
if(true && (invoicePurchaseArchiveInstance?.documentName)) {
printHtmlPart(9)
invokeTag('message','g',26,['code':("invoicePurchaseArchive.documentName.label"),'default':("Document Name")],-1)
printHtmlPart(10)
invokeTag('fieldValue','g',28,['bean':(invoicePurchaseArchiveInstance),'field':("documentName")],-1)
printHtmlPart(11)
}
else {
printHtmlPart(5)
expressionOut.print(hasErrors(bean: invoicePurchaseArchiveInstance, field: 'documentName', 'error'))
printHtmlPart(12)
invokeTag('message','g',36,['code':("invoicePurchaseArchive.documentName.label"),'default':("Document Name")],-1)
printHtmlPart(13)
invokeTag('textField','g',40,['class':("wider-fields"),'name':("documentName"),'id':("documentName"),'value':(invoicePurchaseArchiveInstance?.documentName)],-1)
printHtmlPart(4)
}
printHtmlPart(8)
if(true && (invoicePurchaseArchiveInstance?.documentFile)) {
printHtmlPart(14)
expressionOut.print(invoicePurchaseArchiveInstance.id)
printHtmlPart(15)
}
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1425573434000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
