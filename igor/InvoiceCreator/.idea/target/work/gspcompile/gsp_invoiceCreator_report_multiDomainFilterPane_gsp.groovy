import com.genrep.invoiceCreator.Currencies
import  com.genrep.client.Client
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_report_multiDomainFilterPane_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/report/_multiDomainFilterPane.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(fp.containerId)
printHtmlPart(1)
invokeTag('set','g',6,['var':("renderForm"),'value':("true")],-1)
printHtmlPart(2)
if(true && (renderForm)) {
printHtmlPart(3)
expressionOut.print(fp.formName)
printHtmlPart(4)
expressionOut.print(fp.formName)
printHtmlPart(5)
expressionOut.print(fp.formMethod)
printHtmlPart(6)
expressionOut.print(createLink(action: fp.formAction))
printHtmlPart(7)
expressionOut.print(fp.domains)
printHtmlPart(8)
}
printHtmlPart(2)
for( propMap in (fp.properties) ) {
printHtmlPart(9)
if(true && (propMap.key.toString().equals("issueDate"))) {
printHtmlPart(10)
invokeTag('set','g',18,['var':("paramF"),'value':(params."${propMap.key}From")],-1)
printHtmlPart(10)
invokeTag('set','g',19,['var':("paramT"),'value':(params."${propMap.key}To")],-1)
printHtmlPart(10)
invokeTag('set','g',20,['var':("param"),'value':(params."${propMap.key}")],-1)
printHtmlPart(11)
}
else if(true && (propMap.key.toString().equals("client"))) {
printHtmlPart(10)
invokeTag('set','g',26,['var':("param"),'value':(params."${propMap.key}")],-1)
printHtmlPart(12)
}
else if(true && (propMap.key.toString().equals("invoiceNumber"))) {
printHtmlPart(10)
invokeTag('set','g',32,['var':("param"),'value':(params."${propMap.key}")],-1)
printHtmlPart(13)
}
else if(true && (propMap.key.toString().equals("inCurrency"))) {
printHtmlPart(10)
invokeTag('set','g',38,['var':("curr"),'value':(params."${propMap.key}")],-1)
printHtmlPart(10)
if(true && (curr!=null)) {
printHtmlPart(14)
invokeTag('set','g',40,['var':("param"),'value':(Currencies."${curr}".currency)],-1)
printHtmlPart(10)
}
else {
printHtmlPart(14)
invokeTag('set','g',43,['var':("param"),'value':(null)],-1)
printHtmlPart(10)
}
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (propMap.value.toString().equals("class java.lang.String"))) {
printHtmlPart(17)
expressionOut.print(propMap.key)
printHtmlPart(4)
expressionOut.print(propMap.key)
printHtmlPart(18)
expressionOut.print(param)
printHtmlPart(19)
}
else if(true && (propMap.value.toString().equals("class com.genrep.client.Client"))) {
printHtmlPart(10)
invokeTag('select','g',60,['id':(propMap.key),'name':(propMap.key),'from':(Client.list()),'optionKey':("id"),'optionValue':("name"),'noSelection':(['':'']),'value':(param),'class':("many-to-one")],-1)
printHtmlPart(20)
}
else if(true && (propMap.value.toString().equals("class java.util.Date"))) {
printHtmlPart(21)
expressionOut.print(propMap.key)
printHtmlPart(4)
expressionOut.print(propMap.key)
printHtmlPart(18)
expressionOut.print(param)
printHtmlPart(22)
expressionOut.print(propMap.key)
printHtmlPart(23)
expressionOut.print(propMap.key)
printHtmlPart(24)
expressionOut.print(propMap.key)
printHtmlPart(25)
expressionOut.print(paramF)
printHtmlPart(26)
expressionOut.print(propMap.key.toString())
printHtmlPart(27)
expressionOut.print(propMap.key)
printHtmlPart(28)
expressionOut.print(propMap.key)
printHtmlPart(29)
expressionOut.print(propMap.key)
printHtmlPart(30)
expressionOut.print(paramT)
printHtmlPart(26)
expressionOut.print(propMap.key.toString())
printHtmlPart(31)
}
else if(true && (propMap.value.toString().equals("class java.util.Currency"))) {
printHtmlPart(10)
if(true && (param)) {
printHtmlPart(14)
invokeTag('currencySelect','g',76,['id':(propMap.key),'name':(propMap.key),'value':(param),'noSelection':([null:'']),'from':(Currencies.values())],-1)
printHtmlPart(10)
}
else {
printHtmlPart(14)
invokeTag('currencySelect','g',82,['id':(propMap.key),'name':(propMap.key),'value':(param),'noSelection':([null:'']),'optionKey':("currency"),'from':(Currencies.values())],-1)
printHtmlPart(10)
}
printHtmlPart(20)
}
printHtmlPart(32)
}
printHtmlPart(33)
if(true && (params?.noImported && params?.noImported.equals('on'))) {
printHtmlPart(20)
invokeTag('checkBox','g',95,['name':("noImported"),'checked':("true")],-1)
printHtmlPart(34)
}
else {
printHtmlPart(20)
invokeTag('checkBox','g',98,['name':("noImported"),'checked':("false")],-1)
printHtmlPart(34)
}
printHtmlPart(35)
invokeTag('actionSubmit','g',104,['class':("btn saveButton"),'value':("Apply"),'action':(fp?.action)],-1)
printHtmlPart(36)
expressionOut.print(fp.containerId.toString())
printHtmlPart(37)
if(true && (renderForm)) {
printHtmlPart(38)
}
printHtmlPart(39)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427890006000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
