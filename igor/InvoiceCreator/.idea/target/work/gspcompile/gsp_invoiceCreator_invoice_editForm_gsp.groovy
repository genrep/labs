import com.genrep.invoiceCreator.ExchangeRate
import  com.genrep.invoiceCreator.Currencies
import  com.genrep.invoiceCreator.Invoice
import com.genrep.client.Client
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoice_editForm_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoice/_editForm.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'client', 'error'))
printHtmlPart(2)
invokeTag('message','g',7,['code':("invoice.client.label"),'default':("Client")],-1)
printHtmlPart(3)
invokeTag('select','g',14,['id':("client"),'name':("client.id"),'from':(Client.list()),'optionKey':("id"),'optionValue':("name"),'required':(""),'value':(invoiceInstance?.client?.id),'class':("comboBox")],-1)
printHtmlPart(4)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'invoiceDescription', 'error'))
printHtmlPart(5)
invokeTag('message','g',23,['code':("invoice.invoiceDescription.label"),'default':("Description")],-1)
printHtmlPart(6)
invokeTag('textArea','g',27,['name':("invoiceDescription"),'id':("invoiceDescription"),'value':(invoiceInstance?.invoiceDescription)],-1)
printHtmlPart(7)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'inCurrency', 'error'))
printHtmlPart(8)
invokeTag('message','g',34,['code':("invoice.inCurrency.label"),'default':("Base Currency")],-1)
printHtmlPart(3)
invokeTag('currencySelect','g',39,['name':("inCurrency"),'value':(invoiceInstance.inCurrency?:Currencies.MKD.currency),'from':(Currencies.values()),'disabled':("true")],-1)
printHtmlPart(9)
if(true && (invoiceInstance.invoiceBodyOut==null)) {
printHtmlPart(10)
invokeTag('message','g',47,['code':("invoice.cbOutInvoice.label"),'default':("In Other Currency")],-1)
printHtmlPart(11)
invokeTag('checkBox','g',51,['name':("cbOutInvoice"),'checked':("false"),'onchange':("generateOutInvoice(this)")],-1)
printHtmlPart(12)
}
printHtmlPart(13)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'outCurrency', 'error'))
printHtmlPart(14)
invokeTag('message','g',59,['code':("invoice.outCurrency.label"),'default':("To Currency")],-1)
printHtmlPart(11)
invokeTag('currencySelect','g',65,['name':("outCurrency"),'id':("outCurrency"),'value':(invoiceInstance.outCurrency?:Currencies.MKD.currency),'disabled':("true"),'from':(Currencies.values())],-1)
printHtmlPart(15)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'invoiceCounter', 'error'))
printHtmlPart(16)
invokeTag('message','g',73,['code':("invoice.invoiceCounter.label"),'default':("Invoice Counter")],-1)
printHtmlPart(3)
invokeTag('field','g',78,['type':("number"),'name':("invoiceCounter"),'id':("invoiceCounter"),'onchange':("generateInvoiceNumber()"),'value':(invoiceInstance?.invoiceCounter),'required':("")],-1)
printHtmlPart(17)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'invoiceYear', 'error'))
printHtmlPart(18)
invokeTag('message','g',86,['code':("invoice.invoiceYear.label"),'default':("Invoice Year")],-1)
printHtmlPart(3)
invokeTag('field','g',91,['type':("number"),'name':("invoiceYear"),'id':("invoiceYear"),'onchange':("generateInvoiceNumber()"),'value':(invoiceInstance?.invoiceYear),'required':("")],-1)
printHtmlPart(7)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'invoiceNumber', 'error'))
printHtmlPart(19)
invokeTag('message','g',98,['code':("invoice.invoiceNumber.label"),'default':("Invoice Number")],-1)
printHtmlPart(20)
invokeTag('textField','g',103,['name':("invoiceNumber"),'id':("invoiceNumber"),'value':(invoiceInstance?.invoiceNumber),'readonly':(""),'required':("")],-1)
printHtmlPart(7)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'invoiceNumberSort', 'error'))
printHtmlPart(21)
invokeTag('message','g',110,['code':("invoice.invoiceNumberSort.label"),'default':("Invoice Number Sort")],-1)
printHtmlPart(3)
invokeTag('textField','g',114,['name':("invoiceNumberSort"),'id':("invoiceNumberSort"),'value':(invoiceInstance?.invoiceNumberSort)],-1)
printHtmlPart(7)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'swiftCode', 'error'))
printHtmlPart(22)
invokeTag('message','g',121,['code':("invoice.swiftCode.label"),'default':("Swift Code")],-1)
printHtmlPart(3)
invokeTag('textField','g',125,['name':("swiftCode"),'id':("swiftCode"),'value':(invoiceInstance?.swiftCode)],-1)
printHtmlPart(7)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'paymentPercentage', 'error'))
printHtmlPart(23)
invokeTag('message','g',132,['code':("invoice.paymentPercentage.label"),'default':("Payment Percentage (%)")],-1)
printHtmlPart(3)
invokeTag('field','g',138,['type':("number"),'name':("paymentPercentage"),'id':("paymentPercentage"),'min':("0"),'max':("100"),'step':("any"),'value':(invoiceInstance?.paymentPercentage)],-1)
printHtmlPart(9)
if(true && (invoiceInstance?.status!=com.genrep.invoiceCreator.Statuses.FINALIZED)) {
printHtmlPart(24)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'status', 'error'))
printHtmlPart(25)
invokeTag('message','g',146,['code':("invoice.status.label"),'default':("Finalize Invoice")],-1)
printHtmlPart(11)
invokeTag('checkBox','g',150,['name':("invoiceStatus"),'checked':("false")],-1)
printHtmlPart(12)
}
printHtmlPart(26)
invokeTag('render','g',162,['template':("/invoice/formInvoiceItem")],-1)
printHtmlPart(27)
invokeTag('render','g',168,['template':("formInvoiceBody")],-1)
printHtmlPart(28)
invokeTag('render','g',172,['template':("dialog")],-1)
printHtmlPart(29)
invokeTag('render','g',176,['template':("editFormProject")],-1)
printHtmlPart(30)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'issueDate', 'error'))
printHtmlPart(31)
invokeTag('message','g',182,['code':("invoice.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(32)
expressionOut.print(formatDate(date:invoiceInstance.issueDate?:new Date(),format: 'dd.MM.yyyy'))
printHtmlPart(33)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'paymentDate', 'error'))
printHtmlPart(34)
invokeTag('message','g',195,['code':("invoice.paymentDate.label"),'default':("Payment Date")],-1)
printHtmlPart(35)
expressionOut.print(formatDate(date:invoiceInstance?.paymentDate,format: 'dd.MM.yyyy'))
printHtmlPart(36)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427800903000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
