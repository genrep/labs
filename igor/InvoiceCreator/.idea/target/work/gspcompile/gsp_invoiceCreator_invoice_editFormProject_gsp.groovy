import grails.converters.JSON
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoice_editFormProject_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoice/_editFormProject.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
if(true && (project)) {
printHtmlPart(1)
invokeTag('message','g',7,['code':("invoice.project.showLabel"),'default':("Project")],-1)
printHtmlPart(2)
createTagBody(2, {->
printHtmlPart(3)
expressionOut.print(project?.title)
printHtmlPart(4)
})
invokeTag('link','g',13,['controller':("project"),'action':("show"),'id':(project?.id)],2)
printHtmlPart(5)
expressionOut.print(project.contractNumberBearer)
printHtmlPart(6)
expressionOut.print(project?.id)
printHtmlPart(7)
expressionOut.print(project?.id)
printHtmlPart(8)
}
else if(true && (guiAction)) {
printHtmlPart(9)
if(true && (guiAction.equals('remove'))) {
printHtmlPart(10)
invokeTag('message','g',29,['code':("invoice.project.label"),'default':("Add to Project")],-1)
printHtmlPart(11)
invokeTag('select','g',35,['id':("project"),'name':("project"),'from':(projectList),'noSelection':(['':'']),'onchange':("showProjectsContractNumber('${projectList.contractNumberBearer as JSON}',this,'add')"),'optionKey':("id"),'optionValue':("title"),'class':("many-to-one")],-1)
printHtmlPart(12)
expressionOut.print(params?.id)
printHtmlPart(13)
}
else if(true && (guiAction.equals('update'))) {
printHtmlPart(10)
invokeTag('message','g',48,['code':("invoice.project.label"),'default':("Add to Project")],-1)
printHtmlPart(11)
invokeTag('select','g',54,['id':("project"),'name':("project"),'from':(projectList),'onchange':("showProjectsContractNumber('${projectList.contractNumberBearer as JSON}',this,'change')"),'optionKey':("id"),'optionValue':("title"),'class':("many-to-one")],-1)
printHtmlPart(12)
expressionOut.print(params?.id)
printHtmlPart(13)
}
printHtmlPart(14)
}
else {
printHtmlPart(1)
invokeTag('message','g',68,['code':("invoice.project.label"),'default':("Add to Project")],-1)
printHtmlPart(2)
invokeTag('select','g',74,['id':("project"),'name':("project"),'from':(projectList),'noSelection':(['':'']),'onchange':("showProjectsContractNumber('${projectList.contractNumberBearer as grails.converters.JSON}',this,'add')"),'optionKey':("id"),'optionValue':("title"),'class':("many-to-one")],-1)
printHtmlPart(15)
}
printHtmlPart(16)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427817337000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
