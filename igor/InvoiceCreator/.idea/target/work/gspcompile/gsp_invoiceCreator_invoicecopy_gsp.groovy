import com.genrep.invoiceCreator.Invoice
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoicecopy_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoice/copy.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',6,['var':("entityName"),'value':(message(code: 'invoice.label', default: 'Invoice'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',7,['code':("default.copy.label"),'default':("Copy Invoice")],-1)
})
invokeTag('captureTitle','sitemesh',7,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',7,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(4)
invokeTag('message','g',15,['code':("default.home.label")],-1)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('message','g',16,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('link','g',16,['class':("menuButton"),'action':("index")],2)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',18,['code':("default.new.label"),'args':([entityName])],-1)
})
invokeTag('link','g',18,['class':("menuButton"),'action':("create")],2)
printHtmlPart(7)
invokeTag('message','g',23,['code':("default.copy.label"),'default':("Make a copy of this invoice by entering new Invoice Number")],-1)
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(11)
createTagBody(3, {->
printHtmlPart(12)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(13)
expressionOut.print(error.field)
printHtmlPart(14)
}
printHtmlPart(15)
invokeTag('message','g',31,['error':(error)],-1)
printHtmlPart(16)
})
invokeTag('eachError','g',32,['bean':(invoiceInstance),'var':("error")],3)
printHtmlPart(17)
})
invokeTag('hasErrors','g',34,['bean':(invoiceInstance)],2)
printHtmlPart(1)
createTagBody(2, {->
printHtmlPart(18)
expressionOut.print(hasErrors(bean: invoiceInstance, field: 'invoiceNumber', 'error'))
printHtmlPart(19)
invokeTag('message','g',40,['code':("invoice.invoiceNumber.label"),'default':("Invoice Number")],-1)
printHtmlPart(20)
invokeTag('textField','g',45,['name':("newInvoiceNumber"),'id':("newInvoiceNumber"),'required':("")],-1)
printHtmlPart(21)
invokeTag('actionSubmit','g',51,['class':("save"),'action':("saveCopy"),'value':(message(code: 'default.button.copy.label', default: 'Make a copy'))],-1)
printHtmlPart(22)
})
invokeTag('form','g',53,['url':([resource: invoiceInstance, action: 'saveCopy']),'method':("PUT")],2)
printHtmlPart(23)
})
invokeTag('captureBody','sitemesh',55,[:],1)
printHtmlPart(24)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427882438000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
