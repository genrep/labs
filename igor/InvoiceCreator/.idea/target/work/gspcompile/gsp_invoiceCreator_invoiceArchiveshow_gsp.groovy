import com.genrep.invoiceRepository.Taxes
import  com.genrep.client.Client
import  com.genrep.invoiceRepository.InvoiceArchive
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoiceArchiveshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoiceArchive/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'invoiceArchive.label', default: 'InvoiceArchive'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(0)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(3)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(4)
invokeTag('message','g',13,['code':("default.home.label")],-1)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('message','g',14,['code':("invoiceArchive.button.index"),'default':("List Of Imported Invoices")],-1)
})
invokeTag('link','g',14,['class':("menuButton"),'action':("index")],2)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',16,['code':("invoiceArchive.button.newImport"),'default':("New Import")],-1)
})
invokeTag('link','g',16,['class':("menuButton"),'action':("newImport")],2)
printHtmlPart(7)
invokeTag('message','g',20,['code':("default.show.label"),'args':([entityName])],-1)
printHtmlPart(8)
if(true && (flash.message)) {
printHtmlPart(9)
expressionOut.print(flash.message)
printHtmlPart(10)
}
printHtmlPart(11)
if(true && (invoiceArchiveInstance?.docMongoId)) {
printHtmlPart(12)
invokeTag('message','g',28,['code':("invoiceArchive.docMongoId.label"),'default':("Doc Mongo Id")],-1)
printHtmlPart(13)
invokeTag('fieldValue','g',30,['bean':(invoiceArchiveInstance),'field':("docMongoId")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.invoiceNumber)) {
printHtmlPart(16)
invokeTag('message','g',37,['code':("invoiceArchive.invoiceNumber.label"),'default':("Invoice Number")],-1)
printHtmlPart(17)
invokeTag('fieldValue','g',39,['bean':(invoiceArchiveInstance),'field':("invoiceNumber")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.swiftCode)) {
printHtmlPart(18)
invokeTag('message','g',46,['code':("invoiceArchive.swiftCode.label"),'default':("Swift Code")],-1)
printHtmlPart(19)
invokeTag('fieldValue','g',48,['bean':(invoiceArchiveInstance),'field':("swiftCode")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.client?.id)) {
printHtmlPart(20)
invokeTag('message','g',55,['code':("invoiceArchive.client.label"),'default':("Client")],-1)
printHtmlPart(21)
createTagBody(3, {->
printHtmlPart(22)
expressionOut.print(Client.get(invoiceArchiveInstance?.client.id).toString())
printHtmlPart(23)
})
invokeTag('link','g',60,['controller':("client"),'action':("show"),'id':(invoiceArchiveInstance?.client?.id)],3)
printHtmlPart(24)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.invoiceDescription)) {
printHtmlPart(25)
invokeTag('message','g',70,['code':("invoice.invoiceDescription.label"),'default':("Description")],-1)
printHtmlPart(26)
expressionOut.print(raw(invoiceArchiveInstance.invoiceDescription))
printHtmlPart(27)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.totalSum)) {
printHtmlPart(28)
invokeTag('message','g',79,['code':("invoiceArchive.totalSum.label"),'default':("Total Sum")],-1)
printHtmlPart(29)
expressionOut.print(invoiceArchiveInstance?.totalSum?.decodeDecimalNumber())
printHtmlPart(24)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.taxFreeSum)) {
printHtmlPart(30)
invokeTag('message','g',90,['code':("invoiceArchive.taxFreeSum.label"),'default':("Tax Free Sum")],-1)
printHtmlPart(31)
expressionOut.print(invoiceArchiveInstance?.taxFreeSum?.decodeDecimalNumber())
printHtmlPart(24)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.taxPriceSum)) {
printHtmlPart(32)
invokeTag('message','g',101,['code':("invoiceArchive.taxPriceSum.label"),'default':("Tax Price Sum")],-1)
printHtmlPart(33)
expressionOut.print(invoiceArchiveInstance?.taxPriceSum?.decodeDecimalNumber())
printHtmlPart(24)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.taxes)) {
printHtmlPart(34)
if(true && (invoiceArchiveInstance?.taxes?.containsKey(Taxes.ZERO))) {
printHtmlPart(35)
invokeTag('message','g',113,['code':("invoiceArchive.taxes.zero"),'default':("Tax (0%)")],-1)
printHtmlPart(36)
expressionOut.print(invoiceArchiveInstance?.taxes?.get(Taxes.ZERO).decodeDecimalNumber())
printHtmlPart(37)
}
printHtmlPart(38)
if(true && (invoiceArchiveInstance?.taxes.containsKey(Taxes.FIVE))) {
printHtmlPart(35)
invokeTag('message','g',122,['code':("invoiceArchive.taxes.five"),'default':("Tax (5%)")],-1)
printHtmlPart(36)
expressionOut.print(invoiceArchiveInstance?.taxes?.get(Taxes.FIVE).decodeDecimalNumber())
printHtmlPart(37)
}
printHtmlPart(38)
if(true && (invoiceArchiveInstance?.taxes.containsKey(Taxes.EIGHTEEN))) {
printHtmlPart(35)
invokeTag('message','g',131,['code':("invoiceArchive.taxes.eigtheen"),'default':("Tax (18%)")],-1)
printHtmlPart(36)
expressionOut.print(invoiceArchiveInstance?.taxes?.get(Taxes.EIGHTEEN).decodeDecimalNumber())
printHtmlPart(37)
}
printHtmlPart(39)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.inCurrency)) {
printHtmlPart(40)
invokeTag('message','g',141,['code':("invoiceArchive.currency.label"),'default':("Currency")],-1)
printHtmlPart(41)
invokeTag('fieldValue','g',143,['bean':(invoiceArchiveInstance),'field':("inCurrency")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.exchangeRate)) {
printHtmlPart(42)
invokeTag('message','g',150,['code':("invoiceArchive.exchangeRate.label"),'default':("Exchange Rate")],-1)
printHtmlPart(43)
createTagBody(3, {->
expressionOut.print(invoiceArchiveInstance?.exchangeRate?.encodeAsHTML())
})
invokeTag('link','g',152,['controller':("exchangeRate"),'action':("show"),'id':(invoiceArchiveInstance?.exchangeRate?.id)],3)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.documentFile)) {
printHtmlPart(44)
invokeTag('message','g',159,['code':("invoiceArchive.documentFile.label"),'default':("Document File")],-1)
printHtmlPart(45)
createTagBody(3, {->
printHtmlPart(23)
expressionOut.print(invoiceArchiveInstance?.documentName?:'Invoice Document')
printHtmlPart(46)
})
invokeTag('link','g',163,['controller':("invoiceArchive"),'action':("showInvoiceArchiveFile"),'target':("_blank"),'id':(invoiceArchiveInstance?.id)],3)
printHtmlPart(27)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.documentName)) {
printHtmlPart(47)
invokeTag('message','g',170,['code':("invoiceArchive.documentName.label"),'default':("Document Name")],-1)
printHtmlPart(48)
invokeTag('fieldValue','g',172,['bean':(invoiceArchiveInstance),'field':("documentName")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.projectId)) {
printHtmlPart(49)
invokeTag('message','g',180,['code':("invoiceArchive.project.label"),'default':("Project")],-1)
printHtmlPart(50)
invokeTag('set','g',183,['var':("project"),'value':(com.genrep.project.Project.findWhere(technicalNumber:invoiceArchiveInstance?.projectId))],-1)
printHtmlPart(23)
createTagBody(3, {->
printHtmlPart(22)
expressionOut.print(project?.title)
printHtmlPart(23)
})
invokeTag('link','g',186,['controller':("project"),'action':("show"),'id':(project?.id)],3)
printHtmlPart(27)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.issueDate)) {
printHtmlPart(51)
invokeTag('message','g',193,['code':("invoiceArchive.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(52)
invokeTag('formatDate','g',196,['format':("dd.MM.yyyy"),'date':(invoiceArchiveInstance?.issueDate)],-1)
printHtmlPart(27)
}
printHtmlPart(15)
if(true && (invoiceArchiveInstance?.dateCreated)) {
printHtmlPart(53)
invokeTag('message','g',203,['code':("invoiceArchive.dateCreated.label"),'default':("Date Created")],-1)
printHtmlPart(54)
invokeTag('formatDate','g',206,['format':("dd.MM.yyyy"),'date':(invoiceArchiveInstance?.dateCreated)],-1)
printHtmlPart(27)
}
printHtmlPart(55)
createTagBody(2, {->
printHtmlPart(56)
createTagBody(3, {->
invokeTag('message','g',214,['code':("default.button.edit.label"),'default':("Edit")],-1)
})
invokeTag('link','g',214,['class':("edit"),'action':("edit"),'resource':(invoiceArchiveInstance)],3)
printHtmlPart(34)
invokeTag('actionSubmit','g',215,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(57)
})
invokeTag('form','g',217,['url':([resource:invoiceArchiveInstance, action:'delete']),'method':("DELETE")],2)
printHtmlPart(58)
})
invokeTag('captureBody','sitemesh',219,[:],1)
printHtmlPart(59)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427817256000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
