import com.genrep.statements.BankStatementDoc
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementDocshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatementDoc/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'bankStatementDoc.label', default: 'BankStatementDoc'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(2)
invokeTag('javascript','asset',9,['src':("statement.js")],-1)
printHtmlPart(0)
})
invokeTag('captureHead','sitemesh',10,[:],1)
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(3)
invokeTag('message','g',12,['code':("default.link.skip.label"),'default':("Skip to content&hellip;")],-1)
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',15,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',16,['code':("bankStatmentDoc.list"),'default':("List Bank Statements")],-1)
})
invokeTag('link','g',16,['class':("menuButton"),'action':("index")],2)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',17,['code':("bankStatmentDoc.new"),'default':("New Bank Statement")],-1)
})
invokeTag('link','g',17,['class':("menuButton"),'action':("create")],2)
printHtmlPart(8)
invokeTag('message','g',21,['code':("bankStatement.show.label"),'default':("Bank Statement Basic Information")],-1)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
if(true && (bankStatementDocInstance?.uuid)) {
printHtmlPart(13)
invokeTag('message','g',29,['code':("bankStatementDoc.uuid.label"),'default':("Uuid")],-1)
printHtmlPart(14)
invokeTag('fieldValue','g',31,['bean':(bankStatementDocInstance),'field':("uuid")],-1)
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (bankStatementDocInstance?.documentFile)) {
printHtmlPart(17)
invokeTag('message','g',38,['code':("bankStatementDoc.documentFile.label"),'default':("Statement Doc")],-1)
printHtmlPart(18)
createTagBody(3, {->
printHtmlPart(19)
expressionOut.print(bankStatementDocInstance?.documentName)
printHtmlPart(20)
})
invokeTag('link','g',44,['class':("doc-${bankStatementDocInstance.mimeType.split('/')[1]}"),'controller':("worker"),'action':("readFileFromMongoDb"),'target':("_blank"),'params':([docMongoId: bankStatementDocInstance.uuid, domain: 'BankStatementDoc'])],3)
printHtmlPart(21)
}
printHtmlPart(16)
if(true && (bankStatementDocInstance?.isTransferred)) {
printHtmlPart(22)
invokeTag('message','g',51,['code':("bankStatementDoc.isTransferred.label"),'default':("Is Transferred")],-1)
printHtmlPart(23)
invokeTag('formatBoolean','g',53,['boolean':(bankStatementDocInstance?.isTransferred)],-1)
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (bankStatementDocInstance?.issueDate)) {
printHtmlPart(24)
invokeTag('message','g',60,['code':("bankStatementDoc.issueDate.label"),'default':("Issue Date")],-1)
printHtmlPart(25)
invokeTag('formatDate','g',63,['date':(bankStatementDocInstance?.issueDate),'format':("dd.MM.yyyy")],-1)
printHtmlPart(15)
}
printHtmlPart(16)
if(true && (bankStatementDocInstance?.dateCreated)) {
printHtmlPart(26)
invokeTag('message','g',70,['code':("bankStatementDoc.dateCreated.label"),'default':("Date Created")],-1)
printHtmlPart(27)
invokeTag('formatDate','g',73,['date':(bankStatementDocInstance?.dateCreated),'format':("dd.MM.yyyy")],-1)
printHtmlPart(15)
}
printHtmlPart(28)
createTagBody(2, {->
printHtmlPart(29)
createTagBody(3, {->
invokeTag('message','g',82,['code':("statements.action.parse"),'default':("Show Details")],-1)
})
invokeTag('link','g',82,['action':("showBankStatementDetails"),'resource':(bankStatementDocInstance)],3)
printHtmlPart(30)
invokeTag('actionSubmit','g',83,['class':("delete"),'action':("delete"),'value':(message(code: 'default.button.delete.label', default: 'Delete')),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(31)
})
invokeTag('form','g',83,['url':([resource:bankStatementDocInstance, action:'delete']),'method':("DELETE")],2)
printHtmlPart(32)
})
invokeTag('captureBody','sitemesh',83,[:],1)
printHtmlPart(33)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
