import com.genrep.statements.BankStatementArchive
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementArchiveindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatementArchive/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
invokeTag('captureMeta','sitemesh',6,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(2)
invokeTag('set','g',7,['var':("entityName"),'value':(message(code: 'bankStatementArchive.label', default: 'BankStatementArchive'))],-1)
printHtmlPart(2)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',8,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',8,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',8,[:],2)
printHtmlPart(3)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
invokeTag('message','g',11,['code':("default.link.skip.label"),'default':("Skip to content&hellip;")],-1)
printHtmlPart(5)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(6)
invokeTag('message','g',14,['code':("default.home.label")],-1)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',15,['code':("default.new.label"),'args':([entityName])],-1)
})
invokeTag('link','g',15,['class':("create"),'action':("create")],2)
printHtmlPart(8)
invokeTag('message','g',19,['code':("default.list.label"),'args':([entityName])],-1)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
invokeTag('sortableColumn','g',27,['property':("docMongoId"),'title':(message(code: 'bankStatementArchive.docMongoId.label', default: 'Doc Mongo Id'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',29,['property':("bankStatementId"),'title':(message(code: 'bankStatementArchive.bankStatementId.label', default: 'Bank Statement Id'))],-1)
printHtmlPart(14)
invokeTag('message','g',31,['code':("bankStatementArchive.ownerOfStatement.label"),'default':("Owner Of Statement")],-1)
printHtmlPart(15)
invokeTag('sortableColumn','g',33,['property':("documentFile"),'title':(message(code: 'bankStatementArchive.documentFile.label', default: 'Document File'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',35,['property':("documentName"),'title':(message(code: 'bankStatementArchive.documentName.label', default: 'Document Name'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',37,['property':("dateCreated"),'title':(message(code: 'bankStatementArchive.dateCreated.label', default: 'Date Created'))],-1)
printHtmlPart(16)
loop:{
int i = 0
for( bankStatementArchiveInstance in (bankStatementArchiveInstanceList) ) {
printHtmlPart(17)
expressionOut.print((i % 2) == 0 ? 'even' : 'odd')
printHtmlPart(18)
createTagBody(3, {->
expressionOut.print(fieldValue(bean: bankStatementArchiveInstance, field: "docMongoId"))
})
invokeTag('link','g',45,['action':("show"),'id':(bankStatementArchiveInstance.id)],3)
printHtmlPart(19)
expressionOut.print(fieldValue(bean: bankStatementArchiveInstance, field: "bankStatementId"))
printHtmlPart(19)
expressionOut.print(fieldValue(bean: bankStatementArchiveInstance, field: "ownerOfStatement"))
printHtmlPart(20)
createTagBody(3, {->
printHtmlPart(21)
expressionOut.print(bankStatementArchiveInstance.documentName?:"link")
printHtmlPart(22)
})
invokeTag('link','g',55,['controller':("worker"),'action':("readFileFromMongoDb"),'target':("_blank"),'params':([docMongoId: bankStatementArchiveInstance.docMongoId, domain: 'BankStatementArchive'])],3)
printHtmlPart(19)
expressionOut.print(fieldValue(bean: bankStatementArchiveInstance, field: "documentName"))
printHtmlPart(23)
invokeTag('formatDate','g',58,['date':(bankStatementArchiveInstance.dateCreated)],-1)
printHtmlPart(24)
i++
}
}
printHtmlPart(25)
invokeTag('paginate','g',66,['total':(bankStatementArchiveInstanceCount ?: 0)],-1)
printHtmlPart(26)
})
invokeTag('captureBody','sitemesh',67,[:],1)
printHtmlPart(27)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
