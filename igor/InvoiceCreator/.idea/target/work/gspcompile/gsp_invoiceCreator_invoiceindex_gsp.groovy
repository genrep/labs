import com.genrep.invoiceCreator.Invoice
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoiceindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoice/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',6,['var':("entityName"),'value':(message(code: 'invoice.label', default: 'Invoice'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',7,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',7,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',7,[:],2)
printHtmlPart(1)
invokeTag('javascript','asset',8,['src':("invoice.js")],-1)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',9,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
invokeTag('message','g',13,['code':("default.link.skip.label"),'default':("Skip to content&hellip;")],-1)
printHtmlPart(5)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(6)
invokeTag('message','g',17,['code':("default.home.label")],-1)
printHtmlPart(7)
createTagBody(2, {->
invokeTag('message','g',19,['code':("default.new.label"),'args':([entityName])],-1)
})
invokeTag('link','g',19,['class':("menuButton"),'action':("create")],2)
printHtmlPart(8)
createTagBody(2, {->
invokeTag('message','g',20,['code':("invoiceArchive.home.label"),'default':("Importing of Invoices")],-1)
printHtmlPart(9)
})
invokeTag('link','g',21,['class':("menuButton"),'controller':("invoiceArchive"),'action':("index")],2)
printHtmlPart(10)
if(true && (flash.message)) {
printHtmlPart(11)
expressionOut.print(flash.message)
printHtmlPart(12)
}
printHtmlPart(13)
invokeTag('filterPane','gnrp',33,['domain':("com.genrep.invoiceCreator.Invoice"),'filterParams':("issueDate,client,invoiceNumber"),'customForm':("true")],-1)
printHtmlPart(14)
invokeTag('sortableColumn','g',40,['property':("invoiceNumber"),'title':(message(code: 'invoice.invoiceNumber.label', default: 'Invoice Number'))],-1)
printHtmlPart(15)
invokeTag('sortableColumn','g',43,['property':("invoiceNumberSort"),'title':(message(code: 'invoice.invoiceNumberSort.label', default: 'Invoice Number Sort'))],-1)
printHtmlPart(16)
invokeTag('message','g',45,['code':("invoice.client.label"),'default':("Client")],-1)
printHtmlPart(17)
invokeTag('sortableColumn','g',48,['property':("issueDate"),'title':(message(code: 'invoice.issueDate.label', default: 'Issue Date'))],-1)
printHtmlPart(15)
invokeTag('sortableColumn','g',51,['property':("invoiceDescription"),'title':(message(code: 'invoice.invoiceDescription.label', default: 'Description'))],-1)
printHtmlPart(15)
invokeTag('sortableColumn','g',54,['property':("invoiceBodyIn.totalSum"),'title':(message(code: 'invoice.totalSum.label', default: 'Total Sum'))],-1)
printHtmlPart(15)
invokeTag('sortableColumn','g',56,['property':("currency"),'title':(message(code: 'invoice.inCurrency.label', default: 'Currency'))],-1)
printHtmlPart(15)
invokeTag('sortableColumn','g',59,['property':("status"),'title':(message(code: 'invoice.status.label', default: 'Status'))],-1)
printHtmlPart(16)
invokeTag('message','g',61,['code':("invoice.documents.label"),'default':("Documents")],-1)
printHtmlPart(18)
invokeTag('message','g',63,['code':("invoice.action.label"),'default':("Action")],-1)
printHtmlPart(19)
loop:{
int i = 0
for( invoiceInstance in (invoiceInstanceList) ) {
printHtmlPart(20)
expressionOut.print((i % 2) == 0 ? 'even' : 'odd')
printHtmlPart(21)
createTagBody(3, {->
expressionOut.print(fieldValue(bean: invoiceInstance, field: "invoiceNumber"))
printHtmlPart(22)
})
invokeTag('link','g',74,['action':("show"),'id':(invoiceInstance.id)],3)
printHtmlPart(23)
expressionOut.print(fieldValue(bean: invoiceInstance, field: "invoiceNumberSort"))
printHtmlPart(23)
expressionOut.print(fieldValue(bean: invoiceInstance, field: "client"))
printHtmlPart(24)
invokeTag('formatDate','g',81,['date':(invoiceInstance.issueDate),'format':("dd.MM.yyyy")],-1)
printHtmlPart(23)
expressionOut.print(raw(invoiceInstance?.invoiceDescription))
printHtmlPart(23)
expressionOut.print(invoiceInstance.invoiceBodyIn.totalSum)
printHtmlPart(23)
expressionOut.print(fieldValue(bean: invoiceInstance, field: "inCurrency"))
printHtmlPart(23)
expressionOut.print(invoiceInstance.status)
printHtmlPart(23)
createClosureForHtmlPart(25, 3)
invokeTag('link','g',91,['class':("button-in-table"),'action':("showDocuments"),'id':(invoiceInstance.id)],3)
printHtmlPart(23)
createTagBody(3, {->
printHtmlPart(26)
expressionOut.print(message(code: 'default.button.delete.label', default: 'Delete'))
printHtmlPart(27)
})
invokeTag('form','g',97,['url':([resource: invoiceInstance, action: 'delete']),'method':("DELETE")],3)
printHtmlPart(28)
i++
}
}
printHtmlPart(29)
invokeTag('paginate','g',106,['total':(invoiceInstanceCount ?: 0),'params':(params)],-1)
printHtmlPart(30)
})
invokeTag('captureBody','sitemesh',109,[:],1)
printHtmlPart(31)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1427813692000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
