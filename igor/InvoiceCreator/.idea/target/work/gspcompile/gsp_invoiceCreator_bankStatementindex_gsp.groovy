import com.genrep.statements.BankStatement
import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_bankStatementindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/bankStatement/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',5,['gsp_sm_xmlClosingForEmptyTag':(""),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',6,['var':("entityName"),'value':(message(code: 'bankStatement.label', default: 'BankStatement'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',7,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',7,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',7,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',8,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
expressionOut.print(createLink(uri: '/'))
printHtmlPart(5)
invokeTag('message','g',14,['code':("default.home.label")],-1)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('message','g',15,['code':("bankStatment.create"),'default':("New Bank Statement")],-1)
})
invokeTag('link','g',15,['class':("menuButton"),'action':("create")],2)
printHtmlPart(7)
expressionOut.print(createLink(uri: '/statements/documents'))
printHtmlPart(5)
invokeTag('message','g',16,['code':("bankStatementsDoc.home.label"),'default':("Importing of Statements")],-1)
printHtmlPart(8)
invokeTag('message','g',21,['code':("bankStatement.list.label"),'default':("Bank Statements")],-1)
printHtmlPart(9)
if(true && (flash.message)) {
printHtmlPart(10)
expressionOut.print(flash.message)
printHtmlPart(11)
}
printHtmlPart(12)
invokeTag('sortableColumn','g',30,['property':("bankStatementId"),'title':(message(code: 'bankStatement.bankStatementId.label', default: 'Bank Statement Id'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',33,['property':("issueDate"),'title':(message(code: 'bankStatement.issueDate.label', default: 'Issue Date'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',36,['property':("bankAccountNumber"),'title':(message(code: 'bankStatement.bankAccountNumber.label', default: 'Bank Account Number'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',39,['property':("balanceBefore"),'title':(message(code: 'bankStatement.balanceBefore.label', default: 'Balance Before'))],-1)
printHtmlPart(13)
invokeTag('sortableColumn','g',42,['property':("balanceAfter"),'title':(message(code: 'bankStatement.balanceAfter.label', default: 'Balance After'))],-1)
printHtmlPart(14)
loop:{
int i = 0
for( bankStatementInstance in (bankStatementInstanceList) ) {
printHtmlPart(15)
expressionOut.print((i % 2) == 0 ? 'even' : 'odd')
printHtmlPart(16)
createTagBody(3, {->
expressionOut.print(fieldValue(bean: bankStatementInstance, field: "bankStatementId"))
})
invokeTag('link','g',52,['action':("show"),'id':(bankStatementInstance.id)],3)
printHtmlPart(17)
invokeTag('formatDate','g',54,['date':(bankStatementInstance.issueDate),'format':("dd.MM.yyyy")],-1)
printHtmlPart(17)
expressionOut.print(fieldValue(bean: bankStatementInstance, field: "bankAccountNumber"))
printHtmlPart(17)
expressionOut.print(fieldValue(bean: bankStatementInstance, field: "balanceBefore"))
printHtmlPart(17)
expressionOut.print(fieldValue(bean: bankStatementInstance, field: "balanceAfter"))
printHtmlPart(18)
i++
}
}
printHtmlPart(19)
invokeTag('paginate','g',68,['total':(bankStatementInstanceCount ?: 0)],-1)
printHtmlPart(20)
})
invokeTag('captureBody','sitemesh',71,[:],1)
printHtmlPart(21)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1423222873000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
