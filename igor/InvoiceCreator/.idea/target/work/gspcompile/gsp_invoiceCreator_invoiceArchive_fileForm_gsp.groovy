import org.codehaus.groovy.grails.plugins.metadata.GrailsPlugin
import org.codehaus.groovy.grails.web.pages.GroovyPage
import org.codehaus.groovy.grails.web.taglib.*
import org.codehaus.groovy.grails.web.taglib.exceptions.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_invoiceCreator_invoiceArchive_fileForm_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/invoiceArchive/_fileForm.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'documentFile', 'error'))
printHtmlPart(1)
invokeTag('message','g',4,['code':("invoiceArchive.documentFile.label"),'default':("Document File")],-1)
printHtmlPart(2)
expressionOut.print(hasErrors(bean: invoiceArchiveInstance, field: 'documentName', 'error'))
printHtmlPart(3)
invokeTag('message','g',15,['code':("invoiceArchive.documentName.label"),'default':("Document Name")],-1)
printHtmlPart(4)
invokeTag('textField','g',20,['class':("wider-fields"),'required':(""),'name':("documentName"),'id':("documentName"),'value':(invoiceArchiveInstance?.documentName)],-1)
printHtmlPart(5)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1425294333000L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'html'
public static final String TAGLIB_CODEC = 'none'
}
