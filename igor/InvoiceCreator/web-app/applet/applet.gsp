<%--
  Created by IntelliJ IDEA.
  User: igor
  Date: 7/7/14
  Time: 2:06 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <g:applyLayout name="mainApplet.gsp"/>

    <title><g:message code="invoice.applet" default="Invoice Signing Applet"/></title>
    <SCRIPT src="web-files/dtjava.js"></SCRIPT>
    <script>
        function javafxEmbed() {
            dtjava.embed(
                    {
                        url: 'FXSigningApplet.jnlp',
                        placeholder: 'javafx-app-placeholder',
                        width: 963,
                        height: 700,
                        params: {
                            downloadUrl: '${params.downloadUrl}',
                            sessionId: '${params.sessionId}',
                            docMongoId: '${params.docMongoId}',
                            documentSaveUrl: '${params.documentSaveUrl}'
                        }
                    },
                    {
                        javafx: '2.2+'
                    },
                    {}
            );
        }
        dtjava.addOnloadCallback(javafxEmbed);
    </script>

</head>

<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" controller="invoice" action="show"
                    params="[id:session.invoiceId]">
            <g:message code="invoice.applet.backButton" default="Back to Invoice"/>
        </g:link>
        </li>
    </ul>
</div>
<div id='javafx-app-placeholder'></div>
</body>
</html>
