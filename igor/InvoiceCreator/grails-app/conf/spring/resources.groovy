import com.genrep.auth.GenrepUserDetailsService
import com.genrep.handlers.LogoutSuccessHandler
import com.genrep.office.OfficeClientService
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.util.Environment

//import org.springframework.security.authentication.encoding.PlaintextPasswordEncoder

//import com.genrep.invoiceCreator.ApplicationContextHolder

// Place your Spring DSL code here
beans = {

//    applicationContextHolder(ApplicationContextHolder) { bean ->
//        bean.factoryMethod = 'getInstance'
//    }

    def conf = SpringSecurityUtils.securityConfig

    logoutSuccessHandler(LogoutSuccessHandler) {
        redirectStrategy = ref('redirectStrategy')
        defaultTargetUrl = conf.logout.afterLogoutUrl // '/'
        alwaysUseDefaultTargetUrl = conf.logout.alwaysUseDefaultTargetUrl // false
        targetUrlParameter = conf.logout.targetUrlParameter // null
        useReferer = conf.logout.redirectToReferer // false
    }

    userDetailsService(GenrepUserDetailsService)
//    passwordEncoder(PlaintextPasswordEncoder)

    Environment.executeForCurrentEnvironment {

        development {
            userRoleService(com.genrepsoft.toolkit.authentication.RegistrationClient) {
                protocol = "http"
                uri = "e-office.genrepsoft.net"
                port = "80"
                appContext = "e-office-registration"
                servletContext = "userRegistrationService"
            }

            officeClientService(OfficeClientService) {
                protocol = "http"
                uri = "10.10.30.32"
                port = "8081"
                appContext = "/e-office-grails"
                loginContext = "/api/login"

                username = "mugren"
                password = "mugren"
            }
        }

        test {
            userRoleService(com.genrepsoft.toolkit.authentication.RegistrationClient) {
                protocol = "http"
                uri = "ec2-54-159-141-100.compute-1.amazonaws.com"
                port = "8080"
                appContext = "/"
                servletContext = "userRegistrationService"

            }
        }

        production {
            userRoleService(com.genrepsoft.toolkit.authentication.RegistrationClient) {
                protocol = grailsApplication.config.registration.protocol
                uri = grailsApplication.config.registration.uri
                port = grailsApplication.config.registration.port
                appContext = grailsApplication.config.registration.appContext
                servletContext = grailsApplication.config.registration.servletContext

            }

            officeClientService(OfficeClientService) {
                protocol = grailsApplication.config.office.protocol
                uri = grailsApplication.config.office.uri
                port = grailsApplication.config.office.port
                appContext = grailsApplication.config.office.appContext
                loginContext = grailsApplication.config.office.loginContext

                username = grailsApplication.config.office.username
                password = grailsApplication.config.office.password
            }

//            userRoleService(com.genrepsoft.toolkit.authentication.RegistrationClient) {
//                protocol = "http"
//                uri = "ec2-54-159-141-100.compute-1.amazonaws.com"
//                port = 8080
//                appContext = "/"
//                servletContext = "userRegistrationService"
//            }
//        }
        }
    }
}