grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.work.dir = "target/work"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
//grails.project.war.file = "target/${appName}-${appVersion}.war"

grails.project.fork = [
        // configure settings for compilation JVM, note that if you alter the Groovy version forked compilation is required
        //  compile: [maxMemory: 256, minMemory: 64, debug: false, maxPerm: 256, daemon:true],

        // configure settings for the test-app JVM, uses the daemon by default
        test: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, daemon:true],
        // configure settings for the run-app JVM
//    run: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
        run:false,
        // configure settings for the run-war JVM
        war: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256, forkReserve:false],
        // configure settings for the Console UI JVM
        console: [maxMemory: 768, minMemory: 64, debug: false, maxPerm: 256]
]

// inline plugins / ovie bea so tripati ../
grails.plugin.location.'mongo-gridfs' = "../../../e-office/e-office-grails-plugins/mongo-gridfs"
grails.plugin.location.'locking-manager' = "../../../e-office/e-office-grails-plugins/lockingManager"


//grails.tomcat.jvmArgs = ["-Djava.security.policy=${CATALINA_BASE}/conf/catalina.policy"]
// For reloading a changes in classes
grails.reload.enabled = true

grails.project.dependency.resolver = "maven" // or ivy
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
        // excludes 'ehcache'
        excludes 'grails-docs'
    }
    log "error" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    checksums true // Whether to verify checksums on resolve
    legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

    repositories {
        inherits true // Whether to inherit repository definitions from plugins

        grailsPlugins()
        grailsHome()
        mavenLocal()
        grailsCentral()
        mavenCentral()
        // uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
        //mavenRepo "http://repository.codehaus.org"
        //mavenRepo "http://download.java.net/maven/2/"
        //mavenRepo "http://repository.jboss.com/maven2/"
        mavenRepo "http://www.genrepsoft.net/nexus/content/repositories/snapshots"
        mavenRepo "http://www.genrepsoft.net/nexus/content/repositories/releases"
        mavenRepo "http://www.genrepsoft.net/nexus/content/groups/public"
    }

    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes e.g.
        runtime 'mysql:mysql-connector-java:5.1.27'
        runtime 'org.postgresql:postgresql:9.3-1100-jdbc41'
//        runtime 'commons-collections:commons-collections:3.2.1'
//        runtime 'org.mongodb:mongo-java-driver:2.12.3'  // The version in the plugin is wrong. It is missing a class

//        compile 'net.sf.jasperreports:jasperreports:5.5.0'
        compile ( 'com.genrep.toolbox:toolbox-logging:1.x-SNAPSHOT' )
                {
                    excludes "log4j"
                    excludes "toolbox-utils"

                }
        compile 'com.genrep.toolbox:toolbox-hessian:1.2-SNAPSHOT'
        compile ('com.genrep.toolbox:toolbox-core:1.5-SNAPSHOT' )
                {
                    excludes 'toolbox-nanoxml','toolbox-classutils','toolbox-spring','toolbox-utils','toolbox-multicast-udp',
                            'servlet-api','toolbox-hibernate','toolbox-hessian','toolbox-jackson','mongo-java-driver','spring-data-mongodb'

                }
        compile ('com.genrep.toolbox:toolbox-annotations:1.4-SNAPSHOT')
        compile ( 'com.genrep.authentication:app-authentication:3.0.1-SNAPSHOT' )
                {
                    excludes 'toolbox-spring','servlet-api','toolbox-core','toolbox-ldap','skaringa','codec','core','cobra'

                }

        compile 'eu.bitwalker:UserAgentUtils:1.15'
        compile 'org.codehaus.groovy.modules.http-builder:http-builder:0.7.1'


        // Da se proveri dali ova vazi za grails 2.5.0
        build('org.grails:grails-docs:2.5.3') {
            excludes 'itext'
        }
    }

    plugins {
        // plugins for the build system only
        build ':tomcat:7.0.55.3'

        // plugins for the compile step
//        compile ":tomcat:8.0.14.1"
        compile ":scaffolding:2.1.2"
        compile ':cache:1.1.8'
//        compile ':asset-pipeline:2.3.9'
        compile ':asset-pipeline:2.1.5'
        compile(":jasper:1.11.0"){
            excludes 'jackson-core','jackson-annotations'
        }
        compile ":cxf:2.1.1"
//        compile ":ws-client:1.0"
        compile ":cxf-client:2.1.1"
        compile(':mongodb:3.0.3')
//                {
//            excludes 'mongo-java-driver'
//        }
        compile ':spring-security-core:2.0.0'

        // plugins needed at runtime but not for compilation
//        runtime ':hibernate4:4.3.8.1' // or ':hibernate:3.6.10.19'
        runtime ':hibernate4:4.3.10' // or ':hibernate:3.6.10.19'
        runtime ":jquery:1.11.1"

    }
}
