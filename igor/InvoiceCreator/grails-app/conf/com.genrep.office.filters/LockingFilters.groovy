package com.genrep.office.filters

import com.genrep.concurrency.locking.LockedObject
import com.genrep.invoiceCreator.Invoice
import org.codehaus.groovy.grails.web.util.WebUtils
import org.springframework.context.MessageSource
import org.springframework.web.servlet.support.RequestContextUtils

class LockingFilters {

    def springSecurityService
    def lockingService
    MessageSource messageSource

    def filters = {
        editInvoice(controller: 'invoice', action: 'edit|delete') {
            before = {
                def user = springSecurityService?.principal
                String simpleKey = Invoice.class.name + "-" + params?.id
                String username = user?.username
                String sessionId = session.id
                def fullName = user?.fullName
                def lock = lockingService.isLockedByUserForAction(simpleKey, username, sessionId, actionName)
                // if the object is locked
                if (lock != null) {
                    if (lock instanceof LockedObject) {
                        log.info "Object is locked by ${lock?.lockedBy}"
                        // stop execution
                        def message = null
                        if (lock.lockedBy.equals(username)) {
                            String[] args = new String[1]
                            args[0] = messageSource.getMessage("${lock.lockedFor}", null, "${lock.lockedFor}", RequestContextUtils.getLocale(request))
                            message = messageSource.getMessage('invoice.locked.byUser', args, "This invoice is already locked by you for ${lock?.lockedFor}", RequestContextUtils.getLocale(request))
                            flash.message = message
                        } else {
                            String[] args = new String[1]
                            args[0] = lock?.info // fullName
                            args[0] = messageSource.getMessage("${lock.lockedFor}", null, "${lock.lockedFor}", RequestContextUtils.getLocale(request))
                            message = messageSource.getMessage('invoice.locked.byOtherUser', args, "Invoice is locked by another user ${lock?.info} for ${lock?.lockedFor}", RequestContextUtils.getLocale(request))
                            flash.message = message
                        }
                        redirect controller: 'invoice', action: 'show', id: params?.id
                        return false
                    } else {
                        params.lockedByUser = true
                    }
                } else {
                    if (params.lockedByUser == null) {
                        def invoiceInstance = Invoice.findById(params?.id as Long)
                        if (invoiceInstance != null) {
                            log.info("Locking domain ${invoiceInstance} by user ${username} for editing")
                            lockingService.acquireLockByDomain(invoiceInstance, [username: username, sessionId: sessionId, info: fullName, lockedFor: "editing"])
                        }
                    }
                }
            }
        }
        releaseLock(controller: '*', action: "*") {
            after = { Map model ->
                String referrer = request.getHeader("referer")
                def id
                if (referrer != null) {
                    id = getParamId(referrer)
                }
                if (!request.requestURL.contains("/edit")) {
                    if (id) {
                        log.info("Using referer for unlocikng")
                        lockingService.releaseLock(Invoice.class.name + "-" + id, ["sessionID": session.id])
                    } else if (model?.invoiceInstance?.id) {
                        log.info("Using model for unlocikng")
                        lockingService.releaseLock(Invoice.class.name + "-" + model?.invoiceInstance?.id, ["sessionID": session.id])
                    } else log.info("Unlocking did not happen... unable to identify the domain ID!")
                }
                return true // always exit this filter rule
            }
        }
    }

    private def String getParamId(String url) {
        def basicUrl = getBasicUrl(url).split("/")
        if (basicUrl.size() > 3) {
            return basicUrl[3]
        }
        return null
    }

    private def String getBasicUrl(String url){
        def webUtils = WebUtils.retrieveGrailsWebRequest()
        def request = webUtils.request
        def server = request.scheme+"://"+request.serverName
        if(request.serverPort!=80){
            server+=":"+request.serverPort
        }
        def appContext = server+request.contextPath
        def shortUrl = url - appContext
        return shortUrl
    }

}