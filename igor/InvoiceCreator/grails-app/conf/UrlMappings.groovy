
class UrlMappings {

	static mappings = {

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/statements/documents/$action?/$id?(.$format)?"{
            controller = 'bankStatementDoc'
        }

        "/statements/$action?/$id?(.$format)?"{
            controller = 'bankStatement'
        }

        "/statements/items/$action?/$id?(.$format)?"{
            controller = 'bankStatementItem'
        }

        "/statements/archives/$action?/$id?(.$format)?"{
            controller = 'bankStatementArchive'
        }

        // REST mappings
        "/rest/$restController"{
            controller = {"${params.restController}Rest"}
            action = [GET:"index"]
        }
        "/rest/$restController/$id"{
            controller = {"${params.restController}Rest"}        // ex. /rest/project/5
            action = [GET:"show"]
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "404"(view:'/404')
	}
}
