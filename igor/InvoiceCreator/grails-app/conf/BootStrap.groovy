import com.genrep.document.InvoiceAttachment
import grails.converters.JSON
import grails.util.Environment
import org.codehaus.groovy.grails.web.json.JSONObject


class BootStrap {

//    def webService
    def grailsApplication
    def invoiceCounterService

    def init = { servletContext ->

        // =========  INIT BEANS  ===========
        invoiceCounterService.init()
        // ========= *** END ***  ===========

        if(Environment.current == Environment.DEVELOPMENT ){

//        def catalinaBase = java.lang.System.properties.getProperty('catalina.base')
//            System.properties.each {
//                println it
//            }

//            grailsApplication.addArtefact(com.genrep.authentication.User)
//
//            println "GRAILS APPLICATION LOADED CLASSES"
//            grailsApplication.allClasses.each {
//                println it
//            }
//            println "GRAILS APPLICATION LOADED ARTEFACTS"
//            grailsApplication.allArtefacts.each {
//                println it
//            }

            // Create Client
//            def client = Client.findByName('MOF') ?: new Client(name:"MOF").save(failOnError: true)

            // Create Invoice Item
//            def invoiceItem = InvoiceItem.findByDescription("Месечен надомест по договор 03-1645/10 за превентивно и адаптивно одржување" +
//                    "и унапредување на електронскиот систем за едукација месец април 2014") ?: new InvoiceItem(
//                    description: "Месечен надомест по договор 03-1645/10 за превентивно и адаптивно одржување" +
//                            "и унапредување на електронскиот систем за едукација месец април 2014",
//                    quantity: 1,
//                    unitPrice: 28500,
//                    tax: Taxes.EIGHTEEN,
//                    orderIndex: 0
//
//            ).save(failOnError: true)
//
//            // Create new Invoice Instance
//            def invoice = Invoice.findByInvoiceNumber('028/2014') ?: new Invoice(client: client,
//                    invoiceNumber: "028/2014",
//                    issueDate: new Date().clearTime(),
//                    paymentDate: new Date().plus(15),
//                    status:Statuses.GENERATED,
//                    inCurrency: 'MKD',
//                    name:"Фактура за БЈН (exam)",
//                    invoiceDescription: "Систем за обука на економски оператори"
//                    ).save(flush: true, failOnError: true)
//
//            // Create Invoice Body and fill it with items
//            // def invoiceBodyIn = new InvoiceBody(invoice: invoice)
////            def invoiceBodyIn = InvoiceBody.findByInvoice(invoice)?: new InvoiceBody()
//            def invoiceBody = new InvoiceBodyIn()
//            if(invoiceBody.invoice == null) {
//
//                invoiceBody.addToInvoiceItems(invoiceItem)
//                invoiceBody.invoice = invoice
//                invoiceBody.currency = invoice.inCurrency
//                invoiceBody.save(flush: true, failOnError: true)
//
//                // Save the Invoice with the body
//                invoice.invoiceBodyIn = invoiceBody
//
//                if (invoice.hasErrors()) {
//                    invoice.errors.allErrors.each {
//                        println it
//                    }
//                }
//
//                invoice.save(flush: true)
//
//            }
//
//            def clonedInvoice = invoice.clone('029/2014')
//            println clonedInvoice.properties
//            clonedInvoice.save(flush: true,failOnError: true)
//            int c = 14
//            while(c > 0){
//                def clone = invoice.clone(c.toString()+'/2014')
//                clone.save(flush: true,failOnError: true)
//                c--
//            }

//            println "Put some Exchange Rates"
            // Create Exchange Rates
//            def mkdToEur = ExchangeRate.findByBaseCurrencyAndToCurrency(Currency.getInstance('MKD'),Currency.getInstance('EUR')) ?:
//                    new ExchangeRate(
//                    baseCurrency: 'MKD',
//                    toCurrency: 'EUR',
//                    baseCurrencyMeanValue: 1,
//                    toCurrencyMeanValue: 61.6827,
//                    rate: 0.016).save(flush: true, failOnError: true)
//            def mkdToUsd = ExchangeRate.findByBaseCurrencyAndToCurrency(Currency.getInstance('MKD'),Currency.getInstance('USD')) ?:
//                    new ExchangeRate(
//                    baseCurrency: 'MKD',
//                    toCurrency: 'USD',
//                    baseCurrencyMeanValue: 1,
//                    toCurrencyMeanValue: 61.6827,
//                    rate: 0.022).save(flush: true, failOnError: true)
//            def mkdToGbp = ExchangeRate.findByBaseCurrencyAndToCurrency(Currency.getInstance('MKD'),Currency.getInstance('GBP')) ?:
//                    new ExchangeRate(
//                    baseCurrency: 'MKD',
//                    toCurrency: 'GBP',
//                    baseCurrencyMeanValue: 1,
//                    toCurrencyMeanValue: 77.0552,
//                    rate: 1/77.0552).save(flush: true, failOnError: true)
//            def mkdToChf = ExchangeRate.findByBaseCurrencyAndToCurrency(Currency.getInstance('MKD'),Currency.getInstance('CHF')) ?:
//                    new ExchangeRate(
//                    baseCurrency: 'MKD',
//                    toCurrency: 'CHF',
//                    baseCurrencyMeanValue: 1,
//                    toCurrencyMeanValue: 50.6426,
//                    rate: 1/50.6426).save(flush: true, failOnError: true)
//
//            Kurs kurs = new Kurs()
//            KursSoap kursSoapClient = kurs.getKursSoap()
//            println kursSoapClient.getExchangeRate('18.06.2014','18.06.2014')

            // Test bankStatements and relationship between Mongo and Hibernate
//            File tempFile = File.createTempFile("temp-file", "ext", null)
//
//            BankStatementDoc bsd = new BankStatementDoc(
//                documentFile: tempFile.readBytes(),
//                issueDate: new Date()
//            ).save(flush: true)
//
//            new BankStatement(
//                    statementId : UUID.randomUUID(),
//                    issueDate : new Date(),
//                    bankAccountNumber : UUID.randomUUID(),
//                    balanceBefore: 100000,
//                    balanceAfter: 100000,
//                    bankStatementDocId: bsd.uuid
//            ).save(flush: true)

        }

        grailsApplication.config.flatten().entrySet().each {
            println it.key + " = " + it.value
        }

//        def invoiceArchives = InvoiceArchive.findAll()
//        for (invoice in invoiceArchives){
//            println invoice.invoiceNumber
//            def invoiceNumber = invoice.invoiceNumber.split("/")
//            if(invoiceNumber[1].contains(" ")){
//                invoiceNumber[1]=invoiceNumber[1].split(" ")[0]
//            }
//            invoice.invoiceCounter = Long.parseLong(invoiceNumber[0])
//            invoice.invoiceYear = Long.parseLong(invoiceNumber[1])
//            if(invoice.save(flush: true)){
//                def ice = InvoiceCounterEvidence.findWhere(invoiceNumber: invoice.invoiceNumber)
//                if(ice==null){
//                    new InvoiceCounterEvidence(
//                            invoiceCounter:  invoice.invoiceCounter,
//                            invoiceYear: invoice.invoiceYear,
//                            invoiceNumber: invoice.invoiceNumber,
//                            invoiceType: invoice.getClass().getSimpleName()
//                    ).save(flush: true)
//                }
//            }
//        }
    }
    def destroy = {
//        def ctx = org.codehaus.groovy.grails.web.context.ServletContextHolder.servletContext.getAttribute(org.codehaus.groovy.grails.web.servlet.GrailsApplicationAttributes.APPLICATION_CONTEXT);
    }
}
