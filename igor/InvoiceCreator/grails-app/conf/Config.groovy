import com.genrep.concurrency.locking.provider.DomainObjectLockingProvider
import com.genrep.invoiceCreator.Money
import grails.plugin.springsecurity.SecurityConfigType

// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination

// The ACCEPT header will not be used for content negotiation for user agents containing the following strings (defaults to the 4 major rendering engines)
grails.mime.disable.accept.header.userAgents = ['Gecko', 'WebKit', 'Presto', 'Trident']
grails.mime.types = [ // the first one is the default format
    all:           '*/*', // 'all' maps to '*' or the first available format in withFormat
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    hal:           ['application/hal+json','application/hal+xml'],
    xml:           ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*']
grails.resources.adhoc.includes = ['/images/**', '/css/**', '/js/**', '/plugins/**']

// Legacy setting for codec used to encode data with ${}
grails.views.default.codec = "html"

// The default scope for controllers. May be prototype, session or singleton.
// If unspecified, controllers are prototype scoped.
grails.controllers.defaultScope = 'singleton'

// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside ${}
                scriptlet = 'html' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        // filteringCodecForContentType.'text/html' = 'html'
    }
}


grails.converters.encoding = "UTF-8"
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

// configure passing transaction's read-only attribute to Hibernate session, queries and criterias
// set "singleSession = false" OSIV mode in hibernate configuration after enabling
grails.hibernate.pass.readonly = false
// configure passing read-only to OSIV session by default, requires "singleSession = false" OSIV mode
grails.hibernate.osiv.readonly = false

// Enable DBConsole for H2
grails.dbconsole.enabled = true
//grails.dbconsole.urlRoot = '/admin/dbconsole'

environments {
    development {
        grails.logging.jul.usebridge = true
    }
    test {
        grails.gsp.enable.reload = true
    }
    production {
        grails.config.locations = ["classpath:preferences.properties"]
//        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.com"
    }
}

// log4j configuration
log4j = {

    def catalinaBase = System.properties.getProperty('catalina.base')
    if (!catalinaBase) catalinaBase = '.'
    def logDirectory = "${catalinaBase}"+File.separator+"logs"

    appenders {

        console name:'stdout'
        rollingFile name: 'applog', file: "${logDirectory}${File.separator}InvoiceCreator.log".toString(),
                                    maxFileSize: '100MB'
//        rollingFile name: 'testLog', file:"${logDirectory}${File.separator}test.log".toString(),
//                                    maxFileSize: '100MB'
        'null' name:'stacktrace' // prevent Grails trying to create stacktrace.log
    }

    root {
        info 'stdout', 'applog'
    }

    // Send full stack traces to the main appName.log file
    warn applog:'StackTrace'

    // individual warn/all logger configurations as before

    error  'org.codehaus.groovy.grails.web.servlet',        // controllers
           'org.codehaus.groovy.grails.web.pages',          // GSP
           'org.codehaus.groovy.grails.web.sitemesh',       // layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping',        // URL mapping
           'org.codehaus.groovy.grails.commons',            // core / classloading
           'org.codehaus.groovy.grails.plugins',            // plugins
           'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'

    debug  'com.genrep.invoiceCreator'
    debug  'com.genrep.authentication'
    debug  'com.genrep.invoiceRepository'
    debug  'com.genrep.statements'
    debug  'com.genrep.sharedControllers'
    debug  'com.genrep.account'
    debug  'com.genrep.client'
    debug  'com.genrep.domain'
    debug  'com.genrep.filterPane'
    debug  'com.genrep.sharedHelpers'

//    debug testLog:'com.genrep.InvoiceCreator'

}

grails.databinding.dateFormats = [
        'dd.MM.yyyy','MMddyyyy', 'yyyy-MM-dd HH:mm:ss.S', "yyyy-MM-dd'T'hh:mm:ss'Z'"]

grails.gorm.default.mapping = {
    "user-type" type: Money, class: Money
}

//grails.databinding.convertEmptyStringsToNull=false
grails.databinding.trimStrings=true

cxf {
    client {
        nbrmClient {
            wsdl = "http://www.nbrm.mk/klservice/kurs.asmx?wsdl"

            clientInterface = mk.nbrm.klservice.KursSoap
            serviceEndpointAddress = "http://www.nbrm.mk/klservice/kurs.asmx"
        }
    }
}
// For converting closure in controllers to actions which are more memory efficient
grails.compile.artefacts.closures.convert = true

grails.plugin.springsecurity.rejectIfNoRule = true
grails.plugin.springsecurity.securityConfigType = SecurityConfigType.InterceptUrlMap
grails.plugin.springsecurity.password.algorithm = 'MD5'
grails.plugin.springsecurity.password.hash.iterations = 1
grails.plugin.springsecurity.dao.hideUserNotFoundExceptions = false
grails.plugin.springsecurity.logout.postOnly = false

grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.genrep.auth.GenrepUser'


grails.plugin.springsecurity.interceptUrlMap = [
        '/exchangeRate/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/invoice/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/invoiceArchive/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/invoiceItem/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/report/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/client/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/vendor/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/jasper/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/jasperDemo/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/plugins/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/statements/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/worker/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/invoiceDocument/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/bankAccount/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/invoicePurchaseArchive/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/project/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/quantityUnit/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/paymentTerm/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/invoiceAttachment/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/mongoBlob/**': ['IS_AUTHENTICATED_REMEMBERED'],
        '/applet/**': ['permitAll'],
        '/js/**':        ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/css/**':       ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/images/**':    ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/*':            ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/login/**':     ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/logout/**':    ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/dbconsole/**':  ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/assets/**':  ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/temp/**':  ['IS_AUTHENTICATED_ANONYMOUSLY'],
        '/rest/**': ['IS_AUTHENTICATED_ANONYMOUSLY']
]


lockingServiceConfig {
    provider {
        type = DomainObjectLockingProvider
    }
}