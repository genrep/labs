import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject

class JsonWorkaroundBootStrap {
    def init = { servletContext ->
        // activate workaround for GRAILS-10823
        println("activating workaround for GRAILS-10823 - use this only for Grails 2.3.3")
        org.springframework.util.ReflectionUtils.findField(JSONObject, "useStreamingJavascriptEncoder").with {
            accessible = true
            set(null, false)
        }

        // ==== DYNAMIC METHODS INJECTION ====
        // Define the new method
        JSON.metaClass.static.parse = {String json, Class clazz ->

            List jsonObjs = JSON.parse(json)

            jsonObjs.collect {JSONObject jsonObj ->

                // If the user hasn't provided a targetClass read the 'class' property in the JSON to figure out which type to convert to
                def targetClass = clazz ?: jsonObj.get('class') as Class
                def targetInstance = targetClass.newInstance()

                // Set the properties of targetInstance
                jsonObj.entrySet().each {entry ->

                    if (entry.key != "class") {
                        targetInstance."$entry.key" = entry.value
                    }
                }
                targetInstance
            }

        } // end of the method
    }
    def destroy = {
    }
}