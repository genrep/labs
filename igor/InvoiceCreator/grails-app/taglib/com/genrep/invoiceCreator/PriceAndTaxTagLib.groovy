package com.genrep.invoiceCreator

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat

class PriceAndTaxTagLib {

    //static defaultEncodeAs = 'html'
    //static encodeAsForTags = [tagName: 'raw']
    /**
     * Creates new price or tax field
     *
     * @attr id REQUIRED
     */
    def priceAndTaxField = {attrs->

        def writer = out
        char decimalSeparator = ','
        def pattern = '###0.00'
        String output = ''
        def html = """"""

        if (!attrs.id)
            throwTagError("Tag [priceField] is missing required attribute [id]")

        def id = attrs.id
        def name = id

//        if (!attrs.number)
//            throwTagError("Tag [priceField] is missing required attribute [number]")

        // Create a DecimalFormat that fits your requirements
        if(attrs.value!=null) {
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator(decimalSeparator)

            DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
            decimalFormat.setParseBigDecimal(true);

            def number
            if(attrs.value instanceof String){
                number = (BigDecimal) decimalFormat.parse(attrs.value)
            }else{
                number = attrs.value
            }

            // Format the output
            Locale loc = new Locale('mk', 'MK')
            NumberFormat nf = NumberFormat.getNumberInstance(loc);
            DecimalFormat df = (DecimalFormat) nf;
            df.applyPattern(pattern);
            output = df.format(number);
        }

        def dataTag = attrs.find{k,v->
            k.startsWith('data-')
        }


        if(dataTag){
            def dataName = dataTag.key
            def dataValue = dataTag.value

            html  =  """
                <input type="text" name="${id}" id="${id}" value="${output}" ${dataName}="${dataValue}" />
            """
        }
        else{
            html  =  """
                <input type="text" name="${id}" id="${id}" value="${output}" />
            """
        }

        writer << html
    }
}


