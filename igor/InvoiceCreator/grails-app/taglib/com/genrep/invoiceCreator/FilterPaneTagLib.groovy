package com.genrep.invoiceCreator

import com.genrep.filterPane.FilterPaneUtils
import org.codehaus.groovy.grails.commons.GrailsDomainClass

class FilterPaneTagLib {

//    static defaultEncodeAs = [taglib: 'html']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
    static namespace = "gnrp"

    def grailsApplication

    private static final String DEFAULT_FORM_METHOD = 'post'
    private static final String DefaultFilterPaneId = 'filterPane'

    def filterPane = { attrs, body ->

//        domain="com.genrep.invoiceCreator.Invoice"
//        filterParams="issueDate,client,invoiceNumber"
//        customForm="true"

        if (!attrs.domain) {
            log.error("domain attribute is required")
            return
        }

        // Validate required info
        GrailsDomainClass domain = FilterPaneUtils.resolveDomainClass(grailsApplication, attrs.domain)
        if (domain == null) {
            log.error("Unable to resolve domain class for ${attrs.domain}")
            return
        }

        if(!attrs.filterParams){
            log.error("filterParams attribute is required")
            return
        }

        List filterParams = resolveListAttribute(attrs.filterParams)
        List persistentProps = domain.persistentProperties as List

        if(!validateFilterParams(filterParams,persistentProps)){
            log.error("filterParams attribute contains wrong param!")
            return
        }

        def renderModel = [:]

        renderModel.domain = domain.name
        renderModel.formName = attrs.formName ?: 'filterPaneForm'
        renderModel.containerId = attrs.id ?: DefaultFilterPaneId
        renderModel.formMethod = attrs.formMethod ?: DEFAULT_FORM_METHOD
        renderModel.controller = attrs.controller
        renderModel.action = attrs.action ?: 'filter'
//        renderModel.customForm = "true".equalsIgnoreCase(attrs?.customForm) || attrs?.customForm == true
        renderModel.formAction = renderModel.controller ?
                g.createLink(controller: renderModel.controller, action: renderModel.action) :
                renderModel.action

        renderModel.properties = [:]
        filterParams.each { param ->
            // Loop param names and put them in properties map with name as key and type as value
            renderModel.properties.putAt(param,domain.getPropertyByName(param).type)
        }

        out << g.render(template: "/invoice/filterPane", model: [fp: renderModel])
    }


    def multiDomainFilterPane = { attrs, body ->

        if (!attrs.domains) {
            log.error("domains attribute is required")
            return
        }

        if (!attrs.filterParams) {
            log.error("filterParams attribute is required")
            return
        }

        List domains = attrs.domains.split(',')
        List filterParams = resolveListAttribute(attrs.filterParams)

        for(domain in domains) {
            // Validate required info
            GrailsDomainClass domainClass = FilterPaneUtils.resolveDomainClass(grailsApplication, domain)
            if (domainClass == null) {
                log.error("Unable to resolve domain class for ${attrs.domainClass}")
                return
            }

            List persistentProps = domainClass.persistentProperties as List

            // It is possible the required filter param to be found like domain property in some association
            // from the main domain. Simple check code will be this:
//            List secondLevelProps = []
//            persistentProps.each {
//                println it
//                if (it.isAssociation()) {
//                    it.referencedDomainClass.persistentProperties.each {
//                        secondLevelProps << it
//                    }
//                }
//            }
//            validateFilterParams(filterParams, secondLevelProps)

            if (!validateFilterParams(filterParams, persistentProps)) {
                log.error("filterParams attribute contains wrong param!")
                return
            }
        }

        def renderModel = [:]

        renderModel.domains = domains
        renderModel.formName = attrs.formName ?: 'filterPaneForm'
        renderModel.containerId = attrs.id ?: DefaultFilterPaneId
        renderModel.formMethod = attrs.formMethod ?: DEFAULT_FORM_METHOD
        renderModel.controller = attrs.controller
        renderModel.action = attrs.action ?: 'multiDomainFilter'
        renderModel.formAction = renderModel.controller ?
                g.createLink(controller: renderModel.controller, action: renderModel.action) :
                renderModel.action

        renderModel.properties = [:]
        GrailsDomainClass tempDom = FilterPaneUtils.resolveDomainClass(grailsApplication, domains[0])
        filterParams.each { fparam ->
            // Loop param names and put them in properties map with name as key and type as value
            renderModel.properties.putAt(fparam,tempDom.getPropertyByName(fparam).type)
        }

        out << g.render(template: "/report/multiDomainFilterPane", model: [fp: renderModel])
    }

    private Boolean validateFilterParams(List filterParams, List persistentProps){
        def persistentPropsNames = persistentProps.collect {it.name}
        for(param in filterParams){
            if(!persistentPropsNames.contains(param))
                return false
        }
        return true
    }

    private List resolveListAttribute(attr) {
        List temp = []
        if (attr != null) {
            if (attr instanceof List) {
                temp = attr
            } else if (attr instanceof String) {
                temp = attr.split(",") as List
            }
        }
        temp.collect { it.trim() }
    }


}
