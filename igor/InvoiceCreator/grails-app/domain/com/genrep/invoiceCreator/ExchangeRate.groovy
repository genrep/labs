package com.genrep.invoiceCreator

import org.grails.databinding.BindUsing

import java.text.SimpleDateFormat

class ExchangeRate {

    @BindUsing({ obj, source -> Currency.getInstance(source['baseCurrency']) })
    Currency baseCurrency
    @BindUsing({ obj, source -> Currency.getInstance(source['toCurrency'])})
    Currency toCurrency

    int nominal = 1
    BigDecimal baseCurrencyMeanValue
    BigDecimal toCurrencyMeanValue

    BigDecimal rate

    Date date = new Date().clearTime()

    static constraints = {
        nominal()
        rate(scale:4)
        baseCurrencyMeanValue(scale:4)
        toCurrencyMeanValue(scale:4)
        date(unique:['baseCurrency', 'toCurrency'])
    }

    String toString() {
        SimpleDateFormat sformat = new SimpleDateFormat("dd.MM.yyyy")
        String datum = sformat.format(date)
        return datum+": $baseCurrency to $toCurrency @ $rate"
    }
}
