package com.genrep.invoiceCreator

import com.genrep.account.BankAccount
import com.genrep.domain.AInvoice
import com.genrep.invoiceCounter.InvoiceCounterEvidence
import org.grails.databinding.BindUsing

//import javax.enterprise.context.SessionScoped
//
//@SessionScoped
class Invoice extends AInvoice {

    static constraints = {
        name(maxSize: 200,nullable: true,blank: true)
        invoiceDescription(maxSize: 3000,nullable: true,blank: true)
        client()
        invoiceCounter()
        invoiceYear()
        invoiceNumber unique: true , validator: {
            if(it==null)
                ['my.custom.message']
        }
        swiftCode(nullable:true,blank:true)
        invoiceBodyIn(nullable: true)
        invoiceBodyOut(nullable: true)
        issueDate()
        paymentDate(nullable:true,validator: { val, obj ->
            val?.after(obj.issueDate)
        })
        dateCreated()
        status(nullable: true)
        inCurrency()
        outCurrency(nullable: true)
        exchangeRate(nullable: true)
        invoiceNumberSort(nullable: true, blank: true)
        paymentPercentage(nullable: true)
        bankAccount(nullable: true)
        paidByStatement(nullable: true)
        attention nullable: true
        paymentDeadline nullable: true
        totalPriceInWords nullable: true, maxSize: 1000
        invoiceReversal nullable: true
    }

    static mapping = {
        sort invoiceYear: "desc"
        sort invoiceCounter: "desc"
    }

    // InvoiceBodyIn is the Invoice in its original inCurrency
    // InvoiceBodyOut is the Invoice converted in other inCurrency
    InvoiceBodyIn invoiceBodyIn
    InvoiceBodyOut invoiceBodyOut

    String name
    String invoiceDescription
    String invoiceNumberSort

    Date paymentDate // issueDate + paymentDeadline
    Date dateCreated
    Date invoiceReversal // date of invoice reversal. If null, invoice is not reversed

    Statuses status

    @BindUsing({ obj, source -> Currency.getInstance(source['inCurrency']) })
    Currency inCurrency
    @BindUsing({ obj, source -> Currency.getInstance(source['outCurrency'])})
    Currency outCurrency

    // TODO: da se apdejtira pri copy na invoice
    ExchangeRate exchangeRate

    BigDecimal paymentPercentage
    String paidByStatement

    BankAccount bankAccount

    // Custom fields for Invoice PDF report
    // Don't clone
    Integer paymentDeadline // in days
    String attention
    String totalPriceInWords

    Invoice clone(String invoiceNumber,String invoiceCounter, String invoiceYear){
        return new Invoice(
                client:this?.client,
                invoiceNumber: invoiceNumber,
                invoiceCounter: invoiceCounter as Long,
                invoiceYear: invoiceYear as Long,
                invoiceBodyIn: this.invoiceBodyIn?.clone(),
                invoiceBodyOut: this.invoiceBodyOut?.clone(),
                issueDate: new Date(),
                status: Statuses.GENERATED,
                inCurrency: this?.inCurrency?.toString(),
                outCurrency: this.outCurrency?.toString() ?: "",
//                exchangeRate: this?.exchangeRate,
                invoiceNumberSort: invoiceYear+"-"+invoiceCounter,
                bankAccount: this?.bankAccount,
                invoiceDescription: this?.invoiceDescription
        )
    }

    def beforeInsert(){
        name = invoiceNumberSort.concat("-").concat(Converter.getInstance().convertText(client.toString(),Converter.ConvertType.CYR2LAT))
        if(paymentDeadline && paymentDate == null) {
            paymentDate = issueDate.plus(paymentDeadline)
        }
        else{
            if(paymentDate!=null) {
                paymentDeadline = paymentDate.minus(issueDate)
            }
        }
        new InvoiceCounterEvidence(
                invoiceCounter: invoiceCounter,
                invoiceYear: invoiceYear,
                invoiceNumber: invoiceNumber,
                invoiceType: getClass().getSimpleName()
        ).save()
    }

    def beforeUpdate(){
        name = invoiceNumberSort.concat("-").concat(Converter.getInstance().convertText(client.toString(),Converter.ConvertType.CYR2LAT))
        if(paymentDeadline && paymentDate == null) {
            paymentDate = issueDate.plus(paymentDeadline)
        }
        else {
            if(paymentDate!=null) {
                paymentDeadline = paymentDate.minus(issueDate)
            }
        }

        if(isDirty('invoiceNumber')){
            def invoiceCounterEvidence = InvoiceCounterEvidence.findWhere(invoiceNumber: getPersistentValue('invoiceNumber'))
            invoiceCounterEvidence.properties = properties
            invoiceCounterEvidence.save()
        }
    }


}

public enum Statuses {
    GENERATED('Generated'),
    SENT('Sent'),
    SIGNED('Signed'),
    FINALIZED('Finalized')

    String name

    Statuses(String name) {
        this.name = name
    }
}

public enum Currencies {
    MKD(Currency.getInstance('MKD')),
    EUR(Currency.getInstance('EUR')),
    USD(Currency.getInstance('USD')),
    GBP(Currency.getInstance('GBP')),
    CHF(Currency.getInstance('CHF'))

    Currency currency
    Currencies(Currency currency){
        this.currency = currency
    }
}