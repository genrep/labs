package com.genrep.invoiceCreator

import com.genrep.invoiceRepository.Taxes
import com.genrep.quantityUnit.QuantityUnit
import org.grails.databinding.BindUsing

class InvoiceItem {

    static constraints = {
        orderIndex()
        description(maxSize: 2048)
        quantity()
        unitPrice(scale:2)
        tax()
        taxLabel(nullable: true)
        taxPricePerUnit(nullable: true,scale:2)
        taxFreePrice(nullable: true,scale:2)
        taxPrice(nullable: true,scale:2)
        totalPrice(nullable: true,scale:2)
        quantityUnit nullable: true
    }
    // Vnesuva korisnik
    int orderIndex = 0
    String description
    Double quantity
    String quantityUnit

    BigDecimal unitPrice

    @BindUsing({ obj, source ->
        if(source['tax'] instanceof String){
            Taxes.valueOf(source['tax'])
        }
        else {
            source['tax']
        }
    })
    Taxes tax
    String taxLabel

    // Presmetka na sistem
    BigDecimal taxPricePerUnit
    BigDecimal taxFreePrice
    BigDecimal taxPrice
    BigDecimal totalPrice

    UUID sessionId = UUID.randomUUID()

//    static transients = ['sessionId']
    static mapping = {
        sort 'orderIndex'
    }

    def beforeInsert(){
        def taxHelp = tax.value / 100
        taxPricePerUnit = unitPrice * taxHelp
        taxFreePrice = quantity * unitPrice
        taxPrice = quantity * taxPricePerUnit
        totalPrice = taxFreePrice + taxPrice
        taxLabel = tax?.value?.toString()
    }

    def beforeUpdate(){
        taxLabel = tax?.value?.toString()
    }

    HashMap getEditableFields(){
        def map = [:]
        map << [
                id: id,
                sessionId : sessionId.toString(),
                description:description,
                quantity:quantity,
                quantityUnit:quantityUnit,
                unitPrice:unitPrice?.decodeDecimalNumber(),
                tax:tax.toString()]

        return map
    }

    InvoiceItem clone(){
        return new InvoiceItem(orderIndex:this.orderIndex,
                description:this.description,
                quantity:this.quantity,
                unitPrice:this.unitPrice,
                tax:this.tax,
                taxPricePerUnit:this.taxPricePerUnit,
                taxFreePrice:this.taxFreePrice,
                taxPrice:this.taxPrice,
                totalPrice:this.totalPrice,
                quantityUnit:this?.quantityUnit
        )
    }
}
