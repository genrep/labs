package com.genrep.invoiceCreator

import java.math.RoundingMode

class InvoiceBody {

    static constraints = {
        currency()
        invoiceItems()
        taxFreeSum(nullable: true,scale:2)
        taxPriceSum(nullable: true,scale:2)
        totalSum(nullable: true,scale:2)
    }

    static hasMany = [invoiceItems:InvoiceItem]

    static mapping = {
        invoiceItems cascade: 'all,save-update,delete,all-delete-orphan'
    }
    static fetchMode = [invoiceItems: 'eager']

    Currency currency
    BigDecimal taxFreeSum
    BigDecimal taxPriceSum
    BigDecimal totalSum

    List<InvoiceItem> invoiceItems

    BigDecimal getTotalSumRounded(){
        return taxFreeSum.setScale(0,RoundingMode.HALF_EVEN).add(taxPriceSum.setScale(0,RoundingMode.HALF_EVEN))
    }

    def afterUpdate(){
        if(invoiceItems?.size()>0) {
            def items = invoiceItems
            BigDecimal _taxFreeSum = 0
            BigDecimal _taxPriceSum = 0
            BigDecimal _totalSum = 0

            items.each { invoiceItem ->
                _taxFreeSum += invoiceItem.taxFreePrice.toBigDecimal()
                _taxPriceSum += invoiceItem.taxPrice.toBigDecimal()
                _totalSum += invoiceItem.totalPrice.toBigDecimal()
            }

            taxFreeSum = _taxFreeSum
            taxPriceSum = _taxPriceSum
            totalSum = _totalSum
        }else{
            taxFreeSum = 0
            taxPriceSum = 0
            totalSum = 0
        }
    }

    def afterInsert(){

        if(invoiceItems?.size()>0) {
            def items = invoiceItems
            BigDecimal _taxFreeSum = 0
            BigDecimal _taxPriceSum = 0
            BigDecimal _totalSum = 0

            items.each { invoiceItem ->
                _taxFreeSum += invoiceItem.taxFreePrice.toBigDecimal()
                _taxPriceSum += invoiceItem.taxPrice.toBigDecimal()
                _totalSum += invoiceItem.totalPrice.toBigDecimal()
            }

            taxFreeSum = _taxFreeSum
            taxPriceSum = _taxPriceSum
            totalSum = _totalSum
        }
    }
}
