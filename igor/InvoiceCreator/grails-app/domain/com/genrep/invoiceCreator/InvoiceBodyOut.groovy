package com.genrep.invoiceCreator

class InvoiceBodyOut extends InvoiceBody {

    static belongsTo = [invoice:Invoice]

    InvoiceBodyOut clone(){
        List<InvoiceItem> invoiceItems = new ArrayList<InvoiceItem>()
        this.invoiceItems.each { invoiceItem ->
            invoiceItems << invoiceItem.clone()
        }
        return new InvoiceBodyOut(currency: this.currency,
                invoiceItems: invoiceItems,
                taxFreeSum: this.taxFreeSum,
                taxPriceSum: this.taxPriceSum,
                totalSum: this.totalSum)
    }
}
