package com.genrep.invoiceCreator

class InvoiceBodyIn extends InvoiceBody {

    static belongsTo = [invoice:Invoice]

    InvoiceBodyIn clone(){
        List<InvoiceItem> invoiceItems = new ArrayList<InvoiceItem>()
        this.invoiceItems.each { invoiceItem ->
            invoiceItems << invoiceItem.clone()
        }
        return new InvoiceBodyIn(currency: this.currency,
                invoiceItems: invoiceItems,
                taxFreeSum: this.taxFreeSum,
                taxPriceSum: this.taxPriceSum,
                totalSum: this.totalSum)
    }
}
