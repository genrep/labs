package com.genrep.statements

import com.genrep.account.BankAccount

class BankStatement {

    static constraints = {
        ownerOfStatement()
        bankStatementId()
        issueDate()
        balanceBefore()
        balanceAfter()
        currency(nullable:true)
        bankStatementDocId(nullable:true)
        items(nullable: true)
    }

    // This bank account should be the same as one of the creditor or debtor from the transcation
    // fields of BankStatementItem
    BankAccount ownerOfStatement
    String bankStatementId
    Date issueDate
    BigDecimal balanceBefore
    BigDecimal balanceAfter
    Currency currency
    // If the invoice is imported from file
    String bankStatementDocId
    String bankAccountNumber // For parsing

    static hasMany = [items:BankStatementItem]
    static transients = ['bankAccountNumber']

    int findLargestOrderNumber(){
        int largest = 0
        for( item in items ){
            if(item.orderNumber > largest){
                largest = item.orderNumber
            }
        }
        return largest
    }
}
