package com.genrep.statements

import com.genrep.domain.Transaction

class BankStatementItem extends Transaction {

    static constraints = {
        orderNumber()
        publicEntity(blank: false)
        publicEntityBankAccountNumber(blank: false)
        transferSum()
        // Money receives are 'credited' money delivers are 'debited'
        transferFlow(nullable:true,inList: ["credited","debited"])
        transactionCode(blank: false)
        shortDescription(blank: false)
        borrowingCode(blank: false)
        approvalCode(blank: false)
        sessionId()
        //Transaction relevant fields
        creditor nullable:true
        debtor nullable:true
        amount nullable:true
        date nullable:true
    }

   static belongsTo = [BankStatement]

    // This fields are relevant for the report
    int orderNumber = 0
    String publicEntity
    String publicEntityBankAccountNumber
    BigDecimal transferSum
    String transferFlow
    String shortDescription
    String borrowingCode
    String approvalCode

    UUID sessionId = UUID.randomUUID()

    def beforeInsert(){
        // if transferSum is positive
        if(transferSum.compareTo(BigDecimal.ZERO) > 0){
            transferFlow = 'credited'
        }else{
            transferFlow = 'debited'
        }
    }
}
