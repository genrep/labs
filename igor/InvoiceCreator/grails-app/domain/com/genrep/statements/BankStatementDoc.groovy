package com.genrep.statements

/**
 * This class is used for imported Bank Statements from a matching standard
 */
class BankStatementDoc {

    static constraints = {
        uuid()
        documentFile()
        documentName(nullable: true, blank:true)
        mimeType(nullable:true,blank:true)
        isTransferred()
        issueDate(nullable: true)
        dateCreated()
    }

    String uuid = UUID.randomUUID()
    byte [] documentFile
    String documentName
    String mimeType
    boolean isTransferred = false

    Date issueDate
    Date dateCreated

    static mapWith = "mongo"

    static mapping = {
        collection "BankStatementDocuments"
//        database "InvoiceRepository"
    }
}
