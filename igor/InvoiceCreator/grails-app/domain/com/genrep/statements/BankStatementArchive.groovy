package com.genrep.statements


/**
 * This class is used for importing Bank Statements purely as documents/files
 */
class BankStatementArchive {

    static constraints = {
        docMongoId()
        bankStatementId(unique:true)
        ownerOfStatement(nullable:true)
        documentFile()
        documentName(nullable:true,blank:true)
        dateCreated()
        issueDate(nullable:true)
    }

    static mapWith = "mongo"

    static mapping = {
        collection "BankStatementArchives"
//        database "InvoiceRepository"
    }

    String docMongoId = UUID.randomUUID().toString()

    String bankStatementId
    Long ownerOfStatement // BankAccount foreign key

    byte [] documentFile
    String documentName

    Date dateCreated
    Date issueDate

}
