package com.genrep.quantityUnit

class QuantityUnit {

    String enName
    String mkName

    static constraints = {
        enName nullable: true, unique: true
        mkName nullable: true
    }

}
