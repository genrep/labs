package com.genrep.vendor

import com.genrep.account.BankAccount

class Vendor {

    static constraints = {
        name(blank:false)
        department(nullable: true,blank: true)
        address(nullable: true, blank: true)
        bankAccount(nullable:true)
        viewInfo(nullable: true,blank: true,maxSize: 3000)

    }

    String name
    String department
    String address
    BankAccount bankAccount
    String bankAccountNumber
    String viewInfo

    static transients = ['bankAccountNumber']

    static mapping = {
        sort 'name'
    }

    String toString(){
        name
    }
}
