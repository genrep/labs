package com.genrep.invoiceCounter

class InvoiceCounterEvidence {

    static constraints = {
        invoiceType()
        invoiceCounter()
        invoiceYear()
        invoiceNumber()
        dateCreated()
    }

    String invoiceType
    Long invoiceCounter
    Long invoiceYear
    String invoiceNumber

    Date dateCreated
}
