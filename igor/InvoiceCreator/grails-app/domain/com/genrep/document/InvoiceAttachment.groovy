package com.genrep.document

import com.genrep.invoiceCreator.Invoice

class InvoiceAttachment {

    static constraints = {
        name()
        attachmentType()
        invoice()
    }

    String name
    String attachmentType
    Invoice invoice

    static mapWith = "mongo"
}
