package com.genrep.project

import com.genrep.client.Client

class ProjectDocument {

    static constraints = {
        docMongoId()
        technicalNumber()
        client()
        totalPrice()
        currency()
        documentFile()
        documentName()
        mimeType(nullable: true,blank:true)
        dateCreated()
    }

    static mapWith = "mongo"

    static mapping = {
        collection "ProjectDocuments"
    }

    String docMongoId = UUID.randomUUID()
    String technicalNumber  // this associates the Project with the ProjectDocument
    Client client

    BigDecimal totalPrice
    Currency currency

    byte [] documentFile
    String documentName
    String mimeType

    Date dateCreated
}
