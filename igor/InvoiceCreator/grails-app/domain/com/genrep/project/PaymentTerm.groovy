package com.genrep.project

import com.genrep.invoiceCreator.Invoice

class PaymentTerm {

    /**
     * Amount of payment price from the total price (percent)
     */
    Double amountOfPaymentPrice
    /**
     * Description of the payment term
     */
    String termDescription
    /**
     * Date of the payment
     */
    Date dateOfPayment

    static belongsTo = [project:Project]

    static hasMany = [invoices:Invoice]

    static constraints = {
        amountOfPaymentPrice()
        termDescription()
        dateOfPayment()
        project()
        invoices(nullable: true)
    }
}
