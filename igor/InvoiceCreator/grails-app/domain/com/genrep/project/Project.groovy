package com.genrep.project

import com.genrep.client.Client
import com.genrep.invoiceCreator.Invoice
import com.genrep.invoiceRepository.Taxes
import org.grails.databinding.BindUsing

class Project {

    /**
     * Project technical number
     */
    String technicalNumber
    /**
     * Project Title
     */
    String title
    /**
     * Project short description
     */
    String description
    /**
     * Contract Number of the contractor of the project
     */
    String contractNumber
    /**
     * Contract Number of the bearer of the project
     */
    String contractNumberBearer
    /**
     * Project estimated payment price (without tax)
     */
    BigDecimal price
    /**
     * Currency of the payment price
     */
    Currency currency
    /**
     * Tax of the price
     */
    @BindUsing({ obj, source ->
        if(source['tax'] instanceof String){
            Taxes.valueOf(source['tax'])
        }
        else {
            source['tax']
        }
    })
    Taxes tax
    /**
     * Total payment price of the project (included tax calculation)
     */
    BigDecimal totalPrice
    /**
     * Estimated start date
     */
    Date startDate
    /**
     * Estimated end date
     */
    Date endDate
    /**
     * State of the project
     */
    ProjectState projectState
    /**
     * Client associated with the project
     */
    Client client

    static hasMany = [invoices:Invoice, paymentTerms: PaymentTerm]

    static constraints = {
        technicalNumber(unique: true)
        title()
        description maxSize: 3000, nullable: true, blank: true
        contractNumber()
        contractNumberBearer()
        price nullable: true
        currency nullable: true
        tax nullable: true
        totalPrice nullable: true
        startDate nullable: true
        endDate nullable: true
        invoices  nullable: true
        paymentTerms nullable: true
        projectState nullable: true
        client()
    }
}

public enum ProjectState{
    PENDING("Pending"),
    DEVELOPMENT("Development"),
    FINISHED("Finished"),
    MAINTENANCE("Maintenance")

    String name

    ProjectState(String name){
        this.name = name
    }

}