package com.genrep.account

/**
 * Created by igor on 10/7/14.
 */
class PersonalBankAccount extends BankAccount {

    String firstName
    String lastName
    String ssn

    static constraints ={
        entityName(nullable: true)
        bankName(blank: false)
        bankAccountNumber(blank: false,unique: true)
        balance(nullable:true)
        firstName(blank: false)
        lastName(blank: false)
        ssn(nullable: true)
    }
}
