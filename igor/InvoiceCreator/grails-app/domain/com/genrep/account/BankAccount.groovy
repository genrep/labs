package com.genrep.account

class BankAccount {

    static constraints ={
        entityName(blank: false)
        bankName(blank: true, nullable:true)
        bankAccountNumber(unique:true,blank: false)
        taxNumber(nullable: true,blank: true)
        swiftCode(nullable:true,blank:true)
        balance(nullable:true)
        owner()
        printView(nullable: true,blank: true)
    }

    String entityName
    String bankName
    String bankAccountNumber
    String taxNumber
    String swiftCode
    BigDecimal balance
    boolean owner = false

    String printView

    String toString(){
        entityName
    }
}
