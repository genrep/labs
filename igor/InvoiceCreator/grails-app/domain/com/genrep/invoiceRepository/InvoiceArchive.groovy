package com.genrep.invoiceRepository

import com.genrep.domain.AInvoice
import com.genrep.invoiceCounter.InvoiceCounterEvidence
import com.genrep.invoiceCreator.ExchangeRate

class InvoiceArchive extends AInvoice {

    static constraints = {
        docMongoId()
        invoiceNumber(unique:true)
        invoiceCounter()
        invoiceYear()
        swiftCode(nullable: true,blank: true)
        client()
        totalSum()
        taxPriceSum()
        taxFreeSum()
        taxes(nullable: true)
        inCurrency()
        exchangeRate(nullable: true)
        documentFile(minSize: 1) // minSize=1 because we don't want to allow empty byte array
        documentName()
        issueDate()
        dateCreated()
        invoiceDescription(nullable: true, blank: true)
        projectId(nullable: true)
    }

    static mapWith = "mongo"

    static mapping = {
        collection "InvoiceArchives"
//        database "InvoiceRepository"
        sort 'invoiceNumber'
    }

    String docMongoId = UUID.randomUUID()
    BigDecimal totalSum
    BigDecimal taxPriceSum
    BigDecimal taxFreeSum

    Map<Taxes,BigDecimal> taxes

    Currency inCurrency
    ExchangeRate exchangeRate

    byte [] documentFile
    String documentName
    String invoiceDescription

    String projectId // foreign key to Project

    Date dateCreated

    def beforeInsert(){
        issueDate = issueDate.clearTime()
        new InvoiceCounterEvidence(
                invoiceCounter: invoiceCounter,
                invoiceYear: invoiceYear,
                invoiceNumber: invoiceNumber,
                invoiceType: getClass().getSimpleName()
        ).save()
    }
    def beforeUpdate() {
        if (this.isDirty('issueDate')) {
            issueDate = issueDate.clearTime()
        }
        // isDirty is not supported in mongo so this is workaround
        def invoiceArchive = InvoiceArchive.collection?.findOne(id)
        if(invoiceArchive.invoiceNumber!=invoiceNumber){
            def invoiceCounterEvidence = InvoiceCounterEvidence.findWhere(invoiceNumber: invoiceArchive.invoiceNumber)
            invoiceCounterEvidence.properties = properties
            invoiceCounterEvidence.save()
        }
    }
}

public enum Taxes {
    ZERO(0),
    FIVE(5),
    EIGHTEEN(18)

    BigDecimal value

    Taxes(BigDecimal value) {
        this.value = value
    }

//    String toString(){
//        value.toString()
//    }

    static Taxes getByValue(String value){
        BigDecimal val = new BigDecimal(value)
        def result
        switch(val) {
            case 0:
                result = ZERO
                break
            case 5:
                result = FIVE
                break
            case 18:
                result = EIGHTEEN
                break
        }
        result
    }

}
