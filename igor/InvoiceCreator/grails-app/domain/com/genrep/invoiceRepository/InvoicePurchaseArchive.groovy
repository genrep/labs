package com.genrep.invoiceRepository

import com.genrep.domain.AInvoice
import com.genrep.invoiceCreator.ExchangeRate
import com.genrep.vendor.Vendor

class InvoicePurchaseArchive extends AInvoice {

    static constraints = {
        admissionCounterNumber nullable:true, validator: { value, domain->
            if(!domain.isProInvoice){
                return value!=null
            }
        }
        admissionCounterNumberView nullable:true, unique: true, validator: { value, domain->
            if(!domain.isProInvoice){
                return value!=null
            }
        }
        docMongoId()
        invoiceNumber()
        invoiceCounter(nullable: true)
        invoiceYear()
        swiftCode(nullable: true,blank: true)
        client(nullable: true)
        vendor()
        totalSum()
        taxPriceSum()
        taxFreeSum()
        taxes(nullable: true)
        inCurrency()
        exchangeRate(nullable: true)
        documentFile(minSize: 1) // minSize=1 because we don't want to allow empty byte array
        documentName()
        issueDate()
        dateCreated()
        paymentDate(nullable: true)
        paidOnDate(nullable: true)
        invoiceDescription(nullable: true, blank: true)
        isProInvoice()
        refInvoice(nullable: true)
        paymentPercentage(nullable: true)
    }

    static mapWith = "mongo"

    static mapping = {
        collection "InvoicePurchaseArchives"
//        database "InvoiceRepository"
//        sort issueDate: "desc"
        sort([invoiceYear:'desc', admissionCounterNumber:'desc'])
    }

    Long admissionCounterNumber
    String admissionCounterNumberView

    String docMongoId = UUID.randomUUID()
    BigDecimal totalSum
    BigDecimal taxPriceSum
    BigDecimal taxFreeSum

    Map<Taxes,BigDecimal> taxes

    Currency inCurrency
    ExchangeRate exchangeRate
    // Instead of Client
    Vendor vendor

    byte [] documentFile
    String documentName
    String invoiceDescription

    Date dateCreated
    Date paymentDate
    Date paidOnDate

    BigDecimal paymentPercentage


    // ProInvoice features
    boolean isProInvoice = false
    // String refInvoice                       // Reference to ProInvoice. ComboBox of ProInvoice numbers
    InvoicePurchaseArchive refInvoice          // Reference to ProInvoice. ComboBox of ProInvoice numbers

    def beforeInsert(){
        issueDate = issueDate.clearTime()
    }
    def beforeUpdate(){
        if(this.isDirty('issueDate')) {
            issueDate = issueDate.clearTime()
        }
    }

    def beforeDelete(){
        if(refInvoice!=null){
            def ref = findWhere(id: refInvoice.id)
            if(ref){
                ref.refInvoice = null
                ref.save()
            }
        }
    }

    def afterInsert(){
        if(!isProInvoice && refInvoice!=null){
            def proInvoice = findWhere(id: refInvoice.id)
            if(proInvoice){
                proInvoice.refInvoice = this
                proInvoice.save()
            }
        }
    }

    static String createAdmissionCounterNumberView(Long year, Long counter){
        if(counter == null){
            return null
        }
        return counter.toString().concat("/").concat(year.toString())
    }
}
