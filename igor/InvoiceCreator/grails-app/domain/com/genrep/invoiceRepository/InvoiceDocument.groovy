package com.genrep.invoiceRepository

import com.genrep.domain.AInvoice
import com.genrep.invoiceCreator.ExchangeRate

/**
 * InvoiceDocument is Mongo representation of the Invoice object.
 * It would be created and saved as it is when new signed PDF of the invoice is created by the applet.
 * Or it can be exported manually without signing.
 * InvoiceNumber is crucial field for referencing the real Invoice object.
 */
class InvoiceDocument extends AInvoice {

    static constraints = {
        docMongoId()
        invoiceCounter()
        invoiceYear()
        invoiceNumber()
        swiftCode(nullable: true,blank:true)
        client()
        totalSum()
        inCurrency()
        exchangeRate(nullable: true)
        documentFile()
        documentName()
        mimeType(nullable: true,blank:true)
        issueDate()
        dateCreated()
        documentOrigin(nullable: true)
    }

    static mapWith = "mongo"

    static mapping = {
        collection "InvoiceDocuments"
//        database "InvoiceRepository"
    }

    String docMongoId = UUID.randomUUID()

    BigDecimal totalSum
    Currency inCurrency
    ExchangeRate exchangeRate
    DocumentOrigin documentOrigin

    byte [] documentFile
    String documentName
    String mimeType

    Date dateCreated

}

public enum DocumentOrigin {
    SIGNING_APPLET("Signing Applet"),
    MANUAL_TRANSFER("Manual Transfer")

    String name

    DocumentOrigin(String name) {
        this.name = name
    }
}
