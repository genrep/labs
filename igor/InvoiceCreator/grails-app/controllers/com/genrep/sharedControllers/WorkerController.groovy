package com.genrep.sharedControllers

import com.genrep.account.BankAccount
import com.genrep.client.Client
import com.genrep.invoiceRepository.InvoiceDocument
import com.genrep.invoiceRepository.InvoicePurchaseArchive
import com.genrep.sharedHelpers.DocumentFileService
import com.genrep.statements.BankStatementDoc

import static org.springframework.http.HttpStatus.*


class WorkerController {

    DocumentFileService documentFileService

    def readFileFromMongoDb(String docMongoId, String domain) {

        def domainInstance = grailsApplication.getDomainClasses().find{
            it.name == domain
        }.clazz

        def iDoc = domainInstance.findByDocMongoId(docMongoId)
        if (iDoc) {
            byte[] file = iDoc.documentFile
            response.setContentType("application/pdf")
            response.setContentLength(file.length)
            response.setHeader("Content-disposition", "filename=${iDoc.documentName}")
            OutputStream out = response.getOutputStream()
            out.write(file)
            out.close()
        } else {
            render view:'/404',status: NOT_FOUND
            return
        }
    }

//          Groovier way
//        new File("report.pdf").withOutputStream { outputStream ->
//            outputStream << pdfRenderingService.render(template: '/report/report', model: [serial: 12345])
//        }

    def refreshBankAccountSelect(){

        def select = """
                    ${select(id:"ownerOfStatement",name:"ownerOfStatement.id",from:BankAccount.findAllByOwner(true),
                optionKey:"id",optionValue:"entityName",required:"",value:'"${bankStatementInstance?.ownerOfStatement?.id}"',
                class:"comboBox")}
                        """
        render select
    }

    def refreshClientSelect(){

        if(params.proInvoiceId){
            def clientId = InvoicePurchaseArchive.get(Long.parseLong(params.proInvoiceId)).clientId
            def select = """
                    ${select(id:"client",name:"client.id",from:Client.list(),
                    optionKey:"id",optionValue:"name",required:"",value:clientId,
                    class:"comboBox")}
                        """
            render select
            return
        }
        else{
            def select = """
                    ${select(id:"client",name:"client.id",from:Client.list(),
                    optionKey:"id",optionValue:"name",required:"",value:'"${clientInstance?.client?.id}"',
                    class:"comboBox")}
                        """
            render select
            return
        }

    }

    def probeFileContentType(){
        def file = params.file
        return documentFileService.probeContentType(file)
    }

}
