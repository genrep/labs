package com.genrep.invoiceCreator

import mk.nbrm.klservice.Kurs
import mk.nbrm.klservice.KursSoap

import java.math.RoundingMode

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ExchangeRateController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
//    def webService
    KursSoap kursSoapClient

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ExchangeRate.list(params), model: [exchangeRateInstanceCount: ExchangeRate.count()]
    }

    def show(ExchangeRate exchangeRateInstance) {
        respond exchangeRateInstance
    }

    def create() {
        respond new ExchangeRate(params)
    }

    @Transactional
    def save(ExchangeRate exchangeRateInstance) {
        if (exchangeRateInstance == null) {
            notFound()
            return
        }

        if (exchangeRateInstance.baseCurrency.currencyCode == 'MKD') {
            BigDecimal dividend = new BigDecimal("1")
            BigDecimal rate = dividend.divide(exchangeRateInstance.rate, 4, RoundingMode.HALF_EVEN)
            exchangeRateInstance.baseCurrencyMeanValue = 1
            exchangeRateInstance.toCurrencyMeanValue = rate
        } else {
            exchangeRateInstance.baseCurrencyMeanValue = exchangeRateInstance.rate
            exchangeRateInstance.toCurrencyMeanValue = 1
        }

        if (exchangeRateInstance.validate() && exchangeRateInstance.hasErrors()) {
            respond exchangeRateInstance.errors, view: 'create'
            return
        }

        exchangeRateInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'exchangeRate.label', default: 'ExchangeRate'), exchangeRateInstance.id])
                redirect exchangeRateInstance
            }
            '*' { respond exchangeRateInstance, [status: CREATED] }
        }
    }

    def edit(ExchangeRate exchangeRateInstance) {
        respond exchangeRateInstance
    }

    @Transactional
    def update(ExchangeRate exchangeRateInstance) {
        if (exchangeRateInstance == null) {
            notFound()
            return
        }

        if (exchangeRateInstance.hasErrors()) {
            respond exchangeRateInstance.errors, view: 'edit'
            return
        }

        exchangeRateInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ExchangeRate.label', default: 'ExchangeRate'), exchangeRateInstance.id])
                redirect exchangeRateInstance
            }
            '*' { respond exchangeRateInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ExchangeRate exchangeRateInstance) {

        if (exchangeRateInstance == null) {
            notFound()
            return
        }

        exchangeRateInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ExchangeRate.label', default: 'ExchangeRate'), exchangeRateInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    def getExchangeRate = {
        Kurs kurs = new Kurs()
        kursSoapClient = kurs.getKursSoap()
        def rates = kursSoapClient.getExchangeRate('18.06.2014', '18.06.2014')
        println rates
        render rates
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'exchangeRate.label', default: 'ExchangeRate'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
