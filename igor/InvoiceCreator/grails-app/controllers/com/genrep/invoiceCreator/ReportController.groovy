package com.genrep.invoiceCreator

import com.genrep.domain.InvoiceBean
//import org.apache.cxf.service.invoker.SessionFactory

class ReportController  {

    def reportService

//    org.hibernate.SessionFactory sessionFactory

    def index() {}

    def invoiceReport() {
        def i = Invoice.get(Long.parseLong(params.invoice_id))
        def parameters = reportService.buildInvoiceJasperParams(params,session)

        List results = []
        i.invoiceBodyIn.invoiceItems.each {
            results.add(it)
        }

        chain(controller: 'jasper', action: 'index', model:[data: results], params: parameters)
    }

    def invoiceReportOut() {

        def i = Invoice.get(Long.parseLong(params.invoice_id))
        def parameters = reportService.buildInvoiceJasperParams(params,session)

        List results = []
        i.invoiceBodyOut.invoiceItems.each {
            // println it.properties
            results << it
        }

        chain(controller: 'jasper', action: 'index', model: [data: results], params: parameters)
    }

    /**
     * View actions
     */
    def quartal(){}

    /**
     * Functionalities
     */
    def multiDomainFilter(){
        println params

        params.max = 100
        boolean showStats = false
        Map statMap = [:]

        List reportList = reportService.quartalReportFilter(params)

        if(!params.inCurrency.equals('null') && !params.client.equals("")){
            showStats = true
            statMap.put('size',reportList.size())
            statMap.put('totalSum',reportService.calculateSumOfField(InvoiceBean.class,reportList,'totalSum'))
            statMap.put('taxPriceSum',reportService.calculateSumOfField(InvoiceBean.class,reportList,'taxPriceSum'))
            statMap.put('taxFreeSum',reportService.calculateSumOfField(InvoiceBean.class,reportList,'taxFreeSum'))
        }

        if(params.inCurrency.equals('null')){
            params.inCurrency=null
        }

        render(view: 'quartal',
                model:[reportList: reportList, invoiceInstanceCount: reportList.size(), showStats:showStats,
                       statMap: statMap],
                params: params
        )
    }
}
