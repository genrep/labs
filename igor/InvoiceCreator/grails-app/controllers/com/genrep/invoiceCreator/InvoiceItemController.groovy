package com.genrep.invoiceCreator

import com.genrep.invoiceRepository.Taxes
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR
import static org.springframework.http.HttpStatus.OK

class InvoiceItemController {

    /*
      Helper Methods For manipulation of InvoiceItem
    */
    def invoiceService

    @Transactional
    def saveInvoiceItem(InvoiceItem invoiceItemInstance) {

//        def tax = Taxes."${params.tax}"
        def unitPrice = Money.formatStringToBigDecimal(params?.unitPrice)
        invoiceItemInstance.properties = params
        invoiceItemInstance.unitPrice = unitPrice

        // Keep it in memory only. Save on cascade when save method is called !
        invoiceItemInstance.beforeInsert()

        if (invoiceItemInstance.hasErrors()) {
            respond invoiceItemInstance, [formats: ['json'], status: INTERNAL_SERVER_ERROR]
            return
        }

        def invoiceInstance = session?.invoiceInstance as Invoice

        if(invoiceInstance.invoiceBodyIn == null){
            def invoiceBody = new InvoiceBodyIn()
            invoiceBody.invoice = invoiceInstance
            invoiceInstance.invoiceBodyIn = invoiceBody
        }
        if(invoiceInstance.invoiceBodyIn?.invoiceItems==null){
            invoiceItemInstance.orderIndex = 0
        }else{
            invoiceItemInstance.orderIndex = invoiceInstance.invoiceBodyIn?.invoiceItems.size()
        }

        invoiceInstance.invoiceBodyIn.addToInvoiceItems(invoiceItemInstance)
        invoiceInstance.invoiceBodyIn.afterUpdate()

        if(invoiceInstance.invoiceBodyOut!=null){
             invoiceService.addInvoiceItemIntoOutBody(invoiceInstance,invoiceItemInstance)
        }

        session?.invoiceInstance = invoiceInstance
        response.status = OK.value()

        render template: '/invoice/formInvoiceBody', model: [invoiceInstance: invoiceInstance]

    }

    @Transactional
    def deleteInvoiceItem(InvoiceItem invoiceItemInstance){

        def invoiceInstance = session?.invoiceInstance as Invoice

        // In case we are deleting existing object in database
        if(invoiceItemInstance.id!=null) {

            invoiceInstance.invoiceBodyIn.removeFromInvoiceItems(invoiceInstance.invoiceBodyIn.invoiceItems.find {
                it.id == invoiceItemInstance.id
            })
            invoiceInstance.invoiceBodyIn.afterUpdate()

            if(invoiceInstance.invoiceBodyOut!=null){
                invoiceInstance.invoiceBodyOut.removeFromInvoiceItems(invoiceInstance.invoiceBodyOut.invoiceItems.find {
                    it.sessionId == invoiceItemInstance.sessionId
                })
                invoiceInstance.invoiceBodyOut.afterUpdate()
            }

        }
        // We are removing object from memory inspecting if it's still not persisted
        else{
//                def invoiceBodyIn=invoiceInstance.invoiceBodyIn.merge(flush:true)
            def invoiceBody=invoiceInstance.invoiceBodyIn
            def invoiceItem = invoiceBody.invoiceItems.find {
                it.inspect().contains("unsaved") && it.sessionId.equals(UUID.fromString(params.sessionId))
            }
            invoiceBody.removeFromInvoiceItems(invoiceItem)
            invoiceBody.afterUpdate()
        }

        reorderItemsOnDelete(invoiceInstance.invoiceBodyIn.invoiceItems)

        session?.invoiceInstance = invoiceInstance

        response.status = OK.value()
        render template: '/invoice/formInvoiceBody', model: [invoiceInstance: invoiceInstance]
    }

    def updateInvoiceItemDetails(InvoiceItem invoiceItem){

        def invoice = session?.invoiceInstance as Invoice
        if (invoiceItem.id != null) {

            invoice.invoiceBodyIn.removeFromInvoiceItems(invoice.invoiceBodyIn.invoiceItems.find {
                it.id == invoiceItem.id
            })

            params.unitPrice = Money.formatStringToBigDecimal(params.unitPrice)
            invoiceItem.properties = params
            invoiceItem.beforeInsert()

            invoice.invoiceBodyIn.addToInvoiceItems(invoiceItem)
            invoice.invoiceBodyIn.afterUpdate()

            if(invoice.invoiceBodyOut!=null){
                invoiceService.updateInvoiceItemFromOutBody(invoice,invoiceItem)
            }

        }else{
            // Only viable identificator is the sessionID
            def invoiceBody=invoice.invoiceBodyIn
            def item = invoiceBody.invoiceItems.find {
                it.inspect().contains("unsaved") && it.sessionId.equals(UUID.fromString(params.sessionId))
            }
            if(item!=null) {
                invoice.invoiceBodyIn.removeFromInvoiceItems(invoice.invoiceBodyIn.invoiceItems.find {
                    it.sessionId == item.sessionId
                })

                params.unitPrice = Money.formatStringToBigDecimal(params.unitPrice)
                item.properties = params
                item.beforeInsert()

                invoice.invoiceBodyIn.addToInvoiceItems(item)
                invoice.invoiceBodyIn.afterUpdate()
            }
        }

        invoice?.invoiceBodyIn?.invoiceItems.sort{it.orderIndex}
        if(invoice.invoiceBodyOut!=null){
            invoice?.invoiceBodyOut?.invoiceItems.sort{it.orderIndex}
        }

        session?.invoiceInstance = invoice

        response.status = OK.value()
        render template: '/invoice/formInvoiceBody', model: [invoiceInstance: invoice]
    }

    def updateInvoiceItemPosition(){

        int position = Integer.parseInt(params.position)
        def step = params.step

        def invoice = session?.invoiceInstance as Invoice
        List invoiceItems = invoice.invoiceBodyIn.invoiceItems

        reorderItemsOnClick(invoiceItems,step,position)

        if(invoice?.invoiceBodyOut!=null){
            List invoiceItemsOut = invoice.invoiceBodyOut.invoiceItems
            reorderItemsOnClick(invoiceItemsOut,step,position)
        }

        session?.invoiceInstance = invoice

        response.status = OK.value()
        render template: '/invoice/formInvoiceBody', model: [invoiceInstance: invoice]

    }

    /*
        The following methods are helper functions
     */

    void reorderItemsOnDelete(List invoiceItems){

        if(invoiceItems.size()>0) {
            int first = invoiceItems[0].orderIndex
            int middle = 0
            if (first > 0) {
                for (int i = 0; i < invoiceItems.size(); i++) {
                    invoiceItems[i].orderIndex = invoiceItems[i].orderIndex - 1
                }
            } else {
                for (int i = 0; i < invoiceItems.size()-1; i++) {
                    if (invoiceItems[i].orderIndex + 1 < invoiceItems[i + 1].orderIndex) {
                        middle = i + 1
                        break
                    }
                }
                if(middle>0) {
                    for (middle; middle < invoiceItems.size(); middle++) {
                        invoiceItems[middle].orderIndex = invoiceItems[middle].orderIndex - 1
                    }
                }
            }

            invoiceItems.sort{it.orderIndex}
        }

    }

    void reorderItemsOnClick(List invoiceItems, String step, int position){

        for(int i=0; i<invoiceItems.size();i++){
            if(invoiceItems[i].orderIndex == position){
                if(step.equals('up')){
                    invoiceItems[i].setOrderIndex(invoiceItems[i].orderIndex - 1)
                    invoiceItems[i-1].setOrderIndex(invoiceItems[i-1].orderIndex + 1)
                }
                else{
                    invoiceItems[i].setOrderIndex(invoiceItems[i].orderIndex + 1)
                    invoiceItems[i+1].setOrderIndex(invoiceItems[i+1].orderIndex - 1)
                }
                break
            }
        }

        invoiceItems.sort{it.orderIndex}
    }
}
