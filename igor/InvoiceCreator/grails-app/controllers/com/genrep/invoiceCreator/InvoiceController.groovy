package com.genrep.invoiceCreator

import com.genrep.document.InvoiceAttachment
import com.genrep.invoiceCounter.InvoiceCounterEvidence
import com.genrep.invoiceRepository.InvoiceDocument
import com.genrep.office.DocumentType
import com.genrep.project.Project
import grails.converters.JSON
import groovy.json.JsonSlurper
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject
import org.hibernate.FetchMode
import org.joda.time.DateTime


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional


@Transactional(readOnly = true)
class InvoiceController {

    // vo drugi klasi, tuka dinamicki e injectiran log objektot
    // private static final log = LogFactory.getLog(this)

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

//    char decimalSeparator = ','
//    String pattern = "###0,00"
    def projectService
    def invoiceService
    def invoiceCounterService
    def officeClientService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        if(params.sort && params.sort.contains(".")){
            respond Invoice.list(params), model: [invoiceInstanceCount: Invoice.count()]
            return
        }
        else{
            def customSortFields
            if(params.sort && params.sort.contains(",")){
                customSortFields = params.sort.split(",")
            }
            def list = Invoice.createCriteria().list{
                if(customSortFields){
                    // order(params.sort,params.order) moze i bez split ako ima ,
                    customSortFields.each {
                        order(it,params.order)
                    }
                }
                else if(params.sort && params.order){
                    order(params.sort,params.order)
                }
                else{
                    order('invoiceYear','desc')
                    order('invoiceCounter','desc')
                }
                if(params.offset){
                    firstResult params.offset as int
                }
                if(params.max){
                    maxResults params.max
                }
            }

            respond list, model: [invoiceInstanceCount: Invoice.count()]
            return
        }

    }

    def filter() {
        println "*********************************************************"
        println params
        println "*********************************************************"
        Integer max = null
        if(params.max) max = Integer.parseInt(params.max)
        params.max = Math.min(max ?: 10, 100)
        def invoiceList = invoiceService.doFilter(params)
        int count = invoiceList.totalCount
        render(view: 'index',
                model:[invoiceInstanceList: invoiceList, invoiceInstanceCount: count],
                params: params
        )
    }

    def show(Invoice invoiceInstance) {
        log.info "Opening Invoice with ID:"+invoiceInstance?.id.toString()+" Class Name: "+this.getClass()
        def project = Project.createCriteria().get {
            fetchMode 'invoices', FetchMode.JOIN
            invoices {
                eq 'id', invoiceInstance.id
            }
        }

        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT")
        response.setHeader("Cache-Control", "private, no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0")
        response.setHeader("Pragma", " no-cache")
        response.setHeader("Last-Modified", new Date().toString());

        respond invoiceInstance, model: [project:project]
    }

    def showDocuments(Invoice invoiceInstance){
        def invoiceNumber = invoiceInstance.invoiceNumber
        List<InvoiceDocument> invoiceDocuments = InvoiceDocument.findAllByInvoiceNumber(invoiceNumber)
        [ invoiceDocuments:invoiceDocuments, invoiceId: invoiceInstance.id ]
    }

    def attachment(Invoice invoiceInstance){
        List<DocumentType> documentTypeList = officeClientService.callMethod("/api/documentType/list",'get',DocumentType)
//        officeClientService.validate()
        def attachments = InvoiceAttachment.findAllByInvoice(invoiceInstance)
        [invoiceAttachmentInstanceList:attachments,invoiceAttachmentInstanceCount:attachments.size(),
         invoiceInstance: invoiceInstance, documentTypeInstanceList: documentTypeList]
    }

    def create() {

        int currentYear = new DateTime().getYear()
        def invoiceCounter = invoiceCounterService.getNextInvoiceCounter(currentYear)
        params.invoiceYear = (Long)currentYear
        params.invoiceCounter = invoiceCounter
        params.invoiceNumber = invoiceCounter.toString()+"/"+currentYear.toString()
        params.invoiceNumberSort = currentYear.toString()+"-"+invoiceCounter.toString()

        def invoiceInstance = new Invoice(params)
        session.setAttribute('invoiceInstance', invoiceInstance)
        def projects = Project.findAll()
        respond invoiceInstance, model: [projectList:projects]
    }

    /*
    Insert of Invoice Object in database
     */
    @Transactional
    def save(Invoice invoiceInstance) {

        if (invoiceInstance == null) {
            notFound()
            return
        }

        def invoice = session.invoiceInstance as Invoice
        def invoiceBody = invoice.invoiceBodyIn
        if(invoiceBody==null){
            invoiceBody = new InvoiceBodyIn()
            invoiceBody.invoice = invoiceInstance
        }
        invoiceBody.currency = invoiceInstance.inCurrency

        invoiceInstance.status = Statuses.GENERATED
        invoiceInstance.invoiceBodyIn = invoiceBody

        if (invoiceInstance.hasErrors()) {
            invoiceInstance.errors.allErrors.each {
                println it
            }
            log.error 'Object invoiceInstance has errors with bind parameters and cannot be saved!'
            respond invoiceInstance.errors, view: 'create',model: [cbOutInvoice:true]
            return
        }

        if(params.cbOutInvoice && params.cbOutInvoice=="on"){
            if((params.inCurrency!='MKD' && params.outCurrency!='MKD') ||
                    params.inCurrency==params.outCurrency){
                log.warn 'Wrong currencies set. Cannot save.'
                flash.message = message(code: 'invoice.currencies.invalid', default: 'At least one of the currencies must be MKD')
                respond invoiceInstance.errors, view: 'create',model: [cbOutInvoice:true]
                return
            }
        }

        def invoiceCounterEvidence = InvoiceCounterEvidence.findWhere(invoiceNumber: invoiceInstance.invoiceNumber)
        if(invoiceCounterEvidence && invoiceCounterEvidence.invoiceType!=invoiceInstance.getClass().getSimpleName()){
            flash.message = message(code: 'invoice.invoiceNumber.duplicate', default: "Invoice Number {0} is used. Choose another one", args:[invoiceInstance.invoiceNumber])
            log.error 'Object invoiceInstance has errors with bind parameters and cannot be saved!'
            respond invoiceInstance.errors, view: 'create',model: [cbOutInvoice:true]
            return
        }

        if(params.cbOutInvoice && params.cbOutInvoice=="on" &&
                invoiceInstance.outCurrency != null &&
                invoiceInstance.inCurrency != invoiceInstance.outCurrency &&
                invoiceBody.invoiceItems!=null){
            //call service method
            invoiceService.generateOutInvoiceBody(invoiceInstance)
        }

        // Finally, we save
        invoiceInstance.save flush: true

        if (invoiceInstance.hasErrors()) {
            flash.message = message(code: 'default.not.created.message', args: [message(code: 'invoiceInstance.label', default: 'Invoice'), invoiceInstance.invoiceNumber])
            log.error invoiceInstance.errors
            redirect invoiceInstance
        }
        else {
            projectService.saveOrUpdateInvoiceForProject(params?.project, invoiceInstance, "save")
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'invoiceInstance.label', default: 'Invoice'), invoiceInstance.invoiceNumber])
        redirect invoiceInstance
    }

    def edit(Invoice invoiceInstance) {
        if (invoiceInstance == null) {
            notFound()
            return
        }

        response.setHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT")
        response.setHeader("Cache-Control", "private, no-store, max-age=0, no-cache, must-revalidate, post-check=0, pre-check=0")
        response.setHeader("Pragma", " no-cache")
        response.setHeader("Last-Modified", new Date().toString());

        if(invoiceInstance.status!=Statuses.FINALIZED) {
            session.setAttribute('invoiceInstance', invoiceInstance)
            // This variable is set like safe proof that merge won't be called twice
            // if validation fails since invoiceItem is already validated.
            session.safeUpdate = 0
            List<Project> projects = Project.findAll()
            Project project = Project.createCriteria().get {
                fetchMode 'invoices', FetchMode.JOIN
                invoices {
                    eq 'id', invoiceInstance.id
                }
            }
            projects.remove(project)
            respond invoiceInstance, model: [projectList:projects,project: project]
        }
        else {
            flash.message=message(code: 'invoice.edit.error', default:"Can't edit finalized invoice.")
            respond invoiceInstance, view: 'show'
        }
    }

    @Transactional
    def update(Invoice invoiceInstance) {

        // Regular binding update
        if (invoiceInstance == null) {
            notFound()
            return
        }

        // Update the body first from session with cascade !
        def invoice = session.invoiceInstance as Invoice

        if(session.safeUpdate == 0) {

            invoiceInstance.invoiceBodyIn = invoice.invoiceBodyIn.merge()
            if(invoice.invoiceBodyOut!=null){
                invoiceInstance.invoiceBodyOut = invoice.invoiceBodyOut.merge()
            }

            session.safeUpdate = 1
        }

        if (invoiceInstance.hasErrors()) {
            invoiceInstance.errors.allErrors.each {
                println it
            }
            log.error 'Object invoiceInstance has errors with bind parameters and cannot be saved!'
            chain(action: 'updateError', model: [invoiceInstance:invoiceInstance])
            return
        }

        if(invoiceInstance?.invoiceBodyIn!=null && params.outCurrency){
            if((invoiceInstance?.inCurrency.currencyCode!='MKD' && params.outCurrency!='MKD') ||
                    invoiceInstance?.inCurrency.currencyCode==params.outCurrency){
                log.warn 'Wrong currencies set. Cannot save.'
                flash.message = message(code: 'invoice.currencies.invalid', default: 'Wrong currencies set. Cannot save.')
                chain(action: 'updateError', model: [invoiceInstance:invoiceInstance])
                return
            }
        }

        def invoiceCounterEvidence = InvoiceCounterEvidence.findWhere(invoiceNumber: invoiceInstance.invoiceNumber)
        if(invoiceCounterEvidence && invoiceCounterEvidence.invoiceType!=invoiceInstance.getClass().getSimpleName()){
            flash.message = message(code: 'invoice.invoiceNumber.duplicate', default: "Invoice Number {0} is used. Choose another one", args:[invoiceInstance.invoiceNumber])
            log.error 'Object invoiceInstance has errors with bind parameters and cannot be saved!'
            chain(action: 'updateError', model: [invoiceInstance:invoiceInstance])
            return
        }

        // Check whether we want to lock editing of the Invoice by finalizing it.
//        if(params.invoiceStatus && params.invoiceStatus.equals("on")){
//            invoiceInstance.status = Statuses.FINALIZED
//        }

        if(params.cbOutInvoice && params.cbOutInvoice=="on" &&
                invoiceInstance.outCurrency != null &&
                invoiceInstance.inCurrency != invoiceInstance.outCurrency &&
                invoiceInstance.invoiceBodyIn.invoiceItems!=null){
            //call service method
            invoiceService.generateOutInvoiceBody(invoiceInstance)
        }

        if(params.reverseInvoice && params.reverseInvoice=="on"){
            invoiceInstance.invoiceReversal = new Date()
        }

        if(invoiceInstance.save(flush: true)){
            projectService.saveOrUpdateInvoiceForProject(params?.project, invoiceInstance, "update")
        }

        // Rendering the respond to show.gsp
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Invoice.label', default: 'Invoice'), invoiceInstance.invoiceNumber])
                redirect invoiceInstance
            }
            '*' { respond invoiceInstance, [status: OK] }
        }
    }

    def updateError(){
        def invoiceInstance = chainModel?.invoiceInstance
        List<Project> projects = Project.findAll()
        Project project = Project.createCriteria().get {
            fetchMode 'invoices', FetchMode.JOIN
            invoices {
                eq 'id', invoiceInstance.id
            }
        }
        projects.remove(project)
        respond invoiceInstance.errors, view: 'edit',model: [cbOutInvoice:true,projectList:projects,project: project]
    }

    @Transactional
    def delete(Invoice invoiceInstance) {

        if (invoiceInstance == null) {
            notFound()
            return
        }
        def id = invoiceInstance.id
        def invoiceNumber = invoiceInstance.invoiceNumber
        projectService.removeInvoiceFromProject(invoiceInstance)

        try {

            invoiceInstance.delete(flush: true)

            def invoiceCounterEvidence = InvoiceCounterEvidence.findWhere(invoiceNumber: invoiceNumber)
            invoiceCounterEvidence.delete(flush: true)

            List<InvoiceDocument> invoiceDocuments = InvoiceDocument.findAllByInvoiceNumber(invoiceNumber)
            for (doc in invoiceDocuments) {
                doc.delete(flush: true)
            }

        } catch (Exception ex) {
            log.error("Problem deleting Invoice")
            log.error(ex.fillInStackTrace())
        }

        log.info "Invoice with ID:" + id + " was successfully deleted."

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Invoice.label', default: 'Invoice'), invoiceNumber])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    def copy(Invoice invoiceInstance){
        int currentYear = new DateTime().getYear()
        def invoiceCounter = invoiceCounterService.getNextInvoiceCounter(currentYear)
        params.invoiceYear = (Long)currentYear
        params.invoiceCounter = invoiceCounter
        params.invoiceNumber = invoiceCounter.toString()+"/"+currentYear.toString()
        respond invoiceInstance
    }

    @Transactional
    def saveCopy(Invoice invoiceInstance){

        // Regular binding update
        if (invoiceInstance == null) {
            notFound()
            return
        }

        try {
            def clonedInvoice = invoiceInstance.clone(params.newInvoiceNumber,params.newInvoiceCounter,params.newInvoiceYear)
            clonedInvoice.save(flush: true,failOnError: true)
            // Successful cloning
            flash.message=message(code: 'invoice.copy.success', default:"Invoice was successfully copied.")
            redirect clonedInvoice

        }
        catch(Exception ve){
            log.error(ve.fillInStackTrace())
            flash.message=message(code: 'invoice.copy.error', default:"This invoiceNumber is already used. Try another one.")
            respond invoiceInstance, view: 'copy'
        }

    }

    def updateInvoiceProject(Long id){

        if(params.guiAction.equals("undo")){
            def project = Project.get(id)
            render template: 'editFormProject', model: [project: project]
            return
        }
        else {
            def projects = Project.findAll {
                id != id
            }
            render template: 'editFormProject', model: [projectList: projects, guiAction: params.guiAction]
            return
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoiceInstance.label', default: 'Invoice'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

}
