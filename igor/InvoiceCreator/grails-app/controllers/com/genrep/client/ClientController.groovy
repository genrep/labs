package com.genrep.client

import com.genrep.account.BankAccount
import com.genrep.invoiceCreator.Money

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ClientController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        //respond Client.list(params), model:[clientInstanceCount: Client.count()]
        render view:"index" ,model:[clientInstanceList: Client.list(params), clientInstanceCount: Client.count()]
    }

    def show(Client clientInstance) {

        respond clientInstance
    }

    def create() {
        respond new Client(params)
    }

    @Transactional
    def save(Client clientInstance ){

        if (clientInstance == null) {
            notFound()
            return
        }

        if (clientInstance.hasErrors()) {
            respond clientInstance.errors, view:'create'
            return
        }

        if(!params.bankAccountNumber.equals("")){
            def bankAccountNumber = params.bankAccountNumber
            def bankAccount = BankAccount.findByBankAccountNumber(bankAccountNumber) ?: new BankAccount(
                    entityName: clientInstance.name,
                    bankAccountNumber: bankAccountNumber
            ).save(flush: true)

            clientInstance.bankAccount = bankAccount
        }

        clientInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'clientInstance.label', default: 'Client'), clientInstance.id])
                redirect clientInstance
            }
            '*' { respond clientInstance, [status: CREATED] }
        }
    }

    def edit(Client clientInstance) {
        respond clientInstance
    }

    @Transactional
    def update(Client clientInstance) {
        if (clientInstance == null) {
            notFound()
            return
        }

        if (clientInstance.hasErrors()) {
            respond clientInstance.errors, view:'edit'
            return
        }

        if(!params.bankAccountNumber.equals("")){
            def bankAccountNumber = params.bankAccountNumber
            def bankAccount = BankAccount.findByBankAccountNumber(bankAccountNumber) ?: new BankAccount(
                    entityName: clientInstance.name,
                    bankAccountNumber: bankAccountNumber
            ).save(flush: true)

            clientInstance.bankAccount = bankAccount
        }

        clientInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Client.label', default: 'Client'), clientInstance.id])
                redirect clientInstance
            }
            '*'{ respond clientInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Client clientInstance) {

        if (clientInstance == null) {
            notFound()
            return
        }

        clientInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Client.label', default: 'Client'), clientInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    @Transactional
    def addClient() {

        def clientInstance = new Client(params)

        if (clientInstance == null) {
            notFound()
            return
        }

        clientInstance.validate()

        if (clientInstance.hasErrors()) {
            println clientInstance.errors.allErrors.each {
                println it
            }
            render template: 'form', model: [clientInstance:clientInstance], status: BAD_REQUEST
            return
        }

        if(!params.bankAccountNumber.equals("")){
            String bankAccountNumber = params.bankAccountNumber
            def bankAccount = BankAccount.findByBankAccountNumber(bankAccountNumber) ?: new BankAccount(
                    entityName: clientInstance.name,
                    bankAccountNumber: bankAccountNumber
            ).save(flush: true)

            clientInstance.bankAccount = bankAccount
        }

        clientInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'client.label', default: 'Client'), clientInstance.id])
                render "Created", status: CREATED
            }
            '*' { render "Created", status: CREATED }
        }
    }

    protected void notFound() {
        request.withFormat {
            println request.format
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'clientInstance.label', default: 'Client'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}
