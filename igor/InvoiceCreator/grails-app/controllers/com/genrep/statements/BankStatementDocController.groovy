package com.genrep.statements

import com.genrep.sharedHelpers.DocumentFileService
import grails.converters.JSON
import org.springframework.web.multipart.MultipartHttpServletRequest

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class BankStatementDocController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    DocumentFileService documentFileService
    BankStatementService bankStatementService

    /**
     *
     * Regular actions
     */
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond BankStatementDoc.list(params), model:[bankStatementDocInstanceCount: BankStatementDoc.count()]
    }

    def show(BankStatementDoc bankStatementDocInstance) {
        respond bankStatementDocInstance
    }

    def create() {
        respond new BankStatementDoc(params)
    }

    @Transactional
    def save(BankStatementDoc bankStatementDocInstance) {
        if (bankStatementDocInstance == null) {
            notFound()
            return
        }

        if (bankStatementDocInstance.hasErrors()) {
            respond bankStatementDocInstance.errors, view:'create'
            return
        }

        MultipartHttpServletRequest multiRequest
        if (request instanceof MultipartHttpServletRequest){
            multiRequest = (MultipartHttpServletRequest)request

            def file = multiRequest.getFile('documentFile')
            def mime = documentFileService.probeContentType(file.getBytes())
            if(params.documentName.equals("")){
                bankStatementDocInstance.documentName = file.originalFilename
            }else{
                bankStatementDocInstance.documentName = params.documentName
            }
            bankStatementDocInstance.mimeType = mime
        }

        bankStatementDocInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bankStatementDoc.label', default: 'BankStatementDoc'), bankStatementDocInstance.id])
                redirect bankStatementDocInstance
            }
            '*' { respond bankStatementDocInstance, [status: CREATED] }
        }
    }

    def edit(BankStatementDoc bankStatementDocInstance) {
        respond bankStatementDocInstance
    }

    @Transactional
    def update(BankStatementDoc bankStatementDocInstance) {
        if (bankStatementDocInstance == null) {
            notFound()
            return
        }

        if (bankStatementDocInstance.hasErrors()) {
            respond bankStatementDocInstance.errors, view:'edit'
            return
        }

        bankStatementDocInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'BankStatementDoc.label', default: 'BankStatementDoc'), bankStatementDocInstance.id])
                redirect bankStatementDocInstance
            }
            '*'{ respond bankStatementDocInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(BankStatementDoc bankStatementDocInstance) {

        if (bankStatementDocInstance == null) {
            notFound()
            return
        }

        def docUuid = bankStatementDocInstance.uuid
        bankStatementDocInstance.delete flush:true

        // if it successfull delete in hibernate also.This should change in future.
        // TODO: DA SE REVDIRA OVA ODNESUVANJE
        try {
            def bs = BankStatement.findByBankStatementDocId(docUuid.toString())
            bs.delete flush: true
        }
        catch(Exception e){
            log.error "Problem removing BankStatement. If the error is NullPointerException then ignore this."
            log.error(e.fillInStackTrace())
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'BankStatementDoc.label', default: 'BankStatementDoc'), bankStatementDocInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    /**
     *
     * Action related to Bank Statement
     */

    def showBankStatementDetails(BankStatementDoc bankStatementDocInstance){

        if(bankStatementDocInstance){
            //check if it is parsed
            def bs = BankStatement.findByBankStatementDocId(bankStatementDocInstance.uuid)
            if(bs){
                //render bankStatement
                def total = bs.balanceAfter-bs.balanceBefore
                render view: 'showBankStatementDetails', model: [bankStatementInstance: bs, saved:true, total:total,
                              bankStatementDocInstance: bankStatementDocInstance]
            }else{
                //parse document
                try {
                    bs = bankStatementService.parseBankStatementDoc(bankStatementDocInstance)
                    session.setAttribute('bankStatementInstance', bs)
                    def total = bs.balanceAfter - bs.balanceBefore
                    render view: 'showBankStatementDetails', model: [bankStatementInstance: bs, total:total]
                }
                catch(Exception e){
                    log.error "Error while parsing document."
                    log.error e.fillInStackTrace()
                    notFound()
                }

            }
        }
    }

    def saveBankStatement(){
        BankStatement bankStatementInstance = session.bankStatementInstance

        if (bankStatementInstance == null) {
            notFound()
            return
        }

        if (bankStatementInstance.hasErrors()) {
            bankStatementInstance.errors.allErrors.each {
                println it
            }
            respond bankStatementInstance.errors, view:'list'
            return
        }

        def total = bankStatementInstance.balanceAfter-bankStatementInstance.balanceBefore

        bankStatementService.preSavingBankStatement(bankStatementInstance)
        if(bankStatementService.saveBankStatement(bankStatementInstance)){
            bankStatementService.postSavingBankStatement(bankStatementInstance)
            flash.message = message(code: 'default.created.message', args: [message(code: 'bankStatement.label', default: 'BankStatement'), bankStatementInstance.id])
            render view: 'showBankStatementDetails', model: [bankStatementInstance: bankStatementInstance, saved:true, total:total]

        }else{
            render view:'showBankStatementDetails', model:[bankStatementInstance:bankStatementInstance,total:total]
        }

    }

    /**
     *
     * MultiImport actions
     */


    def multiImport(){
        session.setAttribute('uploadedStatements',[:])
        return
    }


    def saveMultiImport(){
        def uploadedStatements = session.getAttribute("uploadedStatements") as Map
        try {
            uploadedStatements.each { k, BankStatementDoc v ->
                v.save flush: true
            }
        }
        catch(Exception e){
            log.error "Cannot save multiple imports of statements."
            log.error e.fillInStackTrace()
        }

        flash.message = message(code: 'bankStatementDoc.multiImport.success', default: 'Upload was successful.')
        redirect action:"index", method:"GET"
    }

    def uploadStatementDoc = {

        MultipartHttpServletRequest multiRequest
        if (request instanceof MultipartHttpServletRequest){
            multiRequest = (MultipartHttpServletRequest)request
        }
        else {
            return
        }

        BankStatementDoc bankStatementDocInstance = new BankStatementDoc()

        def file = multiRequest.getFile('documentFile')
        def mime = documentFileService.probeContentType(file.getBytes())
        if(params.documentName.equals("")){
            bankStatementDocInstance.documentName = file.originalFilename
        }else{
            bankStatementDocInstance.documentName = params.documentName
        }
        if(!params.issueDate.equals("")){
            bankStatementDocInstance.issueDate = Date.parse("dd.MM.yyyy",params.issueDate)
        }
        bankStatementDocInstance.mimeType = mime
        bankStatementDocInstance.documentFile = file.bytes


        def uploadedStatements = session.getAttribute("uploadedStatements") as Map
        def uuid = UUID.randomUUID().toString()
        uploadedStatements.put(uuid,bankStatementDocInstance)

        /*********************
         generate response
         **********************/

        def tableRow = """
            <tr id="$uuid">
            <td>$bankStatementDocInstance.documentName</a></td>
            <td>${formatDate(date:bankStatementDocInstance?.issueDate,format:"dd.MM.yyyy")}</td>
            <td><input type="button" value="Remove me" onclick="removeStatementFile('$uuid')"/></td>
            </tr>
        """

        def statementsList = [] << [
                doc_name:bankStatementDocInstance.documentName,
                issue_date:bankStatementDocInstance?.issueDate,
                uuid:uuid,
                table_row: tableRow
        ]

        withFormat {
            json {
                render statementsList as JSON
            }
        }
    }

    def removeStatementDoc = {
        if(params.uuid.equals("")){
            return
        }else{
            def uploadedStatements = session.getAttribute("uploadedStatements") as Map
            def uuid = params.uuid

            if(uploadedStatements.containsKey(uuid)){
                uploadedStatements.remove(uuid)
                render status: OK
            }
        }
    }


    /**
     *  Helper actions
     */

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bankStatementDoc.label', default: 'BankStatementDoc'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render view: '/404', status: NOT_FOUND }
        }
    }
}
