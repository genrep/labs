package com.genrep.statements

import com.genrep.invoiceCreator.Money

import static org.springframework.http.HttpStatus.*

class BankStatementItemController {

    def index() {}

    def addBankStatementItem(){

        def bankStatementItemInstance = new BankStatementItem(params)

        if (bankStatementItemInstance == null) {
            return
        }
        try {
            bankStatementItemInstance.transferSum = Money.formatStringToBigDecimal(params.transferSum)
            BankStatement bankStatementInstance = session.bankStatementInstance
            if(bankStatementInstance.items==null){
                bankStatementItemInstance.orderNumber = 0
            }else{
                bankStatementItemInstance.orderNumber = bankStatementInstance.findLargestOrderNumber()+1
            }
            bankStatementItemInstance.validate()
            if (bankStatementItemInstance.hasErrors()) {
                bankStatementItemInstance.errors.allErrors.each {
                    println it
                }
                render template: '/bankStatement/formItems', model:[bankStatementItemInstance:bankStatementItemInstance,
                                                                    bankStatementInstance:bankStatementInstance, form:params.form],
                        status: OK
                return
            }

            if (bankStatementInstance) {
                bankStatementInstance.addToItems(bankStatementItemInstance)
                render template: '/bankStatement/formItems', model: [bankStatementInstance: bankStatementInstance, form:params.form],
                        status: OK
            }
        }
        catch(Exception e){
            log.error "An error has occurred while trying to add bank statement item"
            log.error e.fillInStackTrace()
            render template: '/bankStatement/formItems', model:[bankStatementItemInstance:bankStatementItemInstance, form:params.form],
                    status: INTERNAL_SERVER_ERROR
        }

    }

    def removeBankStatementItem(){
        BankStatement bankStatementInstance = session.bankStatementInstance
        try{
            if(bankStatementInstance){
                BankStatementItem bankStatementItem = bankStatementInstance.items.find{ it.id == Long.parseLong(params.id) }
                bankStatementInstance.removeFromItems(bankStatementItem)
                render template: '/bankStatement/formItems', model: [bankStatementInstance: bankStatementInstance, form:params.form],
                        status: OK
            }
        }
        catch (Exception e){
            render template: '/bankStatement/formItems', model: [bankStatementInstance: bankStatementInstance, form:params.form],
                    status: INTERNAL_SERVER_ERROR
            log.error e.fillInStackTrace()
        }
    }
}
