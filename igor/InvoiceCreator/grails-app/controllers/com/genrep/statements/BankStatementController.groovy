package com.genrep.statements

import com.genrep.invoiceCreator.Money
import grails.transaction.Transactional
import org.springframework.web.multipart.MultipartHttpServletRequest

import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK

class BankStatementController {

    def bankStatementService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond BankStatement.list(params), model:[bankStatementInstanceCount: BankStatement.count()]
    }

    def show(BankStatement bankStatementInstance) {
        respond bankStatementInstance
    }

    def create(){
        BankStatement bankStatementInstance = new BankStatement()
        session.setAttribute('bankStatementInstance',bankStatementInstance)
        def form = 'create'
        respond bankStatementInstance, model:[form:form]
    }

    def edit(BankStatement bankStatementInstance) {
        respond bankStatementInstance
    }

    def editBankStatementItems(BankStatement bankStatementInstance) {
        session.setAttribute('bankStatementInstance',bankStatementInstance)
        def form = 'editBankStatementItems'
        respond bankStatementInstance, model:[form:form]
    }

    @Transactional
    def save(BankStatement bankStatementInstance) {
        println params
        if (bankStatementInstance == null) {
            notFound()
            return
        }

        BankStatement bankStatement = session.bankStatementInstance
        bankStatementInstance.items = bankStatement.items

        if (bankStatementInstance.hasErrors()) {
            respond bankStatementInstance.errors, view:'create'
            return
        }

        bankStatementInstance.balanceBefore = Money.formatStringToBigDecimal(params.balanceBefore)
        bankStatementInstance.balanceAfter = Money.formatStringToBigDecimal(params.balanceAfter)

        println bankStatementInstance.properties

        bankStatementInstance.save flush:true

        bankStatementService.postSavingBankStatement(bankStatement)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bankStatement.label', default: 'BankStatement'), bankStatementInstance.id])
                redirect bankStatementInstance
            }
            '*' { respond bankStatementInstance, [status: CREATED] }
        }
    }

    @Transactional
    def update(BankStatement bankStatementInstance) {
        if (bankStatementInstance == null) {
            notFound()
            return
        }

        if (bankStatementInstance.hasErrors()) {
            respond bankStatementInstance.errors, view:'edit'
            return
        }

        bankStatementInstance.balanceBefore = Money.formatStringToBigDecimal(params.balanceBefore)
        bankStatementInstance.balanceAfter = Money.formatStringToBigDecimal(params.balanceAfter)

        bankStatementInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'bankStatement.label', default: 'BankStatement'), bankStatementInstance.id])
                redirect bankStatementInstance
            }
            '*' { respond bankStatementInstance, [status: OK] }
        }
    }

    @Transactional
    def updateBankStatementItems(BankStatement bankStatementInstance) {
        println params
        if (bankStatementInstance == null) {
            notFound()
            return
        }

        BankStatement bankStatement = session.bankStatementInstance
        bankStatementInstance.items = bankStatement.items

        if (bankStatementInstance.hasErrors()) {
            respond bankStatementInstance.errors, view:'create'
            return
        }

        bankStatementInstance.save flush:true

        bankStatementService.postSavingBankStatement(bankStatement)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bankStatement.label', default: 'BankStatement'), bankStatementInstance.id])
                redirect bankStatementInstance
            }
            '*' { respond bankStatementInstance, [status: OK] }
        }
    }

    def showBankStatementDetails(BankStatement bankStatementInstance){
        if(bankStatementInstance){

                def total = bankStatementInstance.balanceAfter-bankStatementInstance.balanceBefore
                render view: 'showBankStatementDetails', model: [bankStatementInstance: bankStatementInstance, saved:true, total:total,
                                                                 ]

        }
    }

    @Transactional
    def delete(BankStatement bankStatementInstance) {

        if (bankStatementInstance == null) {
            notFound()
            return
        }

        bankStatementInstance.delete flush:true


        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'BankStatement.label', default: 'BankStatement'), bankStatementInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bankStatement.label', default: 'BankStatement'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render view: '/404', status: NOT_FOUND }
        }
    }
}
