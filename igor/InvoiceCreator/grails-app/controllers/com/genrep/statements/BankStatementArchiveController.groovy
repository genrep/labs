package com.genrep.statements


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class BankStatementArchiveController {

    static allowedMethods = [save: "POST", update: "POST", delete: "DELETE"]

    def documentFileService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def statements = BankStatementArchive.list(params)
        respond statements, model: [bankStatementArchiveInstanceCount: BankStatementArchive.count()]
    }

    def show(BankStatementArchive bankStatementArchiveInstance) {
        respond bankStatementArchiveInstance
    }

    def create() {
        respond new BankStatementArchive(params)
    }

    @Transactional
    def save(BankStatementArchive bankStatementArchiveInstance) {
        if (bankStatementArchiveInstance == null) {
            notFound()
            return
        }

        bankStatementArchiveInstance.ownerOfStatement = params?.bankAccount?.id as Long

        if (bankStatementArchiveInstance.hasErrors()) {
            respond bankStatementArchiveInstance.errors, view: 'create'
            return
        }

        bankStatementArchiveInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bankStatementArchive.label', default: 'BankStatementArchive'), bankStatementArchiveInstance.id])
                redirect bankStatementArchiveInstance
            }
            '*' { respond bankStatementArchiveInstance, [status: CREATED] }
        }
    }

    def edit(BankStatementArchive bankStatementArchiveInstance) {
        respond bankStatementArchiveInstance
    }

    @Transactional
    def update(BankStatementArchive bankStatementArchiveInstance) {
        if (bankStatementArchiveInstance == null) {
            notFound()
            return
        }

        bankStatementArchiveInstance.ownerOfStatement = params?.bankAccount?.id as Long

        if (bankStatementArchiveInstance.hasErrors()) {
            respond bankStatementArchiveInstance.errors, view: 'edit'
            return
        }

        bankStatementArchiveInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'BankStatementArchive.label', default: 'BankStatementArchive'), bankStatementArchiveInstance.id])
                redirect bankStatementArchiveInstance
            }
            '*' { respond bankStatementArchiveInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(BankStatementArchive bankStatementArchiveInstance) {

        if (bankStatementArchiveInstance == null) {
            notFound()
            return
        }

        bankStatementArchiveInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'BankStatementArchive.label', default: 'BankStatementArchive'), bankStatementArchiveInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    boolean checkIfBankStatementArchiveExists(){

        def bankStatementId = params.bankStatementId
        if(bankStatementId){
            def bs = BankStatementArchive.findByBankStatementId(bankStatementId)
            if(bs){
                render 'true'
            }
        }
        render 'false'
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bankStatementArchive.label', default: 'BankStatementArchive'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    def getDocument(Long id){
        def iDoc = BankStatementArchive.findById(id)
        if(iDoc) {
            byte[] file = iDoc.documentFile
            def mime = documentFileService.probeContentType(file)
            response.setContentType(mime)
            response.setContentLength(file.length)
            response.setHeader("Content-disposition", "filename=${iDoc?.documentName?:'Document'}")
            OutputStream out = response.getOutputStream()
            out.write(file)
            out.close()
        }else{
            return
        }
    }

    def deleteDocument(Long id) {
        try {
            def bankStatementArchive = BankStatementArchive.get(id)
            bankStatementArchive.documentFile = null
            bankStatementArchive.documentName = null
            render template: 'editFileForm', bean: bankStatementArchive
        }
        catch(Exception e){
            log.error(e.fillInStackTrace())
            render status: INTERNAL_SERVER_ERROR
        }
    }
}
