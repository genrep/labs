package com.genrep.document



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class InvoiceAttachmentController {

    def invoiceService

    @Transactional
    def save(InvoiceAttachment invoiceAttachmentInstance) {

        if (invoiceAttachmentInstance == null) {
            notFound()
            return
        }

        if (invoiceAttachmentInstance.hasErrors()) {
            // ova ne e tocno
            respond invoiceAttachmentInstance.errors, view:'create'
            return
        }

        if(invoiceAttachmentInstance.save(flush:true)){
            invoiceService.saveInvoiceAttachmentFile(invoiceAttachmentInstance,params)
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'invoiceAttachment.label', default: 'InvoiceAttachment'), invoiceAttachmentInstance.name])
                redirect controller: 'invoice', action: 'attachment', id: invoiceAttachmentInstance.invoice.id
            }
            '*' { respond invoiceAttachmentInstance, [status: CREATED] }
        }
    }

    def delete(InvoiceAttachment invoiceAttachmentInstance){
        if (invoiceAttachmentInstance == null) {
            notFound()
            return
        }

        if (invoiceAttachmentInstance.hasErrors()) {
            // ova ne e tocno
            renderErrors('index',invoiceAttachmentInstance.errors)
            return
        }

        invoiceService.deleteInvoiceAttachment(invoiceAttachmentInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'document.label', default: 'Document'), invoiceAttachmentInstance.name])
                redirect controller: 'invoice', action: 'attachment', id: invoiceAttachmentInstance.invoice.id
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoiceAttachment.label', default: 'InvoiceAttachment'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
