package com.genrep.account

import com.genrep.invoiceCreator.Money
import com.genrep.statements.BankStatement
import grails.plugin.springsecurity.SpringSecurityUtils
import org.springframework.dao.DataIntegrityViolationException

import java.sql.SQLException

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class BankAccountController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def springSecurityService

    def index(Integer max) {
//        println springSecurityService.getPrincipal().properties
//        println SpringSecurityUtils.securityConfig.userLookup.userDomainClassName
//        println springSecurityService.getCurrentUser()
//        println springSecurityService.getCurrentUser().class

        params.max = Math.min(max ?: 10, 100)
        params.owner = true
        respond BankAccount.list(params), model:[bankAccountInstanceCount: BankAccount.count()]
    }

    def show(BankAccount bankAccountInstance) {
        respond bankAccountInstance
    }

    def create() {
        respond new BankAccount(params)
    }

    @Transactional
    def save(BankAccount bankAccountInstance) {

        if (bankAccountInstance == null) {
            notFound()
            return
        }

        if (bankAccountInstance.hasErrors()) {
            respond bankAccountInstance.errors, view:'create'
            return
        }

        bankAccountInstance.balance = Money.formatStringToBigDecimal(params.balance)
        bankAccountInstance.owner = true
        bankAccountInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bankAccount.label', default: 'BankAccount'), bankAccountInstance.id])
                redirect bankAccountInstance
            }
            '*' { respond bankAccountInstance, [status: CREATED] }
        }
    }

    @Transactional
    def addBankAccount() {

        def bankAccountInstance = new BankAccount(params)

        if (bankAccountInstance == null) {
            notFound()
            return
        }

        if(params.balance!=""){
            bankAccountInstance.balance = Money.formatStringToBigDecimal(params.balance)
        }
        bankAccountInstance.owner = true

        bankAccountInstance.validate()

        if (bankAccountInstance.hasErrors()) {
            println bankAccountInstance.errors.allErrors.each {
                println it
            }
            render template: 'form', model: [bankAccountInstance:bankAccountInstance], status: BAD_REQUEST
            return
        }

        bankAccountInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bankAccount.label', default: 'BankAccount'), bankAccountInstance.id])
                render "Created", status: CREATED
            }
            '*' { render "Created", status: CREATED }
        }
    }

    def edit(BankAccount bankAccountInstance) {
        respond bankAccountInstance
    }

    @Transactional
    def update(BankAccount bankAccountInstance) {
        if (bankAccountInstance == null) {
            notFound()
            return
        }

        if (bankAccountInstance.hasErrors()) {
            respond bankAccountInstance.errors, view:'edit'
            return
        }

        bankAccountInstance.balance = Money.formatStringToBigDecimal(params.balance)
        bankAccountInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'BankAccount.label', default: 'BankAccount'), bankAccountInstance.id])
                redirect bankAccountInstance
            }
            '*'{ respond bankAccountInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(BankAccount bankAccountInstance) {

        if (bankAccountInstance == null) {
            notFound()
            return
        }

        try {
            bankAccountInstance.delete(flush:true)
        }
        catch (DataIntegrityViolationException e) {
            log.error(e.fillInStackTrace())
            flash.message = "Could not delete ${bankAccountInstance.entityName}"
            redirect(action: "show", id: bankAccountInstance.id)
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'BankAccount.label', default: 'BankAccount'), bankAccountInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    boolean checkIfBankAccountExists(){

        def bankAccountNumber = params.bankAccountNumber
        if(bankAccountNumber){
            def ba = BankAccount.findByBankAccountNumberAndOwner(bankAccountNumber,true)
            if(ba){
                render 'true'
            }
        }
        render 'false'
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bankAccount.label', default: 'BankAccount'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
