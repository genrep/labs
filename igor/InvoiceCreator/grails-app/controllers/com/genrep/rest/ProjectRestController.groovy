package com.genrep.rest

import com.genrep.project.Project
import grails.rest.RestfulController
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.CREATED
import static org.springframework.http.HttpStatus.NO_CONTENT
import static org.springframework.http.HttpStatus.OK

@Transactional(readOnly = true)
class ProjectRestController extends RestfulController {

    static responseFormats = ['json', 'xml']

    ProjectRestController(){
        super(Project)
    }

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Project.list(params), model:[projectCount: Project.count()]
    }

    def show(Project project) {
        respond project
    }

    @Transactional
    def save(Project project) {
        if(project.hasErrors()){
            respond project.errors, view: 'create'
        }
        else{
            project.save flush: true
            withFormat {
                html {
                    flash.message = message(code: 'default.created.message', args: [message(code: 'project.label', default: 'Project'), project.id])
                    redirect project
                }
                '*'{ render status: CREATED }
            }
        }
    }

    @Transactional
    def update(Project project) {
        if(project.hasErrors()){
            respond project.errors, view: 'edit'
        }
        else{
            project.save flush: true
            withFormat {
                html {
                    flash.message = message(code: 'default.updated.message', args: [message(code: 'project.label', default: 'Project'), project.id])
                    redirect project
                }
                '*'{ render status: OK }
            }
        }
    }

    @Transactional
    def delete(Project project) {
        if(project.hasErrors()){
            respond project.errors, view: 'edit'
        }
        else{
            project.delete flush: true
            withFormat {
                html {
                    flash.message = message(code: 'default.deleted.message', args: [message(code: 'project.label', default: 'Project'), project.id])
                    redirect action:"index", method:"GET"
                }
                '*'{ render status: NO_CONTENT }
            }
        }
    }
}
