package com.genrep.vendor

import com.genrep.account.BankAccount

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class VendorController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        render view:"index" ,model:[vendorInstanceList: Vendor.list(params), vendorInstanceCount: Vendor.count()]
    }

    def show(Vendor vendorInstance) {
        respond vendorInstance
    }

    def create() {
        respond new Vendor(params)
    }

    @Transactional
    def save(Vendor vendorInstance){

        if (vendorInstance == null) {
            notFound()
            return
        }

        if (vendorInstance.hasErrors()) {
            respond vendorInstance.errors, view:'create'
            return
        }

        if(!params.bankAccountNumber.equals("")){
            def bankAccountNumber = params.bankAccountNumber
            def bankAccount = BankAccount.findByBankAccountNumber(bankAccountNumber) ?: new BankAccount(
                    entityName: vendorInstance.name,
                    bankAccountNumber: bankAccountNumber
            ).save(flush: true)

            vendorInstance.bankAccount = bankAccount
        }

        vendorInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'vendorInstance.label', default: 'Vendor'), vendorInstance.id])
                redirect vendorInstance
            }
            '*' { respond vendorInstance, [status: CREATED] }
        }
    }

    def edit(Vendor vendorInstance) {
        respond vendorInstance
    }

    @Transactional
    def update(Vendor vendorInstance) {
        if (vendorInstance == null) {
            notFound()
            return
        }

        if (vendorInstance.hasErrors()) {
            respond vendorInstance.errors, view:'edit'
            return
        }

        if(!params.bankAccountNumber.equals("")){
            def bankAccountNumber = params.bankAccountNumber
            def bankAccount = BankAccount.findByBankAccountNumber(bankAccountNumber) ?: new BankAccount(
                    entityName: vendorInstance.name,
                    bankAccountNumber: bankAccountNumber
            ).save(flush: true)

            vendorInstance.bankAccount = bankAccount
        }

        vendorInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'vendor.label', default: 'Vendor'), vendorInstance.id])
                redirect vendorInstance
            }
            '*'{ respond vendorInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Vendor vendorInstance) {

        if (vendorInstance == null) {
            notFound()
            return
        }

        vendorInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'vendor.label', default: 'Vendor'), vendorInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            println request.format
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'vendorInstance.label', default: 'Vendor'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}
