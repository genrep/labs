package com.genrep.invoiceRepository

import com.genrep.invoiceCreator.Invoice
import com.genrep.invoiceCreator.Statuses
import org.apache.commons.io.FileUtils
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef
import org.springframework.web.multipart.MultipartHttpServletRequest

import javax.servlet.http.HttpServletRequest

import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.OK
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE

class InvoiceDocumentController {

    def reportService
    def jasperService
    def documentFileService

    def pushFileToSigningApplet() {

        Invoice i = Invoice.get(Long.parseLong(params.invoice_id))
        def parameters = reportService.buildInvoiceJasperParams(params,session)

        List results = []
        i.invoiceBodyIn.invoiceItems.each {
            results.add(it)
        }
        LinkedHashMap data = [:]
        data.put('data',results)

        JasperReportDef report = jasperService.buildReportDefinition(parameters, request.getLocale(),data)
        ByteArrayOutputStream byteArray = report.contentStream

        String sessionId = session.id
        String relPath = '/temp/'+sessionId+'/'+System.currentTimeMillis()
        String filePath = request.servletContext.getRealPath(relPath)

        File parent = new File(filePath);
        if (!parent.exists()) {
            parent.mkdirs();
        }
        File outFile = new File(parent, "${i.name.encodeAsUnderscore()}.pdf");
        FileUtils.writeByteArrayToFile(outFile,byteArray.toByteArray())

        String downloadUrl = getBaseUrl(request)+relPath+"/"+outFile.name
        String documentSaveUrl = getBaseUrl(request)+"$controllerUri/popFileFromSigningApplet"
        String docMongoId = /${i.invoiceNumber}/

        redirect uri: getBaseUrl(request)+"/applet/applet.gsp",
                params: [downloadUrl:downloadUrl,sessionId:sessionId, docMongoId: docMongoId,documentSaveUrl:documentSaveUrl]

    }

    private String getBaseUrl(HttpServletRequest request){
        request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()
    }

    def popFileFromSigningApplet() {

        MultipartHttpServletRequest multiRequest
        if (request instanceof MultipartHttpServletRequest){
            multiRequest = (MultipartHttpServletRequest)request
        }
        else {
            render status: BAD_REQUEST
            return
        }

        def file = multiRequest.getFile('fileUpload')
        String invoiceNumber = params?.docMongoId
        String sessionId = params?.sessionId
        String body = null

        if(sessionId.equals(session.id)){
            body = session.body
        }
        def invoice = Invoice.findByInvoiceNumber(invoiceNumber)
        String invoiceBody = "invoiceBody"

        InvoiceDocument iDoc = new InvoiceDocument()
        try{
            iDoc.client = invoice.client
            iDoc.totalSum = invoice."${invoiceBody+body}".totalSum
            iDoc.inCurrency = invoice."${invoiceBody+body}".currency
            if(body.equals('Out')) {
                iDoc.exchangeRate = invoice.exchangeRate
            }
            iDoc.documentFile = file.getBytes()
            iDoc.mimeType = documentFileService.probeContentType(file.getBytes())
            iDoc.documentName = invoice.name
            iDoc.issueDate = invoice.issueDate
            iDoc.invoiceNumber = invoice.invoiceNumber
            iDoc.swiftCode = invoice.swiftCode
            iDoc.save(flush: true,failOnError: true)

        }catch(Exception e){
            //silence is golden
            log.error e.fillInStackTrace()
            render status: SERVICE_UNAVAILABLE
            return
        }
        // If everything is fine change the status to SIGNED
        invoice.setStatus(Statuses.SIGNED)
        if (!invoice.save(flush:true)) {
            invoice.errors.allErrors.each {
                println it
            }
        }
//        def hibSess = sessionFactory.getCurrentSession()
//        hibSess.flush()
        render status: OK

    }

    def transferInvoiceToMongo(){
        println params
        Invoice i = Invoice.get(Long.parseLong(params.invoice_id))
        def invoiceDoc = InvoiceDocument.findWhere(invoiceNumber: i.invoiceNumber)
        if(invoiceDoc && params.currencyCode.equals(i."${params.invoice_body.toLowerCase()+'Currency'}".currencyCode)){
            flash.message = message(code: 'invoice.saveToMongo.exists', default: "Document already exists.")
            redirect controller: 'invoice', action: 'show', id: i.id
            return
        }
        params._format="PDF"
        params._name=i.name
        params._file='InvoiceReport' // this is .jrmx file
        def parameters = reportService.buildInvoiceJasperParams(params,null)

        List results = []
        i."invoiceBody${params.invoice_body}".invoiceItems.each {
            results.add(it)
        }
        LinkedHashMap data = [:]
        data.put('data',results)

        JasperReportDef report = jasperService.buildReportDefinition(parameters, request.getLocale(),data)
        ByteArrayOutputStream byteArray = report.contentStream

        InvoiceDocument iDoc = new InvoiceDocument()
        try{
            iDoc.client = i.client
            iDoc.totalSum = i."${'invoiceBody'+params.invoice_body}".totalSum
            iDoc.inCurrency = i."${'invoiceBody'+params.invoice_body}".currency
            if(params.invoice_body.equals('Out')) {
                iDoc.exchangeRate = i.exchangeRate
            }
            iDoc.documentFile = byteArray.toByteArray()
            iDoc.mimeType = documentFileService.probeContentType(byteArray.toByteArray())
            iDoc.documentName = i.name
            iDoc.issueDate = i.issueDate
            iDoc.invoiceCounter = i.invoiceCounter
            iDoc.invoiceYear = i.invoiceYear
            iDoc.invoiceNumber = i.invoiceNumber
            iDoc.swiftCode = i.swiftCode
            iDoc.documentOrigin = DocumentOrigin.MANUAL_TRANSFER
            iDoc.save(flush: true,failOnError: true)

            flash.message = message(code: 'invoice.saveToMongo.success', default: "Invoice saved as PDF document.")
            redirect controller: 'invoice', action: 'showDocuments', id: i.id

        }catch(Exception e){
            //silence is golden
            log.error e.fillInStackTrace()
            flash.message = message(code: 'invoice.saveToMongo.error', default: "Couldn't save document, error occurred.")
            redirect controller: 'invoice', action: 'show', id: i.id
            return
        }
    }

    def delete(Long id){
        def invoiceDoc = InvoiceDocument.get(id)
        if(invoiceDoc == null){
            flash.message = message(code: 'invoice.showDocuments.noExists', default: "Document doesn't exists.")
            redirect controller: 'invoice', action: 'show', id: params.invoiceId
            return
        }
        invoiceDoc.delete flush: true
        flash.message = message(code: 'invoice.showDocuments.deleted', default: "Document deleted.")
        redirect controller: 'invoice', action: 'showDocuments', id: params.invoiceId
    }
}
