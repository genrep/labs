package com.genrep.invoiceRepository

import com.genrep.invoiceCounter.InvoiceCounterEvidence
import com.genrep.invoiceCreator.Money
import com.genrep.project.Project
import org.joda.time.DateTime
import org.springframework.web.multipart.MultipartHttpServletRequest

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class InvoiceArchiveController {

    static allowedMethods = [save: "POST", update: "POST", delete: "DELETE", showInvoiceArchiveFile:"GET"]

    def documentFileService
    def invoiceCounterService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
//        def list
//        InvoiceArchive.withStatelessSession {
//            list = InvoiceArchive.list(params)
//        }
        def customSortFields
        if(params.sort && params.sort.contains(",")){
            customSortFields = params.sort.split(",")
        }
        def list = InvoiceArchive.createCriteria().list{
            if(customSortFields){
                customSortFields.each {
                    order(it,params.order)
                }
            }
            else if(params.sort && params.order){
                order(params.sort,params.order)
            }
            else{
                order('invoiceYear','desc')
                order('invoiceCounter','desc')
            }
            if(params.offset){
                firstResult params.offset as int
            }
            if(params.max){
                maxResults params.max
            }
        }
        respond list, model:[invoiceArchiveInstanceCount: InvoiceArchive.count()]
    }

    def show(InvoiceArchive invoiceArchiveInstance) {
        respond invoiceArchiveInstance
    }

    def newImport(){
        int currentYear = new DateTime().getYear()
        def invoiceCounter = invoiceCounterService.getNextInvoiceCounter(currentYear)
        params.invoiceYear = (Long)currentYear
        params.invoiceCounter = invoiceCounter
        params.invoiceNumber = invoiceCounter.toString()+"/"+currentYear.toString()

        def projects = Project.findAll()
        respond new InvoiceArchive(params), model: [projectList:projects]
    }

    @Transactional
    def saveImport(InvoiceArchive invoiceArchiveInstance) {

        if (invoiceArchiveInstance == null) {
            notFound()
            return
        }

        invoiceArchiveInstance.totalSum = Money.formatStringToBigDecimal(params.totalSum?:null)
        invoiceArchiveInstance.taxFreeSum = Money.formatStringToBigDecimal(params.taxFreeSum?:null)
        invoiceArchiveInstance.taxPriceSum = Money.formatStringToBigDecimal(params.taxPriceSum?:null)


        if (invoiceArchiveInstance.hasErrors()) {
            respond invoiceArchiveInstance.errors, view:'newImport'
            return
        }

        def invoiceCounterEvidence = InvoiceCounterEvidence.findWhere(invoiceNumber: invoiceArchiveInstance.invoiceNumber)
        if(invoiceCounterEvidence && invoiceCounterEvidence.invoiceType!=invoiceArchiveInstance.getClass().getSimpleName()){
            flash.message = message(code: 'invoiceArchive.invoiceNumber.duplicate', default: "Invoice Number {0} is used. Choose another one", args:[invoiceArchiveInstance.invoiceNumber])
            log.error 'Object invoiceInstance has errors with bind parameters and cannot be saved!'
            respond invoiceArchiveInstance.errors, view: 'newImport'
            return
        }

        MultipartHttpServletRequest multiRequest
        if (request instanceof MultipartHttpServletRequest){
            multiRequest = (MultipartHttpServletRequest)request

            def file = multiRequest.getFile('documentFile')
            def mime = documentFileService.probeContentType(file.getBytes())
            invoiceArchiveInstance.mimeType = mime
            if(params.documentName.equals("")){
                invoiceArchiveInstance.documentName = file.originalFilename
            }
        }

        def taxes = findByRegex(/tax\d+/,params)
        taxes.each {k,v->
            if(!v.equals("")) {
                if(invoiceArchiveInstance.taxes == null) {
                    invoiceArchiveInstance.taxes = new HashMap<Taxes, BigDecimal>()
                }
                switch (k) {
                    case "tax0":
                        invoiceArchiveInstance.taxes.put("ZERO",Money.formatStringToBigDecimal(v))
                        break
                    case 'tax5':
                        invoiceArchiveInstance.taxes.put("FIVE",Money.formatStringToBigDecimal(v))
                        break
                    case 'tax18':
                        invoiceArchiveInstance.taxes.put("EIGHTEEN",Money.formatStringToBigDecimal(v))
                        break
                }
            }
        }

        invoiceArchiveInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'invoiceArchive.label', default: 'InvoiceArchive'), invoiceArchiveInstance.invoiceNumber])
                redirect invoiceArchiveInstance
            }
            '*' { respond invoiceArchiveInstance, [status: CREATED] }
        }
    }

    private def findByRegex = { regex, map ->
        map.findAll { k, v ->
            k =~ regex
        }
    }

    def edit(InvoiceArchive invoiceArchiveInstance) {
        def projects = Project.findAll()
        respond invoiceArchiveInstance, model: [projectList:projects]
    }

    @Transactional
    def updateImport(InvoiceArchive invoiceArchiveInstance) {
        if (invoiceArchiveInstance == null) {
            notFound()
            return
        }

        invoiceArchiveInstance.totalSum = Money.formatStringToBigDecimal(params.totalSum?:null)
        invoiceArchiveInstance.taxFreeSum = Money.formatStringToBigDecimal(params.taxFreeSum?:null)
        invoiceArchiveInstance.taxPriceSum = Money.formatStringToBigDecimal(params.taxPriceSum?:null)


        if (invoiceArchiveInstance.hasErrors()) {
            respond invoiceArchiveInstance.errors, view:'edit'
            return
        }

        def invoiceCounterEvidence = InvoiceCounterEvidence.findWhere(invoiceNumber: invoiceArchiveInstance.invoiceNumber)
        if(invoiceCounterEvidence && invoiceCounterEvidence.invoiceType!=invoiceArchiveInstance.getClass().getSimpleName()){
            flash.message = message(code: 'invoiceArchive.invoiceNumber.duplicate', default: "Invoice Number {0} is used. Choose another one", args:[invoiceArchiveInstance.invoiceNumber])
            log.error 'Object invoicArchiveInstance has errors with bind parameters and cannot be saved!'
            respond invoiceArchiveInstance.errors, view: 'edit'
            return
        }

        def taxes = findByRegex(/tax\d+/,params)
        taxes.each {k,v->
            if(!v.equals("")) {
                if(invoiceArchiveInstance.taxes == null) {
                    invoiceArchiveInstance.taxes = new HashMap<Taxes, BigDecimal>()
                }
                switch (k) {
                    case "tax0":
                        invoiceArchiveInstance.taxes.put("ZERO",Money.formatStringToBigDecimal(v))
                        break
                    case 'tax5':
                        invoiceArchiveInstance.taxes.put("FIVE",Money.formatStringToBigDecimal(v))
                        break
                    case 'tax18':
                        invoiceArchiveInstance.taxes.put("EIGHTEEN",Money.formatStringToBigDecimal(v))
                        break
                }
            }
        }

        invoiceArchiveInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'InvoiceArchive.label', default: 'InvoiceArchive'), invoiceArchiveInstance.invoiceNumber])
                redirect invoiceArchiveInstance
            }
            '*'{ respond invoiceArchiveInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(InvoiceArchive invoiceArchiveInstance) {

        if (invoiceArchiveInstance == null) {
            notFound()
            return
        }
        def invoiceNumber = invoiceArchiveInstance.invoiceNumber
        invoiceArchiveInstance.delete flush:true
        def invoiceCounterEvidence = InvoiceCounterEvidence.findWhere(invoiceNumber: invoiceNumber)
        invoiceCounterEvidence.delete(flush: true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'InvoiceArchive.label', default: 'InvoiceArchive'), invoiceArchiveInstance.invoiceNumber])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoiceArchive.label', default: 'InvoiceArchive'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def showInvoiceArchiveFile(Long id){
        def iDoc = InvoiceArchive.findById(id)
        if(iDoc) {
            byte[] file = iDoc.documentFile
            def mime = documentFileService.probeContentType(file)
            response.setContentType(mime)
            response.setContentLength(file.length)
            response.setHeader("Content-disposition", "filename=${iDoc?.documentName?:'Document'}")
            OutputStream out = response.getOutputStream()
            out.write(file)
            out.close()
        }else{
            return
        }
    }

    def deleteInvoiceArchiveFile(Long id) {
        try {
            def invoiceArchiveInstance = InvoiceArchive.get(id)
            invoiceArchiveInstance.documentFile = null
            invoiceArchiveInstance.documentName = null
            render template: 'editFileForm', bean: invoiceArchiveInstance
        }
        catch(Exception e){
            log.error(e.fillInStackTrace())
            render status: INTERNAL_SERVER_ERROR
        }
    }
}
