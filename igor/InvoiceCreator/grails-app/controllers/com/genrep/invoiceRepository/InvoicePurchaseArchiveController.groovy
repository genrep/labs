package com.genrep.invoiceRepository

import com.genrep.invoiceCreator.Money
import grails.converters.JSON
import org.springframework.web.multipart.MultipartHttpServletRequest

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class InvoicePurchaseArchiveController {

    static allowedMethods = [save: "POST", update: "POST", delete: "DELETE"]

    def documentFileService
    def invoiceCounterService
    def invoiceService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def customSortFields
        if(params.sort && params.sort.contains(",")){
            customSortFields = params.sort.split(",")
        }
        def list = InvoicePurchaseArchive.createCriteria().list{
            if(customSortFields){
                customSortFields.each {
                    order(it,params.order)
                }
            }
            else if(params.sort && params.order){
                order(params.sort,params.order)
            }
            else{
                order('invoiceYear','desc')
                order('admissionCounterNumber','desc')
            }
            if(params.offset){
                firstResult params.offset as int
            }
            if(params.max){
                maxResults params.max
            }
        }
//        respond InvoicePurchaseArchive.list(params), model: [invoicePurchaseArchiveInstanceCount: InvoicePurchaseArchive.count()]
        respond list, model: [invoicePurchaseArchiveInstanceCount: InvoicePurchaseArchive.count()]
    }

    def show(InvoicePurchaseArchive invoicePurchaseArchiveInstance) {
        respond invoicePurchaseArchiveInstance
    }

    def newImport() {
        params.admissionCounterNumber = invoiceCounterService.getPurchaseInvoiceCounter()
        List<InvoicePurchaseArchive> proInvoices = InvoicePurchaseArchive.findAllByIsProInvoiceAndRefInvoice(true,null)
        render view: 'newImport',
                model: [invoicePurchaseArchiveInstance:new InvoicePurchaseArchive(params),
                        proInvoices: proInvoices
                ]
    }

    @Transactional
    def save(InvoicePurchaseArchive invoicePurchaseArchiveInstance) {

        if (invoicePurchaseArchiveInstance == null) {
            notFound()
            return
        }

        Long invoiceYear = Long.parseLong(params.admissionYear_year)
        invoicePurchaseArchiveInstance.invoiceYear = invoiceYear
        invoicePurchaseArchiveInstance.totalSum = Money.formatStringToBigDecimal(params.totalSum?:null)
        invoicePurchaseArchiveInstance.taxFreeSum = Money.formatStringToBigDecimal(params.taxFreeSum?:null)
        invoicePurchaseArchiveInstance.taxPriceSum = Money.formatStringToBigDecimal(params.taxPriceSum?:null)
        invoicePurchaseArchiveInstance.admissionCounterNumberView = InvoicePurchaseArchive.createAdmissionCounterNumberView(invoiceYear, invoicePurchaseArchiveInstance.admissionCounterNumber)

        invoicePurchaseArchiveInstance.validate()

        if (invoicePurchaseArchiveInstance.hasErrors()) {
            List<InvoicePurchaseArchive> proInvoices = InvoicePurchaseArchive.findAllByIsProInvoiceAndRefInvoice(true,null)
            render view: 'newImport',
                    model: [invoicePurchaseArchiveInstance:invoicePurchaseArchiveInstance,
                            proInvoices: proInvoices,
                    ]
            return
        }

        // Found out real mime type and set custom name if submitted
        // documentFile will automatically bound with domain property
        MultipartHttpServletRequest multiRequest
        if (request instanceof MultipartHttpServletRequest){
            multiRequest = (MultipartHttpServletRequest)request

            def file = multiRequest.getFile('documentFile')
            def mime = documentFileService.probeContentType(file.getBytes())
            invoicePurchaseArchiveInstance.mimeType = mime
            if(params.documentName.equals("")){
                invoicePurchaseArchiveInstance.documentName = file.originalFilename
            }
        }

        // Set the taxes individually
        def taxes = findByRegex(/tax\d+/,params)
        taxes.each {k,v->
            if(!v.equals("")) {
                if(invoicePurchaseArchiveInstance.taxes == null) {
                    invoicePurchaseArchiveInstance.taxes = new HashMap<Taxes, BigDecimal>()
                }
                switch (k) {
                    case "tax0":
                        invoicePurchaseArchiveInstance.taxes.put("ZERO",Money.formatStringToBigDecimal(v))
                        break
                    case 'tax5':
                        invoicePurchaseArchiveInstance.taxes.put("FIVE",Money.formatStringToBigDecimal(v))
                        break
                    case 'tax18':
                        invoicePurchaseArchiveInstance.taxes.put("EIGHTEEN",Money.formatStringToBigDecimal(v))
                        break
                }
            }
        }

        if(invoicePurchaseArchiveInstance.save(flush: true)){
            if(!invoicePurchaseArchiveInstance.isProInvoice){
                invoiceCounterService.incrementPurchaseInvoiceCounterNumber(invoiceYear)
            }
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'invoicePurchaseArchive.label', default: 'InvoicePurchaseArchive'), invoicePurchaseArchiveInstance.invoiceNumber])
                redirect invoicePurchaseArchiveInstance
            }
            '*' { respond invoicePurchaseArchiveInstance, [status: CREATED] }
        }
    }

    def edit(InvoicePurchaseArchive invoicePurchaseArchiveInstance) {
        List<InvoicePurchaseArchive> proInvoices = InvoicePurchaseArchive.findAllByIsProInvoiceAndRefInvoice(true,null)
        def admissionYear = new Date(year:invoicePurchaseArchiveInstance.invoiceYear-1900)
        render view: 'edit',
                model: [invoicePurchaseArchiveInstance:invoicePurchaseArchiveInstance,
                        proInvoices: proInvoices,
                        admissionYear: admissionYear
                ]
    }

    @Transactional
    def update(InvoicePurchaseArchive invoicePurchaseArchiveInstance) {
        if (invoicePurchaseArchiveInstance == null) {
            notFound()
            return
        }

        Long invoiceYear = Long.parseLong(params.admissionYear_year)
        invoicePurchaseArchiveInstance.invoiceYear = invoiceYear
        invoicePurchaseArchiveInstance.totalSum = Money.formatStringToBigDecimal(params.totalSum?:null)
        invoicePurchaseArchiveInstance.taxFreeSum = Money.formatStringToBigDecimal(params.taxFreeSum?:null)
        invoicePurchaseArchiveInstance.taxPriceSum = Money.formatStringToBigDecimal(params.taxPriceSum?:null)
        invoicePurchaseArchiveInstance.admissionCounterNumberView = InvoicePurchaseArchive.createAdmissionCounterNumberView(invoiceYear,invoicePurchaseArchiveInstance.admissionCounterNumber)

        invoicePurchaseArchiveInstance.validate()

        if (invoicePurchaseArchiveInstance.hasErrors()) {
            List<InvoicePurchaseArchive> proInvoices = InvoicePurchaseArchive.findAllByIsProInvoiceAndRefInvoice(true,null)
            render view: 'edit',
                    model: [invoicePurchaseArchiveInstance:invoicePurchaseArchiveInstance,
                            proInvoices: proInvoices,
                    ]
            return
        }

        def taxes = findByRegex(/tax\d+/,params)
        taxes.each {k,v->
            if(!v.equals("")) {
                if(invoicePurchaseArchiveInstance.taxes == null) {
                    invoicePurchaseArchiveInstance.taxes = new HashMap<Taxes, BigDecimal>()
                }
                switch (k) {
                    case "tax0":
                        invoicePurchaseArchiveInstance.taxes.put("ZERO",Money.formatStringToBigDecimal(v))
                        break
                    case 'tax5':
                        invoicePurchaseArchiveInstance.taxes.put("FIVE",Money.formatStringToBigDecimal(v))
                        break
                    case 'tax18':
                        invoicePurchaseArchiveInstance.taxes.put("EIGHTEEN",Money.formatStringToBigDecimal(v))
                        break
                }
            }
        }


        if(invoicePurchaseArchiveInstance.isDirty('refInvoice')){
            invoiceService.beforeUpdateInvoicePurchaseArchive(invoicePurchaseArchiveInstance)
        }

        if(invoicePurchaseArchiveInstance.save(flush: true)){
            if(invoicePurchaseArchiveInstance.isDirty('admissionCounterNumber')){
                invoiceCounterService.setPurchaseInvoiceCounter(invoiceYear)
            }
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'InvoicePurchaseArchive.label', default: 'InvoicePurchaseArchive'), invoicePurchaseArchiveInstance.id])
                redirect invoicePurchaseArchiveInstance
            }
            '*' { respond invoicePurchaseArchiveInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(InvoicePurchaseArchive invoicePurchaseArchiveInstance) {

        if (invoicePurchaseArchiveInstance == null) {
            notFound()
            return
        }

        invoicePurchaseArchiveInstance.delete flush: true
        // if delete was successful
        invoiceCounterService.setUpPurchaseInvoiceCounter()

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'InvoicePurchaseArchive.label', default: 'InvoicePurchaseArchive'), invoicePurchaseArchiveInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'invoicePurchaseArchive.label', default: 'InvoicePurchaseArchive'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    private def findByRegex = { regex, map ->
        map.findAll { k, v ->
            k =~ regex
        }
    }

    def showInvoicePurchaseArchiveFile(Long id){
        def iDoc = InvoicePurchaseArchive.findById(id)
        if(iDoc) {
            byte[] file = iDoc.documentFile
            def mime = documentFileService.probeContentType(file)
            response.setContentType(mime)
            response.setContentLength(file.length)
            response.setHeader("Content-disposition", "filename=${iDoc?.documentName?:'Document'}")
            OutputStream out = response.getOutputStream()
            out.write(file)
            out.close()
        }else{
            return
        }
    }

    def deleteInvoicePurchaseArchiveFile(Long id) {
        try {
            def invoicePurchaseArchiveInstance = InvoicePurchaseArchive.get(id)
            invoicePurchaseArchiveInstance.documentFile = null
            invoicePurchaseArchiveInstance.documentName = null
            render template: 'editFileForm', bean: invoicePurchaseArchiveInstance
        }
        catch(Exception e){
            log.error(e.fillInStackTrace())
            render status: INTERNAL_SERVER_ERROR
        }
    }

    def getInvoicePurchaseJSON(Long id){
        def invoicePurchaseArchive = InvoicePurchaseArchive.get(id)
        render invoicePurchaseArchive as JSON
    }

    def getValidAdmissionNumber(){
        def year = params.year as Long
        def res = invoiceCounterService.getPurchaseInvoiceCounter(year)
        render res
    }
}
