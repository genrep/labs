package com.genrep.jasper

import eu.bitwalker.useragentutils.UserAgent
import org.codehaus.groovy.grails.plugins.jasper.JasperReportDef

class JasperController extends org.codehaus.groovy.grails.plugins.jasper.JasperController {

    @Deprecated
    def index = {
        def testModel = this.getProperties().containsKey('chainModel') ? chainModel : null
        addImagesURIIfHTMLReport(params, request.contextPath)
        JasperReportDef report = jasperService.buildReportDefinition(params, request.getLocale(), testModel)
        addJasperPrinterToSession(request.getSession(), report.jasperPrinter)
        generateResponse(report)
    }

    def generateResponse = {reportDef ->
        if (!reportDef.fileFormat.inline && !reportDef.parameters._inline) {
            def fileName = URLEncoder.encode(reportDef.parameters._name ?: reportDef.name,"UTF-8")
            println fileName
            String userAgent = request.getHeader("User-Agent")
            UserAgent ua = UserAgent.parseUserAgentString(userAgent)
            String browser = ua.getBrowser().toString()
            // the response
            response.contentType = reportDef.fileFormat.mimeTyp
            response.characterEncoding = "UTF-8"
            if(browser.contains("FIREFOX")){
                response.setHeader("Content-Disposition","attachment; filename*=UTF-8''"+fileName+ "." + reportDef.fileFormat.extension)
            }
            else{
//                response.setHeader("Content-disposition", "attachment; filename=" + fileName + "." + reportDef.fileFormat.extension)
                response.setHeader("Content-disposition", "attachment; filename*=UTF-8''" + fileName + "." + reportDef.fileFormat.extension)
            }
            response.outputStream << reportDef.contentStream.toByteArray()
        } else {
            render(text: reportDef.contentStream, contentType: reportDef.fileFormat.mimeTyp, encoding: reportDef.parameters.encoding ? reportDef.parameters.encoding : 'UTF-8')
        }
    }
}
