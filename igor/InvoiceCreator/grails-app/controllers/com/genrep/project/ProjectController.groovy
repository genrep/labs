package com.genrep.project

import com.genrep.invoiceRepository.InvoiceArchive
import org.springframework.web.multipart.MultipartHttpServletRequest

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ProjectController {

    def projectService
    def documentFileService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Project.list(params), model:[projectInstanceCount: Project.count()]
    }

    def show(Project projectInstance) {
        def projectDocument = ProjectDocument.findWhere(technicalNumber: projectInstance.technicalNumber)
        def importedInvoices = InvoiceArchive.findWhere(projectId: projectInstance.technicalNumber)
        respond projectInstance,
                model: [projectDocumentInstance:projectDocument,
                        importedInvoicesList: importedInvoices]
    }

    def create() {
        respond new Project(params)
    }

    @Transactional
    def save(Project projectInstance) {
        println params

        if (projectInstance == null) {
            notFound()
            return
        }

        if (projectInstance.hasErrors()) {
            respond projectInstance.errors, view:'create'
            return
        }

        if(projectInstance.save(flush:true)){
            //save document
            MultipartHttpServletRequest multiRequest
            if (request instanceof MultipartHttpServletRequest){
                multiRequest = (MultipartHttpServletRequest)request
                def file = multiRequest.getFile('documentFile')
                if(file!=null && file.size>0) {
                    projectService.saveOrUpdateProjectDocument(projectInstance,file,params?.documentName)
                }
            }
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'project.label', default: 'Project'), projectInstance.id])
                redirect projectInstance
            }
            '*' { respond projectInstance, [status: CREATED] }
        }
    }

    def edit(Project projectInstance) {
        def projectDocument = ProjectDocument.findWhere(technicalNumber: projectInstance.technicalNumber)
        respond projectInstance, model: [projectDocumentInstance: projectDocument]
    }

    @Transactional
    def update(Project projectInstance) {
        if (projectInstance == null) {
            notFound()
            return
        }

        if (projectInstance.hasErrors()) {
            respond projectInstance.errors, view:'edit'
            return
        }

        if(projectInstance.save(flush:true)){
            //update document
            MultipartHttpServletRequest multiRequest
            if (request instanceof MultipartHttpServletRequest) {
                multiRequest = (MultipartHttpServletRequest) request
                def file = multiRequest.getFile('documentFile')
                if (file != null) {
                    projectService.saveOrUpdateProjectDocument(projectInstance, file, params?.documentName)
                }
            }
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Project.label', default: 'Project'), projectInstance.id])
                redirect projectInstance
            }
            '*'{ respond projectInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Project projectInstance) {

        if (projectInstance == null) {
            notFound()
            return
        }

        def technicalNumber = projectInstance.technicalNumber
        projectInstance.delete flush:true

        def projectDocument = ProjectDocument.findWhere(technicalNumber: technicalNumber)
        if (projectDocument) {
            projectDocument.delete(flush: true)
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Project.label', default: 'Project'), projectInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'project.label', default: 'Project'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def showProjectDocumentFile(Long id){
        def iDoc = ProjectDocument.findById(id)
        if(iDoc) {
            byte[] file = iDoc.documentFile
            def mime = documentFileService.probeContentType(file)
            response.setContentType(mime)
            response.setContentLength(file.length)
            response.setHeader("Content-disposition", "filename=${iDoc?.documentName?:'Document'}")
            OutputStream out = response.getOutputStream()
            out.write(file)
            out.close()
        }else{
            return
        }
    }

    def deleteProjectDocumentFile(){
        try {
            if(params.guiAction.equals("delete")){
                def projectDocumentInstance = null
                render template: 'editFileForm', bean: projectDocumentInstance, model: [undoDocumentId: params.id]
            }
            else{
                // undo button
                def projectDocumentInstance = ProjectDocument.get(Long.parseLong(params.id))
                render template: 'editFileForm', model: [projectDocumentInstance: projectDocumentInstance]
            }
        }
        catch(Exception e){
            log.error(e.fillInStackTrace())
            render status: INTERNAL_SERVER_ERROR
        }
    }
}
