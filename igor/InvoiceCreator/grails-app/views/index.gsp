<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Invoice Kreator</title>
		<style type="text/css" media="screen">
			#status {
				background-color: #f5f5f5;
				/*border: .2em solid #fff;*/
				/*margin: 2em 2em 1em;*/
                margin-top: 2px;
				padding: 1em;
				width: 12em;
				float: left;
				/*-moz-box-shadow: 0px 0px 1.25em #ccc;*/
				/*-webkit-box-shadow: 0px 0px 1.25em #ccc;*/
				/*box-shadow: 0px 0px 1.25em #ccc;*/
				/*-moz-border-radius: 0.6em;*/
				/*-webkit-border-radius: 0.6em;*/
				/*border-radius: 0.6em;*/
                display: none;
			}


			.ie6 #status {
				display: inline; /* float double margin fix http://www.positioniseverything.net/explorer/doubled-margin.html */
			}

			#status ul {
				font-size: 0.9em;
				list-style-type: none;
				margin-bottom: 0.6em;
				padding: 0;
			}

			#status li {
				line-height: 1.3;
			}

			#status h1 {
				text-transform: uppercase;
				font-size: 1.1em;
				margin: 0 0 0.3em;
			}

			#page-body {
				/*margin: 2em 1em 1.25em 18em;*/
                margin: 1rem auto 3rem;
                text-align: center;

			}

            #page-body h1{
                background-color: #f5f5f5;
                padding: 2rem 1rem;
                font-size: 3rem;
                /*text-align: center;*/
                text-transform: uppercase;
                font-weight: 300;
                margin: 1rem 0 1rem 0;
            }

			h2 {
				margin-top: 1em;
				margin-bottom: 0.3em;
				font-size: 1em;
			}

			p {
				line-height: 1.5;
				margin: 0.25em 0;
			}

			#controller-list ul {
				list-style-position: inside;
			}

			#controller-list li {
				line-height: 1.3;
				list-style-position: inside;
				margin: 0.25em 0;
			}

            .linkButton{
                font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-size: 1.5rem;
                font-weight: 300;
                text-transform: uppercase;
                color: white;
                text-align: center;
                padding: 2rem 2rem;
                background-color: #998CC2;
                width: 17%;
                height: 4rem;
                margin: 1rem auto 1rem;
                display: inline-block;
                vertical-align: top;
                border-radius: 10rem;
            }
            .linkButton span {
                display:inline-block;
                vertical-align:middle;
            }
            .linkButton:hover{
                background-color: #6D6096;
                /*border: 1rem solid #6D6096;*/
                /*-moz-box-shadow: 0 0 1px 1px #aaaaaa;*/
                /*-webkit-box-shadow: 0 0 1px 1px #aaaaaa;*/
                /*box-shadow: 0 0 1px 1px #aaaaaa;*/
            }

            .linkButton:active{
                position: relative;
                top: 1px;
            }

            a {
                text-decoration: none;
            }

			@media screen and (max-width: 480px) {
				#status {
					display: none;
				}

				#page-body {
					margin: 0 1em 1em;
				}

				#page-body h1 {
					margin-top: 0;
				}


			}
		</style>
	</head>
	<body>
		<div id="status" role="complementary">
			<h1>Application Status</h1>
			<ul>
				<li>App version: <g:meta name="app.version"/></li>
				<li>Grails version: <g:meta name="app.grails.version"/></li>
				<li>Groovy version: ${GroovySystem.getVersion()}</li>
				<li>JVM version: ${System.getProperty('java.version')}</li>
				<li>Reloading active: ${grails.util.Environment.reloadingAgentEnabled}</li>
				<li>Controllers: ${grailsApplication.controllerClasses.size()}</li>
				<li>Domains: ${grailsApplication.domainClasses.size()}</li>
				<li>Services: ${grailsApplication.serviceClasses.size()}</li>
				<li>Tag Libraries: ${grailsApplication.tagLibClasses.size()}</li>
			</ul>
			<h1>Installed Plugins</h1>
			<ul>
				<g:each var="plugin" in="${applicationContext.getBean('pluginManager').allPlugins}">
					<li>${plugin.name} - ${plugin.version}</li>
				</g:each>
			</ul>
		</div>
		<div id="page-body" role="main">
            <h1>Welcome to Invoice Creator</h1>

            <g:link controller="invoice">
                <div class="linkButton">SALES INVOICES</div>
            </g:link>

            <g:link controller="invoicePurchaseArchive">
                <div class="linkButton">PURCHASE INVOICES</div>
            </g:link>

            <g:link controller="client">
                <div class="linkButton">CLIENT</div>
            </g:link>

			<g:link controller="vendor">
                <div class="linkButton">VENDOR</div>
            </g:link>

            <g:link controller="exchangeRate">
                <div class="linkButton">Exchange Rates</div>
            </g:link>

            <g:link controller="bankStatement">
                <div class="linkButton">Statements</div>
            </g:link>

            <g:link controller="bankAccount">
                <div class="linkButton">Bank Accounts</div>
            </g:link>

            <g:link controller="bankStatementArchive">
                <div class="linkButton">STATEMENT PDFs</div>
            </g:link>

			<g:link controller="quantityUnit">
				<div class="linkButton">Quantity Units</div>
			</g:link>

            <g:link controller="project">
                <div class="linkButton">PROJECTS</div>
            </g:link>

		</div>
	</body>
</html>
