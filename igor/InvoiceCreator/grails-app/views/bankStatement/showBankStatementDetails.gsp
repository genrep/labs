<%--
  Created by IntelliJ IDEA.
  User: igor
  Date: 9/18/14
  Time: 12:23 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'bankStatementDoc.label', default: 'BankStatementDoc')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
    <asset:javascript src="statement.js"/>
</head>
<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="bankStatmentDoc.list" default="List Bank Statements" /></g:link></li>
    </ul>
</div>
<div id="show-bankStatement" class="content scaffold-show" role="main">
    <h1><g:message code="bankStatement.showDetails.label" default="Bank Statement Detailed Information" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <ol class="property-list bankStatement">

        <g:if test="${bankStatementInstance?.bankStatementId}">
            <li class="fieldcontain">
                <span id="bankStatementId-label" class="property-label"><g:message code="bankStatement.bankStatementId.label" default="Bank Statement ID" /></span>

                <span class="property-value" ><g:fieldValue bean="${bankStatementInstance}" field="bankStatementId"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.issueDate}">
            <li class="fieldcontain">
                <span id="issueDate-label" class="property-label"><g:message code="bankStatement.issueDate.label" default="Issue Date" /></span>

                <span class="property-value"><g:formatDate date="${bankStatementInstance?.issueDate}" format="dd.MM.yyyy"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.bankAccountNumber}">
            <li class="fieldcontain">
                <span id="bankAccountNumber-label" class="property-label"><g:message code="bankStatement.bankAccountNumber.label" default="Account Number" /></span>

                <span class="property-value" ><g:fieldValue bean="${bankStatementInstance}" field="bankAccountNumber"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.balanceBefore}">
            <li class="fieldcontain">
                <span id="balanceBefore-label" class="property-label"><g:message code="bankStatement.balanceBefore.label" default="Balance Before" /></span>

                <span class="property-value" >${bankStatementInstance.balanceBefore.decodeDecimalNumber()}</span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.balanceAfter}">
            <li class="fieldcontain">
                <span id="balanceAfter-label" class="property-label"><g:message code="bankStatement.balanceAfter.label" default="Balance After" /></span>

                <span class="property-value" >${bankStatementInstance.balanceAfter.decodeDecimalNumber()}</span>

            </li>
        </g:if>

        <g:render template="/bankStatementDoc/tableOfBankStatementItems" />

        <li class="fieldcontain">
            <span id="total-label" class="property-label"><g:message code="bankStatement.total.label" default="Total" /></span>
            <span class="property-value" >${total.decodeDecimalNumber()}</span>

        </li>
    </ol>




</div>
</body>
</html>
