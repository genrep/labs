<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'bankStatement.label', default: 'BankStatement')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
    <asset:javascript src="statement.js"/>
</head>

<body>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="bankStatement.list" default="List Bank Statement"/></g:link></li>
    </ul>
</div>

<div id="create-bankStatement" class="content scaffold-create" role="main">
    <h1><g:message code="bankStatement.create.label" default="Add New Bank Statement"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${bankStatementInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${bankStatementInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form id="formBankStatement" url="[resource: bankStatementInstance, action: 'save']">
        <fieldset class="form">
            <g:render template="form"/>
        </fieldset>
    </g:form>
    <g:form id="formItems" url="[resource: bankStatementItemInstance, action: 'addBankStatementItem']">
        <fieldset class="form">
            <g:render template="formItems"/>
        </fieldset>
    </g:form>
    <fieldset class="buttons">
        <g:submitButton name="create" class="save" form="formBankStatement"
                        value="${message(code: 'default.button.create.label', default: 'Create')}"/>
    </fieldset>

    <div class="includedTemplate">
        <g:include view="bankAccount/_form.gsp"/>
    </div>
</div>
</body>
</html>
