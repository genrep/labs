<%@ page import="com.genrep.statements.BankStatement" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'bankStatement.label', default: 'BankStatement')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <asset:javascript src="statement.js"/>
</head>

<body>
<a href="#edit-bankStatement" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                    default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="bankStatement.list" default="List Bank Statement"/></g:link></li>

    </ul>
</div>

<div id="edit-bankStatement" class="content scaffold-edit" role="main">
    <h1><g:message code="bankStatement.editItems.label" default="Edit Bank Statement Items"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${bankStatementInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${bankStatementInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form id="formItems" url="[resource: bankStatementItemInstance, action: 'addBankStatementItem']">

            <g:render template="formItems"/>
   </g:form>
    <g:form id="formItems" url="[resource: bankStatementInstance, action: 'updateBankStatementItems']">
        <fieldset class="buttons">
            <g:actionSubmit class="save" action="updateBankStatementItems"
                            value="${message(code: 'default.button.update.label', default: 'Update')}"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
