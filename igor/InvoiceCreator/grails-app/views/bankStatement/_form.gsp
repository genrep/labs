<%@ page import="com.genrep.account.BankAccount; com.genrep.statements.BankStatement" %>



<div class="fieldcontain ${hasErrors(bean: bankStatementInstance, field: 'bankStatementId', 'error')}">
    <span class="property-label required">
        <label for="bankStatementId">
            <g:message code="bankStatement.bankStatementId.label" default="Bank Statement Id"/>
        </label>
    </span>
    <span class="property-value">
        <g:textField name="bankStatementId" required="" value="${bankStatementInstance?.bankStatementId}"/>
    </span>

</div>

<div class="fieldcontain ${hasErrors(bean: bankStatementInstance, field: 'issueDate', 'error')}">
    <span class="property-label required">
        <label for="issueDate">
            <g:message code="bankStatement.issueDate.label" default="Issue Date"/>
        </label>
    </span>
    <span class="property-value">
        <input type="text" name="issueDate" id="issueDate" class="date"
               value="${formatDate(date:bankStatementInstance?.issueDate?:new Date(),format: 'dd.MM.yyyy')}"/>
    </span>

</div>

<div class="fieldcontain ${hasErrors(bean: bankStatementInstance, field: 'ownerOfStatement', 'error')}">
    <span class="property-label required">
        <label for="ownerOfStatement">
            <g:message code="bankStatement.ownerOfStatement.label" default="Bank Account"/>
        </label>
    </span>
    <span class="property-value">
        %{--<g:textField name="bankAccountNumber" required="" value="${bankStatementInstance?.bankAccountNumber}"/>--}%
        <g:select id="ownerOfStatement" name="ownerOfStatement.id" from="${BankAccount.findAllByOwner(true)}"
                  optionKey="id" optionValue="entityName"
                  required=""
                  value="${bankStatementInstance?.ownerOfStatement?.id}" class="comboBox"/>
        <input type="button" class="newDialogButton" value="+" onclick="newBankAccountDialog()" />
    </span>

</div>

<div class="fieldcontain ${hasErrors(bean: bankStatementInstance, field: 'balanceBefore', 'error')}">
    <span class="property-label required">
        <label for="balanceBefore">
            <g:message code="bankStatement.balanceBefore.label" default="Balance Before"/>
        </label>
    </span>
    <span class="property-value">
        <g:priceAndTaxField id="balanceBefore" name="balanceBefore" required="" value="${bankStatementInstance?.balanceBefore}"/>
    </span>

</div>

<div class="fieldcontain ${hasErrors(bean: bankStatementInstance, field: 'balanceAfter', 'error')}">
    <span class="property-label required">
        <label for="balanceAfter">
            <g:message code="bankStatement.balanceAfter.label" default="Balance After"/>
        </label>
    </span>
    <span class="property-value">
        <g:priceAndTaxField id="balanceAfter" name="balanceAfter" required="" value="${bankStatementInstance?.balanceAfter}"/>
    </span>
</div>



