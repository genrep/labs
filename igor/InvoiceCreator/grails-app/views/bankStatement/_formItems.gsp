<div id="bankStatementItems">
<!-- ADD NEW -->

<div class="bankStatementItem">
    <input id="bankStatementItemItemButton" type="button" value="Add Transaction" class="buttonsMine createButton"
           onclick="newBankStatementItem()"/>
</div>

<g:render template="/bankStatement/formBankStatementItem"/>

<!-- END NEW -->

<!-- STATEMENT BODY -->
<fieldset class="statement-items-table">
    <g:render template="/bankStatementDoc/tableOfBankStatementItems" />
</fieldset>

</div>