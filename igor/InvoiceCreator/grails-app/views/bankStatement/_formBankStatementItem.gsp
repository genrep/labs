<%@ page import="com.genrep.statements.BankStatementItem" %>
<div id="formBankStatementItem" class="newItemBody">

    <g:hasErrors bean="${bankStatementItemInstance}">
        <ul id="erol" class="errors" role="alert">
            <g:eachError bean="${bankStatementItemInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

    <div class="fieldcontain ${hasErrors(bean: bankStatementItemInstance, field: 'publicEntity', 'error')} ">
        <span class="property-label required">
            <label for="publicEntity">
                <g:message code="bankStatementItem.publicEntity.label" default="Public Entity" />
            </label>
        </span>
        <span class="property-value">
            <g:textField name="publicEntity" id="publicEntity" required="" value="${bankStatementItemInstance?.publicEntity}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: bankStatementItemInstance, field: 'publicEntityBankAccountNumber', 'error')} ">
        <span class="property-label required">
            <label for="publicEntityBankAccountNumber">
                <g:message code="bankStatementItem.publicEntityBankAccountNumber.label" default="Public Entity Bank Acco. No." />
            </label>
        </span>
        <span class="property-value">
            <g:textField name="publicEntityBankAccountNumber" id="publicEntityBankAccountNumber" required=""
            value="${bankStatementItemInstance?.publicEntityBankAccountNumber}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: bankStatementItemInstance, field: 'transferSum', 'error')}">
        <span class="property-label required">
            <label for="transferSum">
                <g:message code="bankStatementItem.transferSum.label" default="Transfer Sum" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="transferSum" name="transferSum" required="" value="${bankStatementItemInstance?.transferSum}" />
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: bankStatementItemInstance, field: 'shortDescription', 'error')} ">
        <span class="property-label required">
            <label for="shortDescription">
                <g:message code="bankStatementItem.shortDescription.label" default="Short Desc." />
            </label>
        </span>
        <span class="property-value">
            <g:textArea name="shortDescription" id="shortDescription" required="" value="${bankStatementItemInstance?.shortDescription}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: bankStatementItemInstance, field: 'transactionCode', 'error')} ">
        <span class="property-label required">
            <label for="transactionCode">
                <g:message code="bankStatementItem.transactionCode.label" default="Trans. Code" />
            </label>
        </span>
        <span class="property-value">
            <g:textField name="transactionCode" id="transactionCode" required="" value="${bankStatementItemInstance?.transactionCode}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: bankStatementItemInstance, field: 'borrowingCode', 'error')}">
        <span class="property-label required">
            <label for="borrowingCode">
                <g:message code="bankStatementItem.borrowingCode.label" default="Borrowing Code" />
            </label>
        </span>
        <span class="property-value">
            <g:textField id="borrowingCode" name="borrowingCode" required="" value="${bankStatementItemInstance?.borrowingCode}" />
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: bankStatementItemInstance, field: 'approvalCode', 'error')}">
        <span class="property-label required">
            <label for="borrowingCode">
                <g:message code="bankStatementItem.approvalCode.label" default="Approval Code" />
            </label>
        </span>
        <span class="property-value">
            <g:textField id="approvalCode" name="approvalCode" required="" value="${bankStatementItemInstance?.approvalCode}" />
        </span>
    </div>

    <div class="itemButtons">

        <input id="addItemButton" type="button" value="Add Item" class="buttonsMine saveButton"
               onclick="addBankStatementItem('${form}')"/>

        <input id="closeItemButton" type="button" value="Close" class="buttonsMine closeButton"
               onclick="closeBankStatementItem()"/>
    </div>

</div>