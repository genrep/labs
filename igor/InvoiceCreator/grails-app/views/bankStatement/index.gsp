<%@ page import="com.genrep.statements.BankStatement" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'bankStatement.label', default: 'BankStatement')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="create"><g:message code="bankStatment.create" default="New Bank Statement" /></g:link></li>
        <li><a class="menuButton" href="${createLink(uri: '/statements/documents')}"><g:message code="bankStatementsDoc.home.label" default="Importing of Statements"/></a></li>
    </ul>
</div>

<div id="list-bankStatement" class="content scaffold-list" role="main">
    <h1><g:message code="bankStatement.list.label" default="Bank Statements"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="bankStatementId"
                              title="${message(code: 'bankStatement.bankStatementId.label', default: 'Bank Statement Id')}"/>

            <g:sortableColumn property="issueDate"
                              title="${message(code: 'bankStatement.issueDate.label', default: 'Issue Date')}"/>

            <g:sortableColumn property="bankAccountNumber"
                              title="${message(code: 'bankStatement.bankAccountNumber.label', default: 'Bank Account Number')}"/>

            <g:sortableColumn property="balanceBefore"
                              title="${message(code: 'bankStatement.balanceBefore.label', default: 'Balance Before')}"/>

            <g:sortableColumn property="balanceAfter"
                              title="${message(code: 'bankStatement.balanceAfter.label', default: 'Balance After')}"/>


        </tr>
        </thead>
        <tbody>
        <g:each in="${bankStatementInstanceList}" status="i" var="bankStatementInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show"
                            id="${bankStatementInstance.id}">${fieldValue(bean: bankStatementInstance, field: "bankStatementId")}</g:link></td>

                <td><g:formatDate date="${bankStatementInstance.issueDate}" format="dd.MM.yyyy"/></td>

                <td>${fieldValue(bean: bankStatementInstance, field: "bankAccountNumber")}</td>

                <td>${fieldValue(bean: bankStatementInstance, field: "balanceBefore")}</td>

                <td>${fieldValue(bean: bankStatementInstance, field: "balanceAfter")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${bankStatementInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
