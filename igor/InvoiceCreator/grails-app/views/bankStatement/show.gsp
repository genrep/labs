<%@ page import="com.genrep.statements.BankStatement" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'bankStatement.label', default: 'BankStatement')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="bankStatement.list" default="List Bank Statement"/></g:link></li>
        <li><g:link class="menuButton" action="create"><g:message code="bankStatment.create" default="New Bank Statement" /></g:link></li>
    </ul>
</div>

<div id="show-bankStatement" class="content scaffold-show" role="main">
    <h1><g:message code="bankStatement.show.label" default="Bank Statement Basic Information"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list bankStatement">

        <g:if test="${bankStatementInstance?.ownerOfStatement}">
            <li class="fieldcontain">
                <span id="ownerOfStatement-label" class="property-label"><g:message
                        code="bankStatement.ownerOfStatement.label" default="Bank Account"/></span>

                <span class="property-value" aria-labelledby="ownerOfStatement-label"><g:fieldValue
                        bean="${bankStatementInstance}" field="ownerOfStatement"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.bankStatementId}">
            <li class="fieldcontain">
                <span id="bankStatementId-label" class="property-label"><g:message
                        code="bankStatement.bankStatementId.label" default="Bank Statement Id"/></span>

                <span class="property-value" aria-labelledby="bankStatementId-label"><g:fieldValue
                        bean="${bankStatementInstance}" field="bankStatementId"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.issueDate}">
            <li class="fieldcontain">
                <span id="issueDate-label" class="property-label"><g:message code="bankStatement.issueDate.label"
                                                                             default="Issue Date"/></span>

                <span class="property-value" aria-labelledby="issueDate-label"><g:formatDate
                        date="${bankStatementInstance?.issueDate}" format="dd.MM.yyyy"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.bankAccountNumber}">
            <li class="fieldcontain">
                <span id="bankAccountNumber-label" class="property-label"><g:message
                        code="bankStatement.bankAccountNumber.label" default="Bank Account Number"/></span>

                <span class="property-value" aria-labelledby="bankAccountNumber-label"><g:fieldValue
                        bean="${bankStatementInstance}" field="bankAccountNumber"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.balanceBefore}">
            <li class="fieldcontain">
                <span id="balanceBefore-label" class="property-label"><g:message
                        code="bankStatement.balanceBefore.label" default="Balance Before"/></span>

                <span class="property-value" aria-labelledby="balanceBefore-label"><g:fieldValue
                        bean="${bankStatementInstance}" field="balanceBefore"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.balanceAfter}">
            <li class="fieldcontain">
                <span id="balanceAfter-label" class="property-label"><g:message code="bankStatement.balanceAfter.label"
                                                                                default="Balance After"/></span>

                <span class="property-value" aria-labelledby="balanceAfter-label"><g:fieldValue
                        bean="${bankStatementInstance}" field="balanceAfter"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.bankStatementDocId}">
            <li class="fieldcontain">
                <span id="bankStatementDocId-label" class="property-label"><g:message
                        code="bankStatement.bankStatementDocId.label" default="Bank Statement Doc Id"/></span>

                <span class="property-value" aria-labelledby="bankStatementDocId-label"><g:fieldValue
                        bean="${bankStatementInstance}" field="bankStatementDocId"/></span>

            </li>
        </g:if>
    </ol>

    <g:form url="[resource: bankStatementInstance, action: 'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link  action="showBankStatementDetails" resource="${bankStatementInstance}"><g:message code="statements.action.parse" default="Show Details" /></g:link>
            <g:link class="edit" action="edit" resource="${bankStatementInstance}"><g:message code="default.button.edit.label"
                                                                                        default="Edit"/></g:link>
            <g:link class="edit" action="editBankStatementItems" resource="${bankStatementInstance}"><g:message code="bankStatements.editItems"
                                                                                        default="Edit Statement Items"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
