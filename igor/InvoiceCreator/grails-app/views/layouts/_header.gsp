<div id="header" xmlns="http://www.w3.org/1999/html">
<div id="loginHeader">
<sec:ifLoggedIn>
    <sec:username/>
    &nbsp;
    <g:link controller='logout'><g:message code="default.logout.label" default="Logout"/></g:link>
</sec:ifLoggedIn>
<sec:ifNotLoggedIn>
    <g:if test="${request.forwardURI!=request.getContextPath()+'/login/auth'}">
        <g:link controller='login' action='auth'><g:message code="default.login.label" default="Login"/></g:link>
    </g:if>
</sec:ifNotLoggedIn>
</div>
</br>
</div>