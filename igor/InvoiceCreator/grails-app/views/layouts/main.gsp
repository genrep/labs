<%@ page import="org.codehaus.groovy.grails.web.context.ServletContextHolder" %>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Invoice Creator"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Ubuntu+Mono|Open+Sans:400,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <asset:javascript src="application.js"/>
    <asset:stylesheet href="application.css"/>
    <asset:link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <g:layoutHead/>
</head>
<body>
<div id="invoiceLogoWrapper">
    <div id="invoiceLogo" role="banner">
        <a href="${ServletContextHolder.getServletContext().getContextPath()}">
            <img src="${assetPath(src: 'tea_icon_lavendar.png')}" alt="Genrep"/>
        </a>
        <img class="pari" src="${assetPath(src: 'header-lavender.png')}" alt="Genrep"/>
    </div>
</div>
<div id="headerWrapper">
    <g:render template="/layouts/header" />
</div>
<g:layoutBody/>
<div id="footerWrapper">
    <div class="footer" role="contentinfo">
        <g:render template="/layouts/footer"/>
    </div>
</div>
<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>
</body>
</html>
