
<%@ page import="com.genrep.client.Client; com.genrep.invoiceRepository.InvoiceArchive" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoiceArchive.label', default: 'InvoiceArchive')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
    <div class="nav" role="navigation">
        <ul>
            <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
            <li><g:link class="menuButton" controller="invoice" action="index"><g:message code="invoice.home.label" default="Invoices"/></g:link> </li>
            <li><g:link class="menuButton" action="newImport"><g:message code="invoiceArchive.button.newImport"
                                                                      default="New Import"/></g:link></li>
        </ul>
    </div>
		<div id="list-invoiceArchive" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="documentName" title="${message(code: 'invoiceArchive.documentName.label', default: 'Name')}" />
					
						<g:sortableColumn property="invoiceYear,invoiceCounter" title="${message(code: 'invoiceArchive.invoiceNumber.label', default: 'Invoice Number')}" />

                        <g:sortableColumn property="client" title="${message(code: 'invoiceArchive.client.label', default: 'Client')}"/>
					
						<g:sortableColumn property="totalSum" title="${message(code: 'invoiceArchive.totalSum.label', default: 'Total Sum')}" />

						<g:sortableColumn property="currency" title="${message(code: 'invoiceArchive.inCurrency.label', default: 'Currency')}" />

                        <g:sortableColumn property="issueDate" title="${message(code: 'invoiceArchive.issueDate.label', default: 'Date ')}" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${invoiceArchiveInstanceList}" status="i" var="invoiceArchiveInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${invoiceArchiveInstance.id}">${fieldValue(bean: invoiceArchiveInstance, field: "documentName")}</g:link></td>
					
						<td>${fieldValue(bean: invoiceArchiveInstance, field: "invoiceNumber")}</td>
					
						<td>${Client.get(invoiceArchiveInstance?.client.id)}</td>
					
						<td>${invoiceArchiveInstance?.totalSum?.decodeDecimalNumber()}</td>
					
						<td>${fieldValue(bean: invoiceArchiveInstance, field: "inCurrency")}</td>

						<td>${formatDate(date: invoiceArchiveInstance?.issueDate, format: 'dd/MM/yyyy')}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${invoiceArchiveInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
