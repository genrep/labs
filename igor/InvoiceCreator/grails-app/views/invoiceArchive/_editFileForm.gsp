<g:if test="${invoiceArchiveInstance?.documentFile}">
    <div class="fieldcontain">
        <span id="documentFile-label" class="property-label"><g:message code="invoiceArchive.documentFile.label" default="Document File" /></span>
        <span  class="property-value"><g:link controller="invoiceArchive" action="showInvoiceArchiveFile" target="_blank"
                                              id="${invoiceArchiveInstance?.id}">
            ${invoiceArchiveInstance?.documentName?:'Invoice Document'}
        </g:link>
        </span>
    </div>
</g:if>
<g:else>
    <div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'documentFile', 'error')}">
        <span class="property-label">
            <label for="documentFile">
                <g:message code="invoiceArchive.documentFile.label" default="Document File" />
            </label>
        </span>
        <span class="property-value">
            <input type="file" id="documentFile" name="documentFile" onchange="setFileName(this,'documentName')" />
        </span>
    </div>
</g:else>

<g:if test="${invoiceArchiveInstance?.documentName}">
    <div class="fieldcontain">
        <span id="documentName-label" class="property-label"><g:message code="invoiceArchive.documentName.label" default="Document Name" /></span>

        <span class="property-value" aria-labelledby="documentName-label"><g:fieldValue bean="${invoiceArchiveInstance}" field="documentName"/></span>

    </div>
</g:if>
<g:else>
    <div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'documentName', 'error')}">
        <span class="property-label">
            <label for="documentName">
                <g:message code="invoiceArchive.documentName.label" default="Document Name" />
            </label>
        </span>
        <span class="property-value">
            <g:textField class="wider-fields" name="documentName" id="documentName" value="${invoiceArchiveInstance?.documentName}"/>
        </span>
    </div>
</g:else>

<g:if test="${invoiceArchiveInstance?.documentFile}">
    <div class="itemButtons">

        <input id="deleteFile" type="button" value="Delete File" class="buttonsMine deleteButton"
               onclick="deleteInvoiceArchiveFile(${invoiceArchiveInstance.id})"/>
    </div>
</g:if>