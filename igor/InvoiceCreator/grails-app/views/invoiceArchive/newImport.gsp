<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoiceArchive.label', default: 'InvoiceArchive')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
        <asset:javascript src="invoice.js"/>
	</head>
	<body>
    <div class="nav" role="navigation">
        <ul>
            <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
            <li><g:link class="menuButton" action="index"><g:message code="invoiceArchive.button.index" default="List Of Imported Invoices"/></g:link></li>
        </ul>
    </div>
		<div id="import-invoiceArchive" class="content scaffold-create" role="main">
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${invoiceArchiveInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${invoiceArchiveInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:uploadForm action="saveImport" method="post">
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
                <fieldset class="form">
                    <g:render template="fileForm"/>
                </fieldset>

				<fieldset class="buttons">
					<g:submitButton name="saveImport" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:uploadForm>
		</div>

    <div class="includedTemplate">
        <g:include view="client/_form.gsp"/>
    </div>

	</body>
</html>
