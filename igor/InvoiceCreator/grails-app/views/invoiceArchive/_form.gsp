<%@ page import="com.genrep.invoiceRepository.Taxes; com.genrep.invoiceCreator.Currencies; com.genrep.invoiceRepository.InvoiceArchive" %>

<div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'invoiceCounter', 'error')}">
    <span class="property-label">
        <label for="invoiceCounter">
            <g:message code="invoice.invoiceCounter.label" default="Invoice Counter"/>
        </label>
    </span>
    <span class="property-value">
        <g:field type="number" name="invoiceCounter" id="invoiceCounter" onchange="generateInvoiceNumber()"
                 value="${invoiceArchiveInstance?.invoiceCounter}" required="" />
    </span>
</div>


<div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'invoiceYear', 'error')}">
    <span class="property-label">
        <label for="invoiceYear">
            <g:message code="invoice.invoiceYear.label" default="Invoice Year"/>
        </label>
    </span>
    <span class="property-value">
        <g:field type="number" name="invoiceYear" id="invoiceYear" onchange="generateInvoiceNumber()"
                 value="${invoiceArchiveInstance?.invoiceYear}" required="" />
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'invoiceNumber', 'error')}">
    <span class="property-label required">
        <label for="invoiceNumber">
            <g:message code="invoiceArchive.invoiceNumber.label" default="Invoice Number" />
        </label>
    </span>
    <span class="property-value">
        <g:textField name="invoiceNumber" required="" readonly="" value="${invoiceArchiveInstance?.invoiceNumber}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'swiftCode', 'error')}">
    <span class="property-label">
        <label for="swiftCode">
            <g:message code="invoiceArchive.swiftCode.label" default="Swift Code" />
        </label>
    </span>
    <span class="property-value">
        <g:textField name="swiftCode" value="${invoiceArchiveInstance?.swiftCode}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'client', 'error')}">
    <span class="property-label required">
        <label for="client">
            <g:message code="invoiceArchive.client.label" default="Client" />
        </label>
    </span>
    <span class="property-value">
        <g:select id="client" name="client.id" from="${com.genrep.client.Client.list()}"
                  optionKey="id" required="" value="${invoiceArchiveInstance?.client?.id}"
                  class="many-to-one" noSelection="['':'Please select client']"/>

        <input type="button" class="newDialogButton" value="+" onclick="newClientDialog()" />
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'invoiceDescription', 'error')}">
    <span id="invoiceDescription-label" class="property-label">
        <label for="invoiceDescription">
            <g:message code="invoice.invoiceDescription.label" default="Description" />
        </label>
    </span>
    <span class="property-value">
        <g:textArea name="invoiceDescription" id="invoiceDescription" cols="40" rows="5" maxlength="3000"
                    value="${invoiceArchiveInstance?.invoiceDescription}"     />
    </span>

</div>

<div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'inCurrency', 'error')}">
    <span class="property-label required">
        <label for="currency">
            <g:message code="invoiceArchive.currency.label" default="Currency" />
        </label>
    </span>
    <span class="property-value">
        <g:currencySelect name="inCurrency" value="${invoiceArchiveInstance?.inCurrency?:Currencies.MKD.currency}"
                          from="${Currencies.values()}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'totalSum', 'error')}">
    <span class="property-label required">
        <label for="totalSum">
            <g:message code="invoiceArchive.totalSum.label" default="Total Sum" />
        </label>
    </span>
    <span class="property-value">
        <g:priceAndTaxField id="totalSum" name="totalSum" required="" value="${invoiceArchiveInstance?.totalSum}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'taxFreeSum', 'error')}">
    <span class="property-label required">
        <label for="taxFreeSum">
            <g:message code="invoiceArchive.taxFreeSum.label" default="Tax Free Sum" />
        </label>
    </span>
    <span class="property-value">
        <g:priceAndTaxField id="taxFreeSum" name="taxFreeSum" required="" value="${invoiceArchiveInstance?.taxFreeSum}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'taxPriceSum', 'error')}">
    <span class="property-label required">
        <label for="taxPriceSum">
            <g:message code="invoiceArchive.taxPriceSum.label" default="Tax Price Sum" />
        </label>
    </span>
    <span class="property-value">
        <g:priceAndTaxField id="taxPriceSum" name="taxPriceSum" required="" value="${invoiceArchiveInstance?.taxPriceSum}"/>
    </span>
</div>

<div class="fieldcontain">
    <span class="property-label">
        <label for="taxes">
            <g:message code="invoiceArchive.taxes.zero" default="Tax (0%)" />
        </label>
    </span>
    <span class="property-value">
        <g:priceAndTaxField id="tax0" name="tax0" value="${invoiceArchiveInstance?.taxes?.get(Taxes.ZERO)}"/>
    </span>
</div>

<div class="fieldcontain">
    <span class="property-label">
        <label for="taxes">
            <g:message code="invoiceArchive.taxes.five" default="Tax (5%)" />
        </label>
    </span>
    <span class="property-value">
        <g:priceAndTaxField id="tax5" name="tax5" value="${invoiceArchiveInstance?.taxes?.get(Taxes.FIVE)}"/>
    </span>
</div>

<div class="fieldcontain">
    <span class="property-label">
        <label for="taxes">
            <g:message code="invoiceArchive.taxes.eighteen" default="Tax (18%)" />
        </label>
    </span>
    <span class="property-value">
        <g:priceAndTaxField id="tax18" name="tax18" value="${invoiceArchiveInstance?.taxes?.get(Taxes.EIGHTEEN)}"/>
    </span>
</div>

<!--project-->
<div class="fieldcontain">
    <span class="property-label">
        <label for="project">
            <g:message code="invoiceArchive.project.label" default="Add to Project"/>
        </label>
    </span>
    <span class="property-value">
        <g:select id="projectId" name="projectId"
                  from="${projectList}" noSelection="['':'']"
                  optionValue="title" optionKey="technicalNumber"
                  value="${invoiceArchiveInstance?.projectId}"
        />
    </span>
</div>
<!--project-->

<div class="fieldcontain ${hasErrors(bean: invoiceArchiveInstance, field: 'issueDate', 'error')}">
    <span class="property-label required">
        <label for="issueDate">
            <g:message code="invoiceArchive.issueDate.label" default="Issue Date" />
        </label>
    </span>
    <span class="property-value">
        <input type="text" name="issueDate" id="issueDate" class="date"
               value="${formatDate(date:invoiceArchiveInstance?.issueDate?:new Date(),format: 'dd.MM.yyyy')}"/>
    </span>
</div>

