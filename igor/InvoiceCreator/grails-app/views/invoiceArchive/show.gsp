
<%@ page import="com.genrep.invoiceRepository.Taxes; com.genrep.client.Client; com.genrep.invoiceRepository.InvoiceArchive" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'invoiceArchive.label', default: 'InvoiceArchive')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="invoiceArchive.button.index" default="List Of Imported Invoices"/></g:link></li>
        <li><g:link class="menuButton" action="newImport"><g:message code="invoiceArchive.button.newImport"
                                                                     default="New Import"/></g:link></li>
    </ul>
</div>
<div id="show-invoiceArchive" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list invoiceArchive">

        %{--<g:if test="${invoiceArchiveInstance?.docMongoId}">--}%
            %{--<li class="fieldcontain">--}%
                %{--<span id="docMongoId-label" class="property-label"><g:message code="invoiceArchive.docMongoId.label" default="Doc Mongo Id" /></span>--}%

                %{--<span class="property-value" aria-labelledby="docMongoId-label"><g:fieldValue bean="${invoiceArchiveInstance}" field="docMongoId"/></span>--}%

            %{--</li>--}%
        %{--</g:if>--}%

        <g:if test="${invoiceArchiveInstance?.invoiceNumber}">
            <li class="fieldcontain">
                <span id="invoiceNumber-label" class="property-label"><g:message code="invoiceArchive.invoiceNumber.label" default="Invoice Number" /></span>

                <span class="property-value" aria-labelledby="invoiceNumber-label"><g:fieldValue bean="${invoiceArchiveInstance}" field="invoiceNumber"/></span>

            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.swiftCode}">
            <li class="fieldcontain">
                <span id="swiftCode-label" class="property-label"><g:message code="invoiceArchive.swiftCode.label" default="Swift Code" /></span>

                <span class="property-value" aria-labelledby="swiftCode-label"><g:fieldValue bean="${invoiceArchiveInstance}" field="swiftCode"/></span>

            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.client?.id}">
            <li class="fieldcontain">
                <span id="client-label" class="property-label"><g:message code="invoiceArchive.client.label" default="Client" /></span>

                <span class="property-value" aria-labelledby="client-label">
                    <g:link controller="client" action="show" id="${invoiceArchiveInstance?.client?.id}">
                        ${Client.get(invoiceArchiveInstance?.client.id).toString()}
                    </g:link>
                </span>

            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.invoiceDescription}">
            <li class="fieldcontain">
                <span id="invoiceDescription-label" class="property-label">
                    <g:message code="invoice.invoiceDescription.label"
                               default="Description"/></span>
                <span class="property-value" aria-labelledby="invoiceDescription-label">
                    ${raw(invoiceArchiveInstance.invoiceDescription)}
                </span>
            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.totalSum}">
            <li class="fieldcontain">
                <span id="totalSum-label" class="property-label"><g:message code="invoiceArchive.totalSum.label" default="Total Sum" /></span>

                <span class="property-value" aria-labelledby="totalSum-label">
                    ${invoiceArchiveInstance?.totalSum?.decodeDecimalNumber()}
                </span>

            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.taxFreeSum}">
            <li class="fieldcontain">
                <span id="taxFreeSum-label" class="property-label"><g:message code="invoiceArchive.taxFreeSum.label" default="Tax Free Sum" /></span>

                <span class="property-value" aria-labelledby="taxFreeSum-label">
                    ${invoiceArchiveInstance?.taxFreeSum?.decodeDecimalNumber()}
                </span>

            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.taxPriceSum}">
            <li class="fieldcontain">
                <span id="taxPriceSum-label" class="property-label"><g:message code="invoiceArchive.taxPriceSum.label" default="Tax Price Sum" /></span>

                <span class="property-value" aria-labelledby="taxPriceSum-label">
                    ${invoiceArchiveInstance?.taxPriceSum?.decodeDecimalNumber()}
                </span>

            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.taxes}">
            <g:if test="${invoiceArchiveInstance?.taxes?.containsKey(Taxes.ZERO)}">
                <li class="fieldcontain">
                    <span  class="property-label"><g:message code="invoiceArchive.taxes.zero" default="Tax (0%)" /></span>

                    <span class="property-value" >${invoiceArchiveInstance?.taxes?.get(Taxes.ZERO).decodeDecimalNumber()}</span>

                </li>
            </g:if>

            <g:if test="${invoiceArchiveInstance?.taxes.containsKey(Taxes.FIVE)}">
                <li class="fieldcontain">
                    <span  class="property-label"><g:message code="invoiceArchive.taxes.five" default="Tax (5%)" /></span>

                    <span class="property-value" >${invoiceArchiveInstance?.taxes?.get(Taxes.FIVE).decodeDecimalNumber()}</span>

                </li>
            </g:if>

            <g:if test="${invoiceArchiveInstance?.taxes.containsKey(Taxes.EIGHTEEN)}">
                <li class="fieldcontain">
                    <span  class="property-label"><g:message code="invoiceArchive.taxes.eigtheen" default="Tax (18%)" /></span>

                    <span class="property-value" >${invoiceArchiveInstance?.taxes?.get(Taxes.EIGHTEEN).decodeDecimalNumber()}</span>

                </li>
            </g:if>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.inCurrency}">
            <li class="fieldcontain">
                <span id="currency-label" class="property-label"><g:message code="invoiceArchive.currency.label" default="Currency" /></span>

                <span class="property-value" aria-labelledby="currency-label"><g:fieldValue bean="${invoiceArchiveInstance}" field="inCurrency"/></span>

            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.exchangeRate}">
            <li class="fieldcontain">
                <span id="exchangeRate-label" class="property-label"><g:message code="invoiceArchive.exchangeRate.label" default="Exchange Rate" /></span>

                <span class="property-value" aria-labelledby="exchangeRate-label"><g:link controller="exchangeRate" action="show" id="${invoiceArchiveInstance?.exchangeRate?.id}">${invoiceArchiveInstance?.exchangeRate?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.documentFile}">
            <li class="fieldcontain">
                <span id="documentFile-label" class="property-label"><g:message code="invoiceArchive.documentFile.label" default="Document File" /></span>
                <span  class="property-value"><g:link controller="invoiceArchive" action="showInvoiceArchiveFile" target="_blank"
                                                      id="${invoiceArchiveInstance?.id}">
                    ${invoiceArchiveInstance?.documentName?:'Invoice Document'}
                </g:link>
                </span>
            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.documentName}">
            <li class="fieldcontain">
                <span id="documentName-label" class="property-label"><g:message code="invoiceArchive.documentName.label" default="Document Name" /></span>

                <span class="property-value" aria-labelledby="documentName-label"><g:fieldValue bean="${invoiceArchiveInstance}" field="documentName"/></span>

            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.projectId}">
            <li class="fieldcontain">
                <span id="project-label" class="property-label">
                    <g:message code="invoiceArchive.project.label" default="Project"/>
                </span>
                <span class="property-value">
                    <g:set var="project" value="${com.genrep.project.Project.findWhere(technicalNumber:invoiceArchiveInstance?.projectId)}"/>
                    <g:link controller="project" action="show" id="${project?.id}">
                        ${project?.title}
                    </g:link>
                </span>
            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.issueDate}">
            <li class="fieldcontain">
                <span id="issueDate-label" class="property-label"><g:message code="invoiceArchive.issueDate.label" default="Issue Date" /></span>

                <span class="property-value" aria-labelledby="issueDate-label">
                    <g:formatDate format="dd.MM.yyyy" date="${invoiceArchiveInstance?.issueDate}" />
                </span>
            </li>
        </g:if>

        <g:if test="${invoiceArchiveInstance?.dateCreated}">
            <li class="fieldcontain">
                <span id="dateCreated-label" class="property-label"><g:message code="invoiceArchive.dateCreated.label" default="Date Created" /></span>

                <span class="property-value" aria-labelledby="dateCreated-label">
                    <g:formatDate format="dd.MM.yyyy" date="${invoiceArchiveInstance?.dateCreated}" />
                </span>
            </li>
        </g:if>

    </ol>
    <g:form url="[resource:invoiceArchiveInstance, action:'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${invoiceArchiveInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
