<%@ page import="com.genrep.account.BankAccount" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'bankAccount.label', default: 'BankAccount')}" />
    <title><g:message code="default.edit.label" args="[entityName]" /></title>
    <asset:javascript src="account.js"/>
</head>
<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="bankAccount.list" default="List Bank Accounts" /></g:link></li>
        <li><g:link class="menuButton" action="create"><g:message code="bankAccount.new" default="New Bank Account" /></g:link></li>
    </ul>
</div>
<div id="edit-bankAccount" class="content scaffold-edit" role="main">
    <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${bankAccountInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${bankAccountInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form url="[resource:bankAccountInstance, action:'update']" method="PUT" >
        <g:hiddenField name="version" value="${bankAccountInstance?.version}" />
        <fieldset class="form">
            <g:render template="form"/>
        </fieldset>
        <fieldset class="buttons">
            <g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
