
<%@ page import="com.genrep.account.BankAccount" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'bankAccount.label', default: 'BankAccount')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="menuButton" action="create"><g:message code="bankAccount.create" default="New Bank Account" /></g:link></li>
			</ul>
		</div>
		<div id="list-bankAccount" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="entityName" title="${message(code: 'bankAccount.entityName.label', default: 'Entity Name')}" />
					
						<g:sortableColumn property="bankName" title="${message(code: 'bankAccount.bankName.label', default: 'Bank Name')}" />
					
						<g:sortableColumn property="bankAccountNumber" title="${message(code: 'bankAccount.bankAccountNumber.label', default: 'Bank Account Number')}" />
					
						<g:sortableColumn property="balance" title="${message(code: 'bankAccount.balance.label', default: 'Balance')}" />

                        <g:sortableColumn property="owner" title="${message(code: 'bankAccount.owner.label', default: 'Is Owner')}" />
					</tr>
				</thead>
				<tbody>
				<g:each in="${bankAccountInstanceList}" status="i" var="bankAccountInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${bankAccountInstance.id}">${fieldValue(bean: bankAccountInstance, field: "entityName")}</g:link></td>
					
						<td>${fieldValue(bean: bankAccountInstance, field: "bankName")}</td>
					
						<td>${fieldValue(bean: bankAccountInstance, field: "bankAccountNumber")}</td>
					
						<td>${fieldValue(bean: bankAccountInstance, field: "balance")}</td>

						<td>${fieldValue(bean: bankAccountInstance, field: "owner")}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${bankAccountInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
