
<%@ page import="com.genrep.account.BankAccount" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'bankAccount.label', default: 'BankAccount')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<div class="nav" role="navigation">
	<ul>
		<li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
		<li><g:link class="menuButton" action="index"><g:message code="bankAccount.list" default="List Bank Accounts" /></g:link></li>
		<li><g:link class="menuButton" action="create"><g:message code="bankAccount.new" default="New Bank Account" /></g:link></li>
	</ul>
</div>
<div id="show-bankAccount" class="content scaffold-show" role="main">
	<h1><g:message code="default.show.label" args="[entityName]" /></h1>
	<g:if test="${flash.message}">
		<div class="message" role="status">${flash.message}</div>
	</g:if>
	<ol class="property-list bankAccount">

		<g:if test="${bankAccountInstance?.entityName}">
			<li class="fieldcontain">
				<span id="entityName-label" class="property-label"><g:message code="bankAccount.entityName.label" default="Entity Name" /></span>

				<span class="property-value" aria-labelledby="entityName-label"><g:fieldValue bean="${bankAccountInstance}" field="entityName"/></span>

			</li>
		</g:if>

		<g:if test="${bankAccountInstance?.bankName}">
			<li class="fieldcontain">
				<span id="bankName-label" class="property-label"><g:message code="bankAccount.bankName.label" default="Bank Name" /></span>

				<span class="property-value" aria-labelledby="bankName-label"><g:fieldValue bean="${bankAccountInstance}" field="bankName"/></span>

			</li>
		</g:if>

		<g:if test="${bankAccountInstance?.bankAccountNumber}">
			<li class="fieldcontain">
				<span id="bankAccountNumber-label" class="property-label"><g:message code="bankAccount.bankAccountNumber.label" default="Bank Account Number" /></span>

				<span class="property-value" aria-labelledby="bankAccountNumber-label"><g:fieldValue bean="${bankAccountInstance}" field="bankAccountNumber"/></span>

			</li>
		</g:if>

		<g:if test="${bankAccountInstance?.taxNumber}">
			<li class="fieldcontain">
				<span id="taxNumber-label" class="property-label"><g:message code="bankAccount.taxNumber.label" default="Tax Number" /></span>

				<span class="property-value" aria-labelledby="taxNumber-label"><g:fieldValue bean="${bankAccountInstance}" field="taxNumber"/></span>

			</li>
		</g:if>


		<g:if test="${bankAccountInstance?.balance}">
			<li class="fieldcontain">
				<span id="balance-label" class="property-label"><g:message code="bankAccount.balance.label" default="Balance" /></span>

				<span class="property-value" aria-labelledby="balance-label"><g:fieldValue bean="${bankAccountInstance}" field="balance"/></span>

			</li>
		</g:if>

		<g:if test="${bankAccountInstance?.printView}">
			<li class="fieldcontain">
				<span id="printView-label" class="property-label"><g:message code="bankAccount.printView.label" default="Print View" /></span>

				<span class="property-value" aria-labelledby="bankAccountNumber-label">
					${raw(bankAccountInstance.printView)}
				</span>

			</li>
		</g:if>

	</ol>
	<g:form url="[resource:bankAccountInstance, action:'delete']" method="DELETE">
		<fieldset class="buttons">
			<g:link class="edit" action="edit" resource="${bankAccountInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
			<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
		</fieldset>
	</g:form>
</div>
</body>
</html>
