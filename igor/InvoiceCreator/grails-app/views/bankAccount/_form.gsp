<%@ page import="com.genrep.account.BankAccount" %>
<div id="bankAccount-form" title="Add New Bank Account">

    <g:hasErrors bean="${bankAccountInstance}">
        <ul id="erol" class="errors" role="alert">
            <g:eachError bean="${bankAccountInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>>
                    <g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

    <div class="fieldcontain ${hasErrors(bean: bankAccountInstance, field: 'entityName', 'error')}">
        <span class="property-label required">
            <label for="entityName">
                <g:message code="bankAccount.entityName.label" default="Entity Name" />
            </label>
        </span>
        <span class="property-value">
            <g:textField name="entityName" class="wider-fields" required="" value="${bankAccountInstance?.entityName}"/>
        </span>

    </div>

    <div class="fieldcontain ${hasErrors(bean: bankAccountInstance, field: 'bankName', 'error')}">
        <span class="property-label">
            <label for="bankName">
                <g:message code="bankAccount.bankName.label" default="Bank Name" />
            </label>
        </span>
        <span class="property-value">
            <g:textField name="bankName" class="wider-fields" value="${bankAccountInstance?.bankName}"/>
        </span>

    </div>

    <div class="fieldcontain ${hasErrors(bean: bankAccountInstance, field: 'bankAccountNumber', 'error')}">
        <span class="property-label required">
            <label for="bankAccountNumber">
                <g:message code="bankAccount.bankAccountNumber.label" default="Bank Account Number" />
            </label>
        </span>
        <span class="property-value">
            <g:textField name="bankAccountNumber" class="wider-fields" required="" value="${bankAccountInstance?.bankAccountNumber}"/>
        </span>

    </div>

    <div class="fieldcontain ${hasErrors(bean: bankAccountInstance, field: 'taxNumber', 'error')}">
        <span class="property-label">
            <label for="taxNumber">
                <g:message code="bankAccount.taxNumber.label" default="Tax Number" />
            </label>
        </span>
        <span class="property-value">
            <g:textField name="taxNumber" class="wider-fields" value="${bankAccountInstance?.taxNumber}"/>
        </span>

    </div>

    <div class="fieldcontain ${hasErrors(bean: bankAccountInstance, field: 'balance', 'error')} ">
        <span class="property-label">
            <label for="balance">
                <g:message code="bankAccount.balance.label" default="Balance" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="balance" name="balance" value="${bankAccountInstance?.balance}"/>
        </span>

    </div>

    <div class="fieldcontain ${hasErrors(bean: bankAccountInstance, field: 'printView', 'error')}">
        <span class="property-label">
            <label for="printView">
                <g:message code="bankAccount.printView.label" default="Print View" />
            </label>
        </span>
        <span class="property-value">
            <g:textArea name="printView" id="printView" cols="40" rows="5" maxlength="500"
                        value="${bankAccountInstance?.printView}"/>
        </span>

    </div>
</div>
