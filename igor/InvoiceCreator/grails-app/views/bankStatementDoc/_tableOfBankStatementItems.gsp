<table>
    <thead>
    <tr>

        %{--<th><g:message code="statements.item.orderNumber" default="No."/></th>--}%
        <th><g:message code="statements.item.publicEntity" default="Public Entity"/></th>
        <th><g:message code="statements.item.publicEntityBankAccountNumber" default="Bank Acc. No."/></th>
        <th><g:message code="statements.item.transferSum" default="Transfer Sum"/></th>
        <th><g:message code="statements.item.transcationCode" default="Trans. Code"/></th>
        <th><g:message code="statements.item.shortDescription" default="Description"/></th>
        <th><g:message code="statements.item.borrowingCode" default="Borrowal"/></th>
        <th colspan="2"><g:message code="statements.item.approvalCode" default="Approval"/></th>

    </tr>
    </thead>
    <tbody>
    <g:each in="${bankStatementInstance?.items?.sort{it.orderNumber}}" status="i" var="bankStatementItem">
        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

            %{--<td>${bankStatementItem?.orderNumber?:i}</td>--}%
            <td>${bankStatementItem.publicEntity}</td>
            <td>${bankStatementItem.publicEntityBankAccountNumber}</td>
            <td>${bankStatementItem.transferSum.decodeDecimalNumber()}</td>
            <td>${bankStatementItem.transactionCode}</td>
            <td>${bankStatementItem.shortDescription}</td>
            <td>${bankStatementItem.borrowingCode}</td>
            <td>${bankStatementItem.approvalCode}</td>
            <td><g:if test="${form=='editBankStatementItems'}">
                <input id="removeItemButton" type="button" value="Remove Item"
                       onclick="removeBankStatementItem(${bankStatementItem.id},'${form}')"/>
            </g:if></td>
        </tr>
    </g:each>
    </tbody>
</table>