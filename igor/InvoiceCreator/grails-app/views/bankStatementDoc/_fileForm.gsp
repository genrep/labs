<div id="uploadForm">
    <div class="fieldcontain">
        <span class="property-label required">
            <label for="statementDoc">
                <g:message code="bankStatementDoc.documentFile.label" default="Statement Doc" />
            </label>
        </span>
        <span class="property-value">
            <input type="file" id="documentFile" name="documentFile" onchange="setFileName(this,'documentName')" />
        </span>

    </div>

    <div class="fieldcontain">
        <span class="property-label">
            <label for="documentName">
                <g:message code="bankStatementDocInstance.documentName.label" default="Document Name" />
            </label>
        </span>
        <span class="property-value">
            <g:textField class="wider-fields" name="documentName" id="documentName"/>
        </span>
    </div>

    <div class="fieldcontain">
        <span class="property-label">
            <label for="issueDate">
                <g:message code="bankStatementDoc.issueDate.label" default="Issue Date" />
            </label>
        </span>
        <span class="property-value">
            <input type="text" name="issueDate" id="issueDate" class="date"/>
        </span>
    </div>

</div>

<div class="fieldcontain">
    <span class="property-value">
        <input type='button' class="buttonsMine" name='submit' value='Add Statement' onclick="uploadStatementFile()" />
    </span>
</div>

<div class="fieldcontain">
    <span class="property-value">
        <div id="progressBar">
            <div>
            </div>
        </div>
    </span>
</div>

<div id="uploadedStatements" class="document-file-div">
    <table id="statementsTable" class="statementsTable">
        <tbody></tbody>
    </table>
</div>

