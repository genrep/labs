
<%@ page import="com.genrep.statements.BankStatementDoc" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'bankStatementDoc.label', default: 'BankStatementDoc')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
        <asset:javascript src="statement.js"/>
	</head>
	<body>

		<div class="nav" role="navigation">
			<ul>
				<li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><a class="menuButton" href="${createLink(uri: '/statements')}"><g:message code="bankStatement.home.label" default="Statements"/></a></li>
				<li><g:link class="menuButton" action="create"><g:message code="bankStatmentDoc.new" default="Import Bank Statement" /></g:link></li>
				<li><g:link class="menuButton" action="multiImport"><g:message code="bankStatmentDoc.multiImport" default="Import Multi Statements" /></g:link></li>
			</ul>
		</div>
		<div id="list-bankStatementDoc" class="content scaffold-list" role="main">
			<h1><g:message code="bankStatementDoc.list.label" default="Imported Documents Of Bank Statements" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>

						<g:sortableColumn property="statementDoc" title="${message(code: 'bankStatementDoc.documentFile.label', default: 'Statement Doc')}" />
					
						<g:sortableColumn property="isTransferred" title="${message(code: 'bankStatementDoc.isTransferred.label', default: 'Is Transferred')}" />
					
						<g:sortableColumn property="issueDate" title="${message(code: 'bankStatementDoc.issueDate.label', default: 'Issue Date')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'bankStatementDoc.dateCreated.label', default: 'Date Created')}" />

                        <th colspan="2"><g:message code="statements.action" default="Action"/></th>
					</tr>
				</thead>
				<tbody>
				<g:each in="${bankStatementDocInstanceList}" status="i" var="bankStatementDocInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">


                        <g:if test="${bankStatementDocInstance.mimeType!=null}">
                        <td><g:link class="doc-${bankStatementDocInstance.mimeType.split('/')[1]}"
                                controller="worker" action="readFileFromMongoDb" target="_blank"
                                params="[docMongoId: bankStatementDocInstance.uuid, domain: 'BankStatementDoc']">
                            ${bankStatementDocInstance?.documentName}
                        </g:link></td>
                        </g:if><g:else>
                        <td><g:link
                                    controller="worker" action="readFileFromMongoDb" target="_blank"
                                    params="[docMongoId: bankStatementDocInstance.uuid, domain: 'BankStatementDoc']">
                            ${bankStatementDocInstance?.documentName}
                        </g:link></td>
                        </g:else>

						<td><g:formatBoolean boolean="${bankStatementDocInstance.isTransferred}" /></td>
					
						<td><g:formatDate date="${bankStatementDocInstance.issueDate}" format="dd.MM.yyyy" /></td>
					
						<td><g:formatDate date="${bankStatementDocInstance.dateCreated}" format="dd.MM.yyyy" /></td>

						<td><g:link action="showBankStatementDetails" id="${bankStatementDocInstance.id}" ><g:message code="statements.action.parse" default="Show Details"/>
                        </g:link></td>

                        <td><g:form url="[resource:bankStatementDocInstance, action:'delete']" method="DELETE">
                            <fieldset class="buttons">
                                <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                            </fieldset>
                        </g:form></td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${bankStatementDocInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
