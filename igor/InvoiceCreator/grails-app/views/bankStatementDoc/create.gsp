<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'bankStatementDoc.label', default: 'BankStatementDoc')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
        <asset:javascript src="statement.js"/>
	</head>
	<body>
		<a href="#create-bankStatementDoc" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
                <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="menuButton" action="index"><g:message code="bankStatmentDoc.list" default="List Bank Statements" /></g:link></li>
			</ul>
		</div>
		<div id="create-bankStatementDoc" class="content scaffold-create" role="main">
			<h1><g:message code="bankStatementDoc.create.label" default="Enter New Bank Statement" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${bankStatementDocInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${bankStatementDocInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form url="[resource:bankStatementDocInstance, action:'save']"  enctype="multipart/form-data">
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
