<%--
  Created by IntelliJ IDEA.
  User: igor
  Date: 9/18/14
  Time: 12:23 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>


<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'bankStatementDoc.label', default: 'BankStatementDoc')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
    <asset:javascript src="statement.js"/>
</head>
<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="bankStatmentDoc.list" default="List Bank Statements" /></g:link></li>
        <li><g:link class="menuButton" action="create"><g:message code="bankStatmentDoc.new" default="New Bank Statement" /></g:link></li>
    </ul>
</div>
<div id="show-bankStatement" class="content scaffold-show" role="main">

    <h1><g:message code="bankStatement.showDetails.label" default="Bank Statement Detailed Information" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <g:hasErrors bean="${bankStatementInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${bankStatementInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

    <ol class="property-list bankStatement">

        <g:if test="${bankStatementInstance?.bankStatementId}">
            <li class="fieldcontain">
                <span id="bankStatementId-label" class="property-label"><g:message code="bankStatement.bankStatementId.label" default="Bank Statement ID" /></span>

                <span class="property-value" ><g:fieldValue bean="${bankStatementInstance}" field="bankStatementId"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.issueDate}">
            <li class="fieldcontain">
                <span id="issueDate-label" class="property-label"><g:message code="bankStatement.issueDate.label" default="Issue Date" /></span>

                <span class="property-value"><g:formatDate date="${bankStatementInstance?.issueDate}" format="dd.MM.yyyy"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.bankAccountNumber}">
            <li class="fieldcontain">
                <span id="bankAccountNumber-label" class="property-label"><g:message code="bankStatement.bankAccountNumber.label" default="Account Number" /></span>

                <span class="property-value" ><g:fieldValue bean="${bankStatementInstance}" field="bankAccountNumber"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.balanceBefore}">
            <li class="fieldcontain">
                <span id="balanceBefore-label" class="property-label"><g:message code="bankStatement.balanceBefore.label" default="Balance Before" /></span>

                <span class="property-value" >${bankStatementInstance.balanceBefore.decodeDecimalNumber()}</span>

            </li>
        </g:if>

        <g:if test="${bankStatementInstance?.balanceAfter}">
            <li class="fieldcontain">
                <span id="balanceAfter-label" class="property-label"><g:message code="bankStatement.balanceAfter.label" default="Balance After" /></span>

                <span class="property-value" >${bankStatementInstance.balanceAfter.decodeDecimalNumber()}</span>

            </li>
        </g:if>

        <g:render template="tableOfBankStatementItems" />

        <li class="fieldcontain">
            <span id="total-label" class="property-label"><g:message code="bankStatement.total.label" default="Total" /></span>
            <span class="property-value" >${total?.decodeDecimalNumber()}</span>

        </li>
    </ol>

    <g:if test="${saved}">
        <g:form url="[resource:bankStatementDocInstance, action:'delete']" method="DELETE">
            <fieldset class="buttons">
                <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
            </fieldset>
        </g:form>
    </g:if>
    <g:else>
        <g:form method="post" action="saveBankStatement" onsubmit="return checkIfBankAccountExists('${bankStatementInstance.bankAccountNumber}')">
            <fieldset class="buttons">
                <g:submitButton name="save" class="save">
                    <g:message code="default.button.save.label" default="Save" />
                </g:submitButton>
            </fieldset>
        </g:form>
    </g:else>

    <div class="includedTemplate">
            <g:include view="bankAccount/_form.gsp"/>
    </div>

</div>
</body>
</html>
