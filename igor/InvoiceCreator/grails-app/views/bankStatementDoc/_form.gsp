<%@ page import="com.genrep.statements.BankStatementDoc" %>


<div class="fieldcontain ${hasErrors(bean: bankStatementDocInstance, field: 'documentFile', 'error')}">
    <span class="property-label required">
        <label for="statementDoc">
            <g:message code="bankStatementDoc.statementDoc.label" default="Statement Doc" />
        </label>
    </span>
    <span class="property-value">
        <input type="file" id="documentFile" name="documentFile" />
    </span>

</div>

<div class="fieldcontain ${hasErrors(bean: bankStatementDocInstance, field: 'documentName', 'error')}">
    <span class="property-label">
        <label for="documentName">
            <g:message code="bankStatementDocInstance.documentName.label" default="Document Name" />
        </label>
    </span>
    <span class="property-value">
        <g:textField class="wider-fields" name="documentName" id="documentName" value="${bankStatementDocInstance?.documentName}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: bankStatementDocInstance, field: 'issueDate', 'error')} ">
    <span class="property-label">
        <label for="issueDate">
            <g:message code="bankStatementDoc.issueDate.label" default="Issue Date" />
        </label>
    </span>
    <span class="property-value">
        <input type="text" name="issueDate" id="issueDate" class="date"
               value="${formatDate(date:bankStatementDocInstance.issueDate,format: 'dd.MM.yyyy')}"/>
    </span>
</div>

