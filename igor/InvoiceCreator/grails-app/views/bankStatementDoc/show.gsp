
<%@ page import="com.genrep.statements.BankStatementDoc" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'bankStatementDoc.label', default: 'BankStatementDoc')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
    <asset:javascript src="statement.js"/>
</head>
<body>
<a href="#show-bankStatementDoc" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="bankStatmentDoc.list" default="List Bank Statements" /></g:link></li>
        <li><g:link class="menuButton" action="create"><g:message code="bankStatmentDoc.new" default="New Bank Statement" /></g:link></li>
    </ul>
</div>
<div id="show-bankStatementDoc" class="content scaffold-show" role="main">
    <h1><g:message code="bankStatement.show.label" default="Bank Statement Basic Information" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list bankStatementDoc">

        <g:if test="${bankStatementDocInstance?.uuid}">
            <li class="fieldcontain">
                <span id="uuid-label" class="property-label"><g:message code="bankStatementDoc.uuid.label" default="Uuid" /></span>

                <span class="property-value" aria-labelledby="uuid-label"><g:fieldValue bean="${bankStatementDocInstance}" field="uuid"/></span>

            </li>
        </g:if>

        <g:if test="${bankStatementDocInstance?.documentFile}">
            <li class="fieldcontain">
                <span id="documentFile-label" class="property-label"><g:message code="bankStatementDoc.documentFile.label" default="Statement Doc" /></span>
                <span class="property-value">
                    <g:link class="doc-${bankStatementDocInstance.mimeType.split('/')[1]}"
                            controller="worker" action="readFileFromMongoDb" target="_blank"
                            params="[docMongoId: bankStatementDocInstance.uuid, domain: 'BankStatementDoc']">
                        ${bankStatementDocInstance?.documentName}
                    </g:link>
                </span>
            </li>
        </g:if>

        <g:if test="${bankStatementDocInstance?.isTransferred}">
            <li class="fieldcontain">
                <span id="isTransferred-label" class="property-label"><g:message code="bankStatementDoc.isTransferred.label" default="Is Transferred" /></span>

                <span class="property-value" aria-labelledby="isTransferred-label"><g:formatBoolean boolean="${bankStatementDocInstance?.isTransferred}" /></span>

            </li>
        </g:if>

        <g:if test="${bankStatementDocInstance?.issueDate}">
            <li class="fieldcontain">
                <span id="issueDate-label" class="property-label"><g:message code="bankStatementDoc.issueDate.label" default="Issue Date" /></span>

                <span class="property-value" aria-labelledby="issueDate-label">
                    <g:formatDate date="${bankStatementDocInstance?.issueDate}" format="dd.MM.yyyy" /></span>

            </li>
        </g:if>

        <g:if test="${bankStatementDocInstance?.dateCreated}">
            <li class="fieldcontain">
                <span id="dateCreated-label" class="property-label"><g:message code="bankStatementDoc.dateCreated.label" default="Date Created" /></span>

                <span class="property-value" aria-labelledby="dateCreated-label">
                    <g:formatDate date="${bankStatementDocInstance?.dateCreated}" format="dd.MM.yyyy" /></span>

            </li>
        </g:if>

    </ol>
    <g:form url="[resource:bankStatementDocInstance, action:'delete']" method="DELETE">
        <fieldset class="buttons">
            %{--<g:link class="edit" action="edit" resource="${bankStatementDocInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>--}%
            <g:link  action="showBankStatementDetails" resource="${bankStatementDocInstance}"><g:message code="statements.action.parse" default="Show Details" /></g:link>
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
