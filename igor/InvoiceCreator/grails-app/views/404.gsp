<%--
  Created by IntelliJ IDEA.
  User: igor
  Date: 9/17/14
  Time: 12:55 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title>Sorry, not found</title>

</head>

<body>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
    </ul>
</div>

<img class="error-404" src="${assetPath(src: '404.jpg')}" alt="404"/>

</body>
</html>
