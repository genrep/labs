<%@ page import="com.genrep.invoiceCreator.ExchangeRate" %>
<%@ page import="com.genrep.invoiceCreator.Currencies" %>



<div class="fieldcontain ${hasErrors(bean: exchangeRateInstance, field: 'date', 'error')} required">
	<label for="date">
		<g:message code="exchangeRate.date.label" default="Date" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="date" precision="day"  value="${exchangeRateInstance?.date}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: exchangeRateInstance, field: 'baseCurrency', 'error')} required">
	<label for="baseCurrency">
		<g:message code="exchangeRate.baseCurrency.label" default="Base Currency" />
		<span class="required-indicator">*</span>
	</label>
	<g:currencySelect name="baseCurrency" value="${exchangeRateInstance?.baseCurrency}"  from="${Currencies.values()}" />

</div>

<div class="fieldcontain ${hasErrors(bean: exchangeRateInstance, field: 'rate', 'error')} required">
	<label for="rate">
		<g:message code="exchangeRate.rate.label" default="Rate" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="rate" value="${fieldValue(bean: exchangeRateInstance, field: 'rate')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: exchangeRateInstance, field: 'toCurrency', 'error')} required">
	<label for="toCurrency">
		<g:message code="exchangeRate.toCurrency.label" default="To Currency" />
		<span class="required-indicator">*</span>
	</label>
	<g:currencySelect name="toCurrency" value="${exchangeRateInstance?.toCurrency}" from="${Currencies.values()}"  />

</div>

