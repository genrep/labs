<%@ page import="com.genrep.invoiceCreator.ExchangeRate" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'exchangeRate.label', default: 'ExchangeRate')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-exchangeRate" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                   default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-exchangeRate" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="date" title="${message(code: 'exchangeRate.date.label', default: 'Date')}"/>

            <g:sortableColumn property="baseCurrency"
                              title="${message(code: 'exchangeRate.baseCurrency.label', default: 'Base Currency')}"/>

            <g:sortableColumn property="toCurrency"
                              title="${message(code: 'exchangeRate.toCurrency.label', default: 'To Currency')}"/>

            <g:sortableColumn property="rate" title="${message(code: 'exchangeRate.rate.label', default: 'Rate')}"/>

        </tr>
        </thead>
        <tbody>
        <g:each in="${exchangeRateInstanceList}" status="i" var="exchangeRateInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show" id="${exchangeRateInstance.id}"><g:formatDate
                        date="${exchangeRateInstance?.date}" format="dd.MM.yyyy"/></g:link></td>

                <td>${fieldValue(bean: exchangeRateInstance, field: "baseCurrency")}</td>

                <td>${fieldValue(bean: exchangeRateInstance, field: "toCurrency")}</td>

                <td><g:formatNumber number="${exchangeRateInstance?.rate}" maxFractionDigits="4"/></td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${exchangeRateInstanceCount ?: 0}"/>
    </div>
</div>
</body>
</html>
