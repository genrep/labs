<%@ page import="com.genrep.invoiceCreator.ExchangeRate" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'exchangeRate.label', default: 'ExchangeRate')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-exchangeRate" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                                   default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-exchangeRate" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list exchangeRate">

        <g:if test="${exchangeRateInstance?.date}">
            <li class="fieldcontain">
                <span id="date-label" class="property-label"><g:message code="exchangeRate.date.label"
                                                                        default="Date"/></span>

                <span class="property-value" aria-labelledby="date-label"><g:formatDate
                        date="${exchangeRateInstance?.date}"/></span>

            </li>
        </g:if>

        <g:if test="${exchangeRateInstance?.baseCurrency}">
            <li class="fieldcontain">
                <span id="baseCurrency-label" class="property-label"><g:message code="exchangeRate.baseCurrency.label"
                                                                                default="Base Currency"/></span>

                <span class="property-value" aria-labelledby="baseCurrency-label"><g:fieldValue
                        bean="${exchangeRateInstance}" field="baseCurrency"/></span>

            </li>
        </g:if>

        <g:if test="${exchangeRateInstance?.rate}">
            <li class="fieldcontain">
                <span id="rate-label" class="property-label"><g:message code="exchangeRate.rate.label"
                                                                        default="Rate"/></span>

                <span class="property-value" aria-labelledby="rate-label">
                    <g:formatNumber number="${exchangeRateInstance?.rate}" maxFractionDigits="4"/>
                </span>

            </li>
        </g:if>

        <g:if test="${exchangeRateInstance?.toCurrency}">
            <li class="fieldcontain">
                <span id="toCurrency-label" class="property-label"><g:message code="exchangeRate.toCurrency.label"
                                                                              default="To Currency"/></span>

                <span class="property-value" aria-labelledby="toCurrency-label"><g:fieldValue
                        bean="${exchangeRateInstance}" field="toCurrency"/></span>

            </li>
        </g:if>

    </ol>
    <g:form url="[resource: exchangeRateInstance, action: 'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${exchangeRateInstance}"><g:message
                    code="default.button.edit.label" default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
