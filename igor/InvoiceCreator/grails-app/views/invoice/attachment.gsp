<%--
  Created by IntelliJ IDEA.
  User: igor
  Date: 16.4.15
  Time: 16:00
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}"/>
    <title><g:message code="invoice.attachemnt.label" default="Invoice Attachments"/></title>
</head>

<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="menuButton" action="create"><g:message code="default.new.label"
                                                                  args="[entityName]"/></g:link></li>
    </ul>
</div>

<div class="content">
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
</div>

<div id="new-attachment" class="content">
    <h1><g:message code="invoice.attachment.add" default="Add Invoice Attachment"/></h1>

    <g:uploadForm controller="invoiceAttachment" action="save">
        <g:hiddenField name="invoice.id" value="${invoiceInstance?.id}"/>
        <div class="fieldcontain ${hasErrors(bean: invoiceAttachmentInstance, field: 'name', 'error')}">
            <span class="property-label required">
                <label for="name">
                    <g:message code="invoiceAttachment.name.label" default="Name" />
                </label>
            </span>
            <span class="property-value">
                <g:textField name="name" required="" value="${invoiceAttachmentInstance?.name}"/>
            </span>
        </div>

        <div class="fieldcontain ${hasErrors(bean: invoiceAttachmentInstance, field: 'attachmentType', 'error')}">
            <span class="property-label required">
                <label for="attachmentType">
                    <g:message code="invoiceAttachment.attachmentType.label" default="Type" />
                </label>
            </span>
            <span class="property-value">
                %{--<g:textField name="attachmentType" required="" value="${invoiceAttachmentInstance?.attachmentType}"/>--}%
                <g:select id="attachmentType" name="attachmentType" from="${documentTypeInstanceList}"
                          required="" optionValue="name" optionKey="name"
                          class="many-to-one"/>
            </span>
        </div>

        <div class="fieldcontain ${hasErrors(bean: invoiceAttachmentInstance, field: 'file', 'error')}">
            <span class="property-label required">
                <label for="file">
                    <g:message code="documentAttachment.file.label" default="File" />
                </label>
            </span>
            <span class="property-value">
                <input type="file" id="file" name="file" />
            </span>
        </div>

        <fieldset class="buttons">
            <g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
        </fieldset>
    </g:uploadForm>
</div>

<div id="list-invoice-attachment" class="content">
    <h1><g:message code="invoice.attachment.list" default="Attachments"/></h1>

    <table>
        <thead>
        <tr>

            <g:sortableColumn property="name"
                              title="${message(code: 'invoiceAttachment.name.label', default: 'Name')}"/>

            <th><g:message code="invoiceAttachment.filename.label" default="File Name"/></th>

            <g:sortableColumn property="attachmentType"
                              title="${message(code: 'invoiceAttachment.attachmentType.label', default: 'Type')}"/>

            <th></th>

        </tr>
        </thead>
        <tbody>
        <g:each in="${invoiceAttachmentInstanceList}" status="i" var="invoiceAttachmentInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td>
                    <mongo:link domainInstance="${invoiceAttachmentInstance}" fieldName="${invoiceAttachmentInstance.attachmentType}">
                            ${invoiceAttachmentInstance?.name}
                    </mongo:link>
                </td>

                <td><mongo:link domainInstance="${invoiceAttachmentInstance}" fieldName="${invoiceAttachmentInstance.attachmentType}"/></td>

                <td>${fieldValue(bean: invoiceAttachmentInstance, field: "attachmentType")}</td>

                <td><g:form url="[resource: invoiceAttachmentInstance, action: 'delete']" method="DELETE">
                    <input type="button" id="deleteInvoiceAttachment" class="button-in-table tbl-delete delete"
                           value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                           onclick="deleteConfirm(this);"         />
                </g:form>
                </td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${invoiceAttachmentInstanceCount ?: 0}" params="${params}"/>
    </div>
</div>

</body>
</html>