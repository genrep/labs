<%@ page import="com.genrep.invoiceRepository.Taxes; com.genrep.invoiceCreator.InvoiceItem" %>



<div id="formInvoiceItem" class="newItemBody">

    <div class="fieldcontain ${hasErrors(bean: invoiceItemInstance, field: 'description', 'error')} ">
        <span class="property-label">
            <label for="description">
                <g:message code="invoiceItem.description.label" default="Description" />

            </label>
        </span>
        <span class="errorLabel" id="descriptionE"></span>
        <div class="jqte-popup">
            <span class="property-value">
                <g:textArea name="description" id="description" cols="40" rows="5" maxlength="500" value=""
                            data-message="${message(code:'invoiceItem.invoiceDescription.error',default:'Description is obligatory')}"/>
            </span>
        </div>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoiceItemInstance, field: 'quantity', 'error')}">
        <span class="property-label required">
            <label for="quantity">
                <g:message code="invoiceItem.quantity.label" default="Quantity" />
            </label>
        </span>
        <span class="property-value">
            <g:field name="quantity" id="quantity" type="number" min="0" value="" step="0.25"
                     data-message="${message(code:'invoiceItem.quantity.error',default:'Fill in the quantity number')}"/>
        </span>
        <span class="errorLabel" id="quantityE"></span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoiceItemInstance, field: 'quantityUnit', 'error')}">
        <span class="property-label">
            <label for="quantityUnit">
                <g:message code="invoiceItem.quantityUnit.label" default="Quantity Unit" />
            </label>
        </span>
        <span class="property-value">
            <g:select id="quantityUnit" name="quantityUnit" from="${com.genrep.quantityUnit.QuantityUnit.list()}"
                      optionValue="enName"
                      optionKey="enName"
                      value="${invoiceItemInstance?.quantityUnit}"
                      data-message="${message(code:'invoiceItem.tax.error',default:'Choose option')}"
                      class="many-to-one"/>
        </span>
        <span class="errorLabel" id="quantityUnitE"></span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoiceItemInstance, field: 'unitPrice', 'error')}">
        <span class="property-label required">
            <label for="unitPrice">
                <g:message code="invoiceItem.unitPrice.label" default="Unit Price" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="unitPrice" data-message="${message(code:'invoiceItem.unitPrice.error',default:'Fill in the unit price')}"/>
        </span>
        <span class="errorLabel" id="unitPriceE"></span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoiceItemInstance, field: 'tax', 'error')}">
        <span class="property-label required">
            <label for="tax">
                <g:message code="invoiceItem.tax.label" default="Tax (%)" />
            </label>
        </span>
        <span class="property-value">
            <g:select id="tax" name="tax" from="${Taxes.values()}"
                      required="" optionValue="value"
                      value="${invoiceItemInstance?.tax}"
                      data-message="${message(code:'invoiceItem.tax.error',default:'Fill in the tax value')}"
                      class="many-to-one"/>
            %{--<g:priceAndTaxField id="tax" value="18,00" data-message="${message(code:'invoiceItem.tax.error',default:'Fill in the tax value')}" />--}%
        </span>
        <span class="errorLabel" id="taxE"></span>

    </div>

    <div class="itemButtons">

        <input id="saveEditFormItemButton" type="button" value="Save Item" class="buttonsMine saveButton"
               onclick="saveFormItem()"/>

        <input id="closeFormItemButton" type="button" value="Close" class="buttonsMine closeButton"
               onclick="closeFormItem()"/>
    </div>

</div>

