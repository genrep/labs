<%@ page import="com.genrep.invoiceCreator.ExchangeRate; com.genrep.invoiceCreator.Invoice" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <asset:javascript src="invoice.js"/>
</head>

<body>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="menuButton" action="create"><g:message code="default.new.label"
                                                                  args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-invoice" class="content scaffold-show" role="main">
    <g:if test="${invoiceInstance?.invoiceReversal==null}">
    <h1><g:message code="invoice.show.label" args="[invoiceInstance?.invoiceNumber]"/></h1>
    </g:if>
    <g:else>
        <h1><g:message code="invoice.show.label" args="[invoiceInstance?.invoiceNumber,'(REVERSED)']"/></h1>
    </g:else>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list invoice">

        <g:if test="${invoiceInstance?.name}">
            <li class="fieldcontain">
                <span id="name-label" class="property-label"><g:message code="invoice.name.label"
                                                                        default="Descriptive Name"/></span>
                <span class="property-value" aria-labelledby="name-label">
                    <g:fieldValue
                            bean="${invoiceInstance}" field="name" />
                </span>
            </li>
        </g:if>

        <g:if test="${invoiceInstance?.client}">
            <li class="fieldcontain">
                <span id="client-label" class="property-label"><g:message code="invoice.client.label"
                                                                          default="Client"/></span>

                <span class="property-value" aria-labelledby="client-label">${invoiceInstance?.client.name}</span>

            </li>
        </g:if>

        <g:if test="${invoiceInstance?.invoiceDescription}">
            <li class="fieldcontain">
                <span id="invoiceDescription-label" class="property-label">
                    <g:message code="invoice.invoiceDescription.label"
                               default="Description"/></span>
                <span class="property-value" aria-labelledby="invoiceDescription-label">
                    ${raw(invoiceInstance.invoiceDescription)}
                </span>
            </li>
        </g:if>

        <g:if test="${invoiceInstance?.invoiceNumber}">
            <li class="fieldcontain">
                <span id="invoiceNumber-label" class="property-label"><g:message code="invoice.invoiceNumber.label"
                                                                                 default="Invoice Number"/></span>

                <span class="property-value" aria-labelledby="invoiceNumber-label"><g:fieldValue
                        bean="${invoiceInstance}" field="invoiceNumber" /></span>

            </li>
        </g:if>

        <g:if test="${invoiceInstance?.invoiceNumberSort}">
            <li class="fieldcontain">
                <span id="invoiceNumberSort-label" class="property-label"><g:message code="invoice.invoiceNumberSort.label"
                                                                                     default="Invoice Number Sort"/></span>

                <span class="property-value" aria-labelledby="invoiceNumberSort-label"><g:fieldValue
                        bean="${invoiceInstance}" field="invoiceNumberSort" /></span>

            </li>
        </g:if>

        <g:if test="${invoiceInstance?.bankAccount}">
            <li class="fieldcontain">
                <span id="bankAccount-label" class="property-label"><g:message code="invoice.bankAccount.label"
                                                                               default="Bank Account"/></span>

                <span class="property-value" aria-labelledby="bankAccount-label"><g:fieldValue
                        bean="${invoiceInstance}" field="bankAccount" /></span>
            </li>
        </g:if>

        <g:if test="${invoiceInstance?.swiftCode}">
            <li class="fieldcontain">
                <span id="swiftCoder-label" class="property-label"><g:message code="invoice.swiftCode.label"
                                                                              default="Swift Code"/></span>

                <span class="property-value" aria-labelledby="swiftCode-label"><g:fieldValue
                        bean="${invoiceInstance}" field="swiftCode" /></span>

            </li>
        </g:if>

        <g:if test="${invoiceInstance?.paymentPercentage}">
            <li class="fieldcontain">
                <span id="paymentPercentage-label" class="property-label"><g:message code="invoice.paymentPercentage.label"
                                                                                     default="Payment Percentage (%)"/></span>

                <span class="property-value" aria-labelledby="paymentPercentage-label"><g:fieldValue
                        bean="${invoiceInstance}" field="paymentPercentage" /></span>

            </li>
        </g:if>

        <g:if test="${invoiceInstance?.paidByStatement}">
            <li class="fieldcontain">
                <span id="paidByStatement-label" class="property-label"><g:message code="invoice.paidByStatement.label"
                                                                                   default="Paid By Statement"/></span>

                <span class="property-value" aria-labelledby="paidByStatement-label"><g:fieldValue
                        bean="${invoiceInstance}" field="paidByStatement" /></span>

            </li>
        </g:if>

        <g:if test="${invoiceInstance?.attention}">
            <li class="fieldcontain">
                <span id="attention-label" class="property-label"><g:message code="invoice.attention.label"
                                                                             default="Attention"/></span>

                <span class="property-value" aria-labelledby="attention-label"><g:fieldValue
                        bean="${invoiceInstance}" field="attention" /></span>

            </li>
        </g:if>

        <g:if test="${invoiceInstance?.paymentDeadline}">
            <li class="fieldcontain">
                <span id="paymentDeadline-label" class="property-label"><g:message code="invoice.paymentDeadline.label"
                                                                                   default="Payment Deadline (In Days)"/></span>

                <span class="property-value" aria-labelledby="paymentDeadline-label"><g:fieldValue
                        bean="${invoiceInstance}" field="paymentDeadline" /></span>

            </li>
        </g:if>

        <g:if test="${invoiceInstance?.totalPriceInWords}">
            <li class="fieldcontain">
                <span id="totalPriceInWords-label" class="property-label"><g:message code="invoice.totalPriceInWords.label"
                                                                                     default="Total Price In Words"/></span>

                <span class="property-value" aria-labelledby="totalPriceInWords-label"><g:fieldValue
                        bean="${invoiceInstance}" field="totalPriceInWords" /></span>

            </li>
        </g:if>

        <g:if test="${invoiceInstance?.status}">
            <li class="fieldcontain">
                <span id="status-label" class="property-label"><g:message code="invoice.status.label"
                                                                          default="Invoice Status"/></span>

                <span class="property-value" aria-labelledby="status-label"><g:fieldValue
                        bean="${invoiceInstance}" field="status"/></span>

            </li>
        </g:if>

        <g:if test="${invoiceInstance?.exchangeRate}">
            <li class="fieldcontain">
                <span id="exchangeRate-label" class="property-label"><g:message code="invoice.exchangeRate.label"
                                                                                default="Exchange Rate"/></span>

                <span class="property-value" aria-labelledby="exchangeRate-label">
                    <g:fieldValue
                            bean="${invoiceInstance}" field="exchangeRate"/>
                </span>

            </li>
        </g:if>


        <li class="fieldcontain">
            <span id="documents-label" class="property-label"><g:message code="invoice.documents.label"
                                                                         default="Show Documents"/></span>

            <span class="property-value" aria-labelledby="exchangeRate-label">
                <g:link action="showDocuments" id="${invoiceInstance.id}" >Click here</g:link>
            </span>

        </li>


        <fieldset class="bodies">
            <g:render template="/invoice/showInvoiceBody"/>
        </fieldset>

        <g:if test="${project}">
            <li class="fieldcontain">
                <span id="project-label" class="property-label">
                    <g:message code="invoice.project.label" default="Project"/>
                </span>
                <span class="property-value">
                    <g:link controller="project" action="show" id="${project?.id}">
                        ${project?.title}
                    </g:link>
                </span>
                <span class="property-value">
                    Contract Number of Project: ${project?.contractNumberBearer}
                </span>
            </li>
        </g:if>

        <g:if test="${invoiceInstance?.issueDate}">
            <li class="fieldcontain">
                <span id="issueDate-label" class="property-label"><g:message code="invoice.issueDate.label"
                                                                             default="Issue Date"/></span>

                <span class="property-value" aria-labelledby="issueDate-label">
                    <g:formatDate date="${invoiceInstance?.issueDate}" format="dd.MM.yyyy" /></span>

            </li>
        </g:if>

        <g:if test="${invoiceInstance?.paymentDate}">
            <li class="fieldcontain">
                <span id="paymentDate-label" class="property-label"><g:message code="invoice.paymentDate.label"
                                                                               default="Payment Date"/></span>

                <span class="property-value" aria-labelledby="paymentDate-label"><g:formatDate
                        date="${invoiceInstance?.paymentDate}" format="dd.MM.yyyy"/></span>

            </li>
        </g:if>

        <g:if test="${invoiceInstance?.invoiceReversal}">
            <li class="fieldcontain">
                <span id="invoiceReversal-label" class="property-label"><g:message code="invoice.invoiceReversal.label"
                                                                                   default="Invoice Reversal"/></span>

                <span class="property-value" aria-labelledby="paymentDate-label"><g:formatDate
                        date="${invoiceInstance?.invoiceReversal}" format="dd.MM.yyyy"/></span>

            </li>
        </g:if>

    </ol>
    <g:form url="[resource: invoiceInstance, action: 'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:if test="${invoiceInstance.invoiceReversal==null}">
                <g:link class="edit" action="edit" resource="${invoiceInstance}"><g:message code="default.button.edit.label"
                                                                                            default="Edit"/></g:link>
            </g:if>

            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>

            <g:if test="${invoiceInstance.invoiceReversal==null}">
                <g:link class="create" action="copy" resource="${invoiceInstance}">
                    <g:message code="default.button.copy.label" default="Make a copy"/>
                </g:link>
            </g:if>

            <g:link class="create" action="attachment" resource="${invoiceInstance}">
                <g:message code="default.button.attachments.label" default="Attachments"/>
            </g:link>

        </fieldset>
    </g:form>


</div>
</body>
</html>
