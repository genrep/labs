<div id="invoiceBody">
    <g:if test="${invoiceInstance?.invoiceBodyIn}">
        <div id="invoiceBodyTable" name="invoiceBodyTable" class="invoiceBodyTableForm">
        <g:if test="${invoiceInstance?.invoiceBodyIn?.invoiceItems}">

            <table id="tableBody">
                <thead>
                <tr>
                    <td></td>
                    <td>Description</td>
                    <td>Quantity</td>
                    <td>Quantity Unit</td>
                    <td>Unit Price</td>
                    <td>Tax (%)</td>
                    <td>Tax Price Per Unit</td>
                    <td>Total Tax Free Price</td>
                    <td>Total Tax Price</td>
                    <td>Total Price</td>
                    <td>Action</td>
                </tr>
                </thead>
                <tbody>
                <g:set var="size" value="${invoiceInstance.invoiceBodyIn?.invoiceItems.size()}"/>
                <g:each in="${invoiceInstance.invoiceBodyIn?.invoiceItems}" status="i" var="invoiceItem">
                    <tr ondblclick="editInvoiceItemDetails(${(invoiceItem.getEditableFields() as grails.converters.JSON).toString()})">
                        <td class="table-arrows">
                            <g:if test="${i>0 && i < size-1}">
                                <g:link url="#" onclick="updateInvoiceItemPosition(${i},'up')"><g:img dir="images" file="arrow-up.png" alter="Up"/></g:link>
                                <g:link url="#" onclick="updateInvoiceItemPosition(${i},'down','edit')"><g:img dir="images" file="arrow-down.png" alter="Down"/></g:link>
                            </g:if>
                            <g:elseif test="${i == 0 && size > 1}">
                                <g:link url="#" onclick="updateInvoiceItemPosition(${i},'down')"><g:img dir="images" file="arrow-down.png" alter="Down"/></g:link>
                            </g:elseif>
                            <g:elseif test="${i == size-1 && size > 1 }">
                                <g:link url="#" onclick="updateInvoiceItemPosition(${i},'up')"><g:img dir="images" file="arrow-up.png" alter="Up"/></g:link>
                            </g:elseif>
                            <g:else>

                            </g:else>

                        </td>
                        <td>${raw(invoiceItem?.description)}</td>
                        <td class="number">${invoiceItem?.quantity}</td>
                        <td class="number">${invoiceItem?.quantityUnit}</td>
                        <td class="number">${invoiceItem?.unitPrice.decodeDecimalNumber()}</td>
                        <td class="number">${invoiceItem?.tax.value}</td>
                        <td class="number">${invoiceItem?.taxPricePerUnit.decodeDecimalNumber()}</td>
                        <td class="number">${invoiceItem?.taxFreePrice.decodeDecimalNumber()}</td>
                        <td class="number">${invoiceItem?.taxPrice.decodeDecimalNumber()}</td>
                        <td class="number">${invoiceItem?.totalPrice.decodeDecimalNumber()}</td>
                        <td><g:remoteLink controller="invoiceItem" action="deleteInvoiceItem" id="${invoiceItem?.id}" params="[sessionId:invoiceItem.sessionId.toString()]"

                                          update="invoiceBody"><g:img dir="images" file="delete.png" alter="Delete"/></g:remoteLink> </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
            </div>

            <div id="invoiceBodySumary" name="invoiceBodySummary" class="sumStyle">
                <table id="tableSummaryBody">

                    <tbody>
                        <tr>
                            <td>Tax Free Sum</td>
                            <td class="number">${invoiceInstance.invoiceBodyIn?.taxFreeSum.decodeDecimalNumber()}</td>
                            <td class="number">${invoiceInstance.invoiceBodyIn?.taxFreeSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber()}</td>
                        </tr>
            <tr>
                <td>Tax Sum</td>
                <td class="number">${invoiceInstance.invoiceBodyIn?.taxPriceSum.decodeDecimalNumber()}</td>
                <td class="number">${invoiceInstance.invoiceBodyIn?.taxPriceSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber()}</td>
            </tr>
            <tr>
                <td>Total Sum</td>
                <td class="number">${invoiceInstance.invoiceBodyIn?.totalSum.setScale(2,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber()}</td>
                <td class="number">${invoiceInstance.invoiceBodyIn?.getTotalSumRounded().decodeDecimalNumber()}</td>
            </tr>
            </tbody>
       </table>
        </g:if>
        </div>
    </g:if>
</div>
