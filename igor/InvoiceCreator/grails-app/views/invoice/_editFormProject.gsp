<%@ page import="grails.converters.JSON" %>
<div id="editFormProject">
    <g:if test="${project}">
        <div class="fieldcontain">
            <span class="property-label">
                <label for="project">
                    <g:message code="invoice.project.showLabel" default="Project"/>
                </label>
            </span>
            <span class="property-value">
                <g:link controller="project" action="show" id="${project?.id}">
                    ${project?.title}
                </g:link>
            </span>
            <span class="property-value">
                Contract Number of Project: ${project.contractNumberBearer}
            </span>
            <span class="property-value buttons">
                <input type="button" value="Remove Project" onclick="updateInvoiceProject(${project?.id},'remove')" />
                <input type="button" value="Change Project" onclick="updateInvoiceProject(${project?.id},'update')"/>
            </span>
        </div>
    </g:if>
    <g:elseif test="${guiAction}">
        <g:if test="${guiAction.equals('remove')}">
            <div class="fieldcontain">
                <span class="property-label">
                    <label for="project">
                        <g:message code="invoice.project.label" default="Add to Project"/>
                    </label>
                </span>
                <span class="property-value">
                    <g:select id="project" name="project" from="${projectList}" noSelection="['':'']"
                              onchange="showProjectsContractNumber('${projectList.contractNumberBearer as JSON}',this,'add')"
                              optionKey="id" optionValue="title" class="many-to-one"/>
                </span>
                <span id="projectContract" class="property-value">
                </span>
                <span class="property-value buttons">
                    <input type="button" value="Undo" onclick="updateInvoiceProject(${params?.id},'undo')" />
                </span>
            </div>
        </g:if>
        <g:elseif test="${guiAction.equals('update')}">
            <div class="fieldcontain">
                <span class="property-label">
                    <label for="project">
                        <g:message code="invoice.project.label" default="Add to Project"/>
                    </label>
                </span>
                <span class="property-value">
                    <g:select id="project" name="project" from="${projectList}"
                              onchange="showProjectsContractNumber('${projectList.contractNumberBearer as JSON}',this,'change')"
                              optionKey="id" optionValue="title" class="many-to-one"/>
                </span>
                <span id="projectContract" class="property-value">
                </span>
                <span class="property-value buttons">
                    <input type="button" value="Undo" onclick="updateInvoiceProject(${params?.id},'undo')" />
                </span>
            </div>
        </g:elseif>
    </g:elseif>
    <g:else>
        <div class="fieldcontain">
            <span class="property-label">
                <label for="project">
                    <g:message code="invoice.project.label" default="Add to Project"/>
                </label>
            </span>
            <span class="property-value">
                <g:select id="project" name="project" from="${projectList}" noSelection="['':'']"
                          onchange="showProjectsContractNumber('${projectList.contractNumberBearer as grails.converters.JSON}',this,'add')"
                          optionKey="id" optionValue="title" class="many-to-one"/>
            </span>
            <span id="projectContract" class="property-value">
            </span>
        </div>
    </g:else>
</div>