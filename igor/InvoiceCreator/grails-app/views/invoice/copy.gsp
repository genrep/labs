<%@ page import="com.genrep.invoiceCreator.Invoice" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}"/>
    <title><g:message code="default.copy.label" default="Copy Invoice"/></title>

</head>

<body>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="menuButton" action="create"><g:message code="default.new.label"
                                                                  args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="copy-invoice" class="content scaffold-edit" role="main">
    <h1><g:message code="default.copy.label" default="Make a copy of this invoice by entering new Invoice Number"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${invoiceInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${invoiceInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form url="[resource: invoiceInstance, action: 'saveCopy']" method="PUT">
        <fieldset class="form">

            <div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceNumber', 'error')}">
                <span class="property-label">
                    <label for="newInvoiceNumber">
                        <g:message code="invoice.invoiceNumber.label" default="Invoice Number"/>
                        <span class="required-indicator">*</span>
                    </label>
                </span>
                <span class="property-value">
                    <g:textField name="newInvoiceNumber" id="newInvoiceNumber" required=""
                                 value="${params.invoiceNumber}"/>
                </span>
            </div>

            <div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceCounter', 'error')}">
                <span class="property-label">
                    <label for="newInvoiceCounter">
                        <g:message code="invoice.invoiceCounter.label" default="Invoice Counter"/>
                        <span class="required-indicator">*</span>
                    </label>
                </span>
                <span class="property-value">
                    <g:textField name="newInvoiceCounter" id="newInvoiceCounter" required=""
                                 value="${params.invoiceCounter}"/>
                </span>
            </div>

            <div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceYear', 'error')}">
                <span class="property-label">
                    <label for="newInvoiceYear">
                        <g:message code="invoice.invoiceYear.label" default="Invoice Year"/>
                        <span class="required-indicator">*</span>
                    </label>
                </span>
                <span class="property-value">
                    <g:textField name="newInvoiceYear" id="newInvoiceYear" required=""
                                 value="${params.invoiceYear}"/>
                </span>
            </div>


        </fieldset>
        <fieldset class="buttons">
            <g:actionSubmit class="save" action="saveCopy"
                            value="${message(code: 'default.button.copy.label', default: 'Make a copy')}"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
