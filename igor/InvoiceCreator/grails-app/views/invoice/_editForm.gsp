<%@ page import="com.genrep.invoiceCreator.ExchangeRate; com.genrep.invoiceCreator.Currencies; com.genrep.invoiceCreator.Invoice" %>
<%@ page import="com.genrep.client.Client" %>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'client', 'error')}">
    <span class="property-label required">
        <label for="client">
            <g:message code="invoice.client.label" default="Client"/>
        </label>
    </span>
    <span class="property-value">
        <g:select id="client" name="client.id" from="${Client.list()}"
                  optionKey="id" optionValue="name"
                  required=""
                  value="${invoiceInstance?.client?.id}" class="comboBox"/>
        <input type="button" class="newDialogButton" value="+" onclick="newClientDialog()" />
    </span>

</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceDescription', 'error')}">
    <span id="invoiceDescription-label" class="property-label">
        <label for="invoiceDescription">
            <g:message code="invoice.invoiceDescription.label" default="Description" />
        </label>
    </span>
    <span class="property-value" aria-labelledby="description-label">
        <g:textArea name="invoiceDescription" id="invoiceDescription" value="${invoiceInstance?.invoiceDescription}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'inCurrency', 'error')}">
    <span class="property-label required">
        <label for="inCurrency">
            <g:message code="invoice.inCurrency.label" default="Base Currency" />
        </label>
    </span>
    <span class="property-value">
        <g:currencySelect name="inCurrency" value="${invoiceInstance.inCurrency?:Currencies.MKD.currency}"
                          from="${Currencies.values()}" disabled="true" />
    </span>
</div>

<g:if test="${invoiceInstance.invoiceBodyOut==null}">
    <div class="fieldcontain">
        <span class="property-label">
            <label>
                <g:message code="invoice.cbOutInvoice.label" default="In Other Currency" />
            </label>
        </span>
        <span class="property-value">
            <g:checkBox name="cbOutInvoice" checked="false"  onchange="generateOutInvoice(this)"/>
        </span>
    </div>
</g:if>
<div id="outCurrencyBlock" class="outCurrencyBlock">
    <div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'outCurrency', 'error')}">
        <span class="property-label">
            <label for="inCurrency">
                <g:message code="invoice.outCurrency.label" default="To Currency" />
            </label>
        </span>
        <span class="property-value">
            <g:currencySelect name="outCurrency" id="outCurrency" value="${invoiceInstance.outCurrency?:Currencies.MKD.currency}"
                              disabled="true"
                              from="${Currencies.values()}"/>
        </span>
    </div>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceCounter', 'error')}">
    <span class="property-label">
        <label for="invoiceCounter">
            <g:message code="invoice.invoiceCounter.label" default="Invoice Counter"/>
        </label>
    </span>
    <span class="property-value">
        <g:field type="number" name="invoiceCounter" id="invoiceCounter" onchange="generateInvoiceNumber()"
                 value="${invoiceInstance?.invoiceCounter}" required="" />
    </span>
</div>


<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceYear', 'error')}">
    <span class="property-label">
        <label for="invoiceYear">
            <g:message code="invoice.invoiceYear.label" default="Invoice Year"/>
        </label>
    </span>
    <span class="property-value">
        <g:field type="number" name="invoiceYear" id="invoiceYear" onchange="generateInvoiceNumber()"
                 value="${invoiceInstance?.invoiceYear}" required="" />
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceNumber', 'error')}">
    <span class="property-label">
        <label for="invoiceNumber">
            <g:message code="invoice.invoiceNumber.label" default="Invoice Number"/>
        </label>
    </span>
    <span class="property-value" aria-labelledby="client-label">
        <g:textField name="invoiceNumber" id="invoiceNumber" value="${invoiceInstance?.invoiceNumber}"
                     readonly="" required=""/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceNumberSort', 'error')}">
    <span class="property-label">
        <label for="invoiceNumberSort">
            <g:message code="invoice.invoiceNumberSort.label" default="Invoice Number Sort"/>
        </label>
    </span>
    <span class="property-value">
        <g:textField name="invoiceNumberSort" id="invoiceNumberSort" value="${invoiceInstance?.invoiceNumberSort}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'bankAccount', 'error')}">
    <span class="property-label">
        <label for="bankAccount">
            <g:message code="invoice.bankAccount.label" default="Bank Account"/>
        </label>
    </span>
    <span class="property-value">
        <g:select name="bankAccount" from="${com.genrep.account.BankAccount.findAllWhere(owner: true)}"
                  value="${invoiceInstance?.bankAccount?.id}"
                  optionValue="entityName" optionKey="id" noSelection="[null:'']"
        />
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'swiftCode', 'error')}">
    <span class="property-label">
        <label for="swiftCode">
            <g:message code="invoice.swiftCode.label" default="Swift Code"/>
        </label>
    </span>
    <span class="property-value">
        <g:textField name="swiftCode" id="swiftCode" value="${invoiceInstance?.swiftCode}" />
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'paymentPercentage', 'error')}">
    <span class="property-label">
        <label for="paymentPercentage">
            <g:message code="invoice.paymentPercentage.label" default="Payment Percentage (%)"/>
        </label>
    </span>
    <span class="property-value">
        <g:field type="number" name="paymentPercentage" id="paymentPercentage"
                 min="0" max="100" step="any"
                 value="${invoiceInstance?.paymentPercentage}" />
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'paidByStatement', 'error')}">
    <span class="property-label">
        <label for="paidByStatement">
            <g:message code="invoice.paidByStatement.label" default="Paid By Statement"/>
        </label>
    </span>
    <span class="property-value">
        <g:textField name="paidByStatement" id="paidByStatement" value="${invoiceInstance?.paidByStatement}" />
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'status', 'error')}">
    <span class="property-label">
        <label for="status">
            <g:message code="invoice.status.label" default="Set Status"/>
        </label>
    </span>
    <span class="property-value">
        <g:select id="status" name="status" from="${com.genrep.invoiceCreator.Statuses.values()}"
                    optionValue="name" value="${invoiceInstance?.status}"
                    class="many-to-one"/>
    </span>
</div>

<div>
    <h2>Invoice Report Fields</h2>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'attention', 'error')}">
    <span class="property-label">
        <label for="attention">
            <g:message code="invoice.attention.label" default="Attention"/>
        </label>
    </span>
    <span class="property-value">
        <g:textField name="attention" id="attention" class="wider-fields" value="${invoiceInstance?.attention}" />
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'paymentDeadline', 'error')}">
    <span class="property-label">
        <label for="paymentDeadline">
            <g:message code="invoice.paymentDeadline.label" default="Payment Deadline (In Days)"/>
        </label>
    </span>
    <span class="property-value">
        <g:field type="numeric" name="paymentDeadline" id="paymentDeadline" class="wider-fields" value="${invoiceInstance?.paymentDeadline}" />
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'totalPriceInWords', 'error')}">
    <span class="property-label">
        <label for="paymentDeadline">
            <g:message code="invoice.totalPriceInWords.label" default="Total Price In Words"/>
        </label>
    </span>
    <span class="property-value">
        <g:textArea name="totalPriceInWords" id="totalPriceInWords" class="wider-fields" value="${invoiceInstance?.totalPriceInWords}" />
    </span>
</div>

%{--<g:if test="${invoiceInstance?.status!=com.genrep.invoiceCreator.Statuses.FINALIZED}">--}%
    %{--<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'status', 'error')}">--}%
        %{--<span class="property-label">--}%
            %{--<label for="invoiceStatus">--}%
                %{--<g:message code="invoice.status.finalize.label" default="Finalize Invoice"/>--}%
            %{--</label>--}%
        %{--</span>--}%
        %{--<span class="property-value">--}%
            %{--<g:checkBox name="invoiceStatus" checked="false"/>--}%
        %{--</span>--}%
    %{--</div>--}%
%{--</g:if>--}%

<!-- ADD NEW -->

<div class="invoiceItem">
    <input id="newFormItemButton" type="button" value="Add New Invoice Item" class="buttonsMine createButton"
           onclick="newFormItem()"/>
</div>

<g:render template="/invoice/formInvoiceItem"/>

<!-- END NEW -->

<!-- INVOICE BODY -->
<fieldset class="form">
    <g:render template="formInvoiceBody"/>
</fieldset>

<!-- DIALOG -->
<g:render template="dialog"/>
<!-- END DIALOG -->

<!--project-->
<g:render template="editFormProject"/>
<!--end project-->

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'issueDate', 'error')} ">
    <span class="property-label required">
        <label for="issueDate">
            <g:message code="invoice.issueDate.label" default="Issue Date"/>
        </label>
    </span>
    <span class="property-value">
        <input type="text" name="issueDate" id="issueDate" class="date" required=""
               value="${formatDate(date:invoiceInstance.issueDate?:new Date(),format: 'dd.MM.yyyy')}"/>
    </span>

</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'paymentDate', 'error')} ">
    <span class="property-label">
        <label for="paymentDate">
            <g:message code="invoice.paymentDate.label" default="Payment Date"/>

        </label>
    </span>
    <span class="property-value">
        <input type="text" name="paymentDate" id="paymentDate" class="date"
               value="${formatDate(date:invoiceInstance?.paymentDate,format: 'dd.MM.yyyy')}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoiceInstance, field: 'invoiceReversal', 'error')} ">
    <span class="property-label">
        <label for="invoiceReversal">
            <g:message code="invoice.invoiceReversal.label" default="Reverse Invoice"/>

        </label>
    </span>
    <span class="property-value">
        <g:checkBox name="reverseInvoice" />
        <g:message code="invoice.invoiceReversal.warning" default="(Careful this can't be undone.)"/>
    </span>
</div>
