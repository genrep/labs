<%@ page import="com.genrep.invoiceCreator.Invoice" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>

    <asset:javascript src="invoice.js"/>

</head>

<body>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="menuButton" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="edit-invoice" class="content scaffold-edit" role="main">
    <h1><g:message message="${invoiceInstance?.name}"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${invoiceInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${invoiceInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form url="[resource: invoiceInstance, action: 'update']" method="PUT">
        <g:hiddenField name="version" value="${invoiceInstance?.version}"/>
        <fieldset class="form">
            <g:render template="editForm"/>
        </fieldset>
        <fieldset class="buttons">
            <g:actionSubmit class="save" action="update"
                            value="${message(code: 'default.button.update.label', default: 'Update')}"/>
            <g:link class="cancel" action="show" resource="${invoiceInstance}">
                <g:message code="default.button.copy.label" default="Cancel"/>
            </g:link>
        </fieldset>
    </g:form>

    <div class="includedTemplate">
        <g:include view="client/_form.gsp"/>
    </div>
</div>
</body>
</html>
