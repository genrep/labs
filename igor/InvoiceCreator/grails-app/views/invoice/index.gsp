<%@ page import="com.genrep.invoiceCreator.Invoice" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <asset:javascript src="invoice.js"/>
</head>

<body>
<a href="#list-invoice" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                              default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="create"><g:message code="default.new.label"
                                                                  args="[entityName]"/></g:link></li>
        <li><g:link class="menuButton" controller="invoiceArchive" action="index"><g:message code="invoiceArchive.home.label" default="Importing of Invoices"/>
        </g:link></li>
    </ul>
</div>

<div id="list-invoice" class="content scaffold-list" role="main">
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <div>
        <gnrp:filterPane domain="com.genrep.invoiceCreator.Invoice"
                         filterParams="issueDate,client,invoiceNumber,status"
                         customForm="true"
        />
    </div>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="invoiceYear,invoiceNumber"
                              title="${message(code: 'invoice.invoiceNumber.label', default: 'Invoice Number')}"/>

            <g:sortableColumn property="invoiceNumberSort"
                              title="${message(code: 'invoice.invoiceNumberSort.label', default: 'Invoice Number Sort')}"/>

            <th><g:message code="invoice.client.label" default="Client"/></th>

            <g:sortableColumn property="issueDate"
                              title="${message(code: 'invoice.issueDate.label', default: 'Issue Date')}"/>

            <g:sortableColumn property="invoiceDescription"
                              title="${message(code: 'invoice.invoiceDescription.label', default: 'Description')}"/>

            <g:sortableColumn property="invoiceBodyIn.totalSum"
                              title="${message(code: 'invoice.totalSum.label', default: 'Total Sum')}"/>

            <g:sortableColumn property="currency" title="${message(code: 'invoice.inCurrency.label', default: 'Currency')}" />

            <g:sortableColumn property="status"
                              title="${message(code: 'invoice.status.label', default: 'Status')}"/>

            <th><g:message code="invoice.documents.label" default="Documents"/></th>

            <th><g:message code="invoice.action.label" default="Action"/></th>

        </tr>
        </thead>
        <tbody>
        <g:each in="${invoiceInstanceList}" status="i" var="invoiceInstance">

            <g:set var="invoiceReversed" value="${invoiceInstance.invoiceReversal!=null?'invoiceReversed':''}"/>

            <tr class="${(i % 2) == 0 ? 'even' : 'odd'} ${invoiceReversed}">



                <td><g:link action="show" id="${invoiceInstance.id}" >${fieldValue(bean: invoiceInstance, field: "invoiceNumber")}
                </g:link></td>

                <td>${fieldValue(bean: invoiceInstance, field: "invoiceNumberSort")}</td>

                <td>${fieldValue(bean: invoiceInstance, field: "client")}</td>


                <td><g:formatDate date="${invoiceInstance.issueDate}" format="dd.MM.yyyy"/></td>

                <td>${raw(invoiceInstance?.invoiceDescription)}</td>

                <td>${invoiceInstance.invoiceBodyIn.totalSum}</td>

                <td>${fieldValue(bean: invoiceInstance, field: "inCurrency")}</td>

                <td>${invoiceInstance.status}</td>

                <td><g:link class="button-in-table" action="showDocuments" id="${invoiceInstance.id}" >Click here</g:link></td>

                <td><g:form url="[resource: invoiceInstance, action: 'delete']" method="DELETE">
                    <input type="button" id="deleteInvoice" class="button-in-table tbl-delete delete"
                           value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                           onclick="deleteConfirm(this);"         />
                </g:form>
                </td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${invoiceInstanceCount ?: 0}" params="${params}"/>
    </div>
</div>
</body>
</html>
