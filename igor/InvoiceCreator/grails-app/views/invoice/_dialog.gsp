<%@ page import="com.genrep.invoiceRepository.Taxes" %>
<div id="dialog-form" title="Update Invoice Item Details">
    <p class="validateTips">All form fields are required.</p>

    <div class="jqte-popup">
        <label for="descriptionD">Description</label>
        <span class="errorLabel" id="descriptionDE"></span>
        <g:textArea name="descriptionD" id="descriptionD" cols="40" rows="5" maxlength="500"
                    data-message="${message(code:'invoiceItem.invoiceDescription.error',default:'Description is obligatory')}"/>
    </div>

    <div class="dialogInvoiceDetails">
        <div>
            <label for="quantityD">Quantity</label>
            <g:field name="quantityD" id="quantityD" type="number" min="0" step="0.25"
                     data-message="${message(code:'invoiceItem.quantity.error',default:'Fill in the quantity number')}"/>
            <span class="errorLabel" id="quantityDE"></span>
        </div>
        <div>
            <label for="quantityUnitD">Quantity Unit</label>
            <g:select id="quantityUnitD" name="quantityUnitD" from="${com.genrep.quantityUnit.QuantityUnit.list()}"
                      optionValue="enName"
                      optionKey="enName"
                      value="${invoiceItemInstance?.quantityUnit}"
                      data-message="${message(code:'invoiceItem.tax.error',default:'Choose option')}"
                      class="many-to-one"/>

            <span class="errorLabel" id="quantityUnitDE"></span>
        </div>
        <div>
            <label for="unitPriceD">Unit price</label>
            <g:priceAndTaxField name="unitPriceD" id="unitPriceD"
                                data-message="${message(code:'invoiceItem.unitPrice.error',default:'Fill in the unit price')}"/>
            <span class="errorLabel" id="unitPriceDE"></span>
        </div>
        <div>
            <label for="taxD">Tax</label>
            <g:select id="taxD" name="taxD" from="${Taxes.values()}"
                      required=""  optionValue="value"
                      data-message="${message(code:'invoiceItem.tax.error',default:'Fill in the tax value')}"
                      class="many-to-one"/>
            %{--<g:priceAndTaxField name="taxD" id="taxD"--}%
            %{--data-message="${message(code:'invoiceItem.tax.error',default:'Fill in the tax value')}"/>--}%
            <span class="errorLabel" id="taxDE"></span>
        </div>
    </div>

</div>