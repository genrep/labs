<%@ page import="java.math.RoundingMode" %>
<g:if test="${invoiceInstance?.invoiceBodyOut}">
    <div id="invoice-tabs">
        <ul>
            <li><a href="#showInvoiceBody">${invoiceInstance.inCurrency}</a></li>
            <li><a href="#showInvoiceBodyOut">${invoiceInstance.outCurrency}</a></li>
        </ul>

        <g:if test="${invoiceInstance?.invoiceBodyIn && invoiceInstance?.invoiceBodyIn.invoiceItems.size()>0}">
            <div id="showInvoiceBody">
                <div class="currencyTab">
                    <p><g:message code="invoice.inCurrency.desc" default="Currency: "/>${invoiceInstance.inCurrency}</p>
                </div>
                <div id="invoiceBodyTable" name="invoiceBodyTable" class="invoiceBodyTable">
                    <table id="tableBody">
                        <thead>
                        <tr>
                            <td>Description</td>
                            <td>Quantity</td>
                            <td>Quantity Unit</td>
                            <td>Unit Price</td>
                            <td>Tax (%)</td>
                            <td>Tax Price Per Unit</td>
                            <td>Tax-Free Price</td>
                            <td>Tax Price</td>
                            <td>Total Price</td>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${invoiceInstance.invoiceBodyIn?.invoiceItems}" status="i" var="invoiceItem">
                            <tr>
                                <td>${raw(invoiceItem?.description)}</td>
                                <td class="number"> ${invoiceItem?.quantity}</td>
                                <td class="number"> ${invoiceItem?.quantityUnit}</td>
                                <td class="number">${invoiceItem?.unitPrice.decodeDecimalNumber()}</td>
                                <td class="number">${invoiceItem?.tax.value}</td>
                                <td class="number">${invoiceItem?.taxPricePerUnit.decodeDecimalNumber()}</td>
                                <td class="number">${invoiceItem?.taxFreePrice.decodeDecimalNumber()}</td>
                                <td class="number">${invoiceItem?.taxPrice.decodeDecimalNumber()}</td>
                                <td class="number">${invoiceItem?.totalPrice.decodeDecimalNumber()}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>

                <div id="invoiceBodySumary" name="invoiceBodySummary" class="sumStyle">
                    <table id="tableSummaryBody">
                        <tbody>
                        <tr>
                            <td>Tax-Free Sum</td>
                            <td class="number">${invoiceInstance.invoiceBodyIn?.taxFreeSum.decodeDecimalNumber()}</td>
                            <td class="number">${invoiceInstance.invoiceBodyIn?.taxFreeSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber()}</td>
                        </tr>
                        <tr>
                            <td>Tax Sum</td>
                            <td class="number">${invoiceInstance.invoiceBodyIn?.taxPriceSum.decodeDecimalNumber()}</td>
                            <td class="number">${invoiceInstance.invoiceBodyIn?.taxPriceSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber()}</td>
                        </tr>
                        <tr>
                            <td>Total Sum</td>
                            <td class="number">${invoiceInstance.invoiceBodyIn?.totalSum.setScale(2,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber()}</td>
                            <td class="number">${invoiceInstance.invoiceBodyIn?.getTotalSumRounded().decodeDecimalNumber()}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <g:jasperReport jasper="InvoiceReport" format="PDF" name="${invoiceInstance?.name.replace(' ','_')}"
                                controller="report"
                                action="invoiceReport">
                    <input type="hidden" name="client_id" value="${invoiceInstance.client.id}"/>
                    <input type="hidden" name="invoice_id" value="${invoiceInstance.id}"/>
                    <input type="hidden" name="currencyCode" value="${invoiceInstance.inCurrency.currencyCode}" />
                    <input type="hidden" name="invoice_body" value="In"/>
                    <input type="hidden" name="language" value="MKD" />
                </g:jasperReport>

                <g:jasperReport jasper="InvoiceReport" format="PDF" name="${invoiceInstance?.name.replace(' ','_')}-en"
                                controller="report"
                                action="invoiceReport">
                    <input type="hidden" name="client_id" value="${invoiceInstance.client.id}"/>
                    <input type="hidden" name="invoice_id" value="${invoiceInstance.id}"/>
                    <input type="hidden" name="currencyCode" value="${invoiceInstance.inCurrency.currencyCode}" />
                    <input type="hidden" name="invoice_body" value="In"/>
                    <input type="hidden" name="language" value="ENG" />
                </g:jasperReport>

                <g:jasperReport jasper="InvoiceReport" format="PDF" name="Applet Signing (MK)"
                                controller="invoiceDocument"
                                action="pushFileToSigningApplet">
                    <input type="hidden" name="client_id" value="${invoiceInstance.client.id}"/>
                    <input type="hidden" name="invoice_id" value="${invoiceInstance.id}"/>
                    <input type="hidden" name="currencyCode" value="${invoiceInstance.inCurrency.currencyCode}" />
                    <input type="hidden" name="invoice_body" value="In"/>
                    <input type="hidden" name="language" value="MKD" />
                </g:jasperReport>

                <g:form controller="invoiceDocument" action="transferInvoiceToMongo" method="post">
                    <input type="hidden" name="client_id" value="${invoiceInstance.client.id}"/>
                    <input type="hidden" name="invoice_id" value="${invoiceInstance.id}"/>
                    <input type="hidden" name="currencyCode" value="${invoiceInstance.inCurrency.currencyCode}" />
                    <input type="hidden" name="invoice_body" value="In"/>
                    <input type="hidden" name="language" value="MKD" />

                    <fieldset class="buttons">
                        <g:actionSubmit class="save" action="transferInvoiceToMongo"
                                        value="${message(code: 'invoice.transferInvoice.button', default: 'Save as PDF')}"/>
                    </fieldset>
                </g:form>

            </div>
        </g:if>

        <g:if test="${invoiceInstance?.invoiceBodyOut && invoiceInstance?.invoiceBodyOut.invoiceItems.size()>0}">
            <div id="showInvoiceBodyOut">
                <div class="currencyTab">
                    <p><g:message code="invoice.outCurrency.desc" default="Currency: "/>${invoiceInstance.outCurrency}</p>
                </div>
                <div id="invoiceBodyOutTable" name="invoiceBodyOutTable" class="invoiceBodyTable">
                    <table id="tableBodyOut">
                        <thead>
                        <tr>
                            <td>Description</td>
                            <td>Quantity</td>
                            <td>Quantity Unit</td>
                            <td>Unit Price</td>
                            <td>Tax (%)</td>
                            <td>Tax Price Per Unit</td>
                            <td>Tax-Free Price</td>
                            <td>Tax Price</td>
                            <td>Total Price</td>
                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${invoiceInstance.invoiceBodyOut?.invoiceItems}" status="i" var="invoiceItem">
                            <tr>
                                <td>${raw(invoiceItem?.description)}</td>
                                <td class="number"> ${invoiceItem?.quantity}</td>
                                <td class="number"> ${invoiceItem?.quantityUnit}</td>
                                <td class="number">${invoiceItem?.unitPrice.decodeDecimalNumber()}</td>
                                <td class="number">${invoiceItem?.tax.value}</td>
                                <td class="number">${invoiceItem?.taxPricePerUnit.decodeDecimalNumber()}</td>
                                <td class="number">${invoiceItem?.taxFreePrice.decodeDecimalNumber()}</td>
                                <td class="number">${invoiceItem?.taxPrice.decodeDecimalNumber()}</td>
                                <td class="number">${invoiceItem?.totalPrice.decodeDecimalNumber()}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>

                <div id="invoiceBodyOutSumary" name="invoiceBodySummary" class="sumStyle">
                    <table id="tableSummaryBodyOut">
                        <tbody>
                        <tr>
                            <td>Tax-Free Sum</td>
                            <td class="number">${invoiceInstance.invoiceBodyOut?.taxFreeSum.decodeDecimalNumber()}</td>
                            <td class="number">${invoiceInstance.invoiceBodyOut?.taxFreeSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber()}</td>
                        </tr>
                        <tr>
                            <td>Tax Sum</td>
                            <td class="number">${invoiceInstance.invoiceBodyOut?.taxPriceSum.decodeDecimalNumber()}</td>
                            <td class="number">${invoiceInstance.invoiceBodyOut?.taxPriceSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber()}</td>
                        </tr>
                        <tr>
                            <td>Total Sum</td>
                            <td class="number">${invoiceInstance.invoiceBodyOut?.totalSum.setScale(2,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber()}</td>
                            <td class="number">${invoiceInstance.invoiceBodyOut?.getTotalSumRounded().decodeDecimalNumber()}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <g:jasperReport jasper="InvoiceReport" format="PDF" name="${invoiceInstance?.name.replace(' ','_')}"
                                controller="report"
                                action="invoiceReportOut">
                    <input type="hidden" name="client_id" value="${invoiceInstance.client.id}"/>
                    <input type="hidden" name="invoice_id" value="${invoiceInstance.id}"/>
                    <input type="hidden" name="currencyCode" value="${invoiceInstance.outCurrency.currencyCode}" />
                    <input type="hidden" name="invoice_body" value="Out"/>
                    <input type="hidden" name="language" value="MKD" />
                </g:jasperReport>

                <g:jasperReport jasper="InvoiceReport" format="PDF" name="${invoiceInstance?.name.replace(' ','_')}-en"
                                controller="report"
                                action="invoiceReportOut">
                    <input type="hidden" name="client_id" value="${invoiceInstance.client.id}"/>
                    <input type="hidden" name="invoice_id" value="${invoiceInstance.id}"/>
                    <input type="hidden" name="currencyCode" value="${invoiceInstance.outCurrency.currencyCode}" />
                    <input type="hidden" name="invoice_body" value="Out"/>
                    <input type="hidden" name="language" value="ENG" />
                </g:jasperReport>

                <g:jasperReport jasper="InvoiceReport" format="PDF" name="Applet Signing (MK)"
                                controller="invoiceDocument"
                                action="pushFileToSigningApplet">
                    <input type="hidden" name="client_id" value="${invoiceInstance.client.id}"/>
                    <input type="hidden" name="invoice_id" value="${invoiceInstance.id}"/>
                    <input type="hidden" name="currencyCode" value="${invoiceInstance.outCurrency.currencyCode}" />
                    <input type="hidden" name="invoice_body" value="Out"/>
                    <input type="hidden" name="language" value="MKD" />
                </g:jasperReport>

                <g:form controller="invoiceDocument" action="transferInvoiceToMongo" method="post">
                    <input type="hidden" name="client_id" value="${invoiceInstance.client.id}"/>
                    <input type="hidden" name="invoice_id" value="${invoiceInstance.id}"/>
                    <input type="hidden" name="currencyCode" value="${invoiceInstance.outCurrency.currencyCode}" />
                    <input type="hidden" name="invoice_body" value="Out"/>
                    <input type="hidden" name="language" value="MKD" />

                    <fieldset class="buttons">
                        <g:actionSubmit class="save" action="transferInvoiceToMongo"
                                        value="${message(code: 'invoice.transferInvoice.button', default: 'Save as PDF')}"/>
                    </fieldset>
                </g:form>

            </div>
        </g:if>

    </div>
</g:if>
<g:else>
    <g:if test="${invoiceInstance?.invoiceBodyIn && invoiceInstance?.invoiceBodyIn.invoiceItems.size()>0}">
        <div id="showInvoiceBody">
            <div class="currencyTab">
                <p><g:message code="invoice.inCurrency.desc" default="Currency: "/>${invoiceInstance.inCurrency}</p>
            </div>
            <div id="invoiceBodyTable" name="invoiceBodyTable" class="invoiceBodyTable">
                <table id="tableBody">
                    <thead>
                    <tr>
                        <td>Description</td>
                        <td>Quantity</td>
                        <td>Quantity Unit</td>
                        <td>Unit Price</td>
                        <td>Tax (%)</td>
                        <td>Tax Price Per Unit</td>
                        <td>Tax-Free Price</td>
                        <td>Tax Price</td>
                        <td>Total Price</td>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${invoiceInstance.invoiceBodyIn?.invoiceItems}" status="i" var="invoiceItem">
                        <tr>
                            <td>${raw(invoiceItem?.description)}</td>
                            <td class="number"> ${invoiceItem?.quantity}</td>
                            <td class="number"> ${invoiceItem?.quantityUnit}</td>
                            <td class="number">${invoiceItem?.unitPrice.decodeDecimalNumber()}</td>
                            <td class="number">${invoiceItem?.tax.value}</td>
                            <td class="number">${invoiceItem?.taxPricePerUnit.decodeDecimalNumber()}</td>
                            <td class="number">${invoiceItem?.taxFreePrice.decodeDecimalNumber()}</td>
                            <td class="number">${invoiceItem?.taxPrice.decodeDecimalNumber()}</td>
                            <td class="number">${invoiceItem?.totalPrice.decodeDecimalNumber()}</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>

            <div id="invoiceBodySumary" name="invoiceBodySummary" class="sumStyle">
                <table id="tableSummaryBody">
                    <tbody>
                    <tr>
                        <td>Tax-Free Sum</td>
                        <td class="number">${invoiceInstance.invoiceBodyIn?.taxFreeSum.decodeDecimalNumber()}</td>
                        <td class="number">${invoiceInstance.invoiceBodyIn?.taxFreeSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber()}</td>
                    </tr>
                    <tr>
                        <td>Tax Sum</td>
                        <td class="number">${invoiceInstance.invoiceBodyIn?.taxPriceSum.decodeDecimalNumber()}</td>
                        <td class="number">${invoiceInstance.invoiceBodyIn?.taxPriceSum.setScale(0,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber()}</td>
                    </tr>
                    <tr>
                        <td>Total Sum</td>
                        <td class="number">${invoiceInstance.invoiceBodyIn?.totalSum.setScale(2,java.math.RoundingMode.HALF_EVEN).decodeDecimalNumber()}</td>
                        <td class="number">${invoiceInstance.invoiceBodyIn?.getTotalSumRounded().decodeDecimalNumber()}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <g:jasperReport jasper="InvoiceReport" format="PDF" name="${invoiceInstance?.name.replace(' ','_')}"
                            controller="report"
                            action="invoiceReport">
                <input type="hidden" name="client_id" value="${invoiceInstance.client.id}"/>
                <input type="hidden" name="invoice_id" value="${invoiceInstance.id}"/>
                <input type="hidden" name="currencyCode" value="${invoiceInstance.inCurrency.currencyCode}" />
                <input type="hidden" name="invoice_body" value="In"/>
                <input type="hidden" name="language" value="MKD" />
            </g:jasperReport>

            <g:jasperReport jasper="InvoiceReport" format="PDF" name="${invoiceInstance?.name.replace(' ','_')}-en"
                            controller="report"
                            action="invoiceReport">
                <input type="hidden" name="client_id" value="${invoiceInstance.client.id}"/>
                <input type="hidden" name="invoice_id" value="${invoiceInstance.id}"/>
                <input type="hidden" name="currencyCode" value="${invoiceInstance.inCurrency.currencyCode}" />
                <input type="hidden" name="invoice_body" value="In"/>
                <input type="hidden" name="language" value="ENG" />
            </g:jasperReport>

            <g:jasperReport jasper="InvoiceReport" format="PDF" name="Applet Signing (MK)"
                            controller="invoiceDocument"
                            action="pushFileToSigningApplet">
                <input type="hidden" name="client_id" value="${invoiceInstance.client.id}"/>
                <input type="hidden" name="invoice_id" value="${invoiceInstance.id}"/>
                <input type="hidden" name="currencyCode" value="${invoiceInstance.inCurrency.currencyCode}" />
                <input type="hidden" name="invoice_body" value="In"/>
                <input type="hidden" name="language" value="MKD" />
            </g:jasperReport>

            <g:form controller="invoiceDocument" action="transferInvoiceToMongo" method="post">
                <input type="hidden" name="client_id" value="${invoiceInstance.client.id}"/>
                <input type="hidden" name="invoice_id" value="${invoiceInstance.id}"/>
                <input type="hidden" name="currencyCode" value="${invoiceInstance.inCurrency.currencyCode}" />
                <input type="hidden" name="invoice_body" value="In"/>
                <input type="hidden" name="language" value="MKD" />

                <fieldset class="buttons">
                    <g:actionSubmit class="save" action="transferInvoiceToMongo"
                                    value="${message(code: 'invoice.transferInvoice.button', default: 'Save as PDF')}"/>
                </fieldset>
            </g:form>

        </div>
    </g:if>
</g:else>