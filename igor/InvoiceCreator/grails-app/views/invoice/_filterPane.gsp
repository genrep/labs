<%@ page import="com.genrep.client.Client" %>
<!-- This has been customized to be inline instead of a popup -->
<div id="${fp.containerId}">

    <BR>
    <g:set var="renderForm" value="true"/>

    <g:if test="${renderForm}">
        <form name="${fp.formName}" id="${fp.formName}" method="${fp.formMethod}" action="${createLink(action: fp.formAction)}">
        <input type="hidden" name="domain" id="${fp.domain}" value="${fp.domain}"/>
    </g:if>

    <g:each in="${fp.properties}" var="propMap">

        <g:set var="param" value="${params."${propMap.key}"}"/>

        <div class="fieldcontain">
            <span class="property-label">
                <g:if test="${propMap.key.toString().equals("issueDate")}">
                    <g:set var="paramF" value="${params."${propMap.key}From"}"/>
                    <g:set var="paramT" value="${params."${propMap.key}To"}"/>
                    <label>
                        Issue Date
                    </label>
                </g:if>
                <g:elseif test="${propMap.key.toString().equals("client")}">
                    <label>
                        Client
                    </label>
                </g:elseif>
                <g:elseif test="${propMap.key.toString().equals("invoiceNumber")}">
                    <label>
                        Invoice Number
                    </label>
                </g:elseif>
                <g:elseif test="${propMap.key.toString().equals("status")}">
                    <label>Status</label>
                </g:elseif>
            </span>

            <span class="property-value">
                <g:if test="${propMap.value.toString().equals("class java.lang.String")}">
                    <input type="text" name="${propMap.key}" id="${propMap.key}" value="${param}"/>
                </g:if>
                <g:elseif test="${propMap.value.toString().equals("class com.genrep.client.Client")}">
                    <g:select id="${propMap.key}" name="${propMap.key}" from="${Client.list()}"
                              optionKey="id" optionValue="name"
                              noSelection="${['':'']}"
                              value="${param}" class="many-to-one"/>
                </g:elseif>
                <g:elseif test="${propMap.value.toString().equals("class java.util.Date")}">
                    <input type="hidden" name="${propMap.key}" id="${propMap.key}" value="${param}" />
                    <input type="text" name="${propMap.key}From" id="${propMap.key}From" class="date" value="${paramF}"
                           onchange="filterPaneDateChecker('${propMap.key.toString()}')"/>
                    <input type="text" name="${propMap.key}To" id="${propMap.key}To" class="date" value="${paramT}"
                           onchange="filterPaneDateChecker('${propMap.key.toString()}')"/>
                </g:elseif>
                <g:elseif test="${propMap.value.toString().equals("class com.genrep.invoiceCreator.Statuses")}">
                    <g:select id="${propMap.key}" name="${propMap.key}" from="${com.genrep.invoiceCreator.Statuses.values()}"
                              optionValue="name"
                              noSelection="${['':'']}"
                              value="${param}" class="many-to-one"/>
                </g:elseif>
            </span>
        </div>

    </g:each>

    <div class="buttons">
        <span>
            <g:actionSubmit value="Apply" action="${fp?.action}" />
        </span>
        <span>
            <input type="button" value="Clear Filters" onclick="filterPaneClear('${fp.containerId.toString()}')" />
        </span>
    </div>

    <g:if test="${renderForm}">
        </form>
    </g:if>
</div>
