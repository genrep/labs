<%--
  Created by IntelliJ IDEA.
  User: igor
  Date: 7/10/14
  Time: 3:32 PM
--%>

<%@ page import="com.genrep.invoiceCreator.ExchangeRate; com.genrep.invoiceCreator.Invoice" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
    <asset:javascript src="invoice.js"/>
</head>

<body>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="menuButton" action="create"><g:message code="default.new.label"
                                                                  args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-invoice-documents" class="content scaffold-show" role="main">
    <h1><g:link action="show" id="${invoiceId}"><g:message code="default.showDocuments.label" default="Show Invoice Documents"/></g:link></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <g:if test="${invoiceDocuments}">
        <div class="documents-list">
            <g:each in="${invoiceDocuments}" status="i" var="invoiceDocument">
                <g:form id="${invoiceDocument.id}" controller="invoiceDocument" action="delete" method="post">
                <span>
                    <g:hiddenField name="invoiceId" value="${invoiceId}" />
                    <g:link class="doc-${invoiceDocument.mimeType.split('/')[1]}" controller="worker" action="readFileFromMongoDb" target="_blank"
                            params="[docMongoId: invoiceDocument.docMongoId, domain:'InvoiceDocument']">
                        ${invoiceDocument.documentName}
                    </g:link>
                    <label>[${invoiceDocument.dateCreated.format('dd.MM.yyyy')}]</label>
                    <g:actionSubmit class="delete" action="delete"
                                    value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                                    onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                </g:form>
                </span>
            </g:each>
        </div>
    </g:if>
    <g:else>
        <h2><g:message code="invoice.documents.notFound" default="No documents found for this Invoice"/></h2>
    </g:else>

</div>
</body>
</html>
