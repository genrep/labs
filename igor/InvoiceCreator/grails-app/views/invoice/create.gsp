<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>

    <asset:javascript src="jquery.jalert.js"/>
    <asset:javascript src="invoice.js"/>
    <asset:javascript src="client.js"/>

</head>

<body>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="create-invoice" class="content scaffold-create" role="main">
    <h1><g:message code="default.create.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${invoiceInstance}">
        <ul class="errors" role="alert">
            <g:eachError bean="${invoiceInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form url="[resource: invoiceInstance, action: 'save']">
        <fieldset class="form">
            <g:render template="form"/>
        </fieldset>
        <fieldset class="buttons">
            <g:submitButton name="create" class="save"
                            value="${message(code: 'default.button.create.label', default: 'Create')}"/>
            <g:link class="cancel" action="index">
                <g:message code="default.button.copy.label" default="Cancel"/>
            </g:link>
        </fieldset>
    </g:form>

    <div class="includedTemplate">
        <g:include view="client/_form.gsp"/>
    </div>
</div>
</body>
</html>
