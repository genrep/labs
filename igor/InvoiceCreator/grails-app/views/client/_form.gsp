<%@ page import="com.genrep.client.Client" %>
<div id="client-form" title="Add New Client">

    <g:hasErrors bean="${clientInstance}">
        <ul id="erol" class="errors" role="alert">
            <g:eachError bean="${clientInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>>
                    <g:message
                            error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

    <div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'name', 'error')} ">
        <span class="property-label required">
            <label for="name">
                <g:message code="client.name.label" default="Name" />

            </label>
        </span>
        <span class="property-value">
            <g:textField name="name" value="${clientInstance?.name}"/>
        </span>

    </div>

    <div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'department', 'error')} ">
        <span class="property-label">
            <label for="department">
                <g:message code="client.department.label" default="Department" />

            </label>
        </span>
        <span class="property-value">
            <g:textField name="department" value="${clientInstance?.department}"/>
        </span>

    </div>

    <div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'address', 'error')} ">
        <span class="property-label">
            <label for="address">
                <g:message code="client.address.label" default="Address" />

            </label>
        </span>
        <span class="property-value">
            <g:textField name="address" value="${clientInstance?.address}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'bankAccountNumber', 'error')} ">
        <span class="property-label">
            <label for="bankAccountNumber">
                <g:message code="client.bankAccountNumber.label" default="Bank Account Number" />

            </label>
        </span>
        <span class="property-value">
            <g:textField name="bankAccountNumber" value="${clientInstance?.bankAccountNumber}"/>
        </span>

    </div>

    <div class="fieldcontain ${hasErrors(bean: clientInstance, field: 'viewInfo', 'error')} ">
        <span class="property-label">
            <label for="viewInfo">
                <g:message code="client.viewInfo.label" default="Invoice Print" />
            </label>
        </span>
        %{--<span class="property-value">--}%
            %{--<g:textArea name="viewInfo" id="viewInfo" value="${clientInstance?.viewInfo}"/>--}%
        %{--</span>--}%
        <div class="jqte-popup">
            <span class="property-value">
                <g:textArea name="viewInfo" id="viewInfo" cols="40" rows="5" maxlength="500"
                            value="${clientInstance?.viewInfo}"
                           />
            </span>
        </div>
    </div>

</div>