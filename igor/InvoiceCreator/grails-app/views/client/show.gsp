
<%@ page import="com.genrep.client.Client" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'client.label', default: 'Client')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<a href="#show-client" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="menuButton" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="show-client" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list client">


        <g:if test="${clientInstance?.name}">
            <li class="fieldcontain">
                <span id="name-label" class="property-label"><g:message code="client.name.label" default="Name" /></span>

                <span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${clientInstance}" field="name"/></span>

            </li>
        </g:if>

        <g:if test="${clientInstance?.department}">
            <li class="fieldcontain">
                <span id="department-label" class="property-label"><g:message code="client.department.label" default="Department" /></span>

                <span class="property-value" aria-labelledby="department-label"><g:fieldValue bean="${clientInstance}" field="department"/></span>

            </li>
        </g:if>

        <g:if test="${clientInstance?.address}">
            <li class="fieldcontain">
                <span id="address-label" class="property-label"><g:message code="client.address.label" default="Address" /></span>

                <span class="property-value" aria-labelledby="address-label"><g:fieldValue bean="${clientInstance}" field="address"/></span>

            </li>
        </g:if>


        <g:if test="${clientInstance?.bankAccount}">
            <li class="fieldcontain">
                <span id="bankAccountNumber-label" class="property-label"><g:message code="client.bankAccountNumber.label" default="Bank Account No." /></span>

                <span class="property-value" aria-labelledby="bankAccountNumber-label">${clientInstance.bankAccount.bankAccountNumber}</span>

            </li>
        </g:if>

        <g:if test="${clientInstance?.viewInfo}">
            <li class="fieldcontain">
                <span id="viewInfo-label" class="property-label"><g:message code="client.viewInfo.label" default="Invoice Print" /></span>

                <span class="property-value" aria-labelledby="viewInfo-label">
                    ${raw(clientInstance.viewInfo)}</span>

            </li>
        </g:if>
    </ol>
    <g:form url="[resource:clientInstance, action:'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${clientInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
