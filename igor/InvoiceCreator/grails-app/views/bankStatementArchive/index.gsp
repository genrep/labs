
<%@ page import="com.genrep.statements.BankStatementArchive" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'bankStatementArchive.label', default: 'BankStatementArchive')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-bankStatementArchive" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="menuButton" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-bankStatementArchive" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="bankStatementId" title="${message(code: 'bankStatementArchive.bankStatementId.label', default: 'Bank Statement Id')}" />
					
						<th><g:message code="bankStatementArchive.ownerOfStatement.label" default="Owner Of Statement" /></th>
					
						<g:sortableColumn property="documentFile" title="${message(code: 'bankStatementArchive.documentFile.label', default: 'Document File')}" />
					
						<g:sortableColumn property="documentName" title="${message(code: 'bankStatementArchive.documentName.label', default: 'Document Name')}" />
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'bankStatementArchive.dateCreated.label', default: 'Date Created')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${bankStatementArchiveInstanceList}" status="i" var="bankStatementArchiveInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${bankStatementArchiveInstance.id}">${fieldValue(bean: bankStatementArchiveInstance, field: "bankStatementId")}</g:link></td>

						<td>${fieldValue(bean: bankStatementArchiveInstance, field: "ownerOfStatement")}</td>
					
						%{--<td>${fieldValue(bean: bankStatementArchiveInstance, field: "documentFile")}</td>--}%
                        <td><g:link
                                controller="worker" action="readFileFromMongoDb" target="_blank"
                                params="[docMongoId: bankStatementArchiveInstance.docMongoId, domain: 'BankStatementArchive']">
                            ${bankStatementArchiveInstance.documentName?:"link"}
                        </g:link></td>
					
						<td>${fieldValue(bean: bankStatementArchiveInstance, field: "documentName")}</td>

						<td><g:formatDate date="${bankStatementArchiveInstance.dateCreated}" format="dd.MM.yyyy" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${bankStatementArchiveInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
