<%--
  Created by IntelliJ IDEA.
  User: igor
  Date: 9/24/14
  Time: 11:02 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'bankStatementArchive.label', default: 'BankStatementArchive')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
    <asset:javascript src="statement.js"/>
</head>
<body>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="bankStatmentArchive.list" default="List Bank Statements PDFs" /></g:link></li>
    </ul>
</div>
<div id="create-bankStatementArchive" class="content scaffold-create" role="main">
    <h1><g:message code="bankStatementArchive.multiImportlabel" default="Upload Bank Statements PDFs" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>

    <g:uploadForm action="saveMultiImport" method="post">

        <fieldset class="form">
            <g:render template="fileForm"/>
        </fieldset>

        <fieldset class="buttons">
            <g:submitButton name="saveMultiImport" class="save" value="${message(code: 'bankStatementArchive.multiImport.create.label', default: 'Upload')}" />
        </fieldset>
    </g:uploadForm>
</div>
</body>
</html>
