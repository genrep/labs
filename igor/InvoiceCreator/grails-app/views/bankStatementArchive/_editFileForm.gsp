<g:if test="${bankStatementArchiveInstance?.documentFile}">
    <div class="fieldcontain">
        <span id="documentFile-label" class="property-label"><g:message code="invoicePurchaseArchive.documentFile.label" default="Document File" /></span>
        <span  class="property-value"><g:link controller="invoicePurchaseArchive" action="showInvoicePurchaseArchiveFile" target="_blank"
                                              id="${invoicePurchaseArchiveInstance?.id}">
            ${bankStatementArchiveInstance?.documentName?:'Statement Document'}
        </g:link>
        </span>
    </div>
</g:if>
<g:else>
    <div class="fieldcontain ${hasErrors(bean: bankStatementArchiveInstance, field: 'documentFile', 'error')}">
        <span class="property-label">
            <label for="documentFile">
                <g:message code="invoicePurchaseArchive.documentFile.label" default="Document File" />
            </label>
        </span>
        <span class="property-value">
            <input type="file" id="documentFile" name="documentFile" onchange="setFileName(this,'documentName')" />
        </span>
    </div>
</g:else>

<g:if test="${bankStatementArchiveInstance?.documentName}">
    <div class="fieldcontain">
        <span id="documentName-label" class="property-label"><g:message code="invoicePurchaseArchive.documentName.label" default="Document Name" /></span>

        <span class="property-value" aria-labelledby="documentName-label"><g:fieldValue bean="${bankStatementArchiveInstance}" field="documentName"/></span>

    </div>
</g:if>
<g:else>
    <div class="fieldcontain ${hasErrors(bean: bankStatementArchiveInstance, field: 'documentName', 'error')}">
        <span class="property-label">
            <label for="documentName">
                <g:message code="invoicePurchaseArchive.documentName.label" default="Document Name" />
            </label>
        </span>
        <span class="property-value">
            <g:textField class="wider-fields" name="documentName" id="documentName" value="${bankStatementArchiveInstance?.documentName}"/>
        </span>
    </div>
</g:else>

<g:if test="${bankStatementArchiveInstance?.documentFile}">
    <div class="itemButtons">

        <input id="deleteFile" type="button" value="Delete File" class="buttonsMine deleteButton"
               onclick="deleteDocument(${bankStatementArchiveInstance.id},'statements/archives')"/>
    </div>
</g:if>