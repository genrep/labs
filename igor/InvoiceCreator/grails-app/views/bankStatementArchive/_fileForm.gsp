<div id="uploadForm">
    <div class="fieldcontain">
        <span class="property-label required">
            <label for="statementDoc">
                <g:message code="bankStatementArchive.statementDoc.label" default="Statement Doc" />
            </label>
        </span>
        <span class="property-value">
            <input type="file" id="statementDoc" name="statementDoc" onchange="setFileName(this,'documentName')" />
        </span>

    </div>

    <div class="fieldcontain">
        <span class="property-label">
            <label for="documentName">
                <g:message code="bankStatementArchive.documentName.label" default="Document Name" />
            </label>
        </span>
        <span class="property-value">
            <g:textField class="wider-fields" name="documentName" id="documentName"/>
        </span>
    </div>

    <div class="fieldcontain">
        <span class="property-label required">
            <label for="bankStatementId">
                <g:message code="bankStatementArchive.documentName.label" default="Bank Statement Id" />
            </label>
        </span>
        <span class="property-value">
            <g:textField class="wider-fields" name="bankStatementId" id="bankStatementId"/>
        </span>
    </div>

    <div class="fieldcontain">
        <span class="property-label">
            <label for="issueDate">
                <g:message code="bankStatementArchive.issueDate.label" default="Issue Date" />
            </label>
        </span>
        <span class="property-value">
            <input type="text" name="issueDate" id="issueDate" class="date"/>
        </span>
    </div>

</div>

<div class="fieldcontain">
    <span class="property-value">
        <input type='button' class="buttonsMine" name='submit' value='Add Statement' onclick="uploadStatementArchive()" />
    </span>
</div>

<div class="fieldcontain">
    <span class="property-value">
        <div id="progressBar">
            <div>
            </div>
        </div>
    </span>
</div>

<div id="uploadedStatements" class="document-file-div">
    <table id="statementsTable" class="statementsTable">
        <tbody></tbody>
    </table>
</div>

