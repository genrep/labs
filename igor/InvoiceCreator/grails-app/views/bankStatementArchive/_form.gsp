<%@ page import="com.genrep.statements.BankStatementArchive" %>


<div class="fieldcontain ${hasErrors(bean: bankStatementArchiveInstance, field: 'bankStatementId', 'error')}">
	<span class="property-label required">
		<label for="bankStatementId">
			<g:message code="bankStatementArchive.bankStatementId.label" default="Bank Statement Id" />
		</label>
	</span>
	<span class="property-value">
		<g:textField class="wider-fields" name="bankStatementId" required="" value="${bankStatementArchiveInstance?.bankStatementId}"/>
	</span>
</div>

<div class="fieldcontain ${hasErrors(bean: bankStatementArchiveInstance, field: 'documentFile', 'error')}">
	<span class="property-label">
		<label for="documentFile">
			<g:message code="bankStatementArchive.documentFile.label" default="Document File" />
			<span class="required-indicator">*</span>
		</label>
	</span>
	<span class="property-value">
		<input type="file" id="documentFile" name="documentFile" />
	</span>
</div>

<div class="fieldcontain ${hasErrors(bean: bankStatementArchiveInstance, field: 'documentName', 'error')}">
	<span class="property-label">
		<label for="documentName">
			<g:message code="bankStatementArchive.documentName.label" default="Document Name" />
		</label>
	</span>
	<span class="property-value">
		<g:textField name="documentName" value="${bankStatementArchiveInstance?.documentName}"/>
	</span>
</div>

<div class="fieldcontain ${hasErrors(bean: bankStatementArchiveInstance, field: 'ownerOfStatement', 'error')}">
	<span class="property-label">
		<label for="ownerOfStatement">
			<g:message code="bankStatementArchive.ownerOfStatement.label" default="Owner Of Statement" />
		</label>
	</span>
	<span class="property-value">
		<g:select id="bankAccount" name="bankAccount.id" from="${com.genrep.account.BankAccount.list()}" optionKey="id"
				  value="${bankStatementArchiveInstance?.ownerOfStatement}" class="many-to-one" noSelection="['null': '']"/>
	</span>
</div>


<div class="fieldcontain ${hasErrors(bean: bankStatementArchiveInstance, field: 'issueDate', 'error')}">
	<span class="property-label">
		<label for="issueDate">
			<g:message code="bankStatementArchive.issueDate.label" default="Issue Date" />
		</label>
	</span>
	<span class="property-value">
		<input type="text" name="issueDate" id="issueDate" class="date"
			   value="${formatDate(date:bankStatementArchiveInstance.issueDate?:new Date(),format: 'dd.MM.yyyy')}"/>
	</span>
</div>

