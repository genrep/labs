
<%@ page import="com.genrep.statements.BankStatementArchive" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'bankStatementArchive.label', default: 'BankStatementArchive')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<a href="#show-bankStatementArchive" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
	<ul>
		<li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
		<li><g:link class="menuButton" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
		<li><g:link class="menuButton" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
	</ul>
</div>
<div id="show-bankStatementArchive" class="content scaffold-show" role="main">
	<h1><g:message code="default.show.label" args="[entityName]" /></h1>
	<g:if test="${flash.message}">
		<div class="message" role="status">${flash.message}</div>
	</g:if>
	<ol class="property-list bankStatementArchive">


		<g:if test="${bankStatementArchiveInstance?.bankStatementId}">
			<li class="fieldcontain">
				<span id="bankStatementId-label" class="property-label"><g:message code="bankStatementArchive.bankStatementId.label" default="Bank Statement Id" /></span>

				<span class="property-value" aria-labelledby="bankStatementId-label"><g:fieldValue bean="${bankStatementArchiveInstance}" field="bankStatementId"/></span>

			</li>
		</g:if>

		<g:if test="${bankStatementArchiveInstance?.ownerOfStatement}">
			<li class="fieldcontain">
				<span id="ownerOfStatement-label" class="property-label"><g:message code="bankStatementArchive.ownerOfStatement.label" default="Owner Of Statement" /></span>

				<span class="property-value" aria-labelledby="ownerOfStatement-label">
					<g:link controller="bankAccount" action="show" id="${bankStatementArchiveInstance?.ownerOfStatement}">
						${com.genrep.account.BankAccount.get(bankStatementArchiveInstance?.ownerOfStatement).toString()}
					</g:link>
				</span>
			</li>
		</g:if>


		<g:if test="${bankStatementArchiveInstance?.documentFile}">
			<li class="fieldcontain">
				<span id="documentFile-label" class="property-label"><g:message code="bankStatementArchive.documentFile.label" default="Document File" /></span>
				<span class="property-value">
					<g:link controller="bankStatementArchive" action="getDocument" target="_blank"
							id="${bankStatementArchiveInstance?.id}">
						${bankStatementArchiveInstance?.documentName?:'Statement Document'}
					</g:link>
				</span>
			</li>
		</g:if>

		<g:if test="${bankStatementArchiveInstance?.documentName}">
			<li class="fieldcontain">
				<span id="documentName-label" class="property-label"><g:message code="bankStatementArchive.documentName.label" default="Document Name" /></span>

				<span class="property-value" aria-labelledby="documentName-label"><g:fieldValue bean="${bankStatementArchiveInstance}" field="documentName"/></span>

			</li>
		</g:if>

		<g:if test="${bankStatementArchiveInstance?.dateCreated}">
			<li class="fieldcontain">
				<span id="dateCreated-label" class="property-label"><g:message code="bankStatementArchive.dateCreated.label" default="Date Created" /></span>

				<span class="property-value" aria-labelledby="dateCreated-label">
					<g:formatDate date="${bankStatementArchiveInstance?.dateCreated}" format="dd.MM.yyyy" /></span>

			</li>
		</g:if>

		<g:if test="${bankStatementArchiveInstance?.issueDate}">
			<li class="fieldcontain">
				<span id="issueDate-label" class="property-label"><g:message code="bankStatementArchive.issueDate.label" default="Issue Date" /></span>

				<span class="property-value" aria-labelledby="issueDate-label">
					<g:formatDate date="${bankStatementArchiveInstance?.issueDate}" format="dd.MM.yyyy" /></span>

			</li>
		</g:if>

	</ol>
	<g:form url="[resource:bankStatementArchiveInstance, action:'delete']" method="DELETE">
		<fieldset class="buttons">
			<g:link class="edit" action="edit" resource="${bankStatementArchiveInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
			<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
		</fieldset>
	</g:form>
</div>
</body>
</html>
