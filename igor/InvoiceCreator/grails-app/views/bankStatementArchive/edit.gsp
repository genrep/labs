<%@ page import="com.genrep.statements.BankStatementArchive" %>
<!DOCTYPE html>
<html>
<head>
	<meta name="layout" content="main">
	<g:set var="entityName" value="${message(code: 'bankStatementArchive.label', default: 'BankStatementArchive')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>

	<asset:javascript src="statement.js"/>

</head>
<body>
<a href="#edit-bankStatementArchive" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
	<ul>
		<li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
		<li><g:link class="menuButton" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
		<li><g:link class="menuButton" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
	</ul>
</div>
<div id="edit-bankStatementArchive" class="content scaffold-edit" role="main">
	<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
	<g:if test="${flash.message}">
		<div class="message" role="status">${flash.message}</div>
	</g:if>
	<g:hasErrors bean="${bankStatementArchiveInstance}">
		<ul class="errors" role="alert">
			<g:eachError bean="${bankStatementArchiveInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
			</g:eachError>
		</ul>
	</g:hasErrors>
	<g:form url="[resource:bankStatementArchiveInstance, action:'update']" method="POST"  enctype="multipart/form-data">
		<g:hiddenField name="version" value="${bankStatementArchiveInstance?.version}" />
		<fieldset class="form">
			<g:render template="editForm"/>
		</fieldset>
		<fieldset class="form">
			<div id="editFileForm">
				<g:render template="editFileForm"/>
			</div>
		</fieldset>
		<fieldset class="buttons">
			<g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
		</fieldset>
	</g:form>
</div>
</body>
</html>
