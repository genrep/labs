<%@ page import="com.genrep.invoiceCreator.Currencies; com.genrep.client.Client" %>
<!-- This has been customized to be inline instead of a popup -->
<div id="${fp.containerId}"

<BR>
<g:set var="renderForm" value="true"/>

<g:if test="${renderForm}">
    <form name="${fp.formName}" id="${fp.formName}" method="${fp.formMethod}" action="${createLink(action: fp.formAction)}">
    <input type="hidden" name="domains" id="domains" value="${fp.domains}"/>
</g:if>

<g:each in="${fp.properties}" var="propMap">

    <div class="fieldcontain">
        <span class="property-label">
            <g:if test="${propMap.key.toString().equals("issueDate")}">
                <g:set var="paramF" value="${params."${propMap.key}From"}"/>
                <g:set var="paramT" value="${params."${propMap.key}To"}"/>
                <g:set var="param" value="${params."${propMap.key}"}"/>
                <label>
                    Issue Date
                </label>
            </g:if>
            <g:elseif test="${propMap.key.toString().equals("client")}">
                <g:set var="param" value="${params."${propMap.key}"}"/>
                <label>
                    Client
                </label>
            </g:elseif>
            <g:elseif test="${propMap.key.toString().equals("invoiceNumber")}">
                <g:set var="param" value="${params."${propMap.key}"}"/>
                <label>
                    Invoice Number
                </label>
            </g:elseif>
            <g:elseif test="${propMap.key.toString().equals("inCurrency")}">
                <g:set var="curr" value="${params."${propMap.key}"}"/>
                <g:if test="${curr!=null}">
                    <g:set var="param" value="${Currencies."${curr}".currency}"/>
                </g:if>
                <g:else>
                    <g:set var="param" value="${null}" />
                </g:else>
                <label>
                    Currency
                </label>
            </g:elseif>

        </span>
        <span class="property-value">
            <g:if test="${propMap.value.toString().equals("class java.lang.String")}">
                <input type="text" name="${propMap.key}" id="${propMap.key}" value="${param}"
                />
            </g:if>
            <g:elseif test="${propMap.value.toString().equals("class com.genrep.client.Client")}">
                <g:select id="${propMap.key}" name="${propMap.key}" from="${Client.list()}"
                          optionKey="id" optionValue="name"
                          noSelection="${['':'']}"
                          value="${param}" class="many-to-one"/>
            </g:elseif>
            <g:elseif test="${propMap.value.toString().equals("class java.util.Date")}">
                <input type="hidden" name="${propMap.key}" id="${propMap.key}" value="${param}" />
                <label for="${propMap.key}">From: </label>
                <input type="text" name="${propMap.key}From" id="${propMap.key}From" class="date" value="${paramF}"
                       onchange="filterPaneDateChecker('${propMap.key.toString()}')"/>
                <label for="${propMap.key}">To: </label>
                <input type="text" name="${propMap.key}To" id="${propMap.key}To" class="date" value="${paramT}"
                       onchange="filterPaneDateChecker('${propMap.key.toString()}')"/>
            </g:elseif>
            <g:elseif test="${propMap.value.toString().equals("class java.util.Currency")}">
                <g:if test="${param}">
                    <g:currencySelect id="${propMap.key}" name="${propMap.key}"
                                      value="${param}"
                                      noSelection="${[null:'']}"
                                      from="${Currencies.values()}"/>
                </g:if>
                <g:else>
                    <g:currencySelect id="${propMap.key}" name="${propMap.key}"
                                      value="${param}"
                                      noSelection="${[null:'']}" optionKey="currency"
                                      from="${Currencies.values()}"/>
                </g:else>
            </g:elseif>
        </span>
    </div>
</g:each>

<div class="fieldcontain">
    <span class="property-label">    <label>
        Don't include imported invoices
    </label></span>
    <span class="property-value">
        <g:if test="${params?.noImported && params?.noImported.equals('on')}">
            <g:checkBox name="noImported" checked="true"/>
        </g:if>
        <g:else>
            <g:checkBox name="noImported" checked="false"/>
        </g:else>
    </span>
</div>

<div class="buttons">
        <g:actionSubmit class="btn saveButton" value="Apply" action="${fp?.action}" />
        <input type="button" value="Clear Filters" onclick="filterPaneClear('${fp.containerId.toString()}')" />
</div>

<g:if test="${renderForm}">
    </form>
</g:if>
</div>
