<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Invoice Kreator</title>
    <style type="text/css" media="screen">
    #status {
        background-color: #eee;
        border: .2em solid #fff;
        margin: 2em 2em 1em;
        padding: 1em;
        width: 12em;
        float: left;
        -moz-box-shadow: 0px 0px 1.25em #ccc;
        -webkit-box-shadow: 0px 0px 1.25em #ccc;
        box-shadow: 0px 0px 1.25em #ccc;
        -moz-border-radius: 0.6em;
        -webkit-border-radius: 0.6em;
        border-radius: 0.6em;
    }

    .ie6 #status {
        display: inline; /* float double margin fix http://www.positioniseverything.net/explorer/doubled-margin.html */
    }

    #status ul {
        font-size: 0.9em;
        list-style-type: none;
        margin-bottom: 0.6em;
        padding: 0;
    }

    #status li {
        line-height: 1.3;
    }

    #status h1 {
        text-transform: uppercase;
        font-size: 1.1em;
        margin: 0 0 0.3em;
    }

    #page-body {
        margin: 2em 1em 1.25em 1.25em;
    }

    #page-body h1{
        font-size: 2rem;
        text-align: center;
    }

    h2 {
        margin-top: 1em;
        margin-bottom: 0.3em;
        font-size: 1em;
    }

    p {
        line-height: 1.5;
        margin: 0.25em 0;
    }

    #controller-list ul {
        list-style-position: inside;
    }

    #controller-list li {
        line-height: 1.3;
        list-style-position: inside;
        margin: 0.25em 0;
    }

    .linkButton{
        font-size: 2.5rem;
        text-align: center;
        padding: 2rem 3rem;
        background-color: lavender;
        width: 200px;
        margin: 0 auto;
        margin-top: 2rem;

    }
    .linkButton:hover{
        -moz-box-shadow: 0 0 1px 1px #aaaaaa;
        -webkit-box-shadow: 0 0 1px 1px #aaaaaa;
        box-shadow: 0 0 1px 1px #aaaaaa;
    }
    a {
        text-decoration: none;
    }

    @media screen and (max-width: 480px) {
        #status {
            display: none;
        }

        #page-body {
            margin: 0 1em 1em;
        }

        #page-body h1 {
            margin-top: 0;
        }


    }
    </style>
</head>
<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
</div>
<div id="page-body" role="main">
    <h1>Choose a report</h1>

    <g:link action="quartal">
        <div class="linkButton">QUARTAL REPORT</div>
    </g:link>

</div>
</body>
</html>
