<%@ page import="com.genrep.invoiceCreator.Invoice" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'invoice.label', default: 'Invoice')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <asset:javascript src="invoice.js"/>
</head>

<body>

<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" controller="report" action="index">
            <g:message code="invoice.report.backButton" default="Back to Reports"/>
        </g:link></li>
    </ul>
</div>

<div id="list-invoice" class="content scaffold-list" role="main">
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <div>
        <gnrp:multiDomainFilterPane domains="com.genrep.invoiceCreator.Invoice,com.genrep.invoiceRepository.InvoiceArchive"
                                    filterParams="issueDate,client,inCurrency"
        />
    </div>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="invoiceNumber"
                              title="${message(code: 'invoice.invoiceNumber.label', default: 'Invoice Number')}"/>

            <g:sortableColumn property="aClass"
                              title="${message(code: 'invoice.invoiceNumber.label', default: 'Type')}"/>

            <th><g:message code="invoice.client.label" default="Client"/></th>

            <g:sortableColumn property="issueDate"
                              title="${message(code: 'invoice.issueDate.label', default: 'Issue Date')}"/>


            <g:sortableColumn property="totalSum"
                              title="${message(code: 'invoice.totalSum.label', default: 'Total Sum')}"/>

            <g:sortableColumn property="currency"
                              title="${message(code: 'invoice.totalSum.label', default: 'Currency')}"/>


        </tr>
        </thead>
        <tbody>
        <g:each in="${reportList}" status="i" var="invoiceInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td>${fieldValue(bean: invoiceInstance, field: "invoiceNumber")}</td>

                <td>${fieldValue(bean: invoiceInstance, field: "aClass")}</td>

                <td>${fieldValue(bean: invoiceInstance, field: "client")}</td>


                <td><g:formatDate date="${invoiceInstance.issueDate}" format="dd.MM.yyyy"/></td>


                <td>${fieldValue(bean: invoiceInstance, field: "totalSum")}</td>

                <td>${fieldValue(bean: invoiceInstance, field: "currency")}</td>


            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${invoiceInstanceCount ?: 0}" params="${params}"/>
    </div>

    <g:if test="${showStats==true}">
        <fieldset class="form">
            <g:render template="reportStatistics"/>
        </fieldset>
    </g:if>
</div>
</body>
</html>
