<div id="statDiv">
    <ol>
        <li>
            <span class="property-label"><g:message code="reports.quartal.totalInvoices" default="Number of Invoices : " /></span>

            <span class="property-value" >${statMap.get('size')}</span>
        </li>
        <li>
            <span class="property-label"><g:message code="reports.quartal.currency" default="Currency : " /></span>

            <span class="property-value" >${params.inCurrency}</span>
        </li>
        <li>
            <span class="property-label"><g:message code="reports.quartal.totalSum" default="Total Sum (with tax) : " /></span>

            <span class="property-value" >${statMap.get('totalSum').decodeDecimalNumber()}</span>
        </li>
        <li>
            <span class="property-label"><g:message code="reports.quartal.taxFreeSum" default="Total Sum (without tax) : " /></span>

            <span class="property-value" >${statMap.get('taxFreeSum').decodeDecimalNumber()}</span>
        </li>
        <li>
            <span class="property-label"><g:message code="reports.quartal.taxPriceSum" default="Total Sum of Tax : " /></span>

            <span class="property-value" >${statMap.get('taxPriceSum').decodeDecimalNumber()}</span>
        </li>
    </ol>
</div>