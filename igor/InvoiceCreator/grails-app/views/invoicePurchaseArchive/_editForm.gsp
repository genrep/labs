<%@ page import="com.genrep.invoiceRepository.Taxes; com.genrep.invoiceCreator.Currencies; com.genrep.invoiceRepository.InvoicePurchaseArchive" %>

<g:if test="${invoicePurchaseArchiveInstance.isProInvoice}">
    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'invoiceNumber', 'error')}">
        <span class="property-label required">
            <label for="invoiceNumber">
                <g:message code="invoicePurchaseArchive.invoiceNumber.label" default="(Pro)Invoice Number" />
            </label>
        </span>
        <span class="property-value">
            <g:textField name="invoiceNumber" required="" value="${invoicePurchaseArchiveInstance?.invoiceNumber}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'swiftCode', 'error')} ">
        <span class="property-label">
            <label for="swiftCode">
                <g:message code="invoicePurchaseArchive.swiftCode.label" default="Swift Code" />
            </label>
        </span>
        <span class="property-value">
            <g:textField name="swiftCode" value="${invoicePurchaseArchiveInstance?.swiftCode}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'vendor', 'error')}">
        <span class="property-label required">
            <label for="vendor">
                <g:message code="invoicePurchaseArchive.vendor.label" default="Vendor" />
            </label>
        </span>
        <span class="property-value">
            <g:select id="vendor" name="vendor.id" from="${com.genrep.vendor.Vendor.list()}"
                      optionKey="id" required="" value="${invoicePurchaseArchiveInstance?.vendor?.id}"
                      class="many-to-one" noSelection="['':'Please select vendor']"/>

            %{--<input type="button" class="newDialogButton" value="+" onclick="newClientDialog()" />--}%
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'invoiceDescription', 'error')} ">
        <span id="invoiceDescription-label" class="property-label">
            <label for="invoiceDescription">
                <g:message code="invoicePurchaseArchive.invoiceDescription.label" default="Invoice Description" />
            </label>
        </span>
        <span class="property-value" aria-labelledby="description-label">
            <g:textArea name="invoiceDescription" id="invoiceDescription" value="${invoicePurchaseArchiveInstance?.invoiceDescription}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'inCurrency', 'error')}">
        <span class="property-label required">
            <label for="inCurrency">
                <g:message code="invoicePurchaseArchive.inCurrency.label" default="Currency" />
            </label>
        </span>
        <span class="property-value">
            <g:currencySelect name="inCurrency" value="${invoicePurchaseArchiveInstance?.inCurrency?:Currencies.MKD.currency}"
                              from="${com.genrep.invoiceCreator.Currencies.values()}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'totalSum', 'error')}">
        <span class="property-label required">
            <label for="totalSum">
                <g:message code="invoicePurchaseArchive.totalSum.label" default="Total Sum" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="totalSum" name="totalSum" required="" value="${invoicePurchaseArchiveInstance?.totalSum}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'taxFreeSum', 'error')}">
        <span class="property-label required">
            <label for="taxFreeSum">
                <g:message code="invoicePurchaseArchive.taxFreeSum.label" default="Tax Free Sum" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="taxFreeSum" name="taxFreeSum" required="" value="${invoicePurchaseArchiveInstance?.taxFreeSum}"/>
        </span>
    </div>


    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'taxPriceSum', 'error')}">
        <span class="property-label required">
            <label for="taxPriceSum">
                <g:message code="invoicePurchaseArchive.taxPriceSum.label" default="Tax Price Sum" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="taxPriceSum" name="taxPriceSum" required="" value="${invoicePurchaseArchiveInstance?.taxPriceSum}"/>
        </span>
    </div>


    <div class="fieldcontain">
        <span class="property-label">
            <label for="taxes">
                <g:message code="invoiceArchive.taxes.zero" default="Tax (0%)" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="tax0" name="tax0" value="${invoicePurchaseArchiveInstance?.taxes?.get(com.genrep.invoiceRepository.Taxes.ZERO)}"/>
        </span>
    </div>

    <div class="fieldcontain">
        <span class="property-label">
            <label for="taxes">
                <g:message code="invoiceArchive.taxes.five" default="Tax (5%)" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="tax5" name="tax5" value="${invoicePurchaseArchiveInstance?.taxes?.get(Taxes.FIVE)}"/>
        </span>
    </div>

    <div class="fieldcontain">
        <span class="property-label">
            <label for="taxes">
                <g:message code="invoiceArchive.taxes.eighteen" default="Tax (18%)" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="tax18" name="tax18" value="${invoicePurchaseArchiveInstance?.taxes?.get(Taxes.EIGHTEEN)}"/>
        </span>
    </div>


    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'issueDate', 'error')}">
        <span class="property-label required">
            <label for="issueDate">
                <g:message code="invoicePurchaseArchive.issueDate.label" default="Issue Date" />
            </label>
        </span>
        <span class="property-value">
            <input type="text" name="issueDate" id="issueDate" class="date"
                   value="${formatDate(date:invoicePurchaseArchiveInstance?.issueDate?:new Date(),format: 'dd.MM.yyyy')}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'paymentDate', 'error')}">
        <span class="property-label required">
            <label for="paymentDate">
                <g:message code="invoicePurchaseArchive.paymentDate.label" default="Payment Date" />
            </label>
        </span>
        <span class="property-value">
            <input type="text" name="paymentDate" id="paymentDate" class="date"
                   value="${formatDate(date:invoicePurchaseArchiveInstance?.paymentDate?:new Date(),format: 'dd.MM.yyyy')}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseInstance, field: 'paymentPercentage', 'error')}">
        <span class="property-label">
            <label for="paymentPercentage">
                <g:message code="invoicePurchaseArchive.paymentPercentage.label" default="Payment Percentage (%)"/>
            </label>
        </span>
        <span class="property-value">
            <g:field type="number" name="paymentPercentage" id="paymentPercentage"
                     min="0" max="100" step="any"
                     value="${invoicePurchaseInstance?.paymentPercentage}" />
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'paidOnDate', 'error')}">
        <span class="property-label">
            <label for="paidOnDate">
                <g:message code="invoicePurchaseArchive.paidOnDate.label" default="Paid On Date" />
            </label>
        </span>
        <span class="property-value">
            <input type="text" name="paidOnDate" id="paidOnDate" class="date"
                   value="${formatDate(date:invoicePurchaseArchiveInstance?.paidOnDate,format: 'dd.MM.yyyy')}"/>
        </span>
    </div>


</g:if>
<g:else>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'invoiceNumber', 'error')}">
        <span class="property-label required">
            <label for="invoiceNumber">
                <g:message code="invoicePurchaseArchive.invoiceNumber.label" default="(Pro)Invoice Number" />
            </label>
        </span>
        <span class="property-value">
            <g:textField name="invoiceNumber" required="" value="${invoicePurchaseArchiveInstance?.invoiceNumber}"/>
        </span>
    </div>


    <div id="div-refInvoice" class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'refInvoice', 'error')}">
        <span class="property-label">
            <label for="refInvoice">
                <g:message code="invoicePurchaseArchive.refInvoice.label" default="Has Pro Invoice"/>
            </label>
        </span>
        <span class="property-value">
            <p>
                ${invoicePurchaseArchiveInstance?.refInvoice?.invoiceNumber}
            </p>
            <g:if test="${!proInvoices.isEmpty()}">
                <g:select name="refInvoice.id" id="refInvoice" from="${proInvoices}"
                          onchange="setProInvoiceParams(this)"
                          value="${invoicePurchaseArchiveInstance?.refInvoice?.id}"
                          optionValue="invoiceNumber" optionKey="id" noSelection="['':'']"
                />
            </g:if>
            <g:elseif test="${proInvoices.isEmpty() && invoicePurchaseArchiveInstance.refInvoice!=null}">
                <label>
                    <g:message code="invoicePurchaseArchive.refInvoice.removeProInvoice" default="Remove Pro Invoice ?"/>
                </label>
                <g:checkBox name="refInvoice" checked="false" value="null" />
            </g:elseif>
            <g:else>
                <g:message code="invoicePurchaseArchive.refInvoice.noProInvoice" default="No pro invoices avaliable"/>
            </g:else>
        </span>
    </div>


    <div id="div-admissionCounterNumber" class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'admissionCounterNumber', 'error')}">
        <span class="property-label required">
            <label for="admissionCounterNumber">
                <g:message code="invoicePurchaseArchive.admissionCounterNumber.label" default="Admission Number" />
            </label>
        </span>
        <span class="property-value">
            <input type="number" name="admissionCounterNumber" id="admissionCounterNumber" required=""
                   value="${invoicePurchaseArchiveInstance?.admissionCounterNumber}"/>

            <g:datePicker name="admissionYear" id="admissionYear" precision="year" value="${admissionYear}" relativeYears="[-2..0]" />

            <input type="button" onclick="getValidAdmissionNumber()" value="Refresh Number"/>
        </span>
    </div>


    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'swiftCode', 'error')} ">
        <span class="property-label">
            <label for="swiftCode">
                <g:message code="invoicePurchaseArchive.swiftCode.label" default="Swift Code" />
            </label>
        </span>
        <span class="property-value">
            <g:textField name="swiftCode" value="${invoicePurchaseArchiveInstance?.swiftCode}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'vendor', 'error')}">
        <span class="property-label required">
            <label for="vendor">
                <g:message code="invoicePurchaseArchive.vendor.label" default="Vendor" />
            </label>
        </span>
        <span class="property-value">
            <g:select id="vendor" name="vendor.id" from="${com.genrep.vendor.Vendor.list()}"
                      optionKey="id" required="" value="${invoicePurchaseArchiveInstance?.vendor?.id}"
                      class="many-to-one" noSelection="['':'Please select vendor']"/>

            %{--<input type="button" class="newDialogButton" value="+" onclick="newClientDialog()" />--}%
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'invoiceDescription', 'error')} ">
        <span id="invoiceDescription-label" class="property-label">
            <label for="invoiceDescription">
                <g:message code="invoicePurchaseArchive.invoiceDescription.label" default="Invoice Description" />
            </label>
        </span>
        <span class="property-value" aria-labelledby="description-label">
            <g:textArea name="invoiceDescription" id="invoiceDescription" value="${invoicePurchaseArchiveInstance?.invoiceDescription}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'inCurrency', 'error')}">
        <span class="property-label required">
            <label for="inCurrency">
                <g:message code="invoicePurchaseArchive.inCurrency.label" default="Currency" />
            </label>
        </span>
        <span class="property-value">
            <g:currencySelect name="inCurrency" value="${invoicePurchaseArchiveInstance?.inCurrency?:Currencies.MKD.currency}"
                              from="${com.genrep.invoiceCreator.Currencies.values()}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'totalSum', 'error')}">
        <span class="property-label required">
            <label for="totalSum">
                <g:message code="invoicePurchaseArchive.totalSum.label" default="Total Sum" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="totalSum" name="totalSum" required="" value="${invoicePurchaseArchiveInstance?.totalSum}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'taxFreeSum', 'error')}">
        <span class="property-label required">
            <label for="taxFreeSum">
                <g:message code="invoicePurchaseArchive.taxFreeSum.label" default="Tax Free Sum" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="taxFreeSum" name="taxFreeSum" required="" value="${invoicePurchaseArchiveInstance?.taxFreeSum}"/>
        </span>
    </div>


    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'taxPriceSum', 'error')}">
        <span class="property-label required">
            <label for="taxPriceSum">
                <g:message code="invoicePurchaseArchive.taxPriceSum.label" default="Tax Price Sum" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="taxPriceSum" name="taxPriceSum" required="" value="${invoicePurchaseArchiveInstance?.taxPriceSum}"/>
        </span>
    </div>


    <div class="fieldcontain">
        <span class="property-label">
            <label for="taxes">
                <g:message code="invoiceArchive.taxes.zero" default="Tax (0%)" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="tax0" name="tax0" value="${invoicePurchaseArchiveInstance?.taxes?.get(com.genrep.invoiceRepository.Taxes.ZERO)}"/>
        </span>
    </div>

    <div class="fieldcontain">
        <span class="property-label">
            <label for="taxes">
                <g:message code="invoiceArchive.taxes.five" default="Tax (5%)" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="tax5" name="tax5" value="${invoicePurchaseArchiveInstance?.taxes?.get(Taxes.FIVE)}"/>
        </span>
    </div>

    <div class="fieldcontain">
        <span class="property-label">
            <label for="taxes">
                <g:message code="invoiceArchive.taxes.eighteen" default="Tax (18%)" />
            </label>
        </span>
        <span class="property-value">
            <g:priceAndTaxField id="tax18" name="tax18" value="${invoicePurchaseArchiveInstance?.taxes?.get(Taxes.EIGHTEEN)}"/>
        </span>
    </div>


    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'issueDate', 'error')}">
        <span class="property-label required">
            <label for="issueDate">
                <g:message code="invoicePurchaseArchive.issueDate.label" default="Issue Date" />
            </label>
        </span>
        <span class="property-value">
            <input type="text" name="issueDate" id="issueDate" class="date"
                   value="${formatDate(date:invoicePurchaseArchiveInstance?.issueDate?:new Date(),format: 'dd.MM.yyyy')}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'paymentDate', 'error')}">
        <span class="property-label">
            <label for="paymentDate">
                <g:message code="invoicePurchaseArchive.paymentDate.label" default="Payment Date" />
            </label>
        </span>
        <span class="property-value">
            <input type="text" name="paymentDate" id="paymentDate" class="date"
                   value="${formatDate(date:invoicePurchaseArchiveInstance?.paymentDate,format: 'dd.MM.yyyy')}"/>
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseInstance, field: 'paymentPercentage', 'error')}">
        <span class="property-label">
            <label for="paymentPercentage">
                <g:message code="invoicePurchaseArchive.paymentPercentage.label" default="Payment Percentage (%)"/>
            </label>
        </span>
        <span class="property-value">
            <g:field type="number" name="paymentPercentage" id="paymentPercentage"
                     min="0" max="100" step="any"
                     value="${invoicePurchaseInstance?.paymentPercentage}" />
        </span>
    </div>

    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'paidOnDate', 'error')}">
        <span class="property-label">
            <label for="paidOnDate">
                <g:message code="invoicePurchaseArchive.paidOnDate.label" default="Paid On Date" />
            </label>
        </span>
        <span class="property-value">
            <input type="text" name="paidOnDate" id="paidOnDate" class="date"
                   value="${formatDate(date:invoicePurchaseArchiveInstance?.paidOnDate,format: 'dd.MM.yyyy')}"/>
        </span>
    </div>


</g:else>
