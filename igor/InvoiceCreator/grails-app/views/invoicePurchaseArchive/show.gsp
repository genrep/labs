
<%@ page import="com.genrep.invoiceRepository.Taxes; com.genrep.client.Client; com.genrep.invoiceRepository.InvoicePurchaseArchive" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'invoicePurchaseArchive.label', default: 'InvoicePurchaseArchive')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="invoiceArchive.button.index" default="List Of Imported Invoices"/></g:link></li>
        <li><g:link class="menuButton" action="newImport"><g:message code="invoiceArchive.button.newImport"
                                                                     default="New Import"/></g:link></li>
    </ul>
</div>
<div id="show-invoicePurchaseArchive" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list invoicePurchaseArchive">

        %{--<g:if test="${invoicePurchaseArchiveInstance?.docMongoId}">--}%
            %{--<li class="fieldcontain">--}%
                %{--<span id="docMongoId-label" class="property-label"><g:message code="invoicePurchaseArchive.docMongoId.label" default="Doc Mongo Id" /></span>--}%

                %{--<span class="property-value" aria-labelledby="docMongoId-label"><g:fieldValue bean="${invoicePurchaseArchiveInstance}" field="docMongoId"/></span>--}%

            %{--</li>--}%
        %{--</g:if>--}%

        <g:if test="${invoicePurchaseArchiveInstance?.invoiceNumber}">
            <li class="fieldcontain">
                <g:if test="${invoicePurchaseArchiveInstance?.isProInvoice}">
                    <span id="invoiceNumber-label" class="property-label"><g:message code="invoicePurchaseArchive.proInvoiceNumber.label" default="Pro Invoice Number" /></span>
                </g:if>
                <g:else>
                    <span id="invoiceNumber-label" class="property-label"><g:message code="invoicePurchaseArchive.invoiceNumber.label" default="Invoice Number" /></span>
                </g:else>

                <span class="property-value" aria-labelledby="invoiceNumber-label"><g:fieldValue bean="${invoicePurchaseArchiveInstance}" field="invoiceNumber"/></span>

            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.admissionCounterNumberView}">
            <li class="fieldcontain">
                <span id="admissionCounterNumberView-label" class="property-label"><g:message code="invoicePurchaseArchive.admissionCounterNumberView.label" default="Admission Number" /></span>

                <span class="property-value" aria-labelledby="invoiceNumber-label"><g:fieldValue bean="${invoicePurchaseArchiveInstance}" field="admissionCounterNumberView"/></span>

            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.swiftCode}">
            <li class="fieldcontain">
                <span id="swiftCode-label" class="property-label"><g:message code="invoicePurchaseArchive.swiftCode.label" default="Swift Code" /></span>

                <span class="property-value" aria-labelledby="swiftCode-label"><g:fieldValue bean="${invoicePurchaseArchiveInstance}" field="swiftCode"/></span>

            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.vendor?.id}">
            <li class="fieldcontain">
                <span id="vendor-label" class="property-label"><g:message code="invoicePurchaseArchive.vendor.label" default="Vendor" /></span>

                <span class="property-value" aria-labelledby="client-label">
                    <g:link controller="vendor" action="show" id="${invoicePurchaseArchiveInstance?.vendor?.id}">
                        ${com.genrep.vendor.Vendor.get(invoicePurchaseArchiveInstance?.vendor.id).toString()}
                    </g:link>
                </span>

            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.refInvoice}">
            <li class="fieldcontain">
                <g:if test="${invoicePurchaseArchiveInstance?.isProInvoice}">
                    <span id="refInvoice-label" class="property-label"><g:message code="invoicePurchaseArchive.ProRefInvoice.label" default="Reference to Invoice" /></span>
                </g:if>
                <g:else>
                    <span id="refInvoice-label" class="property-label"><g:message code="invoicePurchaseArchive.refInvoice.label" default="Reference to Pro Invoice" /></span>
                </g:else>

                <span class="property-value" aria-labelledby="refInvoice-label">
                    <g:link controller="invoicePurchaseArchive" action="show" id="${invoicePurchaseArchiveInstance?.refInvoice?.id}">
                        ${invoicePurchaseArchiveInstance?.refInvoice?.invoiceNumber}
                    </g:link>
                </span>
            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.invoiceDescription}">
            <li class="fieldcontain">
                <span id="invoiceDescription-label" class="property-label"><g:message code="invoicePurchaseArchive.invoiceDescription.label" default="Invoice Description" /></span>

                <span class="property-value" aria-labelledby="invoiceDescription-label">
                    <g:fieldValue bean="${invoicePurchaseArchiveInstance}"
                                  field="invoiceDescription"/></span>
            </li>
        </g:if>


        <g:if test="${invoicePurchaseArchiveInstance?.totalSum}">
            <li class="fieldcontain">
                <span id="totalSum-label" class="property-label"><g:message code="invoicePurchaseArchive.totalSum.label" default="Total Sum" /></span>

                <span class="property-value" aria-labelledby="totalSum-label">
                    ${invoicePurchaseArchiveInstance?.totalSum?.decodeDecimalNumber()}
                </span>
            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.taxFreeSum}">
            <li class="fieldcontain">
                <span id="taxFreeSum-label" class="property-label"><g:message code="invoicePurchaseArchive.taxFreeSum.label" default="Tax Free Sum" /></span>

                <span class="property-value" aria-labelledby="taxFreeSum-label">
                    ${invoicePurchaseArchiveInstance?.taxFreeSum?.decodeDecimalNumber()}
                </span>
            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.taxPriceSum}">
            <li class="fieldcontain">
                <span id="taxPriceSum-label" class="property-label"><g:message code="invoicePurchaseArchive.taxPriceSum.label" default="Tax Price Sum" /></span>

                <span class="property-value" aria-labelledby="taxPriceSum-label">
                    ${invoicePurchaseArchiveInstance?.taxPriceSum?.decodeDecimalNumber()}
                </span>
            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.taxes}">
            <g:if test="${invoicePurchaseArchiveInstance?.taxes?.containsKey(com.genrep.invoiceRepository.Taxes.ZERO)}">
                <li class="fieldcontain">
                    <span  class="property-label"><g:message code="invoicePurchaseArchive.taxes.zero" default="Tax (0%)" /></span>

                    <span class="property-value" >${invoicePurchaseArchiveInstance?.taxes?.get(Taxes.ZERO).decodeDecimalNumber()}</span>

                </li>
            </g:if>

            <g:if test="${invoicePurchaseArchiveInstance?.taxes.containsKey(Taxes.FIVE)}">
                <li class="fieldcontain">
                    <span  class="property-label"><g:message code="invoicePurchaseArchive.taxes.five" default="Tax (5%)" /></span>

                    <span class="property-value" >${invoicePurchaseArchiveInstance?.taxes?.get(Taxes.FIVE).decodeDecimalNumber()}</span>

                </li>
            </g:if>

            <g:if test="${invoicePurchaseArchiveInstance?.taxes.containsKey(Taxes.EIGHTEEN)}">
                <li class="fieldcontain">
                    <span  class="property-label"><g:message code="invoicePurchaseArchive.taxes.eigtheen" default="Tax (18%)" /></span>

                    <span class="property-value" >${invoicePurchaseArchiveInstance?.taxes?.get(Taxes.EIGHTEEN).decodeDecimalNumber()}</span>

                </li>
            </g:if>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.inCurrency}">
            <li class="fieldcontain">
                <span id="inCurrency-label" class="property-label"><g:message code="invoicePurchaseArchive.inCurrency.label" default="Currency" /></span>

                <span class="property-value" aria-labelledby="inCurrency-label"><g:fieldValue bean="${invoicePurchaseArchiveInstance}" field="inCurrency"/></span>

            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.exchangeRate}">
            <li class="fieldcontain">
                <span id="exchangeRate-label" class="property-label"><g:message code="invoicePurchaseArchive.exchangeRate.label" default="Exchange Rate" /></span>

                <span class="property-value" aria-labelledby="exchangeRate-label"><g:link controller="exchangeRate" action="show" id="${invoicePurchaseArchiveInstance?.exchangeRate?.id}">${invoicePurchaseArchiveInstance?.exchangeRate?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.documentFile}">
            <li class="fieldcontain">
                <span id="documentFile-label" class="property-label"><g:message code="invoicePurchaseArchive.documentFile.label" default="Document File" /></span>
                <span  class="property-value"><g:link controller="invoicePurchaseArchive" action="showInvoicePurchaseArchiveFile" target="_blank"
                                                      id="${invoicePurchaseArchiveInstance?.id}">
                    ${invoicePurchaseArchiveInstance?.documentName?:'Invoice Document'}
                </g:link>
                </span>
            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.documentName}">
            <li class="fieldcontain">
                <span id="documentName-label" class="property-label"><g:message code="invoicePurchaseArchive.documentName.label" default="Document Name" /></span>

                <span class="property-value" aria-labelledby="documentName-label"><g:fieldValue bean="${invoicePurchaseArchiveInstance}" field="documentName"/></span>

            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.issueDate}">
            <li class="fieldcontain">
                <span id="issueDate-label" class="property-label"><g:message code="invoicePurchaseArchive.issueDate.label" default="Issue Date" /></span>

                <span class="property-value" aria-labelledby="issueDate-label">
                    <g:formatDate format="dd.MM.yyyy" date="${invoicePurchaseArchiveInstance?.issueDate}" />
                </span>
            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.paymentDate}">
            <li class="fieldcontain">
                <span id="paymentDate-label" class="property-label"><g:message code="invoicePurchaseArchive.paymentDate.label" default="Payment Date" /></span>

                <span class="property-value" aria-labelledby="paymentDate-label">
                    <g:formatDate format="dd.MM.yyyy" date="${invoicePurchaseArchiveInstance?.paymentDate}" />
                </span>
            </li>
        </g:if>

        <g:if test="${invoicePurchaseArchiveInstance?.dateCreated}">
            <li class="fieldcontain">
                <span id="dateCreated-label" class="property-label"><g:message code="invoicePurchaseArchive.dateCreated.label" default="Date Created" /></span>

                <span class="property-value" aria-labelledby="dateCreated-label">
                    <g:formatDate format="dd.MM.yyyy" date="${invoicePurchaseArchiveInstance?.dateCreated}" />
                </span>
            </li>
        </g:if>

    </ol>
    <g:form url="[resource:invoicePurchaseArchiveInstance, action:'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${invoicePurchaseArchiveInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
