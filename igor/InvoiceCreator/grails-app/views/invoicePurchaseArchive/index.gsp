
<%@ page import="com.genrep.client.Client; com.genrep.invoiceRepository.InvoicePurchaseArchive" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'invoicePurchaseArchive.label', default: 'InvoicePurchaseArchive')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<a href="#list-invoicePurchaseArchive" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="newImport"><g:message code="invoiceArchive.button.newImport"
                                                                     default="New Import"/></g:link></li>
    </ul>
</div>
<div id="list-invoicePurchaseArchive" class="content scaffold-list" role="main">
    <h1><g:link controller="invoicePurchaseArchive" action="index">
        <g:message code="default.list.label" args="[entityName]" />
    </g:link>
    </h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="vendor" title="${message(code: 'invoicePurchaseArchive.vendor.label', default: 'Vendor')}"/>

            <g:sortableColumn property="documentName" title="${message(code: 'invoicePurchaseArchive.documentName.label', default: 'Name')}" />

            <g:sortableColumn property="invoiceYear,admissionCounterNumber" title="${message(code: 'invoicePurchaseArchive.admissionCounterNumberView.label', default: 'Admission Number')}" />

            <g:sortableColumn property="invoiceNumber" title="${message(code: 'invoicePurchaseArchive.invoiceNumber.label', default: 'Invoice Number')}" />

            <g:sortableColumn property="totalSum" title="${message(code: 'invoicePurchaseArchive.totalSum.label', default: 'Total Sum')}" />

            <g:sortableColumn property="currency" title="${message(code: 'invoicePurchaseArchive.inCurrency.label', default: 'Currency')}" />

            <g:sortableColumn property="issueDate" title="${message(code: 'invoicePurchaseArchive.issueDate.label', default: 'Date ')}" />
        </tr>
        </thead>
        <tbody>
        <g:each in="${invoicePurchaseArchiveInstanceList}" status="i" var="invoicePurchaseArchiveInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td>${com.genrep.vendor.Vendor.get(invoicePurchaseArchiveInstance?.vendor?.id)}</td>

                <td><g:link action="show" id="${invoicePurchaseArchiveInstance.id}">
                    ${fieldValue(bean: invoicePurchaseArchiveInstance, field: "documentName")}
                </g:link></td>

                <td>${fieldValue(bean: invoicePurchaseArchiveInstance, field: "admissionCounterNumberView")}</td>

                <td>${fieldValue(bean: invoicePurchaseArchiveInstance, field: "invoiceNumber")}</td>

                <td>${invoicePurchaseArchiveInstance?.totalSum?.decodeDecimalNumber()}</td>

                <td>${fieldValue(bean: invoicePurchaseArchiveInstance, field: "inCurrency")}</td>

                <td>${formatDate(date: invoicePurchaseArchiveInstance?.issueDate, format: 'dd/MM/yyyy')}</td>
            </tr>
        </g:each>
        </tbody>
    </table>
    <div class="pagination">
        <g:paginate total="${invoicePurchaseArchiveInstanceCount ?: 0}" />
    </div>
</div>
</body>
</html>
