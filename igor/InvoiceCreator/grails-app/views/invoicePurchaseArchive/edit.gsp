<%@ page import="com.genrep.invoiceRepository.InvoicePurchaseArchive" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'invoicePurchaseArchive.label', default: 'InvoicePurchaseArchive')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
        <asset:javascript src="invoice.js"/>
	</head>
	<body>
		<div class="nav" role="navigation">
			<ul>
                <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="menuButton" action="index"><g:message code="invoiceArchive.button.index" default="List Of Imported Invoices"/></g:link></li>
                <li><g:link class="menuButton" action="newImport"><g:message code="invoiceArchive.button.newImport"
                                                                             default="New Import"/></g:link></li>
            </ul>
		</div>
		<div id="edit-invoicePurchaseArchive" class="content scaffold-edit" role="main">
			<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${invoicePurchaseArchiveInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${invoicePurchaseArchiveInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
            <g:uploadForm url="[resource:invoicePurchaseArchiveInstance, action:'update']" method="post">
                <g:hiddenField name="version" value="${invoicePurchaseArchiveInstance?.version}" />
                <fieldset class="form">
                    <g:render template="editForm"/>
                </fieldset>
                <fieldset class="form">
                    <div id="editFileForm">
                        <g:render template="editFileForm"/>
                    </div>
                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="update" class="save" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                </fieldset>
            </g:uploadForm>
		</div>
	</body>
</html>
