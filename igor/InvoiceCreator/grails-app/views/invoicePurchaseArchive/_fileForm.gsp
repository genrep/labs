<div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'documentFile', 'error')}">
    <span class="property-label required">
        <label for="documentFile">
            <g:message code="invoiceArchive.documentFile.label" default="Document File" />
        </label>
    </span>
    <span class="property-value">
        <input type="file" required=""
               id="documentFile" name="documentFile" onchange="setFileName(this,'documentName')" />
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'documentName', 'error')}">
    <span class="property-label required">
        <label for="documentName">
            <g:message code="invoiceArchive.documentName.label" default="Document Name" />
        </label>
    </span>
    <span class="property-value">
        <g:textField class="wider-fields" required=""
                     name="documentName" id="documentName" value="${invoicePurchaseArchiveInstance?.documentName}"/>
    </span>
</div>
