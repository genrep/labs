<g:if test="${invoicePurchaseArchiveInstance?.documentFile}">
    <div class="fieldcontain">
        <span id="documentFile-label" class="property-label"><g:message code="invoicePurchaseArchive.documentFile.label" default="Document File" /></span>
        <span  class="property-value"><g:link controller="invoicePurchaseArchive" action="showInvoicePurchaseArchiveFile" target="_blank"
                                              id="${invoicePurchaseArchiveInstance?.id}">
            ${invoicePurchaseArchiveInstance?.documentName?:'Invoice Document'}
        </g:link>
        </span>
    </div>
</g:if>
<g:else>
    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'documentFile', 'error')}">
        <span class="property-label">
            <label for="documentFile">
                <g:message code="invoicePurchaseArchive.documentFile.label" default="Document File" />
            </label>
        </span>
        <span class="property-value">
            <input type="file" id="documentFile" name="documentFile" onchange="setFileName(this,'documentName')" />
        </span>
    </div>
</g:else>

<g:if test="${invoicePurchaseArchiveInstance?.documentName}">
    <div class="fieldcontain">
        <span id="documentName-label" class="property-label"><g:message code="invoicePurchaseArchive.documentName.label" default="Document Name" /></span>

        <span class="property-value" aria-labelledby="documentName-label"><g:fieldValue bean="${invoicePurchaseArchiveInstance}" field="documentName"/></span>

    </div>
</g:if>
<g:else>
    <div class="fieldcontain ${hasErrors(bean: invoicePurchaseArchiveInstance, field: 'documentName', 'error')}">
        <span class="property-label">
            <label for="documentName">
                <g:message code="invoicePurchaseArchive.documentName.label" default="Document Name" />
            </label>
        </span>
        <span class="property-value">
            <g:textField class="wider-fields" name="documentName" id="documentName" value="${invoicePurchaseArchiveInstance?.documentName}"/>
        </span>
    </div>
</g:else>

<g:if test="${invoicePurchaseArchiveInstance?.documentFile}">
    <div class="itemButtons">

        <input id="deleteFile" type="button" value="Delete File" class="buttonsMine deleteButton"
               onclick="deleteInvoicePurchaseArchiveFile(${invoicePurchaseArchiveInstance.id})"/>
    </div>
</g:if>