
<%@ page import="com.genrep.project.Project" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'project.label', default: 'Project')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="menuButton" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
        <li><g:link class="menuButton" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
    </ul>
</div>
<div id="show-project" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list project">

        <g:if test="${projectInstance?.technicalNumber}">
            <li class="fieldcontain">
                <span id="technicalNumber-label" class="property-label"><g:message code="project.technicalNumber.label" default="Technical Number" /></span>

                <span class="property-value" aria-labelledby="technicalNumber-label"><g:fieldValue bean="${projectInstance}" field="technicalNumber"/></span>

            </li>
        </g:if>

        <g:if test="${projectInstance?.title}">
            <li class="fieldcontain">
                <span id="title-label" class="property-label"><g:message code="project.title.label" default="Title" /></span>

                <span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${projectInstance}" field="title"/></span>

            </li>
        </g:if>

        <g:if test="${projectInstance?.description}">
            <li class="fieldcontain">
                <span class="property-label">
                    <g:message code="project.description.label" default="Description"/>
                </span>
                <span class="property-value">
                    <g:fieldValue bean="${projectInstance}" field="description" />
                </span>
            </li>
        </g:if>

        <g:if test="${projectInstance?.client}">
            <li class="fieldcontain">
                <span class="property-label">
                    <g:message code="project.client.label" default="Client"/>
                </span>
                <span class="property-value">
                    <g:fieldValue bean="${projectInstance}" field="client" />
                </span>
            </li>
        </g:if>

        <g:if test="${projectInstance?.contractNumber}">
            <li class="fieldcontain">
                <span id="contractNumber-label" class="property-label"><g:message code="project.contractNumber.label" default="Contract Number" /></span>

                <span class="property-value" aria-labelledby="contractNumber-label"><g:fieldValue bean="${projectInstance}" field="contractNumber"/></span>

            </li>
        </g:if>

        <g:if test="${projectInstance?.contractNumberBearer}">
            <li class="fieldcontain">
                <span id="contractNumberBearer-label" class="property-label"><g:message code="project.contractNumberBearer.label" default="Contract Number Bearer" /></span>

                <span class="property-value" aria-labelledby="contractNumberBearer-label"><g:fieldValue bean="${projectInstance}" field="contractNumberBearer"/></span>

            </li>
        </g:if>

        <g:if test="${projectInstance?.price}">
            <li class="fieldcontain">
                <span id="price-label" class="property-label"><g:message code="project.price.label" default="Price" /></span>

                <span class="property-value" aria-labelledby="price-label">
                    <g:fieldValue bean="${projectInstance}" field="price"/>
                    <g:fieldValue bean="${projectInstance}" field="currency"/>
                </span>

            </li>
        </g:if>

        <g:if test="${projectInstance?.tax}">
            <li class="fieldcontain">
                <span id="tax-label" class="property-label"><g:message code="project.tax.label" default="Tax (%)" /></span>

                <span class="property-value" aria-labelledby="tax-label">
                    ${projectInstance?.tax?.value}
                </span>

            </li>
        </g:if>

        <g:if test="${projectInstance?.totalPrice}">
            <li class="fieldcontain">
                <span id="totalPrice-label" class="property-label"><g:message code="project.totalPrice.label" default="Total Price" /></span>

                <span class="property-value" aria-labelledby="totalPrice-label">
                    <g:fieldValue bean="${projectInstance}" field="totalPrice"/>
                    <g:fieldValue bean="${projectInstance}" field="currency"/>
                </span>

            </li>
        </g:if>

        <g:if test="${projectInstance?.startDate}">
            <li class="fieldcontain">
                <span id="startDate-label" class="property-label"><g:message code="project.startDate.label" default="Start Date" /></span>

                <span class="property-value" aria-labelledby="startDate-label">
                    <g:formatDate date="${projectInstance?.startDate}" format="dd.MM.yyyy"  />
                </span>

            </li>
        </g:if>

        <g:if test="${projectInstance?.endDate}">
            <li class="fieldcontain">
                <span id="endDate-label" class="property-label"><g:message code="project.endDate.label" default="End Date" /></span>

                <span class="property-value" aria-labelledby="endDate-label">
                    <g:formatDate format="dd.MM.yyyy"  date="${projectInstance?.endDate}" />
                </span>

            </li>
        </g:if>

        <g:if test="${projectInstance?.projectState}">
            <li class="fieldcontain">
                <span class="property-label">
                    <g:message code="project.projectState.label" default="Project State"/>
                </span>
                <span class="property-value">
                    <g:fieldValue bean="${projectInstance}" field="projectState" />
                </span>
            </li>
        </g:if>

        <g:if test="${projectDocumentInstance?.documentFile}">
            <li class="fieldcontain">
                <span id="documentFile-label" class="property-label">
                    <g:message code="project.documentFile.label" default="Document File" />
                </span>
                <span  class="property-value">
                    <g:link controller="project" action="showProjectDocumentFile" target="_blank"
                            id="${projectDocumentInstance?.id}">
                        ${projectDocumentInstance?.documentName?:'Project Document'}
                    </g:link>
                </span>
            </li>
        </g:if>

        <g:if test="${projectDocumentInstance?.documentName}">
            <li class="fieldcontain">
                <span id="documentName-label" class="property-label">
                    <g:message code="project.documentName.label" default="Document Name" />
                </span>
                <span class="property-value" aria-labelledby="documentName-label">
                    <g:fieldValue bean="${projectDocumentInstance}" field="documentName"/>
                </span>
            </li>
        </g:if>

        <g:if test="${projectInstance?.invoices || importedInvoicesList!=null}">
            <li class="fieldcontain">
                <span id="invoices-label" class="property-label"><g:message code="project.invoices.label" default="Invoices" /></span>

                <g:each in="${projectInstance.invoices}" var="i">
                    <span class="property-value" aria-labelledby="invoices-label">
                        <g:link controller="invoice" action="show" id="${i.id}">${i?.invoiceNumber}</g:link>
                    </span>
                </g:each>

            </li>
        </g:if>

        <g:if test="${importedInvoicesList}">
            <li class="fieldcontain">
                <span id="invoices-label" class="property-label"><g:message code="project.importedInvoices.label" default="Imported Invoices" /></span>

                <g:each in="${importedInvoicesList}" var="i">
                    <span class="property-value">
                        <g:link controller="invoiceArchive" action="show" id="${i.id}">${i?.invoiceNumber}</g:link>
                    </span>
                </g:each>
            </li>
        </g:if>

        <g:if test="${projectInstance?.paymentTerms}">
            <li class="fieldcontain">
                <span id="paymentTerms-label" class="property-label"><g:message code="project.paymentTerms.label" default="Payment Terms" /></span>

                <g:each in="${projectInstance.paymentTerms}" var="p">
                    <span class="property-value" aria-labelledby="paymentTerms-label"><g:link controller="paymentTerm" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></span>
                </g:each>

            </li>
        </g:if>

    </ol>
    <g:form url="[resource:projectInstance, action:'delete']" method="DELETE">
        <fieldset class="buttons">
            <g:link class="edit" action="edit" resource="${projectInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
