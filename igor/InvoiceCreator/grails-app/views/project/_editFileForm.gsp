<g:if test="${projectDocumentInstance?.documentFile}">
    <div class="fieldcontain">
        <span id="documentFile-label" class="property-label">
            <g:message code="invoiceArchive.documentFile.label" default="Document File" />
        </span>
        <span  class="property-value"><g:link controller="project" action="showProjectDocumentFile" target="_blank"
                                              id="${projectDocumentInstance?.id}">
            ${projectDocumentInstance?.documentName?:'Project Document'}
        </g:link>
        </span>
    </div>
</g:if>
<g:else>
    <div class="fieldcontain ${hasErrors(bean: projectDocumentInstance, field: 'documentFile', 'error')}">
        <span class="property-label">
            <label for="documentFile">
                <g:message code="project.documentFile.label" default="Document File" />
            </label>
        </span>
        <span class="property-value">
            <input type="file" id="documentFile" name="documentFile" onchange="setFileName(this,'documentName')" />
        </span>
    </div>
</g:else>

<g:if test="${projectDocumentInstance?.documentName}">
    <div class="fieldcontain">
        <span id="documentName-label" class="property-label">
            <g:message code="project.documentName.label" default="Document Name" />
        </span>
        <span class="property-value" aria-labelledby="documentName-label">
            <g:fieldValue bean="${projectDocumentInstance}" field="documentName"/>
        </span>

    </div>
</g:if>
<g:else>
    <div class="fieldcontain ${hasErrors(bean: projectDocumentInstance, field: 'documentName', 'error')}">
        <span class="property-label">
            <label for="documentName">
                <g:message code="project.documentName.label" default="Document Name" />
            </label>
        </span>
        <span class="property-value">
            <g:textField class="wider-fields" name="documentName" id="documentName" value="${projectDocumentInstance?.documentName}"/>
        </span>
    </div>
</g:else>

<g:if test="${projectDocumentInstance?.documentFile}">
    <div class="itemButtons">

        <input id="deleteFile" type="button" value="Delete File" class="buttonsMine deleteButton"
               onclick="deleteProjectDocumentFile(${projectDocumentInstance?.id},'delete')"/>
    </div>
</g:if>

<g:if test="${undoDocumentId}">
    <div class="itemButtons">

        <input type="button" value="Undo" class="buttonsMine deleteButton"
               onclick="deleteProjectDocumentFile(${undoDocumentId},'undo')"/>
    </div>
</g:if>