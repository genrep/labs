
<%@ page import="com.genrep.project.Project" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'project.label', default: 'Project')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link controller="project" class="menuButton" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-project" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="technicalNumber" title="${message(code: 'project.technicalNumber.label', default: 'Technical Number')}" />
					
						<g:sortableColumn property="title" title="${message(code: 'project.title.label', default: 'Title')}" />
					
						<g:sortableColumn property="contractNumber" title="${message(code: 'project.contractNumber.label', default: 'Contract Number')}" />
					
						<g:sortableColumn property="contractNumberBearer" title="${message(code: 'project.contractNumberBearer.label', default: 'Contract Number Bearer')}" />
					
						<g:sortableColumn property="price" title="${message(code: 'project.price.label', default: 'Price')}" />
					
						<g:sortableColumn property="tax" title="${message(code: 'project.tax.label', default: 'Tax (%)')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${projectInstanceList}" status="i" var="projectInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link controller="project" action="show" id="${projectInstance.id}">${fieldValue(bean: projectInstance, field: "technicalNumber")}</g:link></td>
					
						<td>${fieldValue(bean: projectInstance, field: "title")}</td>
					
						<td>${fieldValue(bean: projectInstance, field: "contractNumber")}</td>
					
						<td>${fieldValue(bean: projectInstance, field: "contractNumberBearer")}</td>
					
						<td>${fieldValue(bean: projectInstance, field: "price")}</td>
					
						<td>${projectInstance?.tax?.value}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${projectInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
