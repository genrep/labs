<%@ page import="com.genrep.client.Client; com.genrep.invoiceRepository.Taxes; com.genrep.invoiceCreator.Currencies; com.genrep.project.Project" %>



<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'technicalNumber', 'error')}">
    <span class="property-label required">
        <label for="technicalNumber">
            <g:message code="project.technicalNumber.label" default="Technical Number" />
        </label>
    </span>
    <span class="property-value">
        <g:textField name="technicalNumber" required="" value="${projectInstance?.technicalNumber}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'title', 'error')} ">
    <span class="property-label required">
        <label for="title">
            <g:message code="project.title.label" default="Title" />
        </label>
    </span>
    <span class="property-value">
        <g:textField name="title" class="wider-fields" required="" value="${projectInstance?.title}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'description', 'error')}">
    <span class="property-label">
        <label for="description">
            <g:message code="project.description.label" default="Description" />
        </label>
    </span>
    <span class="property-value">
        <g:textArea name="description" id="description" value="${projectInstance?.description}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'client', 'error')}">
    <span id="client-label" class="property-label  required">
        <label for="client">
            <g:message code="project.client.label" default="Client"/>
        </label>
    </span>
    <span class="property-value" aria-labelledby="client-label">
        <g:select id="client" name="client.id" from="${com.genrep.client.Client.list()}"
                  optionKey="id" optionValue="name"
                  required="" noSelection="['':'Please select client']"
                  value="${projectInstance?.client?.id}" class="comboBox"/>

        %{--<input type="button" class="newDialogButton" value="+" onclick="newClientDialog()" />--}%
    </span>

</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'contractNumber', 'error')}">
    <span class="property-label required">
        <label for="contractNumber">
            <g:message code="project.contractNumber.label" default="Contract Number" />
        </label>
    </span>
    <span class="property-value">
        <g:textField name="contractNumber" required="" value="${projectInstance?.contractNumber}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'contractNumberBearer', 'error')}">
    <span class="property-label required">
        <label for="contractNumberBearer">
            <g:message code="project.contractNumberBearer.label" default="Contract Number Bearer" />
        </label>
    </span>
    <span class="property-value">
        <g:textField name="contractNumberBearer" required="" value="${projectInstance?.contractNumberBearer}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'price', 'error')}">
    <span class="property-label">
        <label for="price">
            <g:message code="project.price.label" default="Price" />
        </label>
    </span>
    <span class="property-value">
        <g:textField name="price" id="price" onchange="calculateTotalPrice()"
                     value="${projectInstance?.price}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'currency', 'error')}">
    <span id="currency-label" class="property-label">
        <label for="currency">
            <g:message code="project.currency.label" default="Currency" />
        </label>
    </span>
    <span class="property-value">
        <g:currencySelect name="currency" value="${projectInstance.currency?:com.genrep.invoiceCreator.Currencies.MKD.currency}"
                          from="${Currencies.values()}" />
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'tax', 'error')}">
    <span class="property-label">
        <label for="tax">
            <g:message code="project.tax.label" default="Tax (%)" />
        </label>
    </span>
    <span class="property-value">
        <g:select id="tax" name="tax" from="${com.genrep.invoiceRepository.Taxes.values()}"
                  optionValue="value" onchange="calculateTotalPrice()"
                  value="${projectInstance?.tax}"
                  class="many-to-one"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'totalPrice', 'error')}">
    <span class="property-label">
        <label for="totalPrice">
            <g:message code="project.totalPrice.label" default="Total Price" />
        </label>
    </span>
    <span class="property-value">
        <g:textField id="totalPrice" name="totalPrice"
                     value="${projectInstance?.totalPrice}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'startDate', 'error')}">
    <span class="property-label">
        <label for="startDate">
            <g:message code="project.startDate.label" default="Start Date" />
        </label>
    </span>
    <span class="property-value">
        <input type="text" name="startDate" id="startDate" class="date"
               value="${formatDate(date:projectInstance?.startDate?:new Date(),format: 'dd.MM.yyyy')}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'endDate', 'error')}">
    <span class="property-label">
        <label for="endDate">
            <g:message code="project.endDate.label" default="End Date" />
        </label>
    </span>
    <span class="property-value">
        <input type="text" name="endDate" id="endDate" class="date"
               value="${formatDate(date:projectInstance?.endDate?:new Date(),format: 'dd.MM.yyyy')}"/>
    </span>
</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'projectState', 'error')}">
    <span class="property-label">
        <label for="projectState">
            <g:message code="project.projectState.label" default="State" />
        </label>
    </span>
    <span class="property-value">
        <g:select id="projectState" name="projectState" from="${com.genrep.project.ProjectState.values()}"
                  optionValue="name"
                  value="${projectInstance?.projectState}"
                  noSelection="['':'']"
                  class="many-to-one"/>
    </span>

</div>

<!-- ProjectDocument -->
<g:if test="${params.action.equals("edit")}">
    <div id="editFileForm">
        <g:render template="editFileForm"/>
    </div>
</g:if>
<g:else>
    <g:render template="fileForm" />
</g:else>
<!-- ProjectDocument -->

%{--<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'invoices', 'error')} ">--}%
%{--<label for="invoices">--}%
%{--<g:message code="project.invoices.label" default="Invoices" />--}%

%{--</label>--}%
%{--<g:select name="invoices" from="${com.genrep.invoiceCreator.Invoice.list()}" multiple="multiple" optionKey="id" size="5" value="${projectInstance?.invoices*.id}" class="many-to-many"/>--}%

%{--</div>--}%

%{--<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'paymentTerms', 'error')} ">--}%
%{--<label for="paymentTerms">--}%
%{--<g:message code="project.paymentTerms.label" default="Payment Terms" />--}%

%{--</label>--}%

%{--<ul class="one-to-many">--}%
%{--<g:each in="${projectInstance?.paymentTerms?}" var="p">--}%
%{--<li><g:link controller="paymentTerm" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>--}%
%{--</g:each>--}%
%{--<li class="add">--}%
%{--<g:link controller="paymentTerm" action="create" params="['project.id': projectInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'paymentTerm.label', default: 'PaymentTerm')])}</g:link>--}%
%{--</li>--}%
%{--</ul>--}%


%{--</div>--}%

