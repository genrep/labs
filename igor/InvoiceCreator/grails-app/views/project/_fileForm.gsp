<div class="fieldcontain ${hasErrors(bean: projectDocumentInstance, field: 'documentFile', 'error')}">
    <span class="property-label">
        <label for="documentFile">
            <g:message code="project.documentFile.label" default="Document File" />
        </label>
    </span>
    <span class="property-value">
        <input type="file"
               id="documentFile" name="documentFile" onchange="setFileName(this,'documentName')" />
    </span>
</div>
<div class="fieldcontain ${hasErrors(bean: projectDocumentInstance, field: 'documentName', 'error')}">
    <span class="property-label">
        <label for="documentName">
            <g:message code="project.documentName.label" default="Document Name" />
        </label>
    </span>
    <span class="property-value">
        <g:textField class="wider-fields"
                     name="documentName" id="documentName" value="${projectDocumentInstance?.documentName}"/>
    </span>
</div>
