
<%@ page import="com.genrep.vendor.Vendor" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'vendor.label', default: 'Vendor')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-vendor" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="menuButton" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="menuButton" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-vendor" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list vendor">
			
				<g:if test="${vendorInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="vendor.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${vendorInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${vendorInstance?.department}">
				<li class="fieldcontain">
					<span id="department-label" class="property-label"><g:message code="vendor.department.label" default="Department" /></span>
					
						<span class="property-value" aria-labelledby="department-label"><g:fieldValue bean="${vendorInstance}" field="department"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${vendorInstance?.address}">
				<li class="fieldcontain">
					<span id="address-label" class="property-label"><g:message code="vendor.address.label" default="Address" /></span>
					
						<span class="property-value" aria-labelledby="address-label"><g:fieldValue bean="${vendorInstance}" field="address"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${vendorInstance?.bankAccount}">
				<li class="fieldcontain">
					<span id="bankAccount-label" class="property-label"><g:message code="vendor.bankAccount.label" default="Bank Account" /></span>
					
						<span class="property-value" aria-labelledby="bankAccount-label"><g:link controller="bankAccount" action="show" id="${vendorInstance?.bankAccount?.id}">${vendorInstance?.bankAccount?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${vendorInstance?.viewInfo}">
				<li class="fieldcontain">
					<span id="viewInfo-label" class="property-label"><g:message code="vendor.viewInfo.label" default="View Info" /></span>
					
						<span class="property-value" aria-labelledby="viewInfo-label"><g:fieldValue bean="${vendorInstance}" field="viewInfo"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:vendorInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${vendorInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
