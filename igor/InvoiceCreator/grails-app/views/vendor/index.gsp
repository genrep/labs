
<%@ page import="com.genrep.vendor.Vendor" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'vendor.label', default: 'Vendor')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-vendor" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="menuButton" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-vendor" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'vendor.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="department" title="${message(code: 'vendor.department.label', default: 'Department')}" />
					
						<g:sortableColumn property="address" title="${message(code: 'vendor.address.label', default: 'Address')}" />
					
						<th><g:message code="vendor.bankAccount.label" default="Bank Account" /></th>
					
						<g:sortableColumn property="viewInfo" title="${message(code: 'vendor.viewInfo.label', default: 'View Info')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${vendorInstanceList}" status="i" var="vendorInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${vendorInstance.id}">${fieldValue(bean: vendorInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: vendorInstance, field: "department")}</td>
					
						<td>${fieldValue(bean: vendorInstance, field: "address")}</td>
					
						<td>${fieldValue(bean: vendorInstance, field: "bankAccount")}</td>
					
						<td>${fieldValue(bean: vendorInstance, field: "viewInfo")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${vendorInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
