<%@ page import="com.genrep.vendor.Vendor" %>
<div id="vendor-form" title="Add New Vendor">

	<g:hasErrors bean="${vendorInstance}">
		<ul id="erol" class="errors" role="alert">
			<g:eachError bean="${vendntInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>>
					<g:message
							error="${error}"/></li>
			</g:eachError>
		</ul>
	</g:hasErrors>

	<div class="fieldcontain ${hasErrors(bean: vendorInstance, field: 'name', 'error')} ">
		<span class="property-label required">
			<label for="name">
				<g:message code="vendor.name.label" default="Name" />

			</label>
		</span>
		<span class="property-value">
			<g:textField name="name" class="wider-fields" value="${vendorInstance?.name}"/>
		</span>

	</div>

	<div class="fieldcontain ${hasErrors(bean: vendorInstance, field: 'department', 'error')} ">
		<span class="property-label">
			<label for="department">
				<g:message code="vendor.department.label" default="Department" />

			</label>
		</span>
		<span class="property-value">
			<g:textField name="department" class="wider-fields" value="${vendorInstance?.department}"/>
		</span>

	</div>

	<div class="fieldcontain ${hasErrors(bean: vendorInstance, field: 'address', 'error')} ">
		<span class="property-label">
			<label for="address">
				<g:message code="vendor.address.label" default="Address" />

			</label>
		</span>
		<span class="property-value">
			<g:textField name="address" class="wider-fields" value="${vendorInstance?.address}"/>
		</span>
	</div>

	<div class="fieldcontain ${hasErrors(bean: vendorInstance, field: 'bankAccountNumber', 'error')} ">
		<span class="property-label">
			<label for="bankAccountNumber">
				<g:message code="vendor.bankAccountNumber.label" default="Bank Account Number" />

			</label>
		</span>
		<span class="property-value">
			<g:textField name="bankAccountNumber" class="wider-fields" value="${vendorInstance?.bankAccountNumber}"/>
		</span>

	</div>

	<div class="fieldcontain ${hasErrors(bean: vendorInstance, field: 'viewInfo', 'error')} ">
		<span class="property-label">
			<label for="viewInfo">
				<g:message code="vendor.viewInfo.label" default="Invoice Print" />
			</label>
		</span>
		%{--<span class="property-value">--}%
		%{--<g:textArea name="viewInfo" id="viewInfo" value="${clientInstance?.viewInfo}"/>--}%
		%{--</span>--}%
		<div class="jqte-popup">
			<span class="property-value">
				<g:textArea name="viewInfo" id="viewInfo" cols="40" rows="5" maxlength="500"
							value="${vendorInstance?.viewInfo}"
				/>
			</span>
		</div>
	</div>

</div>