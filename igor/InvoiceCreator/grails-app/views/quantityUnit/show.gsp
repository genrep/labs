
<%@ page import="com.genrep.quantityUnit.QuantityUnit" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'quantityUnit.label', default: 'QuantityUnit')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="menuButton" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="menuButton" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-quantityUnit" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list quantityUnit">
			
				<g:if test="${quantityUnitInstance?.enName}">
				<li class="fieldcontain">
					<span id="enName-label" class="property-label"><g:message code="quantityUnit.enName.label" default="En Name" /></span>
					
						<span class="property-value" aria-labelledby="enName-label"><g:fieldValue bean="${quantityUnitInstance}" field="enName"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${quantityUnitInstance?.mkName}">
				<li class="fieldcontain">
					<span id="mkName-label" class="property-label"><g:message code="quantityUnit.mkName.label" default="Mk Name" /></span>
					
						<span class="property-value" aria-labelledby="mkName-label"><g:fieldValue bean="${quantityUnitInstance}" field="mkName"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:quantityUnitInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${quantityUnitInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
