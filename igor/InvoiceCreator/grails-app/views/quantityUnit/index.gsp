
<%@ page import="com.genrep.quantityUnit.QuantityUnit" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'quantityUnit.label', default: 'QuantityUnit')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="menuButton" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="menuButton" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-quantityUnit" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="enName" title="${message(code: 'quantityUnit.enName.label', default: 'En Name')}" />
					
						<g:sortableColumn property="mkName" title="${message(code: 'quantityUnit.mkName.label', default: 'Mk Name')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${quantityUnitInstanceList}" status="i" var="quantityUnitInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${quantityUnitInstance.id}">${fieldValue(bean: quantityUnitInstance, field: "enName")}</g:link></td>
					
						<td>${fieldValue(bean: quantityUnitInstance, field: "mkName")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${quantityUnitInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
