<%@ page import="com.genrep.quantityUnit.QuantityUnit" %>



<div class="fieldcontain ${hasErrors(bean: quantityUnitInstance, field: 'enName', 'error')} ">
	<span class="property-label">
		<label for="enName">
			<g:message code="quantityUnit.enName.label" default="En Name" />
		</label>
	</span>
	<span class="property-value">
		<g:textField name="enName" value="${quantityUnitInstance?.enName}"/>
	</span>
</div>

<div class="fieldcontain ${hasErrors(bean: quantityUnitInstance, field: 'mkName', 'error')} ">
	<span class="property-label">
		<label for="mkName">
			<g:message code="quantityUnit.mkName.label" default="Mk Name" />
		</label>
	</span>
	<span class="property-value">
		<g:textField name="mkName" value="${quantityUnitInstance?.mkName}"/>
	</span>
</div>

