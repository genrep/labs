package com.genrep.invoiceCreator

/**
 * Created by igor on 7/9/14.
 */
class UnderscoreCodec {
    static encode = {target->
        target.replaceAll(" ", "_")
    }

    static decode = {target->
        target.replaceAll("_", " ")
    }
}