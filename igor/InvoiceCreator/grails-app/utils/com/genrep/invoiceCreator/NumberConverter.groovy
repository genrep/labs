package com.genrep.invoiceCreator

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.text.DecimalFormat
import java.text.NumberFormat

/**
 *
 * @since 18.08.2015
 * @author Igor Ivanovski
 *
 */
class NumberConverter {

    static private Logger _log = LoggerFactory.getLogger(getClass())
    static protected Logger getLog() { _log }

    static String decimalEncoder(def decimal, String locale){
        String outputPattern = "###0.00"
        Locale loc = new Locale(locale, locale.toUpperCase())
        NumberFormat nf = NumberFormat.getNumberInstance(loc);
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern(outputPattern);
//       println new java.text.DecimalFormat("###0.00",new java.text.DecimalFormatSymbols(new Locale('mk','MK'))).format(decimal)
        try {
            return df.format(decimal)
        }
        catch (Exception e){
            log.error 'Problem formatting decimal number.'+e.message,e.fillInStackTrace()
            return null
        }
    }
}
