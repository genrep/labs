package com.genrep.invoiceCreator

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat

/**
 * Created by igor on 5/8/14.
 */
class DecimalNumberCodec {

    static decode = { decimal ->

        String outputPattern = "###0.00"
        Locale loc = new Locale('mk', 'MK')
        NumberFormat nf = NumberFormat.getNumberInstance(loc);
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern(outputPattern);
//       println new java.text.DecimalFormat("###0.00",new java.text.DecimalFormatSymbols(new Locale('mk','MK'))).format(decimal)
        try {
            return df.format(decimal)
        }
        catch (Exception e){
            log.error 'Problem formatting decimal number.'+e.message,e.fillInStackTrace()
            return null
        }
    }
}
