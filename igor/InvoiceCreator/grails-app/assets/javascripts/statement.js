/**
 * Created by igor on 9/17/14.
 */
//= require jquery/jquery-${org.codehaus.groovy.grails.plugins.jquery.JQueryConfig.SHIPPED_VERSION}
//= require vendor/jquery-ui-1.11.0.js
//= require vendor/alertify.js

$(document).ready(function() {

    $("#issueDate").datepicker({
        dateFormat: 'dd.mm.yy',
        maxDate: new Date()
    });

    $( "#bankAccount-form" ).dialog({
        autoOpen: false,
        height: 300,
        width: 600,
        modal: true,
        open: function( event, ui ) {
            $("#entityName").val('');
            $("#bankName").val('');
            $("#bankAccountNumber").val('');
            $("#balance").val('');
            $("#erol").remove();
        },
        buttons: {
            "Create": function() {

                var form = document.getElementById('bankAccount-form');
                var from = $(this).data('from');

                var entityName = $("#entityName");
                var bankName = $("#bankName");
                var bankAccountNumber = $("#bankAccountNumber");
                var balance = $("#balance");

                var data = {entityName:entityName.val(),
                    bankName:bankName.val(),
                    bankAccountNumber:bankAccountNumber.val(),
                    balance:balance.val()};

//                $(form).load("/InvoiceCreator/bankAccount/addBankAccount",data,function(responseTxt,statusTxt,xhr){
//
//                });
                $.ajax({
                    url: "/InvoiceCreator/bankAccount/addBankAccount",
                    type: "post",
                    data: data,

                    success: function (data) {
                        $('#bankAccount-form').dialog( "close" );
                        alertify.success("Bank Account was successfully added.");
                        if(from === 'create'){
                            $("#ownerOfStatement").load("/InvoiceCreator/worker/refreshBankAccountSelect");
                        }
                    },
                    error: function (data) {
                        $('#bankAccount-form').html(data.responseText);
                    }
                });
            },
            close: function() {
                $( this ).dialog( "close" );
            }}
    });
});

function uploadStatementFile(){

    var file = $("#documentFile").val();
    if(isBlank(file) || isEmpty(file)){
        alertify.log("You have to select file first.");
        return false;
    }

    var fd = new FormData();
    var file = $("#documentFile")[0].files[0];
    var name = $("#documentName").val();
    var date = $("#issueDate").val();

    fd.append('documentFile',file);
    fd.append('documentName',name);
    fd.append('issueDate',date);


    $.ajax({
        xhr: function(){
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            xhr.upload.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    //Do something with upload progress
//                    console.log(percentComplete*100);
                    progress(percentComplete*100, $('#progressBar'));
                }
            }, false);
            return xhr;
        },
        beforeSend:function(){
            $('#progressBar').show();
        },
        url:"/InvoiceCreator/statements/documents/uploadStatementDoc/",
        type: "post",
        data: fd,
        cache: false,
        processData: false,
        contentType: false,

        success: function(data) {

            $.each(data, function(key, value){

//                var tableRef = document.getElementById('statementsTable').getElementsByTagName('tbody')[0];
                var tableRef = $('#statementsTable tbody')

                tableRef.append(value.table_row);

                // Clean inputs

                $("div#uploadForm input[type=file]").val("");
                $("div#uploadForm input[type=text]").val("");
                $("div#uploadForm input[type=textField]").val("");

                hideWithDelay($('#progressBar'))

            });
        }

    });

}

function removeStatementFile(uuid){

    $.ajax({

        url:"/InvoiceCreator/statements/documents/removeStatementDoc/",
        type: "post",
        data: {'uuid':uuid},
        dataType: 'text',
        complete: function(data){

        },
        error: function(data){
            if(data.status == 404){
                alertify.error("Couldn't remove file.");
            }
        },
        success: function(data) {
            $("#"+uuid).remove();
        }
    });
}

function uploadStatementArchive(){

    var file = $("#documentFile").val();
    var statementId = $("#bankStatementId").val();

    /**
     * Validation
     */
    if(isBlank(file) || isEmpty(file)){
        alertify.log("You have to select file first.");
        return false;
    }else if(isBlank(statementId) || isEmpty(statementId)){
        alertify.log("Bank Statement Id is required.");
        return false;
    }

    var file = $("#documentFile")[0].files[0];
    var fileData = new FormData();
    fileData.append('file',file);
    jQuery.get('/InvoiceCreator/worker/probeFileContentType/', fileData, function(data){
        console.log(data);
    });
    jQuery.get('/InvoiceCreator/statements/archives/checkIfBankStatementArchiveExists/', statementId, function(data){
        console.log(data);
    });




    var name = $("#documentName").val();
    var date = $("#issueDate").val();

    var fd = new FormData();

    fd.append('documentFile',file);
    fd.append('documentName',name);
    fd.append('bankStatementId',statementId);
    fd.append('issueDate',date);


    $.ajax({
        xhr: function(){
            var xhr = new window.XMLHttpRequest();
            //Upload progress
            xhr.upload.addEventListener("progress", function(evt){
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    //Do something with upload progress
//                    console.log(percentComplete*100);
                    progress(percentComplete*100, $('#progressBar'));
                }
            }, false);
            return xhr;
        },
        beforeSend:function(){
            $('#progressBar').show();
        },
        url:"/InvoiceCreator/statements/archives/uploadStatementArchive/",
        type: "post",
        data: fd,
        cache: false,
        processData: false,
        contentType: false,

        success: function(data) {

            $.each(data, function(key, value){

//                var tableRef = document.getElementById('statementsTable').getElementsByTagName('tbody')[0];
                var tableRef = $('#statementsTable tbody')

                tableRef.append(value.table_row);

                // Clean inputs

                $("div#uploadForm input[type=file]").val("");
                $("div#uploadForm input[type=text]").val("");
                $("div#uploadForm input[type=textField]").val("");

                hideWithDelay($('#progressBar'))

            });
        }

    });
}

function newBankStatementItem(){
    $("#formBankStatementItem").show();
    $("#bankStatementItemItemButton").hide();
}

function addBankStatementItem(form_name){
    var form = document.getElementById('formItems');
    var data = $(form).serializeArray();
    data.push({name: 'form', value: form_name});
    $(form).load("/InvoiceCreator/statements/items/addBankStatementItem",data,function(responseTxt,statusTxt,xhr){
        if(xhr.status == 200){
            $("#formBankStatementItem").show();
            $("#bankStatementItemItemButton").hide();
        }
    });
//    clearErrorMessages();
//    doAddItem();
}

function removeBankStatementItem(id,form_name){
    $.ajax({
        url: "/InvoiceCreator/statements/items/removeBankStatementItem",
        type: "post",
        data: {'id':id,'form':form_name},

        success: function (data) {
            $('#bankStatementItems').empty();
            $('#bankStatementItems').html(data);
            alertify.info("Removed");
        },
        error: function (data) {
            alertify.error("Error while executing that action");
        }
    });
}

function closeBankStatementItem(){
    clearItemData();
    clearErrorMessages();
    $("#formBankStatementItem").hide();
    $('#bankStatementItemItemButton').show();
}

function clearItemData(){
    $('#formBankStatementItem input[type=text]').val('');
    $('#formBankStatementItem input[type=number]').val('');
    $('#formBankStatementItem textarea').val('');
}

function clearErrorMessages() {

    $("#publicEntity").text('');
    $("#publicEntityBankAccountNumber").text('');
    $("#transferSum").text('');
    $("#shortDescription").text('');
    $("#transactionCode").text('');
    $("#borrowingCode").text('');
    $("#approvalCode").text('');

    document.getElementById('publicEntity').setCustomValidity('');
    document.getElementById('publicEntityBankAccountNumber').setCustomValidity('');
    document.getElementById('transferSum').setCustomValidity('');
    document.getElementById('shortDescription').setCustomValidity('');
    document.getElementById('transactionCode').setCustomValidity('');
    document.getElementById('borrowingCode').setCustomValidity('');
    document.getElementById('approvalCode').setCustomValidity('');
}

function checkIfBankAccountExists(bankAccNo){
    var data = {bankAccountNumber:bankAccNo.trim()};
    var returnValue = false;

    $.ajax({
        url:    '/InvoiceCreator/bankAccount/checkIfBankAccountExists',
        data: data,
        success: function(result) {
            if(result === 'false'){
                $("#bankAccount-form").dialog('open');
                alertify.log("No Bank Account exists for this Bank Account Number. Please create one before saving");

            }else{
                returnValue = true;
            }
        },
        async:   false
    });
    return returnValue;
}

function newBankAccountDialog(){
    $("#bankAccount-form").data('from','create').dialog('open');
}

//function doAddItem() {
//
//    var publicEntity = $("#publicEntity");
//    var publicEntityBankAccountNumber = $("#publicEntityBankAccountNumber");
//    var transferSum = $("#transferSum");
//    var shortDescription = $("#shortDescription");
//    var transactionCode = $("#transactionCode");
//    var borrowingCode = $("#borrowingCode");
//    var approvalCode = $("#approvalCode");
//
//    var items = {'publicEntity':publicEntity.val(),
//                 'publicEntityBankAccountNumber':publicEntityBankAccountNumber.val(),
//                 'transferSum':transferSum.val(),
//                 'shortDescription':shortDescription.val(),
//                 'transactionCode':transactionCode.val(),
//                 'borrowingCode':borrowingCode.val(),
//                 'approvalCode':approvalCode.val()}
////    var jItems = JSON.stringify(items);
//
//        $.ajax({
//            url: "/InvoiceCreator/statements/items/addBankStatementItem",
//            type: "post",
//            data: items,
//
//            success: function (data) {
//
//            },
//            error: function (data) {
//            }
//        });
//}

function deleteDocument(id,context){

    $.ajax({
        url: "/InvoiceCreator/"+context+"/deleteDocument",
        type: "post",
        data: {"id": id},

        success: function (data) {
            $('#editFileForm').empty();
            $('#editFileForm').html(data);
        },
        error: function(data){
            alertify.error("Server error has occurred. Please try later.");
        }
    });
}