//= require jquery/jquery-${org.codehaus.groovy.grails.plugins.jquery.JQueryConfig.SHIPPED_VERSION}
//= require vendor/alertify.js


if (typeof jQuery !== 'undefined') {
    (function($) {
        $('#spinner').ajaxStart(function() {
            $(this).fadeIn();
        }).ajaxStop(function() {
            $(this).fadeOut();
        });
    })(jQuery);


}

function deleteConfirm(obj){

    // confirm dialog
      alertify.confirm("Message", function (e) {
            if (e) {
               $(obj).parents('form:first').submit();
            }
        });
}

function setFileName(from,to){
    var name = from.files[0].name;
    $("#"+to).val(name);
}

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}

function regexDecimalChecker(str){

    var pattern = new RegExp('^\\d+(,\\d{1,2})?$');

    if(pattern.test(str)){
        return false;
    }else{
        return true;
    }
}

function progress(percent, $element) {
    var progressBarWidth = percent * $element.width() / 100;
    percent = parseInt(percent);
    $element.find('div').animate({ width: progressBarWidth },0).html(percent + "%&nbsp;");
}

function hideWithDelay($element){
    setTimeout(function(){
        $element.hide();
    },500);
}

function moveToElement(elementId) {
    //$("body").scrollTop($("#"+elementId).offset().top);
    window.scrollBy(window.pageXOffset, $("#"+elementId).offset().top);
}