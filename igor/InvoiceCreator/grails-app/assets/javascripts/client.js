/**
 * Created by igor on 6.2.15.
 */
//= require jquery/jquery-${org.codehaus.groovy.grails.plugins.jquery.JQueryConfig.SHIPPED_VERSION}
//= require vendor/jquery-ui-1.11.0.js
//= require vendor/jquery-te-1.4.0.min.js

$(document).ready(function(){
    $("#viewInfo").jqte();
});

function fillJqteEditor(value){
    var div = $('.jqte-popup').find("div.jqte_editor");
    div.get(0).innerHTML = value;
}