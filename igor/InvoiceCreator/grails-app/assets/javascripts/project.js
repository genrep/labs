/**
 * Created by igor on 17.3.15.
 */
//= require jquery/jquery-${org.codehaus.groovy.grails.plugins.jquery.JQueryConfig.SHIPPED_VERSION}
//= require vendor/jquery-ui-1.11.0.js
//= require vendor/jquery-te-1.4.0.min.js

$(document).ready(function(){
    $( "#startDate" ).datepicker({
        dateFormat: 'dd.mm.yy'
    });
    $( "#endDate" ).datepicker({
        dateFormat: 'dd.mm.yy'
    });

});

function calculateTotalPrice(){
    var price = $("#price").val();
    var taxElem = document.getElementById("tax");
    var tax = taxElem.options[taxElem.selectedIndex].text;
    tax = parseFloat(tax);
    tax = tax/100;
    price = parseFloat(price);
    var totalPrice = 0;
    if(tax ==0 ){
        totalPrice = price;
    }
    else{
        totalPrice = price + (price * tax);
    }
    console.log(totalPrice);
    $("#totalPrice").val(totalPrice);
}

function deleteProjectDocumentFile(id,action){

    $.ajax({
        url: "/InvoiceCreator/project/deleteProjectDocumentFile",
        type: "post",
        data:{"id":id,"guiAction":action},
        success: function (data) {
            $('#editFileForm').empty();
            $('#editFileForm').html(data);
        },
        error: function(data){
            alertify.error("Server error has occurred. Please try later.");
        }
    });
}