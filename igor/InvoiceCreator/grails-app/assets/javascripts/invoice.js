/**
 * Created by igor on 4/30/14.
 */
//= require jquery/jquery-${org.codehaus.groovy.grails.plugins.jquery.JQueryConfig.SHIPPED_VERSION}
//= require vendor/jquery-ui-1.11.0.js
//= require vendor/jquery-te-1.4.0.min.js

$(document).ready(function(){

    $( "#issueDate" ).datepicker({
        dateFormat: 'dd.mm.yy'
    });

    $( "#issueDateFrom" ).datepicker({
        dateFormat: 'dd.mm.yy'
    });
    $( "#issueDateTo" ).datepicker({
        dateFormat: 'dd.mm.yy'
    });

    $( "#paymentDate" ).datepicker({
        dateFormat:'dd.mm.yy'
    });

    $( "#paidOnDate" ).datepicker({
        dateFormat:'dd.mm.yy'
    });

    $( "#dialog-form" ).dialog({
        autoOpen: false,
        height: 600,
        width: 600,
        modal: true,
        open: function( event, ui ) {
            var data = $(this).data('item');
            $("#descriptionD").val(data.description);
            var div = $('#dialog-form').find("div.jqte_editor");
            div.get(0).innerHTML = data.description;
            $("#quantityD").val(data.quantity);
            $("#quantityUnitD").val(data.quantityUnit);
            $("#unitPriceD").val(data.unitPrice);
            $("#taxD").val(data.tax);
        },
        buttons: {
            "Update": function() {

                var data = $(this).data('item');
                var description = $("#descriptionD");
                var quantity = $("#quantityD");
                var quantityUnit = $("#quantityUnitD");
                var unitPrice = $("#unitPriceD");
                var tax = $("#taxD");

                var items = [description, quantity, unitPrice, tax]; // for validation only

                if (checkIfInvoiceItemIsValid(items)) {

                    $.ajax({
                        url: "/InvoiceCreator/invoiceItem/updateInvoiceItemDetails",
                        type: "post",
                        data: { "id": data.id,
                            "description": description.val(),
                            "quantity": quantity.val(),
                            "quantityUnit": quantityUnit.val(),
                            "unitPrice": unitPrice.val(),
                            "tax": tax.val(),
                            "sessionId": data.sessionId
                        },

                        success: function (data) {

                            renderInvoiceBody(data);

                            $( '#dialog-form ').dialog( "close" );

                        },
                        error: function (data) {
                            $( '#dialog-form' ).dialog( "close" );
                        }
                    });
                }
                else {

                    if (itemErrorCodes.length > 0) {
                        for (var i = itemErrorCodes.length - 1; i >= 0; i--) {

                            $("#" + itemErrorCodes[i] + "DE").text($("#" + itemErrorCodes[i]+"D").data("message"));

                            if (itemErrorCodes[i] == 'description') {
                                $('.jqte_editor').focus();
                            } else {
                                $("#" + itemErrorCodes[i]+"D").focus();
                                document.getElementById(itemErrorCodes[i]+"D").setCustomValidity('invalid');
                            }

                        }

                        while (itemErrorCodes.length > 0) {
                            itemErrorCodes.pop();
                        }
                    }
                }

            },
            close: function() {
                $( this ).dialog( "close" );
            }}
    });

    $( "#client-form" ).dialog({
        autoOpen: false,
        height: 300,
        width: 600,
        modal: true,
        open: function( event, ui ) {
            $("#name").val('');
            $("#department").val('');
            $("#address").val('');
            $("#bankAccountNumber").val('');
            $("#erol").remove();
        },
        buttons: {
            "Create": function() {

                var form = document.getElementById('client-form');

                var name = $("#name");
                var department = $("#department");
                var bankAccountNumber = $("#bankAccountNumber");
                var address = $("#address");

                var data = {name:name.val(),
                    department:department.val(),
                    bankAccountNumber:bankAccountNumber.val(),
                    address:address.val()};

                $.ajax({
                    url: "/InvoiceCreator/client/addClient",
                    type: "post",
                    data: data,

                    success: function (data) {
                        $('#client-form').dialog( "close" );
                        alertify.success("Client was successfully added.");
                        $("#client").load("/InvoiceCreator/worker/refreshClientSelect");

                    },
                    error: function (data) {
                        $('#client-form').html(data.responseText);
                    }
                });
            },
            close: function() {
                $( this ).dialog( "close" );
            }}
    });

    $("#description").jqte();
    $("#descriptionD").jqte();
    $("#invoiceDescription").jqte();

    $("#invoice-tabs").tabs();

});

var itemErrorCodes = [];


function doSaveFormItem() {


    var description = $("#description");
    var quantity = $("#quantity");
    var quantityUnit = $("#quantityUnit");
    var unitPrice = $("#unitPrice");
    var tax = $("#tax");

    var items = [description , quantity, unitPrice, tax]; // for validation only

    if (checkIfInvoiceItemIsValid(items)) {

        $.ajax({
            url: "/InvoiceCreator/invoiceItem/saveInvoiceItem",
            type: "post",
            data: {"description": description.val(), "quantity": quantity.val(),"quantityUnit":quantityUnit.val(),
                "unitPrice": unitPrice.val(), "tax": tax.val()},

            success: function (data) {

                $("#formInvoiceItem").hide();
                $('#newFormItemButton').show();
                renderInvoiceBody(data);

            },
            error: function (data) {
                alertify.error("An error occurred while processing data");
            }
        });
    } else {

        if (itemErrorCodes.length > 0) {
            for (var i = itemErrorCodes.length - 1; i >= 0; i--) {

                $("#" + itemErrorCodes[i] + "E").text($("#" + itemErrorCodes[i]).data("message"));

                if (itemErrorCodes[i] == 'description') {
                    $('.jqte_editor').focus();
                } else {
                    $("#" + itemErrorCodes[i]).focus();
                    document.getElementById(itemErrorCodes[i]).setCustomValidity('invalid');
                }

            }

            while (itemErrorCodes.length > 0) {
                itemErrorCodes.pop();
            }

        }
    }
}

function renderInvoiceBody(data){

    $('#invoiceBody').empty();
    $('#invoiceBody').html(data);

    moveToElement('invoiceBody');

    clearItemFormData();
}

function newFormItem(){
    $("#formInvoiceItem").show();
    $("#newFormItemButton").hide();
}

function saveFormItem(){
    clearErrorMessages('form');
    doSaveFormItem();
}

function closeFormItem(){
    clearItemFormData();
    clearErrorMessages('form');
    $("#formInvoiceItem").hide();
    $('#newFormItemButton').show();
}

function updateInvoiceItemPosition(orderIndex,step){
    $.ajax({
        url: "/InvoiceCreator/invoiceItem/updateInvoiceItemPosition",
        type: "post",
        data: {"position": orderIndex, "step": step},

        success: function (data) {

            renderInvoiceBody(data);
        },
        error: function(data){

        }
    });
}



function editInvoiceItemDetails(item){
    clearErrorMessages('dialog');
    $( "#dialog-form" ).data('item',item).dialog("open")
}

function checkIfInvoiceItemIsValid(items){

    var desc = items[0].val();
    var quantity = items[1].val();
    var unitPrice = items[2].val();
    var tax = items[3].val();
    var result = true;

    if(isBlank(desc) || isEmpty(desc)){
        itemErrorCodes.push('description');
        result=false;
    }
    if(isBlank(quantity) || isEmpty(quantity) || quantity <= 0){
        itemErrorCodes.push('quantity');
        result=false;
    }
    if(isBlank(unitPrice) || isEmpty(unitPrice) || regexDecimalChecker(unitPrice)){
        itemErrorCodes.push('unitPrice');
        result=false;
    }
    if(isBlank(tax) || isEmpty(tax)){
        itemErrorCodes.push('tax');
        result=false;
    }

    return result;
}

function clearErrorMessages(form) {

    if (form == 'form') {
        $("#descriptionE").text('');
        $("#quantityE").text('');
        $("#unitPriceE").text('');
        $("#taxE").text('');

        document.getElementById('description').setCustomValidity('');
        document.getElementById('quantity').setCustomValidity('');
        document.getElementById('unitPrice').setCustomValidity('');
        document.getElementById('tax').setCustomValidity('');
    }
    else if( form == 'dialog'){
        $("#descriptionDE").text('');
        $("#quantityDE").text('');
        $("#unitPriceDE").text('');
        $("#taxDE").text('');

        document.getElementById('descriptionD').setCustomValidity('');
        document.getElementById('quantityD').setCustomValidity('');
        document.getElementById('unitPriceD').setCustomValidity('');
        document.getElementById('taxD').setCustomValidity('');
    }

}

function clearItemFormData(){
    $('#formInvoiceItem input[type=text]').val('');
    $('#formInvoiceItem input[type=number]').val('');
    $('#formInvoiceItem input[name=tax]').val('18,00');
    $('#formInvoiceItem textarea').val('');
    var div = $('#formInvoiceItem').find("div.jqte_editor");
    div.get(0).innerHTML = '';
}

function generateOutInvoice(cb){
    $("#outCurrencyBlock").toggle();
    var cs = document.getElementById('outCurrency');
    if(cs.disabled==true){
        cs.disabled = false;
    }else{
        cs.disabled = true;
    }
}

function filterPaneDateChecker(id){

    var hidden = $("#"+id);
    var dateFrom = $("#"+id+"From");
    var dateTo = $("#"+id+"To");

    if((isBlank(dateFrom.val())||isEmpty(dateFrom.val()))
        && (isBlank(dateTo.val())||isEmpty(dateTo.val()))){
        hidden.val('');
    }else{
        hidden.val('true');
    }
}

function filterPaneClear(id){
    $("#"+id+" input[type=text]").val('');
    var temp="";
    $("#"+id+" select").val(temp);
}

function deleteInvoiceArchiveFile(invoiceId){

    $.ajax({
        url: "/InvoiceCreator/invoiceArchive/deleteInvoiceArchiveFile",
        type: "post",
        data: {"id": invoiceId},

        success: function (data) {
            $('#editFileForm').empty();
            $('#editFileForm').html(data);
        },
        error: function(data){
            alertify.error("Server error has occurred. Please try later.");
        }
    });
}

function deleteInvoicePurchaseArchiveFile(invoiceId){

    $.ajax({
        url: "/InvoiceCreator/invoicePurchaseArchive/deleteInvoicePurchaseArchiveFile",
        type: "post",
        data: {"id": invoiceId},

        success: function (data) {
            $('#editFileForm').empty();
            $('#editFileForm').html(data);
        },
        error: function(data){
            alertify.error("Server error has occurred. Please try later.");
        }
    });
}

function newClientDialog(){
    $("#client-form").dialog('open');
}

function setProInvoiceParams(elem){
    var proInvoiceId = elem.options[elem.selectedIndex].value;
    if(proInvoiceId!==""){
        var clientId,currency,totalSum,taxFreeSum,taxPriceSum;
        $.ajax({
            url: "/InvoiceCreator/invoicePurchaseArchive/getInvoicePurchaseJSON",
            data: {id:proInvoiceId},
            success: function(data){
                clientId = data.client.id;
                currency = data.inCurrency;
                totalSum = data.totalSum;
                taxFreeSum = data.taxFreeSum;
                taxPriceSum = data.taxPriceSum;

                $("#client").val(clientId);
                $("#inCurrency").val(currency);
                $("#totalSum").val(totalSum);
                $("#taxFreeSum").val(taxFreeSum);
                $("#taxPriceSum").val(taxPriceSum);
            },
            dataType: "JSON"
        });
        //var clientElem = document.getElementById('client');
        //clientElem.value = clientId;
        //$("#"+clientId).load("/InvoiceCreator/worker/refreshClientSelect",{proInvoiceId:proInvoiceId});
        // Ova moze da se napravi da bide rerender na cel template !!!
    }
}

function toggleProInvoiceSelect(elem){
    var select = document.getElementById("refInvoice");
    var admission = document.getElementById("admissionCounterNumber");
    if(elem.checked){
        select.disabled=true;
        admission.disabled=true;
        $("#div-refInvoice").hide();
        $("#div-admissionCounterNumber").hide();
    }
    else{
        select.disabled=false;
        admission.disabled=false;
        $("#div-refInvoice").show();
        $("#div-admissionCounterNumber").show();
    }
}

function updateInvoiceProject(id,guiAction){
    $.ajax({
        url: "/InvoiceCreator/invoice/updateInvoiceProject",
        type: "post",
        data: {id:id,"guiAction": guiAction},

        success: function (data) {
            $('#editFormProject').empty();
            $('#editFormProject').html(data);
            // programmatically call onchange event
            document.getElementById("project").onchange();
        },
        error: function(data){
            alertify.error("Server error has occurred. Please try later.");
        }
    });
}

function generateInvoiceNumber(){
    var invoiceCounter = $("#invoiceCounter").val();
    var invoiceYear = $("#invoiceYear").val();

    if(invoiceCounter!=="" && invoiceYear!==""){
        $("#invoiceNumber").val(invoiceCounter+"/"+invoiceYear);
        $("#invoiceNumberSort").val(invoiceYear+"-"+invoiceCounter);
    }
}

function showProjectsContractNumber(projectList,elem,guiAction){
    var array = JSON.parse(projectList);
    var index = elem.selectedIndex;
    var contractNumber;
    if(guiAction === 'add'){
        if(index==0) {
            contractNumber = "";
            $("#projectContract").text(contractNumber);
        }
        else{
            contractNumber = array[index-1];
            $("#projectContract").text("Contract Number of Project: "+contractNumber);
        }
    }
    else if(guiAction === 'change'){
        contractNumber = array[index];
        $("#projectContract").text("Contract Number of Project: "+contractNumber);
    }

}

function getValidAdmissionNumber(){
    var number = $("#admissionCounterNumber");
    var year = document.getElementById("admissionYear_year");
    var value = year.options[year.selectedIndex].value;
    $.ajax({
        url: "/InvoiceCreator/invoicePurchaseArchive/getValidAdmissionNumber",
        type: "post",
        data: {year:value},
        success: function (data) {
            number.val(data);
        },
        error: function(data){
            alertify.error("Server error has occurred. Please try later.");
        }
    });
}