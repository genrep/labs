package com.genrep.invoiceCreator

import com.genrep.invoiceCounter.InvoiceCounterEvidence
import com.genrep.invoiceRepository.InvoicePurchaseArchive
import org.joda.time.DateTime

import java.util.regex.Pattern

class InvoiceCounterService {

    Integer purchaseInvoiceCounterNumber

    def init(){
        setUpPurchaseInvoiceCounter()
    }

    // Set purchaseInvoiceCounterNumber for the current year
    def setUpPurchaseInvoiceCounter(){

        try {
            Calendar cal = Calendar.getInstance()
            InvoicePurchaseArchive invoicePurchaseArchive = InvoicePurchaseArchive.last(sort:'admissionCounterNumber')

            if(invoicePurchaseArchive!=null){

                cal.setTime(new Date())
                int activeYear = cal.get(Calendar.YEAR)
                cal.setTime(invoicePurchaseArchive.issueDate)
                int lastYear = cal.get(Calendar.YEAR)

                if (activeYear == lastYear) {
                    Long counter = invoicePurchaseArchive.admissionCounterNumber
                    purchaseInvoiceCounterNumber = ++counter
                } else {
                    purchaseInvoiceCounterNumber = 1
                }
            }
            else{
                purchaseInvoiceCounterNumber = 1 // first initialisation
            }

        }
        catch (Exception ex){
            log.error("Exception when setting purchase invoice counter.")
            log.error(ex.fillInStackTrace())
            purchaseInvoiceCounterNumber = 1
        }
    }

    def getPurchaseInvoiceCounter(def invoiceYear = null){
        if(invoiceYear==null){
            return purchaseInvoiceCounterNumber
        }
        else{
            def invoicePurchaseArchive = InvoicePurchaseArchive.createCriteria().list {
                eq("invoiceYear",(Long)invoiceYear)
                order "admissionCounterNumber","desc"
                maxResults 1
            }

            if(invoicePurchaseArchive){
                return ++invoicePurchaseArchive[0].admissionCounterNumber
            }
            else{
                1
            }
        }
    }

    void setPurchaseInvoiceCounter(def year){
        def invoicePurchaseArchive = InvoicePurchaseArchive.createCriteria().list {
            eq("invoiceYear",(Long)year)
            order "admissionCounterNumber","desc"
            maxResults 1
        }
        if(invoicePurchaseArchive){
            purchaseInvoiceCounterNumber = ++invoicePurchaseArchive[0].admissionCounterNumber
        }
        else{
            purchaseInvoiceCounterNumber = 1
        }
    }

    // Because we don't increment the counters for proinvoice
    void incrementPurchaseInvoiceCounterNumber(def invoiceYear=null){
        // increment only for the current year.
        if(invoiceYear==getCurrentYear()) {
            purchaseInvoiceCounterNumber++
            Long counter = InvoicePurchaseArchive.last(sort: 'admissionCounterNumber').admissionCounterNumber
            if (counter != null) {
                while (counter >= purchaseInvoiceCounterNumber) {
                    purchaseInvoiceCounterNumber++
                }
            }
        }
    }

    def getNextInvoiceCounter(def currentYear){

        def invoiceEvidence = InvoiceCounterEvidence.createCriteria().list {
            eq("invoiceYear",(Long)currentYear)
            order "invoiceCounter","desc"
            maxResults 1
        }

        if(invoiceEvidence){
            return ++invoiceEvidence[0].invoiceCounter
        }
        else{
            return 1
        }
    }

    def getCurrentYear(){
        new DateTime().getYear()
    }

}
