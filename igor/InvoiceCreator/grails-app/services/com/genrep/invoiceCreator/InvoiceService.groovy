package com.genrep.invoiceCreator

import com.genrep.client.Client
import com.genrep.document.InvoiceAttachment
import com.genrep.filterPane.FilterPaneUtils
import com.genrep.invoiceRepository.InvoicePurchaseArchive
import grails.transaction.Transactional
import org.codehaus.groovy.grails.commons.GrailsDomainClass
import org.springframework.web.multipart.MultipartFile

import java.math.RoundingMode

@Transactional
class InvoiceService {

    def grailsApplication
    def mongoBlobService

    def generateOutInvoiceBody(Invoice invoice) {

        def invoiceBody = invoice.invoiceBodyIn
        def inCurrency = invoice.inCurrency
        def outCurrency = invoice.outCurrency

        def invoiceBodyOut = new InvoiceBodyOut(currency: outCurrency)
        invoiceBodyOut.invoice = invoice
        invoice.invoiceBodyOut = invoiceBodyOut

        def exchangeRate = ExchangeRate.findByBaseCurrencyAndToCurrencyAndDate(inCurrency, outCurrency, invoice.issueDate)
        if (exchangeRate == null) {

            exchangeRate = Money.updateExchangeRates(inCurrency, outCurrency, invoice.issueDate)
        }

        List<InvoiceItem> items
        items = generateInvoiceItems(invoiceBody, outCurrency, invoice.issueDate)

        items.each {
            invoiceBodyOut.addToInvoiceItems(it)
        }


        invoice.exchangeRate = exchangeRate

    }

    List<InvoiceItem> generateInvoiceItems(InvoiceBodyIn invoiceBody, Currency currency, Date date) {
        def items = []

        invoiceBody.invoiceItems.each { item ->
            def itemOut = new InvoiceItem(
                    orderIndex: item.orderIndex,
                    description: item.description,
                    quantity: item.quantity,
                    unitPrice: Money.getInstance(item.unitPrice, invoiceBody.currency).convertTo(currency, date).amount.setScale(2,RoundingMode.HALF_EVEN),
                    tax: item.tax,
                    sessionId: item.sessionId
            ).save(flush: true)
            items.add(itemOut)
        }

        return items
    }

    @Transactional(readOnly = true)
    def updateInvoiceItemFromOutBody(Invoice invoice, InvoiceItem invoiceItem) {

        InvoiceItem item = invoice.invoiceBodyOut.invoiceItems.find {
            it.sessionId = invoiceItem.sessionId
        }

        if (item) {
            def inCurrency = invoice.invoiceBodyIn.currency
            def outCurrency = invoice.invoiceBodyOut.currency
            def date = invoice.issueDate

            invoice.invoiceBodyOut.removeFromInvoiceItems(invoice.invoiceBodyOut.invoiceItems.find {
                it.sessionId == item.sessionId
            })

            item.orderIndex = invoiceItem.orderIndex
            item.description = invoiceItem.description
            item.quantity = invoiceItem.quantity
            item.unitPrice = Money.getInstance(invoiceItem.unitPrice, inCurrency).convertTo(outCurrency, date).amount.setScale(2,RoundingMode.HALF_EVEN)
            item.tax = invoiceItem.tax

            // presmetka odnovo
            item.beforeInsert()

            invoice.invoiceBodyOut.addToInvoiceItems(item)
            invoice.invoiceBodyOut.afterUpdate()
        }
    }

    @Transactional(readOnly = true)
    def addInvoiceItemIntoOutBody(Invoice invoice, InvoiceItem invoiceItem) {

        def inCurrency = invoice.invoiceBodyIn.currency
        def outCurrency = invoice.invoiceBodyOut.currency
        def date = invoice.issueDate

        InvoiceItem item = new InvoiceItem()

        item.orderIndex = invoiceItem.orderIndex
        item.description = invoiceItem.description
        item.quantity = invoiceItem.quantity
        item.unitPrice = Money.getInstance(invoiceItem.unitPrice, inCurrency).convertTo(outCurrency, date).amount.setScale(2,RoundingMode.HALF_EVEN)
        item.tax = invoiceItem.tax
        item.sessionId = invoiceItem.sessionId

        item.beforeInsert()

        invoice.invoiceBodyOut.addToInvoiceItems(item)
        invoice.invoiceBodyOut.afterUpdate()
    }

    def doFilter(params) {

        def result
        GrailsDomainClass domain = FilterPaneUtils.resolveDomainClass(grailsApplication, params.domain)
        List persistentProps = domain.persistentProperties as List
        Map constraintParams = findConstraintParams(params, persistentProps)
        Map constraintParamsType = [:]
        constraintParams.each { param ->
            if (persistentProps.find { it.name == param.key }.type.equals(Date)) {
                def fieldFrom = param.key + 'From'
                def fieldTo = param.key + 'To'
                constraintParams.putAt(fieldFrom, params."${fieldFrom}")
                constraintParams.putAt(fieldTo, params."${fieldTo}")
            }
            constraintParamsType.put(param.key, persistentProps.find { it.name == param.key }.type)
        }

        result = fetchQueryResults(constraintParams, constraintParamsType, domain.getClazz(), params)

        return result
    }

    Map findConstraintParams(Map params, List persistentProps) {
        Map resultParams = [:]
        def persistentPropsNames = persistentProps.collect{ it.name }
        for (param in params) {
            if (param.value != 'null') {
                if (persistentPropsNames.contains(param.key) && !param.value.toString().equals('')) {
                    resultParams << param
                }
            }
        }
        return resultParams
    }

    def fetchQueryResults(Map constraintParams, Map constraintParamsType, Class clazz, Map params) {
        def c = clazz.createCriteria()
        def results = c.list(offset: params.offset, max: params.max) {
            and {

                constraintParamsType.each { typeParam ->

                    if (typeParam.value.equals(String)) {
                        like(typeParam.key, constraintParams.get(typeParam.key))
                    }
                    if (typeParam.value.equals(Client)) {
                        eq(typeParam.key, Client.get(constraintParams.get(typeParam.key)))
                    }
                    if (typeParam.value.equals(Date)) {
                        def fromS = constraintParams.get(typeParam.key + "From")
                        def toS = constraintParams.get(typeParam.key + "To")
                        def from = null
                        def to = null

                        if (!fromS.equals("")) {
                            from = Date?.parse("dd.MM.yyyy", fromS)
                        }
                        if (!toS.equals("")) {
                            to = Date?.parse("dd.MM.yyyy", toS)
                        }

                        if (from && to) {
                            between(typeParam.key, from, to.plus(1))
                        } else if (from && to == null) {
                            ge(typeParam.key, from)
                        } else if (to && from == null) {
                            le(typeParam.key, to)
                        }

                    }
                    if(typeParam.value.equals(Statuses)){
                        eq(typeParam.key, Statuses."${constraintParams.get(typeParam.key)}")
                    }
                }
            }
        }
        return results
    }

    // Zatoa sto ne raboti vo beforeUpdate na Domain
    def beforeUpdateInvoicePurchaseArchive(InvoicePurchaseArchive refInvoiceInstance) {
        InvoicePurchaseArchive.withNewSession {
            if (refInvoiceInstance.refInvoice != null) {
                def refId = refInvoiceInstance.refInvoice.id
                def proInvoice = InvoicePurchaseArchive.where { id == refId }.find()
                if (proInvoice) {
                    proInvoice.refInvoice = refInvoiceInstance
                    proInvoice.save(flush: true)
                }
            } else {
                def idd = refInvoiceInstance.id
                def ref = InvoicePurchaseArchive.where { id == idd }.find()
                if (ref.refInvoice != null) {
                    def proInvoice = InvoicePurchaseArchive.findWhere(id: ref.refInvoice.id)
                    if (proInvoice) {
                        proInvoice.refInvoice = null
                        proInvoice.save(flush: true)
                    }
                }
            }
        }
    }

    def saveInvoiceAttachmentFile(InvoiceAttachment invoiceAttachment, Map params){

        def attachment = containsAttachment(params)
        if(invoiceAttachment != null){
            if(attachment){
                mongoBlobService.saveFile(params?.file,InvoiceAttachment,invoiceAttachment.id,invoiceAttachment.attachmentType)
            }
        }

    }

    boolean containsAttachment(Map params){
        if(params.file && params.file instanceof MultipartFile && params.file.size>0){
            return true
        }
        return false
    }

    def deleteInvoiceAttachment(InvoiceAttachment invoiceAttachment){
        mongoBlobService.deleteFile(InvoiceAttachment,invoiceAttachment.id,invoiceAttachment.attachmentType)
        invoiceAttachment.delete(flush: true)
    }
}