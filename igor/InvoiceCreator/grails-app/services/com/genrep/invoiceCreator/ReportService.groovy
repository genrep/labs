package com.genrep.invoiceCreator

import com.genrep.client.Client
import com.genrep.domain.InvoiceBean
import com.genrep.filterPane.FilterPaneUtils
import com.genrep.invoiceRepository.InvoiceArchive
import com.genrep.project.Project
import grails.transaction.Transactional
import groovy.xml.MarkupBuilder
import org.codehaus.groovy.grails.commons.GrailsDomainClass
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.hibernate.FetchMode
import org.springframework.context.ResourceLoaderAware
import org.springframework.core.io.ResourceLoader

import javax.servlet.http.HttpSession
import java.math.RoundingMode


@Transactional
class ReportService implements ResourceLoaderAware {

    def grailsApplication
    def invoiceService
    ResourceLoader resourceLoader

    def buildInvoiceJasperParams(GrailsParameterMap params, HttpSession session){
        Client c = Client.get(Long.parseLong(params.client_id))
        Invoice i = Invoice.get(Long.parseLong(params.invoice_id))
        String body = params.invoice_body
        String invoiceBody = "invoiceBody"
        String language = params.language

        if(session) {
            session.setAttribute('body', body)
            session.setAttribute('invoiceId', i.id)
        }

        List<TaxHelper> taxes = TaxHelper.generateListOfTaxes(i."${invoiceBody+body}".invoiceItems)
        String taxLabel

        params.put('name',c.name)
        params.put('address',c.address)
        params.put('invoiceNumber',i.invoiceNumber)
//        params.put('invoiceBodyId',i."${invoiceBody+body}".id.toString())
        params.put('issueDate',i.issueDate.format('dd/MM/yyyy'))
        params.put('clientInfo',c.viewInfo)
        params.put('accountInfo',i.bankAccount?.printView)

        // Custom Fields
        params.put('swiftCode',i.swiftCode)
        params.put('attention',i.attention)
        params.put('totalPriceInWords',i.totalPriceInWords)
        params.put('paymentDeadline',i.paymentDeadline)

        // Project Field
        Project project = Project.createCriteria().get {
            fetchMode 'invoices', FetchMode.JOIN
            invoices {
                eq 'id', i.id
            }
        }
        if(project){
            params.put('project',createProjectView(project,language))
        }

        def logoPath = resourceLoader.getResource('/images/genrep_logo.gif').file.path
        params.put('logoPath',logoPath)

        if(language.equals("MKD")){
            params._file=params._file.toString().concat("JB_mk")
            if(params.currencyCode.equals("MKD")){
                params.put('currency',"ден.")
                taxLabel = createTableOfTaxes(taxes,"ДДВ "," ден.",'mk')
                params.put("wat",taxLabel)
            }else if(params.currencyCode.equals("EUR")){
                params.put('currency',"евра")
                taxLabel = createTableOfTaxes(taxes,"ДДВ "," евра",'mk')
                params.put("wat",taxLabel)
            }

            // Prices
            params.put('taxFreeSum',NumberConverter.decimalEncoder(i."${invoiceBody+body}".taxFreeSum.setScale(0,RoundingMode.HALF_EVEN),'mk'))
            params.put('taxPriceSum',NumberConverter.decimalEncoder(i."${invoiceBody+body}".taxPriceSum.setScale(0,RoundingMode.HALF_EVEN),'mk'))
            params.put('totalSum',NumberConverter.decimalEncoder(i."${invoiceBody+body}".getTotalSumRounded(),'mk'))

        }else if(language.equals("ENG")){
            params._file=params._file.toString().concat("JB2_en")
            if(params.currencyCode.equals("MKD")){
                params.put('currency',"den.")
                taxLabel = createTableOfTaxes(taxes,"WAT "," den.",'en')
                params.put("wat",taxLabel)
            }else if(params.currencyCode.equals("EUR")){
                params.put('currency',"euros")
                taxLabel = createTableOfTaxes(taxes,"WAT "," euros",'en')
                params.put("wat",taxLabel)
            }

            // Prices
            params.put('taxFreeSum',NumberConverter.decimalEncoder(i."${invoiceBody+body}".taxFreeSum.setScale(0,RoundingMode.HALF_EVEN),'en'))
            params.put('taxPriceSum',NumberConverter.decimalEncoder(i."${invoiceBody+body}".taxPriceSum.setScale(0,RoundingMode.HALF_EVEN),'en'))
            params.put('totalSum',NumberConverter.decimalEncoder(i."${invoiceBody+body}".getTotalSumRounded(),'en'))

        }

        params.remove('client_id')
        params.remove('invoice_id')
        params.remove('currencyCode')
        params.remove('language')

        params
    }

    private def createTableOfTaxes(list,label,curr, loc) {
        def writer = new StringWriter()
        new MarkupBuilder(writer).table(style: 'text-align:left;') {
            list.each { tax->
                tr {
                    td(label +"("+ tax.taxLabel+")"+"    "+NumberConverter.decimalEncoder(tax.taxPriceSum.setScale(0,RoundingMode.HALF_EVEN),loc)+curr)
//                            td(tax.taxPriceSum.decodeDecimalNumber()+curr)
                }
            }
        }
        writer.toString()
    }

    private String createProjectView(Project project, String locale){
        def projectTitle = project.title
        def projectReference = project.technicalNumber
        if(locale.equals("ENG")) {
            return "FOR: <b> $projectTitle </b> <br/> Reference: $projectReference"
        }
        else{
            // default is MKD
            return "ЗА: <b> $projectTitle </b> <br/> Референца: $projectReference"
        }
    }

    List quartalReportFilter(params) {

        List result = []
        Map constraintParamsType = [:]
        List domains = params.domains.tokenize(',[]').collect {it.trim()}  // converting to List from String
        // Now we should be sure that every domain contains the same filter property

        GrailsDomainClass domainClass = FilterPaneUtils.resolveDomainClass(grailsApplication, domains[0])
        List persistentProps = domainClass.persistentProperties as List
        Map constraintParams = invoiceService.findConstraintParams(params, persistentProps)

        constraintParams.each { param ->
            if (persistentProps.find { it.name == param.key }.type.equals(Date)) {
                def fieldFrom = param.key + 'From'
                def fieldTo = param.key + 'To'
                constraintParams.putAt(fieldFrom, params."${fieldFrom}")
                constraintParams.putAt(fieldTo, params."${fieldTo}")
            }
            constraintParamsType.put(param.key, persistentProps.find { it.name == param.key }.type)
        }

        result = fetchFilterResults(constraintParams, constraintParamsType, domains, params)
        return result
    }

    List fetchFilterResults(Map constraintParams, Map constraintParamsType, List domains, Map params) {
        List results = []
        for (domain in domains) {
            // If we don't want to include Imported Invoices skip them
            if(domain.equals(InvoiceArchive.name) && params.noImported && params.noImported.equals('on')){
                continue
            }
            GrailsDomainClass grailsDomain = FilterPaneUtils.resolveDomainClass(grailsApplication, domain.toString())
            domain = grailsDomain.getClazz()
            def c = domain.createCriteria()
            def domainResults = c.list() {
                and {
                    constraintParamsType.each { constraintParamType ->

                        if (constraintParamType.value.equals(String)) {
                            like(constraintParamType.key, constraintParams.get(constraintParamType.key))
                        }
                        if (constraintParamType.value.equals(Client)) {
                            eq(constraintParamType.key, Client.get(constraintParams.get(constraintParamType.key)))
                        }
                        if (constraintParamType.value.equals(Date)) {
                            def fromS = constraintParams.get(constraintParamType.key + "From")
                            def toS = constraintParams.get(constraintParamType.key + "To")
                            def from = null
                            def to = null

                            if (!fromS.equals("")) {
                                from =Date?.parse("dd.MM.yyyy", fromS)
                            }
                            if (!toS.equals("")) {
                                to = Date?.parse("dd.MM.yyyy", toS)
                            }

                            if (from && to) {
                                if(from.compareTo(to)==0){
                                    eq(constraintParamType.key,from)
                                }else{
                                    between(constraintParamType.key, from, to)
                                }
                            } else if (from && to == null) {
                                ge(constraintParamType.key, from)
                            } else if (to && from == null) {
                                le(constraintParamType.key, to)
                            }

                        }

                        if (constraintParamType.value.equals(Currency)){
                            eq(constraintParamType.key, Currency.getInstance(constraintParams.get(constraintParamType.key)))
                        }
                    }
                }
            }

            if(domainResults){
                if(domain.equals(Invoice)){
                    domainResults.each { obj ->
                        InvoiceBean bean = new InvoiceBean()
                        bean.aClass = Invoice.class
                        bean.uid = obj.id
                        bean.client = obj.client
                        bean.currency = obj.inCurrency
                        bean.invoiceNumber = obj.invoiceNumber
                        bean.issueDate = obj.issueDate
                        bean.taxFreeSum = obj.invoiceBodyIn.taxFreeSum.setScale(0,RoundingMode.HALF_EVEN)
                        bean.taxPriceSum = obj.invoiceBodyIn.taxPriceSum.setScale(0,RoundingMode.HALF_EVEN)
                        bean.totalSum = obj.invoiceBodyIn.getTotalSumRounded()
                        results << bean
                    }
                }else if(domain.equals(InvoiceArchive)){
                    domainResults.each { obj ->
                        InvoiceBean bean = new InvoiceBean()
                        bean.aClass = InvoiceArchive.class
                        bean.uid = obj.docMongoId
                        bean.client = Client.get(obj.client.id)
                        bean.currency = obj.inCurrency
                        bean.invoiceNumber = obj.invoiceNumber
                        bean.issueDate = obj.issueDate
                        bean.taxFreeSum = obj.taxFreeSum.setScale(0,RoundingMode.HALF_EVEN)
                        bean.taxPriceSum = obj.taxPriceSum.setScale(0,RoundingMode.HALF_EVEN)
                        bean.totalSum = obj.totalSum.setScale(0,RoundingMode.HALF_EVEN)
                        results << bean
                    }
                }
            }

        }
        return results
    }

    BigDecimal calculateSumOfField(Class clazz,List beans, String field){
        BigDecimal result = 0
        if(clazz.metaClass.hasProperty(clazz,field)){
            beans.each {bean->
                result = result.plus(bean."$field")
            }
        }
        return result
    }
}