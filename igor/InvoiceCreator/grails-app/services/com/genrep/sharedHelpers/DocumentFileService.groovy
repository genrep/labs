package com.genrep.sharedHelpers

import grails.transaction.Transactional

import java.nio.file.Files

@Transactional
class DocumentFileService {

    def probeContentType(byte[] file) {
        File tempFile = File.createTempFile("temp-file", "ext", null)
        FileOutputStream fos = new FileOutputStream(tempFile)
        fos.write(file)
        String mime = Files.probeContentType(tempFile.toPath())
        tempFile.delete()
        return mime
    }
}
