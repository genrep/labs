package com.genrep.auth

import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.userdetails.GrailsUser
import grails.transaction.Transactional

/**
 * @author: Igor
 */
@Transactional
class SpringSecurityService extends grails.plugin.springsecurity.SpringSecurityService{

    /**
     * Get the domain class instance associated with the current authentication.
     * @return the user
     */
    Object getCurrentUser() {
        if (!isLoggedIn()) {
            return null
        }

        String className = SpringSecurityUtils.securityConfig.userLookup.userDomainClassName
        def User = grailsApplication.getClassForName(className)

        // If User class is not defined in domain
        if(!User){
            Class clazz = Class.forName(className, true, Thread.currentThread().contextClassLoader)
            User = clazz.newInstance()
        }

        if (principal instanceof GrailsUser) {
            User.get principal.id
        }
        else {
            String usernamePropName = SpringSecurityUtils.securityConfig.userLookup.usernamePropertyName
            User.findWhere((usernamePropName): principal.username)
        }
    }

}
