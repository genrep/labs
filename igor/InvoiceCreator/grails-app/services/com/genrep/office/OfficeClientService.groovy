package com.genrep.office

import grails.converters.JSON
import grails.transaction.Transactional
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import org.apache.http.HttpResponse
import org.apache.http.StatusLine
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.message.BasicHeader
import org.apache.http.protocol.HTTP
import org.codehaus.groovy.grails.web.json.JSONObject

@Transactional
class OfficeClientService {

    private String protocol
    private String uri
    private String port

    private String appContext
    private String loginContext
    private String validateContext = "/api/validate"

    private String username
    private String password

    private String token

    String getUrl() {
        if (port.equals("80")) {
            protocol + "://" + uri + appContext
        } else {
            protocol + "://" + uri + ":" + port + appContext
        }
    }

    String getAuthUrl(){
        if (port.equals("80")) {
            protocol + "://" + uri + appContext + loginContext
        } else {
            protocol + "://" + uri + ":" + port + appContext + loginContext
        }
    }

    String getValidateUrl(){
        if (port.equals("80")) {
            protocol + "://" + uri + appContext + validateContext
        } else {
            protocol + "://" + uri + ":" + port + appContext + validateContext
        }
    }

    def authenticate(){

        HttpClient httpclient = new DefaultHttpClient()
        HttpPost httppost = new HttpPost(getAuthUrl())
        JSONObject json = new JSONObject()

        // Add your data
        json.put("username", username)
        json.put("password", password)
        StringEntity se = new StringEntity( json.toString())
        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"))
        httppost.setEntity(se)

        try {
            HttpResponse response = httpclient.execute(httppost)

            if (response != null) {
                StatusLine status = response.getStatusLine()

                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()))

                String line = ""
                String data = ""
                while ((line = rd.readLine()) != null) {
                    data += line
                }

                if (status.getStatusCode() == 200) {
                    JSONObject userPreferences = new JSONObject(data)
                    String receievedToken = userPreferences.getString("access_token")
                    token = receievedToken
                }

            }
        }
        catch (ConnectException ex){
            log.error "Connection refused"
            log.error ex.fillInStackTrace()
        }

    }

    // test method
    def validate(){
        HttpClient httpclient = new DefaultHttpClient()
        HttpPost httppost = new HttpPost(getValidateUrl())
        httppost.setHeader(new BasicHeader("Accept", "application/json"))
        httppost.setHeader("Authorization", "Bearer " + token)
        try {
            HttpResponse response = httpclient.execute(httppost)
            if (response != null) {
                StatusLine status = response.getStatusLine()
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()))
                String line = ""
                String data = ""
                while ((line = rd.readLine()) != null) {
                    data += line
                }
                println status
                println status.properties
                println data
            }
        }
        catch (ConnectException ex){
            log.error "Connection refused"
            log.error ex.fillInStackTrace()
        }
    }

    def callMethod(String path, String methodType , Class clazz, int attempt = 3){
        if(token!=null) {
            HttpClient httpclient = new DefaultHttpClient()
            def httpMethod
            if(methodType.toUpperCase().equals("GET")){
                httpMethod = getHttpGetObject(path)
            }
            else if(methodType.toUpperCase().equals("POST")){
                httpMethod = getHttpPostObject(path)
            }

            HttpResponse response = httpclient.execute(httpMethod)

            if (response != null) {
                StatusLine status = response.getStatusLine()

                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()))

                String line = ""
                String data = ""
                while ((line = rd.readLine()) != null) {
                    data += line
                }

                if (status.getStatusCode() == 200) {
                    def parsedData = JSON.parse(data, clazz)
                    return parsedData
                } else if (status.getStatusCode() == 401) {
                    log.info "${status.reasonPhrase}"
                    log.info "Method was not allowed."
                    log.info "Token has expired. Reauthenticate."
                    token = null
                    if(attempt != 0){
                        authenticate()
                        attempt--
                        callMethod(path,methodType,clazz,attempt)
                    }
                    else {
                        return [] as List
                    }
                }

            } else {
                return [] as List
            }
        }else{
            if(attempt != 0){
                authenticate()
                attempt--
                callMethod(path,methodType,clazz,attempt)
            }
            else{
                return [] as List
            }
        }
    }

    def getHttpGetObject(String path){
        HttpGet httpget = new HttpGet(getUrl() + path)
        httpget.setHeader(new BasicHeader("Accept", "application/json"))
        httpget.setHeader("Authorization", "Bearer " + token)
        return httpget
    }

    def getHttpPostObject(String path){
        HttpPost httppost = new HttpPost(getUrl() + path)
        httppost.setHeader(new BasicHeader("Accept", "application/json"))
        httppost.setHeader("Authorization", "Bearer " + token)
        return httppost
    }

    String getUsername() {
        return username
    }

    void setUsername(String username) {
        this.username = username
    }

    String getPassword() {
        return password
    }

    void setPassword(String password) {
        this.password = password
    }

    String getLoginContext() {
        return loginContext
    }

    void setLoginContext(String loginContext) {
        this.loginContext = loginContext
    }

    void setProtocol(String protocol) {
        this.protocol = protocol
    }

    void setUri(String uri) {
        this.uri = uri
    }

    void setPort(String port) {
        this.port = port
    }

    void setAppContext(String appContext) {
        this.appContext = appContext
    }

    String getProtocol() {

        return protocol
    }

    String getUri() {
        return uri
    }

    String getPort() {
        return port
    }

    String getAppContext() {
        return appContext
    }



}
