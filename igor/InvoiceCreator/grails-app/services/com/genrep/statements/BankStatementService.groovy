package com.genrep.statements

import com.genrep.account.BankAccount
import com.genrep.client.Client
import grails.transaction.Transactional

import java.text.DecimalFormat

@Transactional
class BankStatementService {

    BankStatement parseBankStatementDoc(BankStatementDoc bankStatementDoc) {

        def dbFile = bankStatementDoc.documentFile
        File file = new File('temp.dat')
        file.delete()
        file << dbFile

        BankStatement bs = new BankStatement()
        bs.bankStatementDocId = bankStatementDoc.uuid
        DecimalFormat decimalFormat = new DecimalFormat()
        decimalFormat.setParseBigDecimal(true)

        // Step through each line in the file
        file.eachLine { line, index ->
            // If the line isn't blank
            if (line.trim()) {
                switch (index) {
                    case 1:
                        bs.bankStatementId = line
                        break
                    case 2:
                        bs.issueDate = new Date().parse('dd.MM.yyyy', line)
                        break
                    case 3:
                        bs.bankAccountNumber = line.trim()
                        break
                    case 4:
                        bs.balanceBefore = (BigDecimal) decimalFormat.parse(line)
                        break
                    case 5:
                        bs.balanceAfter = (BigDecimal) decimalFormat.parse(line)
                        break
                    default:
                        BankStatementItem bsItem = new BankStatementItem()
                        bsItem.publicEntity = line[0..69].toString().trim()
                        bsItem.publicEntityBankAccountNumber = line[70..87].toString().trim()
                        bsItem.transferSum = (BigDecimal) decimalFormat.parse(line[88..106].toString().trim())
                        bsItem.transactionCode = line[107..112].toString().trim()
                        bsItem.shortDescription = line[113..184].toString().trim()
                        bsItem.borrowingCode = line[185..208].toString().trim()
                        bsItem.approvalCode = line[209..232].toString().trim()
                        bs.addToItems(bsItem)
                        bsItem.orderNumber = bs.items.size()
                }
            }
        }

        return bs
    }

    boolean saveBankStatement(BankStatement bankStatementInstance){

        try {

            BankAccount bankAccount = BankAccount.findByBankAccountNumber(bankStatementInstance.bankAccountNumber)
            bankStatementInstance.ownerOfStatement = bankAccount
            bankStatementInstance.save flush: true

            // update the status of statement in mongo
            def bsd = BankStatementDoc.findByUuid(bankStatementInstance.bankStatementDocId)
            bsd.isTransferred = true
            bsd.save flush: true
        }
        catch(Exception e){
            log.error "Error saving imported bank statement"
            log.error e.fillInStackTrace()

            return false
        }
        return true
    }

    def preSavingBankStatement(BankStatement bankStatement){
        try {
            def items = bankStatement.items
            items.each { item ->
                def publicEntity = item.publicEntity
                def publicEntityBankAccountNumber = item.publicEntityBankAccountNumber
                def bankAccount = BankAccount.findByBankAccountNumber(publicEntityBankAccountNumber) ?: new BankAccount(
                        entityName: publicEntity,
                        bankAccountNumber: publicEntityBankAccountNumber
                ).save(flush: true)

                def client = Client.findByBankAccount(bankAccount)
                if (client == null) {
                    (new Client(
                            name: publicEntity,
                            bankAccount: bankAccount
                    )).save(flush: true)
                }
            }
        }
        catch(Exception ex){
            log.error "Exception has occurred during pre save processing of bank statement"
            log.error ex.fillInStackTrace()
        }
    }

    def postSavingBankStatement(BankStatement bankStatement){

        try {
            def subject = bankStatement.ownerOfStatement
            def date = bankStatement.issueDate

            def items = bankStatement.items
            items.each { item ->
                def flow = item.transferFlow
                if(flow.equals('credited')){
                    item.debtor = subject
                    item.creditor = BankAccount.findByBankAccountNumber(item.publicEntityBankAccountNumber)
                    item.amount = item.transferSum
                    item.date = date
                }
                else if(flow.equals('debited')){
                    item.debtor = BankAccount.findByBankAccountNumber(item.publicEntityBankAccountNumber)
                    item.creditor = subject
                    item.amount = item.transferSum
                    item.date = date
                }
                item.save(flush: true)
            }

        }
        catch(Exception ex){
            log.error "Exception has occurred during post save processing of bank statement"
            log.error ex.fillInStackTrace()
        }
    }
}