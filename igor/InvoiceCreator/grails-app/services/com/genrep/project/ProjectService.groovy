package com.genrep.project

import com.genrep.invoiceCreator.Invoice
import grails.transaction.Transactional
import org.hibernate.FetchMode

@Transactional
class ProjectService {

    def documentFileService

    def removeInvoiceFromProject(Invoice invoice){
        Project project = Project.createCriteria().get {
            fetchMode 'invoices', FetchMode.JOIN
            invoices {
                eq 'id', invoice.id
            }
        }
        if(project){
            project.removeFromInvoices(invoice)
            project.save(flush: true)
        }
    }

    def saveOrUpdateInvoiceForProject(def id, Invoice invoice, def action){
        if(action.equals("save")) {
            if (id) {
                def project = Project.get(Long.parseLong(id))
                project.addToInvoices(invoice)
                project.save(flush: true)
            }
        }
        else if(action.equals("update")){
            Project project = Project.createCriteria().get {
                fetchMode 'invoices', FetchMode.JOIN
                invoices {
                    eq 'id', invoice.id
                }
            }
            if(id && project!=null){
                // update invoice in project
                project.removeFromInvoices(invoice)
                project.save(flush: true)

                def newProject = Project.get(Long.parseLong(id))
                newProject.addToInvoices(invoice)
                newProject.save(flush: true)
            }
            else if(id && project==null){
                // save invoice in project
                def newProject = Project.get(Long.parseLong(id))
                newProject.addToInvoices(invoice)
                newProject.save(flush: true)
            }
            else if(id.equals("") && project!=null){
                // remove invoice from project
                project.removeFromInvoices(invoice)
                project.save(flush: true)
            }
            else{
                //do nothing
            }
        }
    }

    def saveOrUpdateProjectDocument(Project project, def file, def fileName){
        try {
            if(file.size == 0) {
                //remove file after update
                def projectDocument = ProjectDocument.findWhere(technicalNumber: project.technicalNumber)
                if (projectDocument) {
                    projectDocument.delete(flush: true)
                }
            }
            else {
                def mime = documentFileService.probeContentType(file.getBytes())
                def documentName = fileName ?: file.originalFilename
                ProjectDocument projectDocument = ProjectDocument.findWhere(technicalNumber: project.technicalNumber)?: new ProjectDocument()
                projectDocument.client = project.client
                projectDocument.currency = project.currency
                projectDocument.documentFile = file.getBytes()
                projectDocument.documentName = documentName
                projectDocument.mimeType = mime
                projectDocument.technicalNumber = project.technicalNumber
                projectDocument.totalPrice = project.totalPrice
                projectDocument.save(flush: true)
            }
        }
        catch (Exception ex){
            log.error(ex.fillInStackTrace())
        }
    }
}
