sap.ui.require(["sap/ui/demo/bulletinboard/model/DateFormatter", "sap/ui/core/Locale"],
    function (DateFormatter, Locale) {
        var oFormatter = null;

        QUnit.module("DateFormatter", {
            beforeEach: function () {
                oFormatter = new DateFormatter({
                    now: function () {
                        return new Date(2016, 10, 28, 17, 0, 0, 0).getTime();
                    },
                    locale: new Locale("en-US")
                });
            }
        });

        QUnit.test("Should return empty string if no date is given", function (assert) {
            var sFormattedDate = oFormatter.format(null);
            assert.strictEqual(sFormattedDate, "");
        });

        QUnit.test("Should return time if date from today", function (assert) {
            var oDate = new Date(2016, 10, 28, 12, 5, 0, 0);
            var sFormattedDate = oFormatter.format(oDate);
            assert.strictEqual(sFormattedDate, "12:05 PM");
        });

        QUnit.test("Should return 'Yesterday' if date from yesterday", function (assert) {
            var oDate = new Date(2016, 10, 27);
            var sFormattedDate = oFormatter.format(oDate);
            assert.strictEqual(sFormattedDate, "Yesterday");
        });

        QUnit.test("Should return weekday if date < 7 days ago", function (assert) {
            var oDate = new Date(2016, 10, 23);
            var sFormattedDate = oFormatter.format(oDate);
            assert.strictEqual(sFormattedDate, "Wednesday");
        });

        QUnit.test("Should return date w/o time if date > 7 days ago", function(assert) {
            var oDate = new Date(2016, 10, 17);
            var sFormattedDate = oFormatter.format(oDate);
            assert.strictEqual(sFormattedDate, "Nov 17, 2016");
        });

    });