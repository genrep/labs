/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ch3.dataBean;

/**
 *
 * @author Igor
 */

public class RequestData { 
    
    protected String hobby;
    protected String aversion;
    
    public RequestData() {
    }
    
    public void setHobby(String hobby) {
        this.hobby = hobby;
    }
    
    public String getHobby() {
        return hobby;
    } 
    
    public void setAversion(String aversion) {
        this.aversion = aversion;
    }
    
    public String getAversion() {
        return aversion;
    } 
}
