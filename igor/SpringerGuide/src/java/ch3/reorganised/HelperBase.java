/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ch3.reorganised;

/**
 *
 * @author Igor
 */

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelperBase {
    
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected HttpServlet servlet;
    
    public HelperBase(HttpServlet servlet,
                      HttpServletRequest request,
                      HttpServletResponse response) {
        this.servlet = servlet;
        this.request = request;
        this.response = response;
    }
    
}

