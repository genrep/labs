/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ch3.sharedVariable;

/**
 *
 * @author Igor
 */

import javax.servlet.annotation.WebServlet;

@WebServlet(urlPatterns={"/ch3/sharedVariable/Controller"})
public class Controller 
extends ch3.sharedVariable.error.Controller 
{       
    @Override
    public void incrementSharedVariable() {
        synchronized (this) {
            int temp = accessCount;
            temp++;
            System.out.println(temp);
            try {
              Thread.sleep(3000);
            } catch (java.lang.InterruptedException ie) {
              //Just keep going
            }
            accessCount = temp;
        }
    }  
}
