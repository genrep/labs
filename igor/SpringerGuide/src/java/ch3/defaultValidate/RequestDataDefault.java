/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ch3.defaultValidate;

/**
 *
 * @author Igor
 */

public class RequestDataDefault {
  protected String hobby;
  protected String aversion;
  
  public void setHobby(String hobby) {
    this.hobby = hobby;
  }
  
  public String getHobby() {
    if (isValidHobby()) {
      return hobby;
    }
    return "No Hobby";
  }
  
  public void setAversion(String aversion) {
    this.aversion = aversion;
  }
  
  public String getAversion() {
    if (isValidAversion()) {
      return aversion;
    }
    return "No Aversion";
  }
  
  public boolean isValidHobby() {
    return hobby != null && !hobby.trim().equals("");
  }
  
  public boolean isValidAversion() {
    return aversion != null && !aversion.trim().equals("");
  }  
}

