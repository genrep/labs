/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch7.accountRemove;

/**
 *
 * @author igor
 */


import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import shared.ButtonMethod;
import shared.HibernateHelper;

public class ControllerHelper
    extends ch7.accountLogin.ControllerHelper {
  
  public ControllerHelper(
            HttpServlet servlet,
            HttpServletRequest request,
            HttpServletResponse response) {
        super(servlet, request, response);
  }
  
  @Override
  public String jspLocation(String page) {
    return "/ch7/accountRemove/" + page;
  }
  
  @ButtonMethod(buttonName="removeButton")
  public String removeMethod() {
    HibernateHelper.removeDB(data);
    data = new ch7.accountLogin.RequestDataAccount();
    return jspLocation("Login.jsp");
  }

}

