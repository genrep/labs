<%-- 
    Document   : Login
    Created on : Jul 29, 2016, 5:28:18 PM
    Author     : igor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login Page</title>
    </head>
    <body>
        <p>Please enter your account number 
            to access your data.
        <form method="POST" action="Controller">
            <p>
                Account Number ${helper.errors.accountNumber}
                <input type="text" name="accountNumber" 
                       value="${helper.cart.accountNumber}">
                <input type="submit" name="loginButton" 
                       value="Login">
            </p> 
        </form>
    </body>
</html>

