<%-- 
    Document   : Edit
    Created on : May 14, 2014, 12:34:50 AM
    Author     : Igor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <title>Edit Page</title>
  </head>
  <body>
    <p>
    This servlet demonstrates how shared variables can cause errors in web
    applications.
    <p>
    Open this page in two different browsers, then hit reload in both browsers
    within three seconds. The access count will be the same for both browsers.
    <P>
    This servlet has been accessed ${accessCount} times since the servlet engine was restarted.
  </body>
</html>

