<%-- 
    Document   : Process
    Created on : May 2, 2014, 12:05:39 AM
    Author     : Igor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <title>Process Page</title>
  </head>
  <body>
    <p>
      Thank you for your information. Your hobby 
      of <strong>${param.hobby}</strong> will be added to 
      our records, eventually.
    </p>
  </body>
</html>
