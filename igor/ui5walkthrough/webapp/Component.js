/**
 * Created by igor on 15.11.16.
 */
sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/model/json/JSONModel",
    "sap/ui/demo/wt/controller/HelloDialog",
    "sap/ui/Device"
], function (UIComponent, JSONModel, HelloDialog, Device) {
    "use strict";
    return UIComponent.extend("sap.ui.demo.wt.Component", {
        // metadata : {
        //     rootView: "sap.ui.demo.wt.view.App"
        // },
        metadata: {
            manifest: "json"
        },
        init: function () {
            // call the init function of the parent
            // init is actually the constructor
            UIComponent.prototype.init.apply(this, arguments);
            // set data model
            var oData = {
                recipient: {
                    name: "World"
                }
            };
            var oModel = new JSONModel(oData);
            this.setModel(oModel);
            // disable batch grouping for v2 API of the northwind service
            this.getModel("invoice").setUseBatch(false);
            // set device model
            var oDeviceModel = new JSONModel(Device);
            oDeviceModel.setDefaultBindingMode("OneWay");
            this.setModel(oDeviceModel, "device");
            // set i18n model
            // var i18nModel = new ResourceModel({
            //     bundleName : "sap.ui.demo.wt.i18n.i18n"
            // });
            // this.setModel(i18nModel, "i18n");
            // set dialog
            this.helloDialog = new HelloDialog();
            // create the views based on the url/hash
            this.getRouter().initialize();
        },
        getContentDensityClass: function () {
            if (!this._sContentDensityClass) {
                if (!sap.ui.Device.support.touch) {
                    this._sContentDensityClass = "sapUiSizeCompact";
                } else {
                    this._sContentDensityClass = "sapUiSizeCozy";
                }
            }
            return this._sContentDensityClass;
        },
        exit: function () {
            this.helloDialog.destroy();
        }
    });
});