sap.ui.jsview('webapp.controller.Controle', {

    /**
    * Specifies the Controller belonging to this View.
    * In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
    * @memberOf webapp.controller.Controle **/
    getControllerName : function() {
        return 'webapp.controller.Controle';
    },

    /**
    * Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed.
    * Since the Controller is given to this method, its event handlers can be attached right away.
    * @memberOf webapp.controller.Controle **/
    createContent : function(oController) {

    }

});
